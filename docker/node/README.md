# UI Docker container

This docker container is designed to run a fake node for red.

# Build the image 

`docker build -t red-node .`

# Create container

Note: These values are set below, change as needed:

 * path to red: /home/jamie/projects/mfpl/red
 * DB container: dbdev

    docker create --name "red-node" \
          -v "/home/jamie/projects/mfpl/red":/usr/local/share/red \
          --link "dbdev:dbdev" \
          red-node:latest

