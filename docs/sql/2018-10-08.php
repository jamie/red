<?php

require_once('../../ui/etc/config.inc.php');

require($config['common_src_path'] . '/red.lang.utils.inc.php');
require($config['common_src_path'] . '/red.utils.inc.php');
require($config['common_src_path'] . '/class.red_db.inc.php');

if(!$sql_resource = red_db::init_db($config)) {
 echo "Failed to connect to database.\n";
 exit(1);
}

global $db;
$db = new red_db($sql_resource);
if(!$db) {
  echo "Failed to create db object.\n";
  exit(1);
}

function delete_item($item_id) {
  global $db;
  $item_id = intval($item_id);
  $sql = "UPDATE red_item SET item_status = 'pending-delete' WHERE item_id = " . $item_id;
  // echo "$sql\n";
  echo "D";
  $db->_sql_query($sql);
}

// Return the line matching the given string, and then remove that line
// from $settings.
function reduce_settings($string, &$settings) {
  $lines = explode("\n", $settings);
  $match = FALSE;
  $rebuilt_settings = array();
  foreach($lines as $i => $line) {
    $line = trim($line);
    // Ignore lines starting with a comment.
    if (preg_match('/^[ ]*#/', $line)) {
      // Keep this line in settings, but continue.
      $rebuilt_settings[] = $line;
      continue;
    }
    if (FALSE !== strpos($line, $string)) {
      // Omit this line from settings and return it.
      $match = $line;
      continue;
    }
    $rebuilt_settings[] = $line;
  }
  $settings = implode("\n", $rebuilt_settings);
  return $match;
}

// Get all hosting orders with at least one related red_item_web_conf service.
$sql = "SELECT DISTINCT hosting_order_id FROM red_item WHERE service_id = 7 AND (item_status = 'active' OR item_status = 'disabled')";
$result = $db->_sql_query($sql);

if(!$result) {
  echo "Failed to create result set.\n";
  exit(1);
}

while($row = $db->_sql_fetch_row($result)) {
  $hosting_order_id = $row[0];
  $sql = "SELECT item_status, red_item_web_conf.* FROM red_item JOIN red_item_web_conf USING(item_id) 
    WHERE hosting_order_id = $hosting_order_id AND (item_status = 'active' OR item_status = 'disabled')";
  $item_result = $db->_sql_query($sql);
  $items = array();
  while($item_row = $db->_sql_fetch_assoc($item_result)) {
    $items[] = $item_row;
  }

  if (count($items) == 1) {
    $remaining_item = array_pop($items);
  }
  else {
    // We have to combine two items.
    if (count($items) > 2) {
      echo "Error; More than 2 items for $hosting_order_id\n";
      exit;
    }
    reset($items);
    foreach($items as $item) {
      if ($item['web_conf_port'] == 'http') {
        $http_item = $item;
      }
      elseif ($item['web_conf_port'] == 'https') {
        $https_item = $item;
      }
      else {
        echo "We have a weird port setting for $hosting_order_id\n";
        exit;
      }
    }
    // If one is disabled and the other active, delete the disabled one.
    if ($http_item['item_status'] == 'active' && $https_item['item_status'] == 'disabled') {
      delete_item($https_item['item_id']);
      $remaining_item = $http_item;
    }
    elseif ($https_item['item_status'] == 'active' && $http_item['item_status'] == 'disabled') {
      delete_item($http_item['item_id']);
      $remaining_item = $https_item;
    }
    else {
      // Either they are both disabled or both active we have to combine.
      $remaining_item = $https_item;
      delete_item($http_item['item_id']);
    }
  }

  // Done combining, now we act on $remaining_item.
  $settings = $remaining_item['web_conf_settings']; 
  $item_id = $remaining_item['item_id'];

  $domain_names = $remaining_item['web_conf_server_name'] . ' ' . $remaining_item['web_conf_server_alias'];
  $error_logging_on = reduce_settings('ErrorLog', $settings);
  $access_logging_on = reduce_settings('CustomLog', $settings);
  if ($access_logging_on !== FALSE) {
    $logging = 2; // full logging
  }
  elseif ($error_logging_on !== FALSE) {
    $logging = 1; // only error logging.
  }
  else {
    $logging = 0; // no logging.
  }
  $document_root = NULL;
  $original_document_root = reduce_settings('DocumentRoot', $settings);
  if (empty($original_document_root)) {
    echo "Failed to get document root for $hosting_order_id. \n";
    print_r($remaining_item);
    exit;
  }
  if (preg_match('#DocumentRoot +/home/members/[A-Za-z0-9_-]+/sites/[A-Za-z0-9-.]+/web(.*)$#', $original_document_root, $matches)) {
    $document_root = trim($matches[1]);
    // Remove initial slash if present.
    if(substr($document_root, 0, 1) == '/') {
      $document_root = substr($document_root, 1);
    }
  }
  else {
    echo "Failed to get relative document root, trying against $original_document_root\n";
    print_r($remaining_item);
    exit;
  }
  
  if ($remaining_item['web_conf_port'] == 'http') {
    $tls = 0;
  }
  else {
    $tls = 1;
  }

  $tls_key = NULL;
  $tls_cert = NULL;
  $original_cert_file = reduce_settings('SSLCertificateFile', $settings);
  $original_cert_key_file = reduce_settings('SSLCertificateKeyFile', $settings);
  if ($original_cert_file) {
    if (!$original_cert_key_file) {
      echo "Got cert, but not key.\n";
      print_r($remaining_item);
      exit;
    }
    $tls_cert = trim(str_replace('SSLCertificateFile', '', $original_cert_file));
    $tls_key = trim(str_replace('SSLCertificateKeyFile', '', $original_cert_key_file));
  }
  $settings = addslashes($settings); 
  $sql = "UPDATE red_item_web_conf SET
    web_conf_settings = '$settings',
    web_conf_domain_names = '$domain_names',
    web_conf_logging = '$logging',
    web_conf_document_root = '$document_root',
    web_conf_tls = '$tls',
    web_conf_tls_cert = '$tls_cert',
    web_conf_tls_key = '$tls_key'
    WHERE item_id = $item_id";
  // echo "$sql\n\n\n";
  echo "U";
  $db->_sql_query($sql);
}      


