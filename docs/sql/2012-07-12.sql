INSERT INTO red_service SET service_table = 'red_payment',service_area = 'member', service_name = 'Payments', service_description = "Invoice Payments", service_delete_order = 150, service_order_by = 'payment_date', service_default_host = '', service_id = 27, service_status = 'active';
ALTER TABLE red_payment ADD COLUMN payment_status ENUM('active','deleted') DEFAULT 'active';
ALTER TABLE red_payment CHANGE COLUMN payment_method payment_method ENUM('credit','check','cash','other','transfer') DEFAULT 'credit';
ALTER TABLE red_payment CHANGE COLUMN payment_date payment_date date;

