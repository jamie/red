ALTER TABLE red_item_web_conf CHANGE COLUMN web_conf_php_version web_conf_php_version decimal(3,1) DEFAULT 7.0;
UPDATE red_item_web_conf SET web_conf_php_version = '7.2' WHERE web_conf_php_version = '0.0' AND item_id IN (SELECT item_id FROM red_item WHERE item_status = 'active');
UPDATE red_item_web_conf SET web_conf_php_version = '7.0' WHERE web_conf_php_version = '0.0';

