-- Switch all tables to innodb so we get foreign key support
ALTER TABLE red_contact ENGINE=InnoDB;
ALTER TABLE red_correspondence ENGINE=InnoDB; 
ALTER TABLE red_error_log ENGINE=InnoDB;
ALTER TABLE red_hosting_order ENGINE=InnoDB;
ALTER TABLE red_invoice ENGINE=InnoDB;
ALTER TABLE red_item ENGINE=InnoDB;
ALTER TABLE red_item_cron ENGINE=InnoDB;
ALTER TABLE red_item_dns ENGINE=InnoDB;
ALTER TABLE red_item_email_address ENGINE=InnoDB;
ALTER TABLE red_item_hosting_order_access ENGINE=InnoDB;
ALTER TABLE red_item_list ENGINE=InnoDB;
ALTER TABLE red_item_mysql_db ENGINE=InnoDB;
ALTER TABLE red_item_mysql_user ENGINE=InnoDB;
ALTER TABLE red_item_pass_reset ENGINE=InnoDB;
ALTER TABLE red_item_server_access ENGINE=InnoDB;
ALTER TABLE red_item_user_account ENGINE=InnoDB;
ALTER TABLE red_item_web_app ENGINE=InnoDB;
ALTER TABLE red_item_web_conf ENGINE=InnoDB;
ALTER TABLE red_login_session ENGINE=InnoDB;
ALTER TABLE red_map_user_member ENGINE=InnoDB;
ALTER TABLE red_map_user_server ENGINE=InnoDB;
ALTER TABLE red_member ENGINE=InnoDB;
ALTER TABLE red_note ENGINE=InnoDB;
ALTER TABLE red_password_cache ENGINE=InnoDB;
ALTER TABLE red_payment ENGINE=InnoDB;
ALTER TABLE red_phone ENGINE=InnoDB;
ALTER TABLE red_planned_payment ENGINE=InnoDB;
ALTER TABLE red_postal_address ENGINE=InnoDB;
ALTER TABLE red_server ENGINE=InnoDB;
ALTER TABLE red_service ENGINE=InnoDB;
ALTER TABLE red_sitewide_admin ENGINE=InnoDB;
ALTER TABLE red_tag ENGINE=InnoDB;
ALTER TABLE red_unique_unix_group ENGINE=InnoDB;
ALTER TABLE red_user ENGINE=InnoDB;
ALTER TABLE red_voting_token ENGINE=InnoDB;

-- Now add the constraints

-- First item_ids
ALTER TABLE red_item_cron ADD CONSTRAINT FOREIGN KEY fk_item_id(item_id) REFERENCES red_item (item_id) ON DELETE CASCADE;
ALTER TABLE red_item_dns ADD CONSTRAINT FOREIGN KEY fk_item_id(item_id) REFERENCES red_item (item_id) ON DELETE CASCADE;
ALTER TABLE red_item_email_address ADD CONSTRAINT FOREIGN KEY fk_item_id(item_id) REFERENCES red_item (item_id) ON DELETE CASCADE;
ALTER TABLE red_item_hosting_order_access ADD CONSTRAINT FOREIGN KEY fk_item_id(item_id) REFERENCES red_item (item_id) ON DELETE CASCADE;
ALTER TABLE red_item_list ADD CONSTRAINT FOREIGN KEY fk_item_id(item_id) REFERENCES red_item (item_id) ON DELETE CASCADE;
ALTER TABLE red_item_mysql_db ADD CONSTRAINT FOREIGN KEY fk_item_id(item_id) REFERENCES red_item (item_id) ON DELETE CASCADE;
ALTER TABLE red_item_mysql_user ADD CONSTRAINT FOREIGN KEY fk_item_id(item_id) REFERENCES red_item (item_id) ON DELETE CASCADE;
DELETE FROM red_item_pass_reset WHERE item_id in (2, 26, 48, 88, 141, 307, 378);
ALTER TABLE red_item_pass_reset ADD CONSTRAINT FOREIGN KEY fk_item_id(item_id) REFERENCES red_item (item_id) ON DELETE CASCADE;
DELETE FROM red_item_server_access WHERE item_id in (2848, 2849, 2895, 2938, 2940);
ALTER TABLE red_item_server_access ADD CONSTRAINT FOREIGN KEY fk_item_id(item_id) REFERENCES red_item (item_id) ON DELETE CASCADE;
DELETE FROM red_item_web_conf WHERE item_id IN (2844, 2845, 2896, 2936, 2941);
ALTER TABLE red_item_web_app ADD CONSTRAINT FOREIGN KEY fk_item_id(item_id) REFERENCES red_item (item_id) ON DELETE CASCADE;
ALTER TABLE red_item_web_conf ADD CONSTRAINT FOREIGN KEY fk_item_id(item_id) REFERENCES red_item (item_id) ON DELETE CASCADE;
DELETE FROM red_item_user_account WHERE item_id IN (2842, 2843, 2894, 2937, 2939, 69124);
ALTER TABLE red_item_user_account ADD CONSTRAINT FOREIGN KEY fk_item_id(item_id) REFERENCES red_item (item_id) ON DELETE CASCADE;

-- now hosting order -> item_id
DELETE FROM red_item WHERE item_id IN (3096,3097,3098,12351,12352,12353,12354,12355,12356,12357,12358,12359,12360,12361,12362,22093,22689,22690,22691,22692,22693,22694,22695,22696,22697,22698,22699,22700,22713,22714,22715,22716,22717,22718,22719,22720,22721,22722,22723,22724,22751,22752,22753,22754,22755,22756,22757,22758,22759,22760,22761,22762,22867,22868,22869,22870,22871,22872,22873,22874,22875,22876,22877,22878,22879,22880,22881,22882,22883,22886,36196,36197,36198,36199,36200,36201,36202,36203,36204,36205,36206,36207,36208,36209,36210,36211,36212,36213,36214,36215,36216,36217,36218,37146,37147,37148,37149,37150,37151,37152,37153,37154,37155,37156,37157,37158,37159,37160,37161,37162,37163,37164,63446,63447,63448,63449,63450,63451,63452,63453,63454,63455,63456,63457,63494,63495,63496,63497,63498,63499,63500,63501,63502,63503,63504,63505,63913,63914,63915,63916,63917,63918,63919,63920,63921,66411,66412,66413,66414,66415,66416,66417,66418,66419,66420,66421);
-- Rescue a bunch of lost ussf email lists
UPDATE red_item SET hosting_order_id = 898 WHERE hosting_order_id = 2279;
ALTER TABLE red_item ADD CONSTRAINT FOREIGN KEY fk_hosting_order_id(hosting_order_id) REFERENCES red_hosting_order (hosting_order_id) ON DELETE CASCADE;

-- now the member table relationships
DELETE FROM red_contact WHERE member_id IN (44, 48, 53, 58, 59, 60, 61, 63, 67, 90);
ALTER TABLE red_contact ADD CONSTRAINT FOREIGN KEY fk_member_id(member_id) REFERENCES red_member (member_id) ON DELETE CASCADE;
DELETE FROM red_correspondence WHERE member_id = 0;
ALTER TABLE red_correspondence ADD CONSTRAINT FOREIGN KEY fk_member_id(member_id) REFERENCES red_member (member_id) ON DELETE CASCADE;
ALTER TABLE red_hosting_order ADD CONSTRAINT FOREIGN KEY fk_member_id(member_id) REFERENCES red_member (member_id) ON DELETE CASCADE;
ALTER TABLE red_invoice ADD CONSTRAINT FOREIGN KEY fk_member_id(member_id) REFERENCES red_member (member_id) ON DELETE CASCADE;
ALTER TABLE red_note ADD CONSTRAINT FOREIGN KEY fk_member_id(member_id) REFERENCES red_member (member_id) ON DELETE CASCADE;
ALTER TABLE red_phone ADD CONSTRAINT FOREIGN KEY fk_member_id(member_id) REFERENCES red_member (member_id) ON DELETE CASCADE;
ALTER TABLE red_postal_address ADD CONSTRAINT FOREIGN KEY fk_member_id(member_id) REFERENCES red_member (member_id) ON DELETE CASCADE;
ALTER TABLE red_voting_token ADD CONSTRAINT FOREIGN KEY fk_member_id(member_id) REFERENCES red_member (member_id) ON DELETE CASCADE;
ALTER TABLE red_tag ADD CONSTRAINT FOREIGN KEY fk_member_id(member_id) REFERENCES red_member (member_id) ON DELETE CASCADE;

-- a few odd balls
DELETE FROM red_map_user_member WHERE map_user_member_id IN (448, 453, 512, 596, 614, 618, 643, 695, 768, 1024, 1085, 1112, 1198, 1233, 350, 356, 358, 515, 551, 630, 679);
ALTER TABLE red_map_user_member ADD CONSTRAINT FOREIGN KEY fk_login (login) REFERENCES red_item_user_account (user_account_login) ON DELETE CASCADE;
DELETE FROM red_map_user_server WHERE map_user_server_id IN (5, 36, 44);
ALTER TABLE red_map_user_server ADD CONSTRAINT FOREIGN KEY fk_login (login) REFERENCES red_item_user_account (user_account_login) ON DELETE CASCADE;
ALTER TABLE red_map_user_member ADD CONSTRAINT FOREIGN KEY fk_member_id(member_id) REFERENCES red_member (member_id) ON DELETE CASCADE;
DELETE FROM red_map_user_server WHERE map_user_server_id IN (14,15,55,58,59);
ALTER TABLE red_sitewide_admin ADD CONSTRAINT FOREIGN KEY fk_user_name(user_name) REFERENCES red_item_user_account (user_account_login) ON DELETE CASCADE;
ALTER TABLE red_map_user_server ADD CONSTRAINT FOREIGN KEY fk_server(server) REFERENCES red_server (server) ON DELETE CASCADE;

-- unique unix groups - both members and hosting orders
-- delete orphans
DELETE FROM red_unique_unix_group WHERE unique_unix_group_id IN (1541,1543,1547,1549,1552,1553,1554,1556,1557,1558,1559,1560,1562,1566,1589,1595,1598,1600,1611,1632,1640,1659,1661,1663,1664,1666,1669,1670,1678,1679,1709,1714,1721,1742,2003,2096,2192,2239,2240,2269,2388,2389,2390,2391,2392,2393,2395,2396,2397,2399,2400,2401,2402,2409,2410,2413,2417,2418,2420,2434,2495,2501,2502,2503,2516,2531,2535,2543,2545,2559,2565,2607,2674,2675,2676,2743,2801,2834,2835,2836,2837,2838,2839,2840,2841,2842,2925,2926,2989,2990,3001,3002,3004,3005,3006,3007,3008,3009,3063,3193,3194,3216,3267,3290,3332,3342,3343,3657,3658,3690,3806,3807,3808,3809,3844,3880,3917,4046,4140,4150,4325,4326,4335,4479,4480,4484,4485,4608,4609,4610);

