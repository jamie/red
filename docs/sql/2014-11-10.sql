CREATE TABLE `red_online_payment` (
  `online_payment_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `online_payment_identifier` varchar(128) DEFAULT NULL,
  `invoice_id` mediumint(11) unsigned DEFAULT NULL,
  `bank_id` mediumint(11) unsigned DEFAULT NULL,
  `online_payment_amount` decimal(8,2) DEFAULT NULL,
  `online_payment_date` datetime DEFAULT NULL,
  `online_payment_email` varchar(64) DEFAULT NULL,
  `online_payment_notes` text DEFAULT NULL,
  PRIMARY KEY (`online_payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Chart of accounts';
