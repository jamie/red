ALTER TABLE red_item_user_account ADD COLUMN user_account_gpg_ids text;
ALTER TABLE red_item_user_account ADD COLUMN user_account_auto_response text;
ALTER TABLE red_item_user_account ADD COLUMN user_account_gpg_public_key text;
ALTER TABLE red_item_user_account ADD COLUMN user_account_auto_response_reply_from varchar(128);
ALTER TABLE red_item_user_account ADD COLUMN user_account_auto_response_action varchar(24);
