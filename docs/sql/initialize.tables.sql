-- MySQL dump 10.17  Distrib 10.3.22-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: dbdev    Database: redui
-- ------------------------------------------------------
-- Server version	10.3.22-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `account_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_type` enum('income','expense') NOT NULL DEFAULT 'income',
  `account_name` varchar(128) NOT NULL DEFAULT '',
  `account_description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Chart of accounts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bank`
--

DROP TABLE IF EXISTS `bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank` (
  `bank_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(128) NOT NULL DEFAULT '',
  `bank_number` varchar(128) NOT NULL DEFAULT '',
  `bank_description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`bank_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Bank accounts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `expenditure`
--

DROP TABLE IF EXISTS `expenditure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expenditure` (
  `expenditure_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `bank_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `expenditure_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expenditure_method` enum('debit','credit','check','cash','other') DEFAULT 'debit',
  `expenditure_identifier` varchar(128) NOT NULL DEFAULT '' COMMENT 'If check, check number, if credit, last 4 digits',
  `expenditure_amount` decimal(8,2) DEFAULT NULL,
  `expenditure_notes` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`expenditure_id`),
  KEY `purchase_id` (`purchase_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Record of actually dispending the cash';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `list_subscribe_log`
--

DROP TABLE IF EXISTS `list_subscribe_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_subscribe_log` (
  `list_subscribe_log_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `email_address` varchar(128) NOT NULL DEFAULT '',
  `list` varchar(128) NOT NULL DEFAULT '',
  `subscribe_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`list_subscribe_log_id`),
  KEY `email_address` (`email_address`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mj_lists`
--

DROP TABLE IF EXISTS `mj_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mj_lists` (
  `mj_list_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `friendly_name` varchar(100) NOT NULL DEFAULT '',
  `select_type` varchar(10) NOT NULL DEFAULT '',
  `source_type` varchar(5) NOT NULL DEFAULT '',
  `source` text DEFAULT NULL,
  `extra_option` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`mj_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mj_menus`
--

DROP TABLE IF EXISTS `mj_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mj_menus` (
  `mj_menu_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `menu_type` char(16) NOT NULL DEFAULT '',
  `value` char(64) NOT NULL DEFAULT '',
  `display` char(128) NOT NULL DEFAULT '',
  `display_order` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`mj_menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mj_queries`
--

DROP TABLE IF EXISTS `mj_queries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mj_queries` (
  `mj_query_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `friendly_name` varchar(60) NOT NULL DEFAULT '',
  `sql_statement` text DEFAULT NULL,
  PRIMARY KEY (`mj_query_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mj_relationships`
--

DROP TABLE IF EXISTS `mj_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mj_relationships` (
  `mj_relationship_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `table1` varchar(30) NOT NULL DEFAULT '',
  `relationship` text DEFAULT NULL,
  `table2` varchar(30) NOT NULL DEFAULT '',
  `relationship_type` varchar(25) NOT NULL DEFAULT '',
  `cascade_delete` char(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`mj_relationship_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mj_sql_logs`
--

DROP TABLE IF EXISTS `mj_sql_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mj_sql_logs` (
  `mj_sql_log_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `stamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `username` varchar(25) NOT NULL DEFAULT '',
  `log_type` varchar(10) NOT NULL DEFAULT '',
  `log_table` varchar(50) NOT NULL DEFAULT '',
  `log_id` int(11) NOT NULL DEFAULT 0,
  `passed_sql` text DEFAULT NULL,
  `revert_sql` text DEFAULT NULL,
  PRIMARY KEY (`mj_sql_log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mj_users`
--

DROP TABLE IF EXISTS `mj_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mj_users` (
  `mj_user_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(25) NOT NULL DEFAULT '',
  `pass` char(255) NOT NULL DEFAULT '',
  `db_username` char(25) NOT NULL DEFAULT '',
  `admin` char(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`mj_user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase`
--

DROP TABLE IF EXISTS `purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase` (
  `purchase_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `purchase_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `purchase_amount` decimal(8,2) DEFAULT NULL,
  `purchase_description` text NOT NULL,
  `purchase_receipt` blob NOT NULL COMMENT 'Scan of receipt',
  `purchase_receipt_filename` varchar(128) DEFAULT NULL,
  `purchase_receipt_mime_type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`purchase_id`),
  KEY `account_id` (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Record of spending money';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_contact`
--

DROP TABLE IF EXISTS `red_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_contact` (
  `contact_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `contact_description` varchar(64) NOT NULL DEFAULT '' COMMENT 'E.g. tech contact, etc.',
  `contact_first_name` varchar(64) NOT NULL DEFAULT '',
  `contact_last_name` varchar(64) NOT NULL DEFAULT '',
  `contact_email` varchar(64) NOT NULL DEFAULT '',
  `contact_billing` enum('n','y') NOT NULL DEFAULT 'n' COMMENT 'Is this the billing contact?',
  `contact_status` enum('active','deleted') DEFAULT 'active',
  `contact_lang` varchar(5) DEFAULT 'en_US',
  PRIMARY KEY (`contact_id`),
  KEY `contact_status` (`contact_status`),
  KEY `contact_email` (`contact_email`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_contact_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_correspondence`
--

DROP TABLE IF EXISTS `red_correspondence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_correspondence` (
  `correspondence_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned DEFAULT 0,
  `correspondence_from` varchar(128) DEFAULT NULL,
  `correspondence_to` varchar(128) DEFAULT NULL,
  `correspondence_subject` varchar(128) DEFAULT NULL,
  `correspondence_date` datetime DEFAULT NULL,
  `correspondence_body` text DEFAULT NULL,
  PRIMARY KEY (`correspondence_id`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_correspondence_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Email correspondence';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_error_log`
--

DROP TABLE IF EXISTS `red_error_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_error_log` (
  `error_log_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` mediumint(11) unsigned DEFAULT NULL,
  `error_log_severity` enum('hard','soft') DEFAULT NULL,
  `error_log_type` enum('system','validation') DEFAULT NULL,
  `error_log_message` text NOT NULL,
  `error_log_datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`error_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_hosting_order`
--

DROP TABLE IF EXISTS `red_hosting_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_hosting_order` (
  `hosting_order_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `unique_unix_group_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `hosting_order_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'E.g. 1 yr hosting for domain.org',
  `hosting_order_status` enum('active','deleted','disabled') DEFAULT 'active',
  `hosting_order_host` varchar(128) DEFAULT NULL,
  `hosting_order_identifier` varchar(255) NOT NULL DEFAULT '',
  `hosting_order_notes` text NOT NULL,
  PRIMARY KEY (`hosting_order_id`),
  KEY `member_id` (`member_id`),
  KEY `hosting_order_host` (`hosting_order_host`),
  CONSTRAINT `red_hosting_order_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_invoice`
--

DROP TABLE IF EXISTS `red_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_invoice` (
  `invoice_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned DEFAULT NULL,
  `account_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `invoice_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `invoice_amount` decimal(8,2) NOT NULL DEFAULT 0.00,
  `invoice_status` enum('paid','unpaid','eaten','void','review') DEFAULT 'unpaid',
  `invoice_description` text NOT NULL,
  `invoice_private_notes` text NOT NULL,
  `invoice_currency` varchar(8) DEFAULT 'USD',
  `invoice_date_modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `invoice_type` enum('benefits','membership') DEFAULT NULL,
  PRIMARY KEY (`invoice_id`),
  KEY `account_id` (`account_id`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_invoice_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Record of earning the money';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item`
--

DROP TABLE IF EXISTS `red_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item` (
  `item_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `hosting_order_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `service_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `item_host` varchar(255) NOT NULL DEFAULT '',
  `item_status` enum('active','pending-delete','pending-update','pending-insert','pending-restore','deleted','hard-error','soft-error','transfer-limbo','pending-disable','disabled') DEFAULT NULL,
  `item_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`item_id`),
  KEY `service_id` (`service_id`),
  KEY `hosting_order_id` (`hosting_order_id`),
  KEY `member_id` (`member_id`),
  KEY `item_status` (`item_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Each config item, such as email account, etc.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_cron`
--

DROP TABLE IF EXISTS `red_item_cron`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_cron` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `cron_login` varchar(128) NOT NULL DEFAULT '',
  `cron_schedule` varchar(128) NOT NULL DEFAULT '',
  `cron_cmd` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_cron_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_dns`
--

DROP TABLE IF EXISTS `red_item_dns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_dns` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `dns_zone` varchar(255) DEFAULT NULL,
  `dns_type` enum('mx','a','text','cname','srv','aaaa','ptr','mailstore','sshfp') DEFAULT NULL,
  `dns_fqdn` varchar(255) DEFAULT NULL,
  `dns_ip` varchar(79) DEFAULT NULL,
  `dns_ttl` mediumint(9) DEFAULT NULL,
  `dns_server_name` varchar(255) DEFAULT NULL,
  `dns_text` varchar(255) DEFAULT NULL,
  `dns_dist` tinyint(3) unsigned DEFAULT NULL,
  `dns_port` smallint(5) unsigned DEFAULT 0,
  `dns_weight` smallint(5) unsigned DEFAULT 0,
  `dns_sshfp_algorithm` tinyint(4) NOT NULL DEFAULT 0,
  `dns_sshfp_type` tinyint(4) NOT NULL DEFAULT 0,
  `dns_sshfp_fpr` text DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `dns_type` (`dns_type`),
  KEY `dns_fqdn` (`dns_fqdn`),
  KEY `dns_zone` (`dns_zone`),
  CONSTRAINT `red_item_dns_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_email_address`
--

DROP TABLE IF EXISTS `red_item_email_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_email_address` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `email_address` varchar(255) NOT NULL DEFAULT '',
  `email_address_recipient` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`item_id`),
  KEY `email_address_recipient` (`email_address_recipient`),
  CONSTRAINT `red_item_email_address_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_hosting_order_access`
--

DROP TABLE IF EXISTS `red_item_hosting_order_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_hosting_order_access` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `hosting_order_access_login` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `hosting_order_login` (`hosting_order_access_login`),
  CONSTRAINT `red_item_hosting_order_access_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_list`
--

DROP TABLE IF EXISTS `red_item_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_list` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `list_name` varchar(64) DEFAULT NULL,
  `list_owner_email` varchar(128) DEFAULT NULL,
  `list_domain` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_list_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_mysql_db`
--

DROP TABLE IF EXISTS `red_item_mysql_db`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_mysql_db` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `mysql_db_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_mysql_db_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_mysql_user`
--

DROP TABLE IF EXISTS `red_item_mysql_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_mysql_user` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `mysql_user_name` varchar(16) DEFAULT NULL,
  `mysql_user_password` varchar(41) DEFAULT NULL,
  `mysql_user_db` text DEFAULT NULL,
  `mysql_user_priv` enum('full','read') DEFAULT NULL,
  `mysql_user_max_connections` smallint(6) DEFAULT 25,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_mysql_user_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_pass_reset`
--

DROP TABLE IF EXISTS `red_item_pass_reset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_pass_reset` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `pass_reset_login` varchar(255) DEFAULT NULL,
  `pass_reset_hash` varchar(32) DEFAULT NULL,
  `pass_reset_expires` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pass_reset` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_pass_reset_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_server_access`
--

DROP TABLE IF EXISTS `red_item_server_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_server_access` (
  `item_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `server_access_login` varchar(128) DEFAULT NULL,
  `server_access_public_key` text NOT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_server_access_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_user_account`
--

DROP TABLE IF EXISTS `red_item_user_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_user_account` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `user_account_firstname` varchar(128) DEFAULT NULL,
  `user_account_lastname` varchar(128) DEFAULT NULL,
  `user_account_login` varchar(255) DEFAULT NULL,
  `user_account_password` varchar(255) DEFAULT NULL,
  `user_account_uid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_account_gpg_ids` text DEFAULT NULL,
  `user_account_auto_response` text DEFAULT NULL,
  `user_account_gpg_public_key` text DEFAULT NULL,
  `user_account_auto_response_reply_from` varchar(128) DEFAULT NULL,
  `user_account_auto_response_action` varchar(24) DEFAULT NULL,
  `user_account_mountpoint` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `user_account_uid` (`user_account_uid`),
  KEY `user_account_login` (`user_account_login`),
  CONSTRAINT `red_item_user_account_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_web_app`
--

DROP TABLE IF EXISTS `red_item_web_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_web_app` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `web_app_name` varchar(128) DEFAULT NULL,
  `web_app_version` varchar(16) DEFAULT NULL,
  `web_app_dir` varchar(128) DEFAULT NULL,
  `web_app_mysql_db_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `web_app_mysql_user_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `web_app_cron_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `web_app_initial_admin_email` varchar(128) DEFAULT 'unset@example.org',
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_web_app_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_web_conf`
--

DROP TABLE IF EXISTS `red_item_web_conf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_web_conf` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `web_conf_login` varchar(128) DEFAULT NULL,
  `web_conf_execute_as_user` varchar(128) DEFAULT NULL,
  `web_conf_settings` text DEFAULT NULL,
  `web_conf_max_processes` smallint(6) DEFAULT 12,
  `web_conf_cgi` tinyint(4) DEFAULT 0,
  `web_conf_domain_names` text DEFAULT NULL,
  `web_conf_logging` tinyint(1) DEFAULT 1,
  `web_conf_document_root` varchar(255) DEFAULT NULL,
  `web_conf_tls` tinyint(1) DEFAULT 1,
  `web_conf_tls_redirect` tinyint(1) DEFAULT 1,
  `web_conf_tls_cert` varchar(255) DEFAULT NULL,
  `web_conf_tls_key` varchar(255) DEFAULT NULL,
  `web_conf_cache_settings` text DEFAULT NULL,
  `web_conf_max_memory` mediumint(11) DEFAULT 128,
  `web_conf_php_version` decimal(3,1) DEFAULT 7.0,
  `web_conf_mountpoint` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_web_conf_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_login_session`
--

DROP TABLE IF EXISTS `red_login_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_login_session` (
  `login_session_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `user_hash` varchar(32) DEFAULT NULL,
  `stamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`login_session_id`),
  KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_map_user_member`
--

DROP TABLE IF EXISTS `red_map_user_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_map_user_member` (
  `map_user_member_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) DEFAULT NULL,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY (`map_user_member_id`),
  KEY `fk_login` (`login`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_map_user_member_ibfk_1` FOREIGN KEY (`login`) REFERENCES `red_item_user_account` (`user_account_login`) ON DELETE CASCADE,
  CONSTRAINT `red_map_user_member_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_map_user_server`
--

DROP TABLE IF EXISTS `red_map_user_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_map_user_server` (
  `map_user_server_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL,
  `login` varchar(255) DEFAULT NULL,
  `server` varchar(128) NOT NULL DEFAULT '0',
  `status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY (`map_user_server_id`),
  KEY `fk_login` (`login`),
  KEY `fk_server` (`server`),
  CONSTRAINT `red_map_user_server_ibfk_1` FOREIGN KEY (`login`) REFERENCES `red_item_user_account` (`user_account_login`) ON DELETE CASCADE,
  CONSTRAINT `red_map_user_server_ibfk_2` FOREIGN KEY (`server`) REFERENCES `red_server` (`server`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_member`
--

DROP TABLE IF EXISTS `red_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_member` (
  `member_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `unique_unix_group_id` mediumint(8) unsigned DEFAULT NULL,
  `member_friendly_name` varchar(128) NOT NULL DEFAULT '',
  `member_parent_id` mediumint(11) unsigned DEFAULT NULL,
  `member_status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `member_term` enum('month','year') DEFAULT 'year',
  `member_price` smallint(6) NOT NULL DEFAULT 0,
  `member_type` enum('individual','organization') NOT NULL DEFAULT 'organization',
  `member_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `member_end_date` datetime DEFAULT NULL,
  `member_currency` varchar(8) DEFAULT 'USD',
  `member_benefits_price` smallint(6) DEFAULT NULL,
  `member_benefits_level` enum('basic','standard','extra') DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Additional info about members is stored in other tables';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_member_dns_status`
--

DROP TABLE IF EXISTS `red_member_dns_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_member_dns_status` (
  `member_dns_status_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `member_dns_status` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_dns_status_id`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_member_dns_status_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_note`
--

DROP TABLE IF EXISTS `red_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_note` (
  `note_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `note` text NOT NULL,
  `note_status` enum('active','deleted') DEFAULT NULL,
  `note_modified` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`note_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `red_note_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_online_payment`
--

DROP TABLE IF EXISTS `red_online_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_online_payment` (
  `online_payment_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `online_payment_identifier` varchar(128) DEFAULT NULL,
  `invoice_id` mediumint(11) unsigned DEFAULT NULL,
  `bank_id` mediumint(11) unsigned DEFAULT NULL,
  `online_payment_amount` decimal(8,2) DEFAULT NULL,
  `online_payment_date` datetime DEFAULT NULL,
  `online_payment_email` varchar(64) DEFAULT NULL,
  `online_payment_notes` text DEFAULT NULL,
  `member_id` mediumint(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`online_payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Chart of accounts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_password_cache`
--

DROP TABLE IF EXISTS `red_password_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_password_cache` (
  `item_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `added` timestamp NOT NULL DEFAULT current_timestamp(),
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `added` (`added`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_payment`
--

DROP TABLE IF EXISTS `red_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_payment` (
  `payment_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `bank_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `payment_date` date DEFAULT NULL,
  `payment_method` enum('credit','check','cash','other','transfer') DEFAULT 'credit',
  `payment_identifier` varchar(128) NOT NULL DEFAULT '' COMMENT 'If check, check number, if credit, last 4 digits',
  `payment_amount` decimal(8,2) NOT NULL DEFAULT 0.00,
  `payment_notes` varchar(255) NOT NULL DEFAULT '',
  `payment_status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY (`payment_id`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Record of receiving the money';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_phone`
--

DROP TABLE IF EXISTS `red_phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_phone` (
  `phone_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `phone_country_code` smallint(6) NOT NULL DEFAULT 1,
  `phone_number` bigint(20) NOT NULL,
  `phone_status` enum('active','deleted') DEFAULT NULL,
  `phone_modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `phone_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`phone_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `red_phone_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_planned_payment`
--

DROP TABLE IF EXISTS `red_planned_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_planned_payment` (
  `planned_payment_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `planned_payment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `planned_payment_amount` decimal(8,2) NOT NULL DEFAULT 0.00,
  `planned_payment_status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY (`planned_payment_id`),
  KEY `account_id` (`invoice_id`),
  KEY `planned_payment_date` (`planned_payment_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_postal_address`
--

DROP TABLE IF EXISTS `red_postal_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_postal_address` (
  `postal_address_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `postal_address_street_number` varchar(16) NOT NULL,
  `postal_address_street_name` varchar(255) NOT NULL,
  `postal_address_street_extra` varchar(255) NOT NULL,
  `postal_address_city` varchar(255) NOT NULL,
  `postal_address_province` varchar(255) NOT NULL,
  `postal_address_country` char(2) NOT NULL,
  `postal_address_code` varchar(32) NOT NULL,
  `postal_address_status` enum('active','deleted') DEFAULT NULL,
  `postal_address_is_primary` enum('y','n') DEFAULT 'y',
  PRIMARY KEY (`postal_address_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `red_postal_address_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_server`
--

DROP TABLE IF EXISTS `red_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_server` (
  `server_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `accepting` tinyint(1) DEFAULT 0,
  `server` varchar(128) NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT 0,
  `mountpoint` varchar(8) DEFAULT NULL,
  `server_default_php_version` varchar(8) DEFAULT '7.0',
  PRIMARY KEY (`server_id`),
  KEY `server` (`server`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_service`
--

DROP TABLE IF EXISTS `red_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_service` (
  `service_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `service_table` varchar(64) NOT NULL DEFAULT '',
  `service_area` varchar(32) DEFAULT NULL,
  `service_name` varchar(64) NOT NULL DEFAULT '',
  `service_description` text NOT NULL,
  `service_help_link` varchar(128) DEFAULT NULL,
  `service_default_host` varchar(255) NOT NULL DEFAULT '',
  `service_delete_order` smallint(6) NOT NULL DEFAULT 0,
  `service_order_by` varchar(64) DEFAULT NULL,
  `service_status` enum('active','inactive') DEFAULT 'active',
  `service_item` tinyint(3) unsigned DEFAULT 1,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='E.g. Apache config, Email alias, SIP Phone, etc.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_sitewide_admin`
--

DROP TABLE IF EXISTS `red_sitewide_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_sitewide_admin` (
  `sitewide_admin_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sitewide_admin_id`),
  KEY `user_name` (`user_name`),
  CONSTRAINT `red_sitewide_admin_ibfk_1` FOREIGN KEY (`user_name`) REFERENCES `red_item_user_account` (`user_account_login`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_stripe_cc_log`
--

DROP TABLE IF EXISTS `red_stripe_cc_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_stripe_cc_log` (
  `stripe_cc_log_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `stripe_cc_log_ip` varchar(32) DEFAULT NULL,
  `stripe_cc_log_fail_date` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`stripe_cc_log_id`),
  KEY `fail_date` (`stripe_cc_log_fail_date`),
  KEY `ip` (`stripe_cc_log_ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_tag`
--

DROP TABLE IF EXISTS `red_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_tag` (
  `tag_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `tag` varchar(64) NOT NULL,
  `tag_status` enum('active','deleted') DEFAULT NULL,
  `tag_modified` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`tag_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `red_tag_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_unique_unix_group`
--

DROP TABLE IF EXISTS `red_unique_unix_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_unique_unix_group` (
  `unique_unix_group_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `unique_unix_group_name` varchar(128) DEFAULT NULL,
  `unique_unix_group_status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY (`unique_unix_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_user`
--

DROP TABLE IF EXISTS `red_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_user` (
  `user_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `user_name` varchar(64) NOT NULL DEFAULT '',
  `user_pass` varchar(128) NOT NULL DEFAULT '',
  `user_admin` enum('y','n') DEFAULT 'n',
  `user_hash` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_voting_token`
--

DROP TABLE IF EXISTS `red_voting_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_voting_token` (
  `voting_token_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `voting_token` varchar(16) NOT NULL DEFAULT '',
  `voting_token_election` varchar(16) NOT NULL DEFAULT '',
  `voting_token_status` enum('active','deleted') DEFAULT NULL,
  PRIMARY KEY (`voting_token_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `red_voting_token_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-26 13:22:03
