CREATE TABLE `red_stripe_cc_log` (
  `stripe_cc_log_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `stripe_cc_log_ip` varchar(32),
  `stripe_cc_log_fail_date` timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`stripe_cc_log_id`),
  KEY `fail_date` (`stripe_cc_log_fail_date`),
  KEY `ip` (`stripe_cc_log_ip`)
);

