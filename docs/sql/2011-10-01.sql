CREATE TABLE `red_planned_payment` (
  `planned_payment_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `planned_payment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `planned_payment_amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `planned_payment_status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY (`planned_payment_id`),
  KEY `account_id` (`invoice_id`),
  KEY `planned_payment_date` (`planned_payment_date`)
);

INSERT INTO red_service SET service_table = 'red_planned_payment', service_area = 'member', service_name = 'Planned Payments', service_description = 'Planned Payments', service_delete_order = 200, service_order_by = 'planned_payment_date DESC', service_status = 'active';

