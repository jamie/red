ALTER TABLE red_item_web_conf CHANGE COLUMN web_conf_port web_conf_port enum('http', 'https', 'auto'); 
ALTER TABLE red_item_web_conf DROP COLUMN web_conf_ip; 
ALTER TABLE red_item_web_conf DROP COLUMN web_conf_status; 
