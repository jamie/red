<?php

require_once('../../ui/etc/config.inc.php');

mysql_connect($config['db_host'],$config['db_user'],$config['db_pass']);
mysql_select_db($config['db_name']);

echo "updating member table\n";
$sql = "SELECT member_id, member_friendly_name FROM red_member";
$result = mysql_query($sql);
while($row = mysql_fetch_row($result)) {
  $member_id = $row[0];
  $member_friendly_name = $row[1];
  $hosting_order_start_date = '9999-12-31';
  $hosting_order_status = 'inactive';
  $hosting_order_price = '200';
  $hosting_order_term = 'year';

  $sql = "SELECT hosting_order_start_date, hosting_order_status, hosting_order_term, hosting_order_price FROM red_hosting_order ".
    "WHERE member_id = $member_id AND hosting_order_billing_type = 'Regular' ORDER BY hosting_order_start_date ASC";
  $h_result = mysql_query($sql);
  //echo "Working on: $member_id which is $member_friendly_name\n";
  while($h_row = mysql_fetch_row($h_result)) {
    $date = $h_row[0];
    $status = $h_row[1];
    $term = $h_row[2];
    $price = $h_row[3];
    // set oldest date available
    if($date < $hosting_order_start_date) $hosting_order_start_date = $date;
    // set active and all other fields based on most recent active hosting order
    if($status == 'active') {
      $hosting_order_status = 'active';
      $hosting_order_price = $price;
      $hosting_order_term = $term;
    }
  }
  $sql = "UPDATE red_member SET ".
    "member_start_date = '$hosting_order_start_date',".
    "member_status = '$hosting_order_status',".
    "member_price = '$hosting_order_price',".
    "member_term = '$hosting_order_term' ".
    "WHERE member_id = $member_id";

  //echo "$sql\n";
  if(!mysql_query($sql)) echo "Failed: $sql\n";
}
$sql = "UPDATE red_member SET member_status = 'active' WHERE member_id = 1";
mysql_query($sql);

echo "updating invoices\n";
$sql = "SELECT red_invoice.invoice_id,red_hosting_order.member_id FROM red_invoice JOIN map_invoice_hosting_order USING (invoice_id) ".
  "JOIN red_hosting_order USING (hosting_order_id)";
$result = mysql_query($sql);
echo mysql_error() ;
while($row = mysql_fetch_array($result)) {
  $invoice_id = $row[0];
  $member_id = $row[1];
  $sql = "UPDATE red_invoice SET member_id = $member_id WHERE invoice_id = $invoice_id";
  //echo "$sql\n";
  if(!mysql_query($sql)) echo "Failed: $sql\n";
}


echo "updating correspondence\n";
$sql = "SELECT hosting_order_name,member_id FROM red_hosting_order";
$result = mysql_query($sql);
while($row = mysql_fetch_row($result)) {
  $name = addslashes($row[0]);
  $member_id = intval($row[1]);
  $sql = "SELECT correspondence_id FROM red_correspondence WHERE correspondence_body LIKE '%Order: $name%'";
  $c_result = mysql_query($sql);
  $error = mysql_error();
  if(!empty($error)) echo "Error: $error\n";
  while($c_row = mysql_fetch_row($c_result)) {
    $correspondence_id = $c_row[0];
    $sql = "UPDATE red_correspondence SET member_id = $member_id WHERE correspondence_id = $correspondence_id";
    // echo "$sql\n";
    mysql_query($sql) || die(mysql_error() . " on $sql\n");
  }
}

echo "updating password reset\n";
$sql = "SELECT red_item_pass_reset.item_id,red_item.hosting_order_id FROM red_item JOIN red_item_user_account USING(item_id) JOIN red_item_pass_reset ON red_item_pass_reset.pass_reset_login = red_item_user_account.user_account_login";
$result = mysql_query($sql);
while($row = mysql_fetch_row($result)) {
  $password_reset_id = intval($row[0]);
  $hosting_order_id = intval($row[1]);
  $sql = "INSERT INTO red_item SET hosting_order_id = $hosting_order_id, service_id = 19, item_status = 'active'";
  mysql_query($sql);
  $id = mysql_insert_id();
  $sql = "UPDATE red_item_pass_reset SET item_id = $id WHERE item_id = $password_reset_id";
  //echo "$sql\n";
  mysql_query($sql);
}

echo "inserting user_member_access\n";
// add a single record to the user member access table
// for the oldest user hosting order access record
$sql = "SELECT DISTINCT(member_id) FROM ".
  "red_hosting_order JOIN red_unique_unix_group ".
  "USING(unique_unix_group_id) WHERE hosting_order_status = 'active'";
$result = mysql_query($sql);
while($row = mysql_fetch_row($result)) {
  $username = '';
  $member_id = $row[0];
  // mfpl shouldn't have any member level access, we're all admins
  if($member_id == 1) continue;
  $sql = "SELECT DISTINCT(hosting_order_access_login) FROM  ".
    "red_item_hosting_order_access JOIN red_item USING(item_id) ".
    "JOIN red_hosting_order USING(hosting_order_id) ".
    "JOIN red_member USING(member_id) WHERE ".
    "member_id = $member_id AND item_status = 'active' ".
    "ORDER BY item_modified ASC LIMIT 1";
  // echo $sql . "\n"; continue;
  $login_result = mysql_query($sql);
  $login_row = mysql_fetch_row($login_result);
  $username = $login_row[0];
  if(!empty($username)) { 
    $sql = "INSERT INTO red_map_user_member SET member_id = $member_id, ".
      "login = '$username'";
    mysql_query($sql);
  }
}

      


