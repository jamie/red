INSERT INTO red_service SET service_table = 'red_voting_token', service_name = 'Voting Token', service_area = 'member', service_description = 'Voting Token', service_delete_order = 0, service_order_by = 'voting_token_election DESC'; 
CREATE TABLE `red_voting_token` (
  `voting_token_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `voting_token` varchar(16) NOT NULL DEFAULT '',
  `voting_token_election` varchar(16) NOT NULL DEFAULT '',
  `voting_token_status` enum('active','deleted') DEFAULT NULL,
   PRIMARY KEY (`voting_token_id`),
   KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



