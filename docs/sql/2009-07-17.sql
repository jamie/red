ALTER TABLE red_hosting_order DROP COLUMN hosting_order_term;
ALTER TABLE red_hosting_order DROP COLUMN hosting_order_price;
ALTER TABLE red_hosting_order DROP COLUMN hosting_order_billing_type;
ALTER TABLE red_hosting_order DROP COLUMN hosting_order_start_date;
ALTER TABLE red_hosting_order DROP COLUMN package_id;
ALTER TABLE red_hosting_order DROP COLUMN contract_id;

DROP TABLE village_log;
DROP TABLE dial;
DROP TABLE address;
DROP TABLE contract;
DROP TABLE contract_template;
DROP TABLE allocation;
DROP TABLE rolodex; 
DROP TABLE prospect; 
DROP TABLE prospect_note; 
DROP TABLE package;
DROP TABLE expense_order;
DROP TABLE work_order;
DROP TABLE map_invoice_expense_order;
DROP TABLE map_invoice_hosting_order;
DROP TABLE map_invoice_work_order;
DROP TABLE staff;


DELETE FROM mj_menus WHERE (menu_type = 'search' OR menu_type = 'add') AND mj_menu_id NOT IN (1,2,81,80,84,86,21,22,19,60,21,61,13,14,12,63,62,11,20,60,61) order by value;
DELETE FROM mj_menus WHERE mj_menu_id IN (79,62,63,60,61,77,82);

UPDATE mj_menus SET display = "Purchases" WHERE mj_menu_id = 21;
UPDATE mj_menus SET display = "Purchase" WHERE mj_menu_id = 22;
UPDATE mj_menus SET display = "Expenditure" WHERE mj_menu_id = 12;
UPDATE mj_menus SET display = "Expenditures" WHERE mj_menu_id = 11;
UPDATE mj_menus SET display = "Invoices" WHERE mj_menu_id = 13;
UPDATE mj_menus SET display = "Invoice" WHERE mj_menu_id = 14;
UPDATE mj_menus SET display = "Payments" WHERE mj_menu_id = 19;
UPDATE mj_menus SET display = "Payment" WHERE mj_menu_id = 20;

UPDATE mj_menus SET display_order = 1 WHERE mj_menu_id = 13;
UPDATE mj_menus SET display_order = 2 WHERE mj_menu_id = 19;
UPDATE mj_menus SET display_order = 1 WHERE mj_menu_id = 14;
UPDATE mj_menus SET display_order = 2 WHERE mj_menu_id = 20;
UPDATE mj_menus SET display_order = 5 WHERE mj_menu_id = 22; UPDATE mj_menus SET display_order = 6 WHERE mj_menu_id = 12;

UPDATE mj_menus SET value = 'red_invoice_form' WHERE mj_menu_id =14;  UPDATE mj_menus SET value = 'red_invoice_list' WHERE mj_menu_id =13;
UPDATE mj_menus SET value = 'red_payment_form' WHERE mj_menu_id =20;  UPDATE mj_menus SET value = 'red_payment_list' WHERE mj_menu_id =19;

UPDATE mj_lists SET source = "SELECT red_invoice.invoice_id, CONCAT(DATE_FORMAT(invoice_date,\"%m/%d/%Y\"), ', $', invoice_amount, ', #',red_invoice.invoice_id) FROM red_invoice ORDER BY red_invoice.invoice_id" WHERE mj_list_id= 17;

DELETE FROM mj_lists WHERE mj_list_id IN (23,32,8,9,10,11,12,13,14,15,27,18,15,34,33,31,30,28,36,35);

--- deleting orphaned invoices

DELETE FROM red_invoice WHERE invoice_id IN (2088, 2680);
UPDATE red_invoice SET invoice_status = 'eaten' WHERE invoice_id IN (1666,1674);

UPDATE mj_relationships SET table1 = 'red_payment' WHERE table1 = 'payment';
UPDATE mj_relationships SET table2 = 'red_payment' WHERE table2 = 'payment';
UPDATE mj_relationships SET table1 = 'red_invoice' WHERE table1 = 'invoice';
UPDATE mj_relationships SET table2 = 'red_invoice' WHERE table2 = 'invoice';


DELETE FROM mj_relationships WHERE table1 IN ('contact','contract','contract_template','red_hosting_order','red_unique_unix_group','map_invoice_hosting_order','red_member','map_invoice_work_order','package','red_service','staff','red_item','work_order');
DELETE FROM mj_relationships WHERE table2 IN ('map_invoice_hosting_order','map_invoice_work_order');

