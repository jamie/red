<?php

/**
 * red authorization class. Determines if a given user is an admin
 * and determines which items the user should have access to
 **/

class cred_authz {
  var $_sql_resource;
  var $_user_name;
  var $_errors = array();
  var $_is_admin = null;
  var $_is_disabled = null;

  // Whether the user can use the API.
  var $_is_api_allowed = null;

  function set_sql_resource(&$sql_resource) {
    $this->_sql_resource =& $sql_resource;
  }

  function get_user_name() {
    return addslashes($this->_user_name);
  }

  function set_user_name($user_name) {
    $this->_user_name = $user_name;
  }

  function set_is_admin($val = null) {
    if(!is_null($val)) {
      $this->_is_admin = $val;
      return true;
    }
    $user_name = $this->get_user_name();
    if(empty($user_name)) {
      $this->_set_error(red_t("Please set username before setting is_admin."));
      return false;
    }

    $sql = "SELECT sitewide_admin_id FROM red_sitewide_admin WHERE ".
      "user_name = '$user_name'";
    $result = red_sql_query($sql,$this->_sql_resource);
    if(red_sql_num_rows($result) == 1) {
      $this->_is_admin = true;
      return true;
    }
    $this->_is_admin = false;
    return true;
  }

  // Given a username, get the user's membership level.
  function get_membership_level($user_name = NULL) {
    if(is_null($user_name)) {
      $user_name = $this->get_user_name();
    }

    if(empty($user_name)) return NULL;

    $sql = "SELECT member_benefits_level FROM red_member 
      JOIN red_hosting_order USING(member_id) 
      JOIN red_item USING(hosting_order_id) 
      JOIN red_item_user_account USING(item_id) 
      WHERE user_account_login = '$user_name' AND
      item_status = 'active' AND hosting_order_status = 'active' 
      AND member_status = 'active'";
    $result = red_sql_query($sql,$this->_sql_resource);
    if(red_sql_num_rows($result) == 0) {
      return NULL;
    }

    $row = red_sql_fetch_row($result);
    return $row[0];
  }

  // API access is only allowed if your membership is standard
  // or extra (not basic).
  function set_is_api_allowed($val = null) {
    if(!is_null($val)) {
      $this->_is_api_allowed = $val;
      return true;
    }
    $user_name = $this->get_user_name();
    if(empty($user_name)) {
      $this->_set_error(red_t("Please set username before setting is_api_allowed."));
      return false;
    }

    $level = $this->get_membership_level($user_name);
    if($level == 'standard' || $level == 'extra') {
      $this->_is_api_allowed = TRUE;
      return true;
    }
    $this->_is_api_allowed = FALSE;
    return true;
  }

  function set_is_disabled($val = null) {
    if(!is_null($val)) {
      $this->_is_disabled = $val;
      return true;
    }
    $user_name = $this->get_user_name();
    if(empty($user_name)) {
      $this->_set_error(red_t("Please set username before setting is_disabled."));
      return false;
    }

    $sql = "SELECT item_status FROM red_item JOIN red_item_user_account ".
      "USING(item_id) WHERE user_account_login = '$user_name'";
    $result = red_sql_query($sql,$this->_sql_resource);
    if(red_sql_num_rows($result) == 0) {
      // this shouldn't happen. The login isn't technically disabled, but since
      // it doesn't exist, set to disabled to be safe
      $this->_is_disabled = true;
      return true;
    }
    $row = red_sql_fetch_row($result);
    if($row[0] == 'disabled' || $row[0] == 'pending-disable') {
      $this->_is_disabled = true;
      return true;
    }
    $this->_is_disabled = false;
    return true;
  }

  function get_user_name_item_id() {
    $user_name = $this->get_user_name();
    $sql = "SELECT red_item.item_id FROM red_item JOIN red_item_user_account 
      USING(item_id) WHERE item_status = 'active' AND user_account_login = 
      '$user_name'";
    $result = red_sql_query($sql, $this->_sql_resource);
    $row = red_sql_fetch_row($result);
    if(isset($row[0])) {
      return $row[0];
    }
  }

  function _set_error($v) {
    $this->_errors[] = $v;
  }

  function get_errors() {
    return $this->_errors;
  }

  function is_admin() {
    if(is_null($this->_is_admin)) {
      if(!$this->set_is_admin()) return false;
    }
    return $this->_is_admin;
  }

  function is_disabled() {
    if(is_null($this->_is_disabled)) {
      // if we fail to call set_is_disabled(), then default
      // to closed
      if(!$this->set_is_disabled()) return true;
    }
    return $this->_is_disabled;
  }

  function is_api_allowed() {
    if(is_null($this->_is_api_allowed)) {
      // if we fail to call set_is_disabled(), then default
      // to closed
      if(!$this->set_is_api_allowed()) return FALSE;
    }
    return $this->_is_api_allowed;
  }

  // not yet implemented...
  function check_access($object, $id = null, $parent_id = null ) {
    if($this->is_admin()) return true;
    return false;
  }
    
  function get_member_parent_ids() {
    $sql = "SELECT member_id FROM red_map_user_server WHERE login = '" . 
      $this->get_user_name() . "' AND status = 'active'";
    $result = red_sql_query($sql, $this->_sql_resource);
    $ret = array();
    while($row = red_sql_fetch_row($result)) {
      $ret[] = $row[0];
    }
    return $ret;
  }

  function get_member_ids() {
    $sql = "SELECT member_id FROM red_map_user_member WHERE login = '" . 
      $this->get_user_name() . "' AND status = 'active'";
    $result = red_sql_query($sql, $this->_sql_resource);
    $ret = array();
    while($row = red_sql_fetch_row($result)) {
      $ret[] = $row[0];
    }
    return $ret;
  }

  function get_hosting_order_ids() {
    // Add access via hosting orders.
    $sql = "SELECT hosting_order_id FROM red_item_hosting_order_access ".
      "JOIN red_item USING(item_id) WHERE hosting_order_access_login = '" . 
      $this->get_user_name() . "' AND item_status = 'active' ";
    $result = red_sql_query($sql, $this->_sql_resource);
    $ret = array();
    while($row = red_sql_fetch_row($result)) {
      $ret[] = $row[0];
    }
    // Add access via memberships.
    $sql = "SELECT hosting_order_id FROM red_map_user_member JOIN " .
      "red_member USING(member_id) JOIN red_hosting_order USING(member_id) " . 
      "WHERE status = 'active' && login = '" .  $this->get_user_name() .
      "' AND member_status = 'active' AND hosting_order_status = 'active'";
    $result = red_sql_query($sql, $this->_sql_resource);
    while($row = red_sql_fetch_row($result)) {
      $ret[] = $row[0];
    }
    return $ret;
  }

  function rewrite_sql($query, $object) {
    if($this->is_admin()) return null;

    $joins = array();
    $extra_wheres = array();

    $member_ids = $this->get_member_parent_ids(); 
    if(count($member_ids) > 0) {
      if($object == 'item') {
        $joins[] = " JOIN red_hosting_order USING(hosting_order_id) ";
      }
      $joins[] = " JOIN red_member USING(member_id) ";
      foreach($member_ids as $member_id) {
        $extra_wheres[] = "red_member.member_parent_id = " . intval($member_id); 
      }
    } 

    // Check for member level access
    $member_ids = $this->get_member_ids(); 
    if(count($member_ids) > 0) {
      if($object == 'item') {
        $joins[] = " JOIN red_hosting_order USING(hosting_order_id) ";
      }
      $joins[] = " JOIN red_member USING(member_id) ";
      foreach($member_ids as $member_id) {
        $extra_wheres[] = "red_member.member_id = " . intval($member_id); 
      }
    }
    if($object == 'item') {
      $hosting_order_ids = $this->get_hosting_order_ids(); 
      if(count($hosting_order_ids) > 0) {
        $joins[] = " JOIN red_hosting_order USING(hosting_order_id) ";
        foreach($hosting_order_ids as $hosting_order_id) {
          $extra_wheres[] = "red_item.hosting_order_id = " . intval($hosting_order_id); 
        }
      }
    }
    if(count($extra_wheres) == 0) return $sql;

    preg_match('/^(SELECT .*)(FROM .*)(WHERE .*)/',$query,$matches);
    $select = $matches[1];
    $from = $matches[2];
    $where = $matches[3];

    $joins = array_unique($joins);
    return $select . $from . implode(' ', $joins) . $where . ' AND (' . implode(' OR ', $extra_wheres) . ')';
  }

  function check_insert_perms($object, $parent_id) {
    // Admin has access to everything.
    if($this->is_admin()) return true;

    // If parent id is not specified, lock them out.
    if(empty($parent_id)) return false;

    // Only admins can create payment records.
    if($object == 'payment' && !$this->is_admin()) {
      return FALSE;
    }

    // Find out what level their membership is. Basic membership gets
    // restricted.
    $level = $this->get_membership_level();
    

    // Basic members are allowed to create new ones of these and only
    // these.
    $basic_member_allowed_objects = array(
      'contact',
      'planned_payment',
      'phone',
      'postal_address'
    );

    // Only allow certain objects for basic level members..
    if($level != 'standard' && $level != 'extra' && !in_array($object, $basic_member_allowed_objects)) {
      return FALSE;
    }

    $member_objects = array(
      'contact',
      'invoice',
      'hosting_order',
      'planned_payment',
      'payment', 
      'map_user_member', 
      'phone', 
      'postal_address',
      'item_mysql_db',
      'item_mysql_user',
    );


    $member_parent_ids = $this->get_member_parent_ids(); 
    if($object == 'member' || in_array($object, $member_objects)) {
      if(in_array($parent_id, $member_parent_ids)) return true;
      if(in_array($object,$member_objects)) {
        $member_ids = $this->get_member_ids(); 
        if(in_array($parent_id, $member_ids)) return true;
      }
    }
    if($object == 'item') {
      // Can't create new items if you are a basic member
      if($level != 'standard' && $level != 'extra') {
        return FALSE;
      }

      $hosting_order_ids = $this->get_hosting_order_ids(); 
      if(in_array($parent_id, $hosting_order_ids)) return true;
    }
    return false;
  }
}

?>
