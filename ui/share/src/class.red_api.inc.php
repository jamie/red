<?php

class red_api extends red_db{

  var $_config;
  var $_values = array();
  var $_errors = array();
  var $_output = array();
  var $_construction_options = array();
  var $_log_level = 0;
  var $_params = array(
    'where' => array(),
    'set' => array(),
    'sub' => array(),
  );

  // limit number of records returned on select
  // to avoid memory problems
  var $_max_records = 50;

  // text or json
  var $_output_format;

  // for cli access, auth is redundant
  // for web access, every transaction
  // should be authorized
  var $_check_authz = false;

  var $_authz = null;

  // call parent
  function __construct($sql_resource) {
    parent::__construct($sql_resource);
  }

  function set_error($error) {
    $this->_errors[] = $error;
  }
  function set_values($value) {
    $this->_values[] = $value;
  }
  
  function set_log_level($value) {
    $this->_log_level = $value;
  }

  function log($message, $level) {
    if($level <= $this->_log_level) {
      print "$message\n";
    }
  }

  function init($config) {
    $this->_config = $config;
    require_once($this->_config['common_src_path'] . '/red.lang.utils.inc.php');
    require_once($this->_config['common_src_path'] . '/class.red_ado.inc.php');
    require_once($this->_config['common_src_path'] . '/class.red_item.inc.php');
    require_once($this->_config['common_src_path'] . '/red.utils.inc.php');
    require_once($this->_config['src_path'] . '/functions.inc.php');

    $this->_construction_options = array(
      'mode' => 'ui',
      'notify_host' => $config['notify_host'],
      'sql_resource' => $this->_sql_resource,
      'src_path' => $this->_config['src_path'],
      'common_src_path' => $this->_config['common_src_path'],
      'site_dir_template' => $this->_config['site_dir_template'],
    );
    // Optional config items...
    if(array_key_exists('notify_cmd', $config)) {
      $this->_construction_options['notify_cmd'] = $config['notify_cmd'];
    }
    return true;
  }

  function get_output() {
    return $this->_output;
  }

  function set_output_format($format) {
    $this->_output_format = $format;
  }

  function authenticate() {
    // set auth flag - this will indicate that we 
    // should both authenticate and authorize
    if(!array_key_exists('user_name',$this->_params)) {
      $this->set_error(red_t("Username not provided."));
      return false;
    }
    if(!array_key_exists('user_pass',$this->_params)) {
      $this->set_error(red_t("Password not provided."));
      return false;
    }
    require_once($this->_config['src_path'] . '/class.red_auth.inc.php');

    
    $auth = new cred_auth();
    $auth->set_sql_resource($this->_sql_resource);
    $user_name = $this->_params['user_name'];
    $user_pass = $this->_params['user_pass'];
    if(!$auth->valid_user($user_name, $user_pass)) {
      $this->set_error("Failed authentication.");
      return false;
    }
    $this->_check_authz = true;
    require_once($this->_config['src_path'] . '/class.red_authz.inc.php');
    $this->_authz = new cred_authz();
    $this->_authz->set_sql_resource($this->_sql_resource);
    $this->_authz->set_user_name($user_name);
    // API usage is not permitted from disabled user accounts
    if($this->_authz->is_disabled()) {
      $this->set_error("Your user account is disabled, please contact support.");
      return false;
    }
    // API usage is not permitted from basic memberships 
    if(!$this->_authz->is_api_allowed()) {
      $this->set_error("Your user account is not allowed to use the API.");
      return false;
    }
    return true;
  }

  function run() {
    $this->param_substitute();
    if(!$this->sanity_check()) {
      $this->set_error(red_t("Failed sanity check"));
      return false;
    }

    $object = $this->convert_to_object();

    // return on error
    if(false === $object) return false;

    // $object should be true if action is select (which
    // populates $this->_values, nothing left to do
    if($object === true) return true;

    $params = $this->_params;
    // set delete flag if necessary
    if($params['action'] == 'delete') $object->set_delete_flag();

    // set to pending-disabled if necessary
    if($params['action'] == 'disable') $object->set_disable_flag();

    // set object properties based on supplied user input
    // and validate
    if(!$this->process_user_input($object)) {
      $this->set_error(red_t("Failed validation"));
      return false;
    } elseif ($params['action'] == 'validate') {
      return true;
    }

    // save changes
    if(!$object->commit_to_db()) {
      $this->set_error(red_t("Errors were found when committing to db!"));
      $errors = $object->get_errors('system');
      foreach ($errors as $v)  {
        $this->set_error($v['error']);
      }
      return false;
    } else {
      // success. now, populate the values array that will
      // be returned as output 
      $this->populate_values_from_items(array($object));
      return true;
    }
  }

  /**
   * Given a set of parameters, attempt to create one or more objects
   * based on the given paramaters. On select, create multiple objects
   * matching the criteria in params, on update or delete, try to create
   * the object matching the params that should be update/deleted, on 
   * insert create an empty object to be populated.
   **/
  function convert_to_object() {
    $result = null;
    $params = $this->_params;
    if($params['action'] != 'insert') {
      // get a sql result set based on the params
      // this result set will be restricted to records they
      // should have access to
      $result = $this->get_result_set();
    } else {
      if($this->_check_authz && !$this->_authz->is_admin()) {
        // ensure they have permission to create an object of this type
        $object = $params['object'];
        $parent_id = null;
        if($object == 'item') {
          $parent_id = $params['set']['hosting_order_id'];
        } elseif($object == 'member') {
          if(array_key_exists('member_parent_id', $params['set'])) {
            $parent_id = $params['set']['member_parent_id'];
          } else {
            $parent_id = null;
          }
        } else {
          $parent_id = $params['set']['member_id'];
        }
        if(!$this->_authz->check_insert_perms($object,$parent_id)) {
          $this->set_error(red_t("You are not authorized to create this record."));
          return false;
        }
      }
    }

    // handle errors
    if(false === $result) return false;

    // First, handle select, which could have multiple items
    if($params['action'] == 'select') {
      // $result is null if we can't find any matching
      // items with the parameters given.
      if(is_null($result)) return true;

      // otherwise, populate the output with the values found
      $items = array();
      while($row = mysqli_fetch_assoc($result)) {
        $this->_construction_options['rs'] =& $row;
        $items[] = $this->get_object();
      }
      $this->populate_values_from_items($items);

      // nothing left to do, since we're only selecting
      return true;
    } 
    // if we have a record, prepopulate the item with it's
    // values
    if(!is_null($result)) {
      $row = mysqli_fetch_assoc($result);
      $this->_construction_options['rs'] = $row;
    } else {
      // otherwise, the result set is null.
      // if the action was to delete, then we return true 
      // (trying to delete a record that doesn't exist is considered
      // a success)
      if($params['action'] == 'delete') return true;

      // otherwise, we need to create an empty record
      // we need a few more values
      if(array_key_exists('hosting_order_id',$params['set'])) {
        $this->_construction_options['hosting_order_id'] = $params['set']['hosting_order_id'];
      } elseif(array_key_exists('hosting_order_id',$params['where'])) {
        $this->_construction_options['hosting_order_id'] = $params['where']['hosting_order_id'];
      }
      if(array_key_exists('service_id',$params['set'])) {
        $this->_construction_options['service_id'] = $params['set']['service_id'];
      } elseif(array_key_exists('service_id',$params['where'])) {
        $this->_construction_options['service_id'] = $params['where']['service_id'];
      }
    }
    return $this->get_object();
  }

  // return an object givin the passed params
  function get_object() {
    $params = $this->_params;
    if($params['object'] == 'item') {
      $obj =& red_item::get_red_object($this->_construction_options);
      // don't background the process so we can more easily act
      // on results 
      $obj->set_notify_background_process(FALSE);
    } else {
      $object = $params['object'];
      $object_path = $this->_config['src_path'] . '/modules/class.red.' . $object . '.inc.php';
      $object_name = 'red_' . $object;
      $object_key_field_function = 'get_' . $object . '_id';
      if(file_exists($object_path)) {
        require_once($object_path);
        $obj = new $object_name($this->_construction_options);
      }
    }
    return $obj;
  }

  function populate_values_from_items($items) {
    reset($items);
    foreach($items as $item) {
      $fields = array_keys($item->_datafields);
      $value = array();
      foreach ($fields as $field)  {
        $func = 'get_' . $field;
        $value[$field] = $item->$func();
      }
      $this->set_values($value);
    }
  }
  /**
   * Get a result set matching the passed parameters 
   * in $params
   **/
  function get_result_set() {
    $params = $this->_params;
    $join = array();
    $where = array();
    $fields = array();
    $action = $params['action'];
    if($params['object'] == 'item') {
      $service_id = array_key_exists('service_id',$params['where']) ? $params['where']['service_id'] : null;
      if(is_null($service_id)) {
        $service_id = array_key_exists('service_id',$params['set']) ? $params['set']['service_id'] : null;
      }
      if(is_null($service_id)) {
        $this->set_error(red_t("service_id is required."));
        return false;
      }
      $service_table = red_get_service_table_for_service_id($service_id,$this->_sql_resource);
      $join[] = "JOIN $service_table USING(item_id)";
      $fields[] = "$service_table.*";
    } 
    $safe_object = addslashes($params['object']);
    $table = 'red_' . $safe_object;
    $where[] = "(${safe_object}_status = 'active' OR ${safe_object}_status = 'disabled')";
    $fields[] = "red_${safe_object}.*";
    reset($params);
    foreach($params['where'] as $k => $v) { 
      $where[] = "$k = '$v'";
    }
    $sql = "SELECT " . implode(',',$fields) . " FROM $table " . implode(' ',$join) . " WHERE " . implode(' AND ',$where);
    $this->log("sql, pre rewrite: $sql",1);
    if($this->_check_authz && !$this->_authz->is_admin()) {
      $sql = $this->_authz->rewrite_sql($sql, $params['object']);  
    }
    $this->log("sql, post rewrite: $sql",1);
    $result = red_sql_query($sql,$this->_sql_resource);
    $num = red_sql_num_rows($result);
    if($num > $this->_max_records) {
      $this->set_error(red_t("Max records exceeded. Please limit your select to criteria that will return less than %num recordes.", array('%num' => $this->_max_records)));
      return false;
    }

    if($num == 0) {
      if ($action == 'update' || $action == 'disable' ) {
        $this->set_error(red_t("Failed to find existing record."));
        return false;
      }
    }
    if($num == 0) return null;
    if($num == 1) return $result;
    if($num > 1) {
      if ($action != 'select') {
        $this->set_error(red_t("Found more than one matching record. Aborting."));
        return false;
      } else {
        return $result;
      }
    }
  }

  /**
   * Certain fields can be substituted for other fields
   * (e.g. you can pass the hosting_order_identifier which
   * will be substituted for the hosting_order_id). This
   * function performs that substitution.
   **/
  function param_substitute() {
    $params =& $this->_params;
    if(!array_key_exists('sub',$params)) return;

    foreach ($params['sub'] as $k => $v) {
      if($k == 'hosting_order_identifier' && !array_key_exists('hosting_order_id',$params['where'])) {
        $params['set']['hosting_order_id'] = $this->get_hosting_order_id_for_identifier($v);
      } 
      if($k == 'member_friendly_name' && !array_key_exists('member_id',$params['where'])) {
        $params['set']['member_id'] = $this->get_member_id_for_friendly_name($v);
      }
      if($k == 'mysql_db_name' && !array_key_exists('web_app_mysql_db_id',$params['set'])) {
        $params['set']['web_app_mysql_db_id'] = $this->get_item_id_for_name('mysql_db','mysql_db_name',$params['where']['hosting_order_id'],$v);
      }
      if($k == 'mysql_user_name' && !array_key_exists('web_app_mysql_user_id',$params['set'])) {
        $params['set']['web_app_mysql_user_id'] = $this->get_item_id_for_name('mysql_user','mysql_user_name',$params['where']['hosting_order_id'],$v);
      }
      if($k == 'cron_cmd' && !array_key_exists('web_app_cron_id',$params['set'])) {
        $params['set']['web_app_cron_id'] = $this->get_item_id_for_name('cron','cron_cmd',$v,$params['where']['hosting_order_id']);
      }
      if($k == 'unique_unix_group_name' && !array_key_exists('unique_unix_group_id_id',$params['set'])) {
        $params['set']['unique_unix_group_id'] = $this->get_unique_unix_group_id_for_name($v);
      }
    }
  }

  function get_item_id_for_name($table,$name_field,$name,$hosting_order_id) {
    $name = addslashes($name);
    $table = addslashes($table);
    $name_field = addslashes($name_field);
    $hosting_order_id = intval($hosting_order_id);

    $sql = "SELECT red_item.item_id FROM $table JOIN red_item ".
      "USING(item_id) WHERE $name_field = '$name' " .
      "AND (item_status = 'active' OR item_status = 'disabled') AND hosting_order_id = $hosting_order_id ";
    $result = red_sql_query($sql,$this->_sql_resource);
    $row = red_sql_fetch_row($result);
    $id = intval($row[0]);
    if(!empty($id)) {
      return $id;
    }
    return null;
  }

  function get_unique_unix_group_id_for_name($name) {
    $name = addslashes($name);
    $sql = "SELECT unique_unix_group_id FROM red_unique_unix_group WHERE ".
      "unique_unix_group_name = '$name' " .
      "AND unique_unix_group_status = 'active' ";
    $result = red_sql_query($sql,$this->_sql_resource);
    $row = red_sql_fetch_row($result);
    $unique_unix_group_id = intval($row[0]);
    if(!empty($unique_unix_group_id)) {
      return $unique_unix_group_id;
    }
    return null;
  }

  function get_hosting_order_id_for_identifier($identifier) {
    $hosting_order_identifier = addslashes($identifier);
    $sql = "SELECT hosting_order_id FROM red_hosting_order WHERE ".
      "hosting_order_identifier = '$hosting_order_identifier' " .
      "AND (hosting_order_status = 'active' OR hosting_order_status = 'disabled' ) ";
    $result = red_sql_query($sql,$this->_sql_resource);
    $row = red_sql_fetch_row($result);
    $hosting_order_id = intval($row[0]);
    if(!empty($hosting_order_id)) {
      return $hosting_order_id;
    }
    return null;
  }
  function get_member_id_for_friendly_name($friendly_name) {
    $member_friendly_name = addslashes($friendly_name);
    $sql = "SELECT member_id FROM red_member WHERE ".
      "member_friendly_name = '$friendly_name' " .
      "AND member_status = 'active' ";
    $result = red_sql_query($sql,$this->_sql_resource);
    $row = red_sql_fetch_row($result);
    $member_id = intval($row[0]);
    if(!empty($member_id)) {
      return $member_id;
    }
    return null;


  }
  /**
   * Parse cli passed arguments into an array
   **/
  function parse_cli_arguments($args) {
    foreach($args as $k => $v) {
      if(preg_match('/^--(.*)$/',$v,$matches)) {
        $foo = $matches[1];
        // check for --arg=value syntax
        if(preg_match('/^(.*)=(.*)$/',$foo,$matches)) {
          $field = $matches[1];
          $value = $matches[2];
        } else {
          // otherwise assume --arg value syntax
          $field = $foo;
          $value_index = $k+1;
          if(array_key_exists($value_index,$args) && !preg_match('/^--/',$args[$value_index])) {
            $value = $args[$value_index];
            unset($args[$value_index]);
          } else {
            $this->set_error(red_t("Warning: %field missing value.", array('%field' => $foo)));
            continue;
          }
        }
        $this->parse_argument($field,$value);
      }
    }
  }

  // parse web/REST provided arguments
  function parse_request_arguments($args) {
    foreach($args as $field => $value) {
      $this->parse_argument($field,$value);
    }
  }

  /**
   * parse arguments into an api consistent 
   * array syntax
   **/
  function parse_argument($field,$value) {
    $field = str_replace('-','_',$field);
    // set/sub/where indexes are sanitized here to ensure they
    // only contain letters, numbers and underscore
    // Also, all values can get used in sql statements, so
    // addslashes to everything
    if(preg_match('/^set:([a-z0-1_]+)/',$field,$matches)) {
      $field = $matches[1];
      $this->_params['set'][$field] = addslashes($value);
    } elseif(preg_match('/^sub:([a-z0-1_]+)/',$field,$matches)) {
      $field = $matches[1];
      $this->_params['sub'][$field] = addslashes($value);
    } elseif(preg_match('/^where:([a-z0-1_]+)/',$field,$matches)) {
      $field = $matches[1];
      $this->_params['where'][$field] = addslashes($value);
    } elseif ($field == 'action') {
      $this->_params['action'] = addslashes($value);
    } elseif ($field == 'object') {
      $this->_params['object'] = addslashes($value);
    } elseif ($field == 'log_level') {
      $this->set_log_level(intval($value));
    } elseif ($field == 'user_name') {
      $this->_params['user_name'] = addslashes($value);
    } elseif ($field == 'user_pass') {
      $this->_params['user_pass'] = addslashes($value);
    } 
  }

  function sanity_check() {
    $params = $this->_params;
    $ret = true;

    // ensure known params are integers 
    $validate = array(
      'service_id' => '/^[0-9]+$/',
      'hosting_order_id' => '/^[0-9]+$/',
      'member_id' => '/^[0-9]+$/',
    );

    if(!array_key_exists('action',$params)) {
      $this->set_error(red_t("Missing action parameter."));
      $ret = false; 
      // set to emtpy to avoid missing index errors below
      $params['action'] = '';
    } elseif(!preg_match('/^(delete|insert|validate|update|replace|select|disable)$/',$params['action'])) {
      $this->set_error(red_t("Unknown action"));
      $ret = false;
    }
    if(!array_key_exists('object',$params)) {
      $this->set_error(red_t("Missing object parameter."));
      $ret = false; 
    } elseif(!preg_match('/^(member|item|hosting_order|contact|unique_unix_group|invoice|phone|postal_address)$/',$params['object'])) {
      $this->set_error(red_t("Unknown object."));
      $ret = false;
    }

    $set_actions = array('insert','validate','update','replace');
    if(!in_array($params['action'],$set_actions) && count($params['set']) > 0) {
      $this->set_error(red_t("Set fields are meaningless with this action."));
      $ret = false;
    }
    // validate fields that should be integers
    foreach ($validate as $field => $regexp) {
      if(!array_key_exists($field,$params['where'])) continue;
      if(!preg_match($regexp,$params['where'][$field])) {
        $this->set_error(red_t("Failed regexp for $field"));
        $ret = false;
      }
    }
    if(!$ret) return false;

    // make sure service id is valid
    if(array_key_exists('service_id',$params['where'])) {
      $check = red_get_service_table_for_service_id($params['where']['service_id'],$this->_sql_resource);
      if(empty($check)) {
        $this->set_error(red_t("Service id is invalid."));
        $ret = false;
      }
    }
    if(array_key_exists('hosting_order_id',$params['where'])) {
      $hoid = $params['where']['hosting_order_id'];
      if(!$this->valid_hosting_order_id($hoid)) {
        $this->set_error(red_t("Hosting Order Id doesn't exist or is inactive."));
        $ret = false;
      }
    }
    if(array_key_exists('member_id',$params['where'])) {
      $mid = $params['where']['member_id'];
      if(!$this->valid_member_id($mid)) {
        $this->set_error(red_t("Member Id doesn't exist or is inactive."));
        $ret = false;
      }
    }

    // specific sanity checks
    if($params['object'] == 'item') {
      if($params['action'] == 'insert') {
        if(!array_key_exists('hosting_order_id', $params['set'])) {
          $this->set_error(red_t("You must specify a hosting_order_id."));
          $ret = false;
        }
        if(!array_key_exists('service_id', $params['set'])) {
          $this->set_error(red_t("You must specify a service_id."));
          $ret = false;
        }
      }
    }
    return $ret;
  }

  function valid_member_id($id) {
    $sql = "SELECT member_id FROM red_member WHERE member_id ".
      "= " . intval($id) . " AND member_status = 'active'";
    $result = red_sql_query($sql,$this->_sql_resource);
    if(red_sql_num_rows($result) == 0) {
      return false;
    }
    return true;
  }

  function valid_hosting_order_id($id) {
    $sql = "SELECT hosting_order_id FROM red_hosting_order WHERE hosting_order_id ".
      "= " . intval($id) . " AND (hosting_order_status = 'active' OR hosting_order_status = 'disabled') ";
    $result = red_sql_query($sql,$this->_sql_resource);
    if(red_sql_num_rows($result) == 0) {
      return false;
    }
    return true;
  }

  function process_user_input(&$item) {
    $params = $this->_params;
    $datafields = $item->_datafields;
    reset($datafields);
    foreach ($datafields as $field => $properties)  {
      if($field != 'item_id') {
        $get_function = "get_".$field;
        $set_function = "set_".$field;
        $value = $item->$get_function();
        if(array_key_exists($field,$params['set'])) {
          $value = $params['set'][$field];
        }
        $item->$set_function($value);
      }
    }
    // Special case for creating memberships. It is possible to set
    // extra fields that will trigger the creation of related records.
    if (get_class($item) == 'red_member') {
      $member_extra_fields = array(
        'contact_email',
        'contact_first_name',
        'contact_lang',
        'contact_last_name',
      );
      foreach($member_extra_fields as $extra_field) {
        $get_function = "get_".$extra_field;
        $set_function = "set_".$extra_field;
        $value = $item->$get_function();
        if(array_key_exists($extra_field, $params['set'])) {
          $value = $params['set'][$extra_field];
        }
        $item->$set_function($value);
      }
    }
    // Special case for creating hosting orders. We also need to set the unique unix group
    // name.
    if (get_class($item) == 'red_hosting_order') {
      $hosting_order_extra_fields = array(
        'unique_unix_group_name',
        'notification_email',
        'notification_lang'
      );
      foreach($hosting_order_extra_fields as $extra_field) {
        $get_function = "get_".$extra_field;
        $set_function = "set_".$extra_field;
        $value = $item->$get_function();
        if(array_key_exists($extra_field, $params['set'])) {
          $value = $params['set'][$extra_field];
        }
        $item->$set_function($value);
      }

      // Lastly, if hosting_order_host is empty, we fill in the default.
      $hosting_order_host = $item->get_hosting_order_host();
      if (empty($hosting_order_host)) {
        // Pop off the first of the available options.
        $options = $item->get_host_options();
        $host = array_shift($options);
        $item->set_hosting_order_host($host);
      }
    }
    if($item->validate()) return true;
    $this->set_error(red_t("Validation errors."));
    $errors = $item->get_errors('validation');
    foreach ($errors as $error) {
      $this->set_error($error['error']);
    }
    return false;
  }

  function print_output_text() {
    if(array_key_exists('values',$this->_output) &&
      count($this->_output['values']) == 0) {
      unset($this->_output['values']);
    }
    print_r($this->_output);
  }

  function print_output_json() {
    if(array_key_exists('values',$this->_output) &&
      count($this->_output['values']) == 0) {
      unset($this->_output['values']);
    }
    print json_encode($this->_output);
  }
  function print_output() {
    $this->build_output_array();
    if($this->_output_format == 'text') {
      return $this->print_output_text();
    }
    if($this->_output_format == 'json') {
      return $this->print_output_json();
    }
  }

  function build_output_array() {
    $this->_output = array();
    if(count($this->_errors) > 0) {
      $this->_output['is_error'] = 1;
      $this->_output['error_message'] = $this->_errors;
    } else {
      $this->_output['is_error'] = 0;
      $this->_output['values'] = $this->_values;
    }
  }
}


?>
