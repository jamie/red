<?php

if(!class_exists('red_item_hosting_order_access_ui')) {
  class red_item_hosting_order_access_ui extends red_item {
    var $_hosting_order_access_login;


    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);
      $this->_set_child_datafields();
    }

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to remove access to the hosting order from " . 
        $this->_html_generator->get_tag('span',$this->get_hosting_order_access_login(),$attr) . "?";
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array ('hosting_order_access_login' => array (
                               'req' => true,
                               'pcre'   => RED_LOGIN_MATCHER,
                               'pcre_explanation'   => RED_LOGIN_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Login'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 128,
                               'tblname'   => 'red_item_hosting_order_access'),
                                 ));

    }

    function set_hosting_order_access_login($value) {
      $this->_hosting_order_access_login = $value;
    }
    function get_hosting_order_access_login() {
      return $this->_hosting_order_access_login;
    }
    
  }  
}

?>
