<?php

if(!class_exists('red_item_dns_ui')) {
  class red_item_dns_ui extends red_item_dns {
    var $_javascript_includes = array('scripts/item_dns.js');

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }
  
    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the " . 
        $this->_html_generator->get_tag('span',$this->get_dns_type().  
          "/" . $this->get_dns_fqdn(),$attr) . 
        " dns entry?";
    }

    function get_edit_dns_type() {
      $type = $this->get_dns_type();
      $options = $this->get_dns_type_options();
      return $this->_html_generator->get_select('sf_dns_type',$options,$type);
    }    
    function get_edit_dns_ttl() {
      return $this->get_auto_constructed_edit_field('dns_ttl') . ' seconds';
    }
    function get_read_dns_fqdn() {
      $non_active = '';
      // add a warning if it's an mx record, there's not dns server set
      // (it could be a mx record pointing lists
      if(!$this->name_service_active()) {
        $non_active = $this->_html_generator->get_tag('span','*',
                      array('class' => 'red-non-local-mx'));
      }
      return $this->get_auto_constructed_read_field('dns_fqdn') . $non_active;
    }

    // deterine whether name service is active for the given domain
    function name_service_active() {
      if ($this->is_local($this->get_dns_fqdn())) {
        return TRUE;
      }
      // we can't use a class variable because a new instance of this class
      // is created with every record
      static $dns_lookup_cache = array();

      // if we're offline, provide a warning and fail closed 
      if(!red_single_ping_result('1.1.1.1')) {
        return false;
      }

      $hostname = $this->get_dns_zone(); 
      // check cache
      if(array_key_exists($hostname,$dns_lookup_cache)) return $dns_lookup_cache[$hostname];

      $assigned = $this->get_nameservers($hostname);
      $records = @dns_get_record($hostname,DNS_NS);
      if($records) {
        foreach($records as $k => $record) {
          if(in_array($record['target'],$assigned)) {
            // store for faster lookups
            $dns_lookup_cache[$hostname] = true;
            return true;
          }
        }
      }
      // store for faster lookups
      $dns_lookup_cache[$hostname] = false;
      return false;  
    }

  }
}

?>
