<?php

class red_hosting_order extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'hosting_order_id';
  var $_key_table = 'red_hosting_order';
  var $_red_item_notify_host = true; // whether to notify host or not when creating red items
  var $_red_item_notify_user = 'root'; // what user

  var $host_options;

  // This value is stored in the membership table.
  var $member_benefits_level = NULL;

  function get_red_construction_options() {
    return $this->_construction_options;
  }


  function get_member_benefits_level() {
    if(!is_null($this->member_benefits_level)) {
      return $this->member_benefits_level;
    }

    $member_id = intval($this->get_member_id());
    if(empty($member_id)) return NULL;

    $sql = "SELECT member_benefits_level FROM red_member WHERE member_id = $member_id";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    if(isset($row[0])) {
      $this->member_benefits_level = $row[0];
    }
    return $this->member_benefits_level;
  }

  function get_delete_confirmation_message() {
    return red_t("Are you sure you want to delete the hosting order: @identifier? All related user accounts, email address, email lists, etc. will be deleted.",array('@identifier' => $this->get_hosting_order_identifier()));
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_hosting_order_id());
    return $this->_get_initialize_sql($id);
  }
  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_hosting_order ".
      "WHERE hosting_order_id = " . $id;
    return $sql;
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_hosting_order_status('deleted');
  }
  function set_disable_flag() {
    parent::set_disable_flag();
    $this->set_hosting_order_status('disabled');
  }
  function unset_disable_flag() {
    parent::unset_disable_flag();
    $this->set_hosting_order_status('active');
  }
  /*
   * Helper functioning for creating related DNS red items
   */
  var $_host_ip_address;
  function set_host_ip_address() {
    $host = $this->get_hosting_order_host();
    // Work around for dev sites...
    if ($host == 'docker.local') {
      $this->_host_ip_address = '127.0.0.1';
      return true;
    }
    $dns = dns_get_record($host, DNS_A);
    if (count($dns) < 1) {
      $this->set_error(red_t("Could not determine IP address of @host.", array('@host' => $host)),'system');
      return false;
    }
    $this->_host_ip_address = $dns[0]['ip'];
    return true;
  }

  function get_host_ip_address() {
    return $this->_host_ip_address;
  }

  var $_hosting_order_id;
  function set_hosting_order_id($value) {
    $this->_hosting_order_id = $value;
  }

  function get_hosting_order_id() {
    return $this->_hosting_order_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }

  function get_member_id() {
    return $this->_member_id;
  }

  var $_unique_unix_group_id;
  function set_unique_unix_group_id($value) {
    $this->_unique_unix_group_id = $value;
  }

  function get_unique_unix_group_id() {
    return $this->_unique_unix_group_id;
  }

  var $_unique_unix_group_name;
  function get_unique_unix_group_name() {
    // when validating without committing, we may set the group name
    // to avoid validation errors (the identifier, for example, must
    // be group.mayfirst.org for individual accounts or it won't
    // validate). When actually commiting, the calling script should
    // not set the unique_unix_group_name
    if(empty($this->_unique_unix_group_name)) {
      $unix_group_id = $this->get_unique_unix_group_id();
      if(!empty($unix_group_id)) {
        $sql = "SELECT unique_unix_group_name FROM red_unique_unix_group ".
          "WHERE unique_unix_group_id = $unix_group_id";
        $result = $this->_sql_query($sql);
        $row = $this->_sql_fetch_row($result);
        $this->_unique_unix_group_name = $row[0];
      }
    }
    return $this->_unique_unix_group_name;
  }
  function set_unique_unix_group_name($value) {
    $this->_unique_unix_group_name = $value;
  }

  var $_hosting_order_name;
  function set_hosting_order_name($value) {
    $this->_hosting_order_name = $value;
  }

  function get_hosting_order_name() {
    return $this->_hosting_order_name;
  }


  var $_hosting_order_status;
  function set_hosting_order_status($value) {
    $this->_hosting_order_status = $value;
  }

  function get_hosting_order_status() {
    return $this->_hosting_order_status;
  }

  var $_hosting_order_host;
  function set_hosting_order_host($value) {
    $this->_hosting_order_host = $value;
  }

  function get_hosting_order_host() {
    return $this->_hosting_order_host;
  }

  var $_hosting_order_identifier;
  function get_hosting_order_identifier() {
    return $this->_hosting_order_identifier;
  }

  function set_hosting_order_identifier($value) {
    $this->_hosting_order_identifier = strtolower($value);
  }

  var $_hosting_order_notes;
  function set_hosting_order_notes($value) {
    $this->_hosting_order_notes = $value;
  }

  function get_hosting_order_notes() {
    return $this->_hosting_order_notes;
  }

  // constructor
  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    $hosting_order_id = $this->get_hosting_order_id();
    if(empty($hosting_order_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_hosting_order_status('active');
    }
    $this->_human_readable_description = red_t("Hosting orders are independent orders which can have their own web sites, user accounts, email lists, etc.");
    $this->_human_readable_name = red_t('Hosting orders');

  }


  function _set_datafields() {
    $this->_datafields = array(
      'hosting_order_id' => array(
        'fname' => red_t('Hosting Order id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_hosting_order',
        'req' => FALSE
      ),
      'member_id' => array(
        'fname' => red_t('Member id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => TRUE,
        'tblname' => 'red_hosting_order',
      ),
      'unique_unix_group_id' => array(
        'fname' => red_t('Login'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE,  // actually is required - but we check in additional_validation
        'tblname' => 'red_hosting_order',
      ),

      'hosting_order_host' => array(
        'fname' => red_t('Primary Server'),
        'type' => 'varchar',
        'pcre' => RED_DOMAIN_MATCHER,
        'pcre_explanation' => RED_DOMAIN_EXPLANATION,
        'req' => TRUE,
        'user_insert' => TRUE,
        'user_update' => FALSE,
        'user_visible' => TRUE,
        'tblname' => 'red_hosting_order',
      ),
      'hosting_order_identifier' => array(
        'type' => 'varchar',
        'pcre' => RED_DOMAIN_MATCHER,
        'pcre_explanation' => RED_DOMAIN_EXPLANATION,
        'fname' => red_t('Domain name'),
        'req' => TRUE,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => FALSE,
        'tblname' => 'red_hosting_order',
      ),
      'hosting_order_name' => array(
        'fname' => red_t('Description'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'req' => TRUE,
        'text_length' => 40,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_hosting_order',
      ),
      'hosting_order_notes' => array(
        'fname' => red_t('Notes'),
        'type' => 'text',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE,
        'tblname' => 'red_hosting_order',
      ),
      'hosting_order_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_HOSTING_ORDER_STATUS_MATCHER,
        'pcre_explanation' => RED_HOSTING_ORDER_STATUS_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE,
        'tblname' => 'red_hosting_order',
      )
    );
  }

  var $_notification_email;
  function get_notification_email() {
    return $this->_notification_email;
  }
  function set_notification_email($value) {
    $this->_notification_email = $value;
  }

  var $_notification_lang;
  function get_notification_lang() {
    if(empty($this->_notification_lang)) {
      return 'en_US';
    } else {
      return $this->_notification_lang;
    }
  }
  function set_notification_lang($value) {
    $this->_notification_lang = $value;
  }

  function get_edit_hosting_order_host() {
    $host = $this->get_hosting_order_host();
    if($this->exists_in_db()) return $host;
    $options = $this->get_host_options();
    return $this->_html_generator->get_select('sf_hosting_order_host',$options,$host);
  }

  function get_edit_block_table_elements() {
    $elements = parent::get_edit_block_table_elements();
    // pop off the last row
    $last = array_pop($elements);

    if(!$this->exists_in_db()) {
      // add a special field to ask for the user login name
      $row = array(0 => array(), 1 => array());
      $row[0]['value'] = red_t('Login name');
      $row[0]['attributes'] = array('class' => 'red-edit-field-label');
      $row[1]['value'] = $this->_html_generator->get_form_element('sf_login_name',$this->get_unique_unix_group_name(),'text');
      $elements[] = $row;
    }
    // optional email for sending welcome email
    $row = array(0 => array(), 1 => array());
    $row[0]['value'] = red_t('Email to send login<br /> info (optional)');
    $row[0]['attributes'] = array('class' => 'red-edit-field-label');
    $row[1]['value'] = $this->_html_generator->get_form_element('sf_notification_email',$this->get_notification_email(),'text');
    $elements[] = $row;

    // optional lang to send welcome email
    $default = 'en_US';
    $lang_select = $this->_html_generator->get_select('sf_notification_lang',array('en_US' => red_t('English'),'es_MX' => red_t('Spanish')),$default);

    $row = array(0 => array(), 1 => array());
    $row[0]['value'] = red_t('Language to send<br /> info (optional)');
    $row[0]['attributes'] = array('class' => 'red-edit-field-label');
    $row[1]['value'] = $lang_select;
    $elements[] = $row;

    // put the last element back on.
    $elements[] = $last;
    return $elements;
  }

  function get_host_options() {
    if(!is_null($this->host_options)) return $this->host_options;

    $ret = array();
    // allow access to servers they are currently on
    $sql = "SELECT hosting_order_host FROM red_hosting_order JOIN red_server
             ON red_hosting_order.hosting_order_host = red_server.server
             WHERE member_id = " . $this->get_member_id() .
      " AND hosting_order_status = 'active' and red_server.closed = 0";
    $result = $this->_sql_query($sql);

    while($row = $this->_sql_fetch_row($result)) {
      $server = $row[0];
      $ret[$server] = $server;
    }
    
    // and allow any accepting servers
    $sql = "SELECT server FROM red_server WHERE accepting = 1 AND closed = 0";
    $result = $this->_sql_query($sql);
    while($row = $this->_sql_fetch_row($result)) {
      $server = $row[0];
      $ret[$server] = $server;
    }
    $ret = array_unique($ret);
    $this->host_options = $ret;
    return $ret;
  }

  function additional_validation() {
    // make sure identifier does not start with a www
    if(substr($this->get_hosting_order_identifier(),0,3) == 'www') {
      $this->set_error(red_t("Please omit www in your domain name. Rather than entering www.name.org, simply write name.org."),'validation');
    }

    if(!$this->_delete) {
      if(!$this->hosting_order_identifier_unique()) {
        $this->set_error(red_t("Sorry, that domain name is already in use on our server."),'validation');
      }
    }

    $host_domain = strstr($this->get_hosting_order_host(), '.');
    $right_part_domain = substr($host_domain,1);
    // allow .local,  localhost and loc.cx (Jamie's personal domain) for testing
    if($right_part_domain != 'mayfirst.org' &&
      $right_part_domain != 'local' &&
      $right_part_domain != 'loc.cx' &&
      $this->get_hosting_order_host() != 'localhost') {
      $this->set_error(red_t("Host must end in mayfirst.org."),'validation');
    }

    if(!$this->exists_in_db()) {
      $group_name = $this->get_unique_unix_group_name();
      if(empty($group_name)) {
        $this->set_error(red_t("Please enter a login name."),'validation');
      }
      // make sure we can create unique_unix_group
      if($this->unique_unix_group_in_use()) {
        $this->set_error(red_t("User login is in use by another hosting order or member."),'validation');
      }
    } else {
      // only validate unix_group_id on non-insert actions. On insert, the group is created
      // later.
      $unix_id = $this->get_unique_unix_group_id();
      if(empty($unix_id)) {
        $this->set_error(red_t("Unix group id was left empty."),'validation');
      }
    }

    if(count($this->get_errors('validate')) != 0) return false;
    return true;
  }

  // special validation function - it's acceptable to use a unique unix group
  // that already exists, provided it's a group that is owned by the member
  // account. This kind of validation is not possible with the
  // unique_unix_group object
  function unique_unix_group_in_use() {
    // make sure it's not a unix group for another member
    $unix_group_name = addslashes($this->get_unique_unix_group_name());
    $member_id = intval($this->get_member_id());
    $sql = "SELECT unique_unix_group_id FROM red_unique_unix_group ".
      "LEFT JOIN red_member USING(unique_unix_group_id) ".
      "WHERE unique_unix_group_name = '$unix_group_name' AND ".
      "unique_unix_group_status = 'active' AND member_id != $member_id";
    $result =  $this->_sql_query($sql);
    if($this->_sql_num_rows($result) != 0) return true;

    // make sure it's not being used as a user_account
    $sql = "SELECT item_id FROM red_item_user_account ".
      "JOIN red_item USING(item_id) WHERE user_account_login ".
      "  = '$unix_group_name' AND red_item.item_status != ".
      "'deleted'";
    $result =  $this->_sql_query($sql);
    if($this->_sql_num_rows($result) != 0) return true;
    return false;
  }

  // check for a unique_unix_group_id for the set group_name
  // if one exists that is owned by the member account
  // return the unique_unix_group_id for that group name
  function get_unix_group_id_for_name() {
    $unix_group_name = addslashes($this->get_unique_unix_group_name());
    $member_id = intval($this->get_member_id());
    $sql = "SELECT unique_unix_group_id FROM red_unique_unix_group ".
      "JOIN red_member USING(unique_unix_group_id) ".
      "WHERE unique_unix_group_name = '$unix_group_name' AND ".
      "member_id = $member_id";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return $row[0];
  }

  function hosting_order_identifier_unique() {
    $hosting_order_identifier = $this->get_hosting_order_identifier();
    $id = $this->get_hosting_order_id();
    $sql = "SELECT hosting_order_id FROM red_hosting_order ".
      "WHERE hosting_order_identifier = '$hosting_order_identifier' ".
      "AND hosting_order_status != 'deleted' ";

    if(!empty($id)) $sql .= "AND hosting_order_id != " . intval($id);
    $result =  $this->_sql_query($sql);
    if($this->_sql_num_rows($result) == 0) return true;
    return false;
  }

  function _pre_commit_to_db() {
    // if deleting, first attempt to delete red items before we delete the hosting order
    if($this->_delete) {
      // delete any map user member records related to this hosting order
      if(!$this->delete_map_user_member_records()) {
        $this->set_error(red_t("Failed to delete related user member records. Error: @error.",array('@error' => $this->get_errors_as_string())),'system');
        return false;
      }

      // delete red items first
      if(!$this->handle_related_red_items('delete')) {
        $this->set_error(red_t("Failed to delete related red items. Error: @error", array('@error' => $this->get_errors_as_string())),'system');
        return false;
      }
      // then delete unix group (otherwise red items will fail to delete)
      $unix_group_id = intval($this->get_unique_unix_group_id());
      $hosting_order_id = $this->get_hosting_order_id();
      if(!red_unix_group_id_in_use($unix_group_id,$this->_sql_resource,null,$hosting_order_id)) {
        $sql = "SELECT  * FROM red_unique_unix_group WHERE unique_unix_group_id = $unix_group_id";
        require_once($this->_construction_options['src_path'] . '/modules/class.red.unique_unix_group.inc.php');
        if(!$this->delete_objects($sql,'red_unique_unix_group')) {
          $this->set_error(red_t("Failed to delete unix group."),'system');
          return false;
        }
      }

    } elseif($this->_disable) {
      // disable all related red items
      if(!$this->handle_related_red_items('disable')) {
        $this->set_error(red_t("Failed to disable related red items. Error: @error", array('@error' => $this->get_errors_as_string())),'system');
        return false;
      }
    } elseif(!$this->exists_in_db()) {
      $group_name = $this->get_unique_unix_group_name();
      if(!empty($group_name)) {
        // first, see if the the unique_unix_group already exists. If it does
        // don't try to re-create
        $id = $this->get_unix_group_id_for_name($this->get_unique_unix_group_name());
        if($id) {
          $this->set_unique_unix_group_id($id);
        } else {
          // no group exists, create it from scratch
          require_once($this->_construction_options['src_path'] .
            '/modules/class.red.unique_unix_group.inc.php');
          $values = array(
            'unique_unix_group_name' => $this->get_unique_unix_group_name(),
          );
          if(false === $obj = $this->create_related_object('red_unique_unix_group',$values)) return false;
          $this->set_unique_unix_group_id($obj->get_unique_unix_group_id());
        }
      }
    } elseif($this->exists_in_db()) {
      // ensure all disabled items are re-enabled
      if(!$this->handle_related_red_items('enable')) {
        $this->set_error(red_t("Failed to enable related red items. Error: @error", array('@error' => $this->get_errors_as_string())),'system');
        return false;
      }
    }
    return true;
  }

  function _post_commit_to_db() {
    if($this->_sql_action == 'insert') {
      // This object has just been added to the database
      // We need to add the hosting_order_id to the construction options
      // because our construction options will be used when creating
      // associated red items
      $this->_construction_options['hosting_order_id'] = $this->get_hosting_order_id();

      // unfortunately, we have to create the hosting order (to get the id) before
      // we can validate the red items
      if(!$this->auto_create_default_red_items($validate_only = true)) {
        $this->set_error(red_t("Your hosting order was created, but none of the server items were. You may want to delete this hosting order and try again."),'system');
        return false;
      }
      $ret = $this->auto_create_default_red_items();
      if(!$ret) return $ret;

      $ret = $this->create_user_member_access();
      if(!$ret) return $ret;

    }
    $email = $this->get_notification_email();
    if(!empty($email)) {
      // send welcome message
      if(!$ret = $this->send_welcome_email($email)) return $ret;
    }
    return true;
  }

  function delete_map_user_member_records() {
    require_once($this->_construction_options['src_path'] .
      '/modules/class.red.map_user_member.inc.php');

    $sql = "SELECT red_map_user_member.* FROM red_map_user_member ".
      "JOIN red_item_user_account ON red_map_user_member.login = ".
      "red_item_user_account.user_account_login JOIN red_item ".
      "USING(item_id) WHERE item_status = 'active' AND status = 'active' ".
      "AND red_item.hosting_order_id = " . $this->get_hosting_order_id();
    if(!$this->delete_objects($sql,'red_map_user_member')) {
      $this->set_error("Failed to delete related user member maps." . $this->get_errors_as_string(),'system');
      return false;
    }
    return true;
  }

  function create_user_member_access() {
    // if this is the first hosting order being created for this member,
    // then create a user member access record
    $sql = "SELECT count(hosting_order_id) FROM red_hosting_order WHERE ".
      "hosting_order_status = 'active' AND member_id = " . intval($this->get_member_id());
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    if($row[0] == 1) {
      require_once($this->_construction_options['src_path'] .
        '/modules/class.red.map_user_member.inc.php');
      $values = array(
        'member_id' => $this->get_member_id(),
        'login' => $this->get_unique_unix_group_name(),
      );
      $ret = $this->create_related_object('red_map_user_member',$values);
      if(!$ret) return $ret;
      return true;
    }
    return true;
  }
  function send_welcome_email($email) {
    require_once($this->_construction_options['src_path'] .
      '/modules/class.red.correspondence.inc.php');
    $values = array(
      'member_id' => $this->get_member_id(),
      'correspondence_subject' => $this->get_welcome_message_subject(),
      'correspondence_body' => $this->get_welcome_message_body(),
      'correspondence_to' => $email,
    );
    $ret = $this->create_related_object('red_correspondence',$values);
    if(!$ret) return $ret;
    return true;
  }

  function get_welcome_message_subject() {
    $path = $this->_construction_options['src_path'] . "/../email";
    $lang = $this->get_notification_lang();
    return cred_get_message_template($path,'welcome.subject', $lang);
  }

  function get_welcome_message_body() {
    $hash = $this->get_user_hash_info();
    $path = $this->_construction_options['src_path'] . "/../email";
    $lang = $this->get_notification_lang();
    $template = cred_get_message_template($path,'welcome.body', $lang);
    $array = array(
      '{user_name}' => $this->get_unique_unix_group_name(),
      '{member_friendly_name}' => $this->get_member_friendly_name(),
      '{identifier}' => $this->get_hosting_order_identifier(),
      '{temp_address}' => $this->get_unique_unix_group_name() . '.mayfirst.org',
      '{password_reset_url}' => $hash['password_reset_url'],
      '{password_reset_expire_date}' => $hash['password_reset_expire_date'],
      '{primary_host}' => $this->get_hosting_order_host(),
    );
    return str_replace(array_keys($array),$array,$template);
  }

  function get_member_friendly_name() {
    $sql = "SELECT member_friendly_name FROM red_member WHERE ".
      "member_id = " . intval($this->get_member_id());
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return $row[0];
  }

  function get_user_hash_info() {
    // get password reset hash
    require_once($this->_construction_options['src_path'] .
      '/modules/class.red_item_pass_reset_ui.inc.php');

    $co = $this->get_red_construction_options();
    $username = $this->get_unique_unix_group_name();
    $co['service_id'] = 19;
    $hosting_order_id = $this->get_hosting_order_id();
    $co['hosting_order_id'] = $hosting_order_id;
    $co['child_table'] = 'red_item_pass_reset';
    $pass_reset = new red_item_pass_reset_ui($co);
    $pass_reset->set_pass_reset_login($username);
    $pass_reset->commit_to_db();
    $password_reset_url = $pass_reset->get_hash_link();
    $password_reset_expire_date = $pass_reset->convert_expires_to_friendly_date();
    return array(
      'password_reset_url' => $password_reset_url,
      'password_reset_expire_date' => $password_reset_expire_date
    );
  }

  function auto_create_default_red_items($validate_only = false) {
    if(!$this->create_red_dns($validate_only)) return false;
    if(!$this->create_red_user_account($validate_only)) return false;
    if(!$this->create_red_server_access($validate_only)) return false;
    if(!$this->create_red_web_conf($validate_only)) return false;
    if(!$this->create_red_hosting_order_access($validate_only)) return false;
    $level = $this->get_member_benefits_level();
    if($level == 'basic') {
      // Basic memberships get email address by default.
      if(!$this->create_red_email_address($validate_only)) return false;
    }
    return true;
  }

  function create_red_email_address($validate_only = FALSE) {
    // Email address validation will fail if user account has not yet
    // been created.
    if($validate_only) return true;

    $co = $this->get_red_construction_options();
    $unix_group_name = $this->get_unique_unix_group_name();
    $co['service_id'] = 2;
    $email_address_item =& red_item::get_red_object($co);
    if(!$email_address_item) {
      $this->set_error(red_t("Failed to create email address object."),'system');
      return false;
    }
    $email_address_item->set_email_address_recipient($unix_group_name);
    $email_address_item->set_email_address("${unix_group_name}@${unix_group_name}.mayfirst.org");
    if(!$this->validate_red_item($email_address_item)) return false;
    if(!$this->commit_red_item($email_address_item)) return false;
    return true;
  }

  function create_red_dns($validate_only = false) {
    if(!$this->set_host_ip_address()) return false;
    $level = $this->get_member_benefits_level();

    $ip = $this->get_host_ip_address();
    $host = $this->get_hosting_order_host();
    $co = $this->get_red_construction_options();
    $fqdn = $this->get_hosting_order_identifier();
    $co['service_id'] = 9;
    $unix_group_name = $this->get_unique_unix_group_name();

    if($level == 'basic') {
      // Create only basic level items - just the mx record.
      if(!$this->commit_red_dns_item($co, 'mx', "${unix_group_name}.mayfirst.org", FALSE, $host, $validate_only)) return false;
    }
    else {
      // Create the standard items
      if(!$this->commit_red_dns_item($co,'a',$fqdn,$ip,false,$validate_only)) return false;
      if(!$this->commit_red_dns_item($co,'a',"www.$fqdn",$ip,false,$validate_only)) return false;
      if(!$this->commit_red_dns_item($co,'mx',$fqdn,false,$host,$validate_only)) return false;
    }
    return true;
  }

  function commit_red_dns_item($co,$type,$fqdn,$ip = false,$server = false,$validate_only = false) {
    $ttl = '3600';
    $dns_item =& red_item::get_red_object($co);
    $dns_item->set_dns_sshfp_algorithm(0);
    $dns_item->set_dns_sshfp_type(0);
    if(!$dns_item) {
      $this->set_error(red_t("Failed to create dns object."),'system');
      return false;
    }
    $dns_item->set_dns_type($type);
    $dns_item->set_dns_fqdn($fqdn);
    if($ip) $dns_item->set_dns_ip($ip);
    if($server) $dns_item->set_dns_server_name($server);
    $dns_item->set_dns_ttl($ttl);
    if(!$this->validate_red_item($dns_item)) return false;
    if($validate_only) return true;
    if(!$this->commit_red_item($dns_item)) return false;
    return true;
  }

  function create_red_user_account($validate_only = false) {
    $co = $this->get_red_construction_options();
    $unix_group_name = $this->get_unique_unix_group_name();
    $co['service_id'] = 1;
    $user_account_item =& red_item::get_red_object($co);
    if(!$user_account_item) {
      $this->set_error(red_t("Failed to create user account object."),'system');
      return false;
    }
    $user_account_item->set_user_account_login($unix_group_name);
    $pass = red_generate_random_password();
    if(FALSE === $pass) {
      $this->set_error(red_t("Failed to generate random password."),'system');
      return FALSE;
    }
    $user_account_item->set_user_account_password($pass);
    if(!$this->validate_red_item($user_account_item)) return false;
    if($validate_only) return true;
    if(!$this->commit_red_item($user_account_item)) return false;
    return true;
  }

  function create_red_server_access($validate_only = false) {
    // server access will fail validation if no related user exists,
    // so return true when validating always
    if($validate_only) return true;

    // Basic level access does not get server access.
    $level = $this->get_member_benefits_level();
    if($level == 'basic') return TRUE;

    $co = $this->get_red_construction_options();
    $unix_group_name = $this->get_unique_unix_group_name();
    $co['service_id'] = 3;
    $server_access_item =& red_item::get_red_object($co);
    if(!$server_access_item) {
      $this->set_error(red_t("Failed to create server access object."),'system');
      return false;
    }
    $server_access_item->set_server_access_login($unix_group_name);
    if(!$this->validate_red_item($server_access_item)) return false;

    if(!$this->commit_red_item($server_access_item)) return false;

    return true;
  }
  function create_red_web_conf($validate_only = false) {
    // web_conf will fail validation if no related user exists,
    // so return true when validating always
    if($validate_only) return true;

    // Basic level access does not get web sites.
    $level = $this->get_member_benefits_level();
    if($level == 'basic') return TRUE;

    $co = $this->get_red_construction_options();
    $unix_group_name = $this->get_unique_unix_group_name();
    $co['service_id'] = 7;
    $web_conf_item =& red_item::get_red_object($co);
    if(!$web_conf_item) {
      $this->set_error(red_t("Failed to create web conf object."),'system');
      return false;
    }
    $web_conf_item->set_web_conf_login($unix_group_name);
    $web_conf_item->set_web_conf_execute_as_user($unix_group_name);
    $web_conf_item->set_web_conf_domain_names($this->get_hosting_order_identifier() . ' www.' . $this->get_hosting_order_identifier());
    $web_conf_item->set_web_conf_tls(0);
    if(!$this->validate_red_item($web_conf_item)) return false;
    if(!$this->commit_red_item($web_conf_item)) return false;

    return true;
  }

  function validate_red_item(&$obj) {
    if(!$obj->validate()) {
      $errors = $obj->get_errors_as_string('validation');
      $this->set_error($errors,'validation');
      return false;
    }
    return true;
  }

  function commit_red_item(&$obj) {
    if(!$obj->commit_to_db()) {
      $errors = $obj->get_errors_as_string('system');
      $this->set_error($errors,'system');
      return false;
    }
    return true;
  }

  function create_red_hosting_order_access($validate_only = false) {
    $level = $this->get_member_benefits_level();
    if($level == 'basic') {
      // Basic memberships don't get to modify records except reset the password.
      return TRUE;
    }
    $co = $this->get_red_construction_options();
    $unix_group_name = $this->get_unique_unix_group_name();
    $co['service_id'] = 11;
    $hosting_order_access_item =& red_item::get_red_object($co);
    if(!$hosting_order_access_item) {
      $this->set_error(red_t("Failed to create hosting order access object."),'system');
      return false;
    }
    $hosting_order_access_item->set_hosting_order_access_login($unix_group_name);
    if(!$this->validate_red_item($hosting_order_access_item)) return false;
    if($validate_only) return true;
    if(!$this->commit_red_item($hosting_order_access_item)) return false;
    return true;
  }

  function set_user_input($post) {
    // first load the db values
    $key_field = $this->get_key_field();
    $submit_key_var = 'sf_' . $key_field;
    $this->set_field_value($key_field,$post[$submit_key_var]);
    $this->reset_to_db_values();
    foreach($post as $k => $v) {
      if(preg_match('/sf_(.*)$/',$k,$matches)) {
        $field = $matches[1];
        // This should not be set by the class - it's a
        // helper
        if($field == 'login_name') {
          // set the unique unix group - which will be created in _pre_commit_to_db
          $this->set_unique_unix_group_name($v);
        } elseif($field == 'notification_email') {
          // set the email address - which will be sent to in _post_commit
          $this->set_notification_email($v);
        } elseif($field == 'notification_lang') {
          $this->set_notification_lang($v);
        } else {
          if($this->accept_user_input($field))
            $this->set_field_value($field,$v);
        }
      }
    }
  }

  function handle_related_red_items($action) {
    // $action should be delete, disable or enable
    $co = $this->get_red_construction_options();
    if(empty($co)) {
      $this->set_error(red_t("Please call initialize_red_libraries function before deleting red items."),'system');
      return false;
    }
    $hosting_order_id = intval($this->get_hosting_order_id());
    if($action == 'delete' || $action == 'disable') {
      $item_status_where = "item_status != 'deleted' AND item_status != 'pending-delete'";
      $order = 'ASC';
    } elseif($action == 'enable') {
      $item_status_where = "item_status = 'disabled'";
      $order = 'DESC';
    }

    $sql = "SELECT red_item.item_id,red_item.service_id FROM red_item ".
      "INNER JOIN red_service USING(service_id) WHERE hosting_order_id = ".
      "$hosting_order_id AND $item_status_where " .
      "ORDER BY service_delete_order $order";
    $result = $this->_sql_query($sql);
    while($row = $this->_sql_fetch_row($result)) {
      $item_id = $row[0];
      $service_id = $row[1];
      $table = red_get_service_table_for_service_id($service_id,$this->_sql_resource);
      $sql = "SELECT * FROM red_item INNER JOIN $table ON red_item.item_id = $table.item_id WHERE red_item.item_id = $item_id";
      $item_result = $this->_sql_query($sql);
      $rs = $this->_sql_fetch_assoc($item_result);
      $co['rs'] =& $rs;
      $item =& red_item::get_red_object($co);
      if(!$item) {
        $this->set_error(red_t("Failed to initialize red object with item_id: @item_id.", array('@item_id' => $item_id)),'system');
        // this could be an orphaned item - so we continue
        continue;
      }
      $status = $item->get_item_status();
      // the item may have been deleted by another process (e.g. web app item deleting
      // related mysql database and user)
      if($status == 'deleted' || $status == 'pending-delete') continue;
      if($action == 'delete') {
        $item->set_delete_flag();
      } elseif ($action == 'disable') {
        if($status == 'disable' || $status == 'pending-disable') continue;
        $item->set_disable_flag();
      } elseif ($action == 'enable') {
        // noop - this should result in a "pending-update" status
        // which will result in the item being re-enabled
      } else {
        $this->set_error(red_t("Unknown handle_related_red_items action"),'system');
        return false;
      }
      // don't background the process, or we may return an error because of items that need to be deleted before other items
      // with the exception of DNS items which are slow to process and don't have any dependencies.
      if($item->get_service_id() == 9) {
        $item->set_notify_background_process(TRUE);
      }
      else {
        $item->set_notify_background_process(FALSE);
      }
      if(!$item->validate()) {
        $error = $item->get_errors_as_string();
        $this->set_error(red_t("Failed to validate red item with id: @item_id, error: @error.", array('@item_id' => $item_id, '@error' => $error)),'system');
        return false;
      }
      if(!$item->commit_to_db()) {
        $error = $item->get_errors_as_string() . ':' . $item->get_service_id();
        $this->set_error(red_t("Failed to commit red item. Error: @error.",array('@error' => $error)),'system');
        return false;
      }
    }
    return true;
  }

}
