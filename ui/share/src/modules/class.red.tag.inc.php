<?php

class red_tag extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'tag_id';
  var $_key_table = 'red_tag';

  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_tag_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_tag_status('active');
    }

  }

  function get_delete_confirmation_message() {
    return red_t("Are you sure you want to delete the tag: @tag?", array('@tag' => $this->get_tag()));
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_tag_id());
    return $this->_get_initialize_sql($id);
  }
  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_tag ". 
      "WHERE tag_id = " . $id;
    return $sql;
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_tag_status('deleted');
  }

  var $_tag_id;
  function set_tag_id($value) {
    $this->_tag_id = $value;
  }
  function get_tag_id() {
    return $this->_tag_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }
  function get_member_id() {
    return $this->_member_id;
  }

  var $_tag;
  function set_tag($value) {
    $this->_tag = $value;
  }
  function get_tag() {
    return $this->_tag;
  }


  var $_tag_status;
  function set_tag_status($value) {
    $this->_tag_status = $value;
  }
  function get_tag_status() {
    return $this->_tag_status;
  }

  var $_tag_modified;
  function set_tag_modified($value) {
    $this->_tag_modified = $value;
  }
  function get_tag_modified() {
    return $this->_tag_modified;
  }

  function _set_datafields() {
    $this->_datafields = array(
      'tag_id' => array(
        'fname' => red_t('Tag Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_tag',
        'req' => FALSE 
      ),
      'member_id' => array(
        'fname' => red_t('Member Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_tag',
        'req' => FALSE 
      ),
      'tag' => array(
        'fname' => red_t('Tag'),
        'type' => 'varchar',
        'pcre' => RED_TAG_MATCHER,
        'pcre_explanation' => RED_TAG_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_tag',
        'text_length' => 24,
        'req' => TRUE
      ),
      'tag_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_explanation' => RED_ACTIVE_DELETED_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE, 
        'tblname' => 'red_tag',
      ),
    );
  }
  
}
