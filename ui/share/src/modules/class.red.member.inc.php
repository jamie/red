<?php

class red_member extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'member_id';
  var $_key_table = 'red_member';
  var $member_parent_id_options = array();
  var $_javascript_includes = array('scripts/member.js');

  function get_delete_confirmation_message() {
    $attr = array('class' => 'red-message-variable');
    return "Are you sure you want to delete the member '" .
      $this->_html_generator->get_tag('span',$this->get_member_friendly_name(),$attr) .
      "'? All related hosting orders will be deleted.";
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_member_id());
    return $this->_get_initialize_sql($id);
  }
  function _get_initialize_sql($id) {
    $id = intval($id);
    return "SELECT * FROM red_member ".
      "WHERE member_id = " . $id;
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_member_status('inactive');
  }

  var $_unique_unix_group_id;
  function set_unique_unix_group_id($value) {
    $this->_unique_unix_group_id = $value;
  }

  function get_unique_unix_group_id() {
    return $this->_unique_unix_group_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }

  function get_member_id() {
    return $this->_member_id;
  }

  var $_member_parent_id;
  function set_member_parent_id($value) {
    $this->_member_parent_id = $value;
  }

  function get_member_parent_id() {
    return $this->_member_parent_id;
  }

  var $_member_friendly_name;
  function set_member_friendly_name($value) {
    $this->_member_friendly_name = $value;
  }

  function get_member_friendly_name() {
    return $this->_member_friendly_name;
  }

  var $_member_status;
  function set_member_status($value) {
    $this->_member_status = $value;
  }

  function get_member_status() {
    return $this->_member_status;
  }

  var $_member_benefits_level;
  function set_member_benefits_level($value) {
    $this->_member_benefits_level = $value;
  }
  function get_member_benefits_level() {
    return $this->_member_benefits_level;
  }

  var $_member_benefits_price;
  function set_member_benefits_price($value) {
    $this->_member_benefits_price = $value;
  }

  function get_member_benefits_price() {
    return $this->_member_benefits_price;
  }
  var $_member_term;
  function set_member_term($value) {
    $this->_member_term = $value;
  }

  function get_member_term() {
    return $this->_member_term;
  }

  var $_member_type;
  function set_member_type($value) {
    $this->_member_type = $value;
  }

  function get_member_type() {
    return $this->_member_type;
  }

  var $_member_start_date;
  function set_member_start_date($value) {
    // remove any time garbage that will cause the
    // validation to fail
    if(strlen($value) > 10)
      $value = substr($value,0,10);
    $this->_member_start_date = $value;
  }

  function get_member_start_date() {
    // set default value of today
    if(empty($this->_member_start_date))
      $this->_member_start_date = date('Y-m-d');

    return $this->_member_start_date;
  }
  var $_member_end_date;
  function set_member_end_date($value) {
    // remove any time garbage that will cause the
    // validation to fail
    if(strlen($value) > 10)
      $value = substr($value,0,10);
    $this->_member_end_date = $value;
  }

  function get_member_end_date() {
    return $this->_member_end_date;
  }

  var $_member_price;
  function set_member_price($value) {
    $this->_member_price = $value;
  }
  function get_member_price() {
    return $this->_member_price;
  }

  var $_member_currency;
  function set_member_currency($value) {
    $this->_member_currency = $value;
  }

  function get_member_currency() {
    return $this->_member_currency;
  }
  var $_member_quota;
  function set_member_quota($value) {
    $this->_member_quota = $value;
  }
  function get_member_quota() {
    return $this->_member_quota;
  }

  function _set_datafields() {
    $this->_datafields = array(
      'member_id' => array(
        'fname' => red_t('Member ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_member',
        'req' => FALSE
      ),
      'member_parent_id' => array(
        'fname' => red_t('Member Parent ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_member',
        'req' => FALSE
      ),
      'unique_unix_group_id' => array(
        'fname' => red_t('Unique Unix Group ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_member',
        'req' => false,  // actually it is required - check performed in additional_validation
      ),
      'member_friendly_name' => array(
        'fname' => red_t('Friendly Name'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_member',
        'text_length' => 50,
        'req' => TRUE
      ),
      'member_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_MEMBER_STATUS_MATCHER,
        'pcre_explanation' => RED_MEMBER_STATUS_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),
      'member_type' => array(
        'fname' => red_t('Type'),
        'type' => 'text',
        'pcre' => RED_MEMBER_TYPE_MATCHER,
        'pcre_explanation' => RED_MEMBER_TYPE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),
      'member_currency' => array(
        'fname' => red_t('Member Currency'),
        'type' => 'date',
        'pcre' => RED_CURRENCY_MATCHER,
        'pcre_explanation' => RED_CURRENCY_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),
      'member_benefits_level' => array(
        'fname' => red_t('Benefits Level'),
        'type' => 'text',
        'pcre' => RED_MEMBER_BENEFITS_LEVEL_MATCHER,
        'pcre_explanation' => RED_MEMBER_BENEFITS_LEVEL_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),
      'member_term' => array(
        'fname' => red_t('Term'),
        'type' => 'text',
        'pcre' => RED_MEMBER_TERM_MATCHER,
        'pcre_explanation' => RED_MEMBER_TERM_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),
      'member_price' => array(
        'fname' => red_t('Membership Dues Amount'),
        'type' => 'text',
        'pcre' => RED_INT_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'tblname' => 'red_member',
        'text_length' => 5,
      ),
      'member_benefits_price' => array(
        'fname' => red_t('Benefits Contribution Amount'),
        'type' => 'text',
        'pcre' => RED_INT_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'tblname' => 'red_member',
        'text_length' => 5,
      ),

      'member_start_date' => array(
        'fname' => red_t('Start Date'),
        'type' => 'date',
        'pcre' => RED_DATE_MATCHER,
        'pcre_explanation' => RED_DATE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),
      'member_end_date' => array(
        'fname' => red_t('End Date'),
        'type' => 'date',
        'pcre' => RED_DATE_MATCHER,
        'pcre_explanation' => RED_DATE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),
      'member_quota' => array(
        'fname' => red_t('Quota'),
        'type' => 'int',
        'pcre' => RED_BYTES_MATCHER,
        'pcre_explanation' => RED_BYTES_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),

    );

  }

  var $_unique_unix_group_name;
  function get_unique_unix_group_name() {
    // when validating without committing, we may set the group name
    // to avoid validation errors (the identifier, for example, must
    // be group.mayfirst.org for individual accounts or it won't
    // validate). When actually commiting, the calling script should
    // not set the unique_unix_group_name
    if(empty($this->_unique_unix_group_name)) {
      $unix_group_id = $this->get_unique_unix_group_id();
      if(empty($unix_group_id)) return false;
      $sql = "SELECT unique_unix_group_name FROM red_unique_unix_group ".
        "WHERE unique_unix_group_id = $unix_group_id";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      $this->_unique_unix_group_name = $row[0];
    }
    return $this->_unique_unix_group_name;
  }

  function set_unique_unix_group_name($value) {
    $this->_unique_unix_group_name = $value;
  }

  var $_contact_email;
  function get_contact_email() {
    return $this->_contact_email;
  }
  function set_contact_email($v) {
    $this->_contact_email = $v;
  }

  var $_contact_lang;
  function get_contact_lang() {
    return $this->_contact_lang;
  }
  function set_contact_lang($v) {
    $this->_contact_lang = $v;
  }

  var $_contact_first_name;
  function get_contact_first_name() {
    return $this->_contact_first_name;
  }
  function set_contact_first_name($v) {
    $this->_contact_first_name = $v;
  }

  var $_contact_last_name;
  function get_contact_last_name() {
    return $this->_contact_last_name;
  }
  function set_contact_last_name($v) {
    $this->_contact_last_name = $v;
  }

  function additional_validation() {
    if (!$this->_delete) {
      // Ensure we can parse the quota
      if (red_machine_readable_bytes($this->get_member_quota()) === FALSE) {
        $this->set_error(red_t("The member quota does not seem to be in a readable format."),'validation');
      }
      // Ensure the quota is not less then what is already allocated.
      $total = red_get_quota_for_all_items($this->_sql_resource, $this->get_member_id()); 
      if ($total > red_machine_readable_bytes($this->get_member_quota())) {
        $total_readable = red_human_readable_bytes($total);
        $this->set_error(red_t("The member quota you chose is less then the total quota already allocated. Please edit some of your resources to lower the quota allocated or ensure your member quota is over @total_readable.", [ '@total_readable' => $total_readable ]),'validation');
      }

    }

    if(!$this->member_friendly_name_unique()) {
      $error = red_t("The member name you have chosen is already in use.");
      $this->set_error($error,'validation');
    }

    if(!$this->exists_in_db()) {
      // if a unique_unix_group_id is being passed, make sure it exists
      $unix_id = $this->get_unique_unix_group_id();
      if(!empty($unix_id)) {
        $sql = "SELECT unique_unix_group_name FROM red_unique_unix_group ".
          "WHERE unique_unix_group_id = $unix_id";
        $result = $this->_sql_query($sql);
        if($this->_sql_num_rows($result) == 0){
          $this->set_error(red_t("No matching unique unix group found."),'validation');
        }
      } else {
        // otherwise, make sure we can create unique_unix_group
        require_once($this->_construction_options['src_path'] .
          '/modules/class.red.unique_unix_group.inc.php');
        $values = array(
          'unique_unix_group_name' => $this->get_unique_unix_group_name(),
        );
        // (this function sets its own errors)
        $this->create_related_object('red_unique_unix_group',$values,$validate_only = true);
      }

      // if we are passing in contact names to create a related contact,
      // make sure we have valid contact values
      $email = $this->get_contact_email();
      if(!empty($email)) {
        $first_name = $this->get_contact_first_name();
        $last_name = $this->get_contact_last_name();
        if(empty($first_name)) {
          $this->set_error(red_t("First name is required."),'validation');
        }
        if(empty($last_name)) {
          $this->set_error(red_t("Last name is required."),'validation');
        }
        if(!preg_match(RED_EMAIL_MATCHER,$email)) {
          $this->set_error(red_t("That didn't look like a valid email address."),'validation');
        }
      }
    } else {
      // only validate unix_group_id on non-insert actions. On insert, the group is created
      // later.
      $unix_id = $this->get_unique_unix_group_id();
      if(empty($unix_id)) {
        $this->set_error(red_t("Unix group id was left empty."),'validation');
      }
      if($this->_delete) {
        $member_id = $this->get_member_id();
        $sql = "SELECT invoice_id FROM red_invoice WHERE member_id = " .
          intval($member_id) . " " .
          "AND (invoice_status = 'unpaid' OR invoice_status = 'review')";
        $result = $this->_sql_query($sql);
        if(red_sql_num_rows($result) > 0) {
          $this->set_error(red_t("This member has unpaid invoices. Please deal with those before deleting."),'validation');
        }
        if($this->still_has_hosting_order()) {
          $this->set_error(red_t("This member has one or more hosting orders. Please deal with those before deleting."),'validation');
        }
      }
    }
    if(count($this->get_errors('validate')) != 0) return false;
    return true;
  }

  function member_friendly_name_unique() {
    // only use this rule if the member has not been submitted
    if(empty($this->_member_id)) {
      $member_friendly_name = addslashes($this->get_member_friendly_name());
      $sql = "SELECT member_id FROM red_member ".
        "WHERE member_friendly_name = '$member_friendly_name' AND member_status = 'active'";
      $result =  $this->_sql_query($sql);
      if($this->_sql_num_rows($result) != 0) {
        $row = $this->_sql_fetch_assoc($result);
        return false;
      }
    }
    return true;
  }

  // constructor
  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - set defaults
    if(empty($this->_member_id)) {
      $this->set_member_status('active');
      $this->set_member_term('year');
      $this->set_member_type('organization');
      $this->set_member_price(RED_USD_ORG_MEMBERSHIP_DUES);
      $this->set_member_benefits_price(RED_USD_ORG_BENEFITS_DUES);
      $this->set_member_parent_id(0);
    }
    if(empty($this->_member_parent_id))
      $this->set_member_parent_id(0);

  }

  function _pre_commit_to_db() {
    $this->set_member_quota(red_machine_readable_bytes($this->get_member_quota()));
    $co = $this->_construction_options;
    // if deleting, first attempt to delete hosting orders before we delete the member
    if($this->_delete) {
      // delete unix group
      $unix_group_id = intval($this->get_unique_unix_group_id());
      $member_id = intval($this->get_member_id());
      if(!red_unix_group_id_in_use($unix_group_id,$this->_sql_resource,$member_id)) {
        $unix_group_id = intval($this->get_unique_unix_group_id());
        $sql = "SELECT  * FROM red_unique_unix_group WHERE unique_unix_group_id = $unix_group_id";

        require_once($co['src_path'] . '/modules/class.red.unique_unix_group.inc.php');
        if(!$this->delete_objects($sql,'red_unique_unix_group')) {
          $this->set_error("Failed to delete unix group.",'system');
          return false;
        }
      }

      // delete maps
      require_once($co['src_path'] .
        '/modules/class.red.map_user_server.inc.php');
      $sql = "SELECT * FROM red_map_user_server ".
        "WHERE member_id = $member_id AND status != 'deleted'";
      if(!$this->delete_objects($sql,'red_map_user_server')) {
        $this->set_error("Failed to delete user server map.",'system');
        return false;
      }

      // delete contacts
      require_once($co['src_path'] .
        '/modules/class.red.contact.inc.php');
      $sql = "SELECT * FROM red_contact ".
        "WHERE member_id = $member_id AND contact_status != 'deleted'";
      if(!$this->delete_objects($sql,'red_contact')) {
        $this->set_error(red_t("Failed to delete contact."),'system');
        return false;
      }

      // Update member_end_date field
      $end_date = date('Y-m-d');
      $this->set_member_end_date($end_date);
    } elseif(!$this->exists_in_db()) {
      // create the unique_unix_group if we are inserting
      $unix_id = $this->get_unique_unix_group_id();
      if(empty($unix_id)) {
        require_once($co['src_path'] .
          '/modules/class.red.unique_unix_group.inc.php');
        $values = array(
          'unique_unix_group_name' => $this->get_unique_unix_group_name(),
        );
        if(false === $obj = $this->create_related_object('red_unique_unix_group',$values)) return false;
        $this->set_unique_unix_group_id($obj->get_unique_unix_group_id());
      }
    }
    return true;
  }

  function _post_commit_to_db() {
    if($this->_sql_action == 'insert') {
      // This object has just been added to the database
      // We need to add the member_id to the construction options
      // because our construction options will be used when creating
      // associated map_user_server record
      $this->_construction_options['member_id'] = $this->get_member_id();
      $email = $this->get_contact_email();
      if(!empty($email)) {
        // create a related contact record
        require_once($this->_construction_options['src_path'] .
          '/modules/class.red.contact.inc.php');
        $values = array(
          'member_id' => $this->get_member_id(),
          'contact_email' => $this->get_contact_email(),
          'contact_first_name' => $this->get_contact_first_name(),
          'contact_lang' => $this->get_contact_lang(),
          'contact_last_name' => $this->get_contact_last_name(),
          'contact_billing' => 'y',
        );
        $ret = $this->create_related_object('red_contact',$values);
        if(!$ret) return $ret;
      }

      // create invoice
      require_once($this->_construction_options['src_path'] .
        '/modules/class.red.invoice.inc.php');
      if($this->get_member_price() > 0) {
        $values = array(
          'invoice_type' => 'membership'
        );
        $invoice = $this->create_related_object('red_invoice',$values);
        if(!$invoice) return $invoice;
      }
      if($this->get_member_benefits_price() > 0) {
        $values = array(
          'invoice_type' => 'benefits'
        );
        $invoice = $this->create_related_object('red_invoice',$values);
        if(!$invoice) return $invoice;
      }
      if(!$this->send_billing_statement()) {
        red_set_message(red_t("Failed to send billing statement."), 'error');
      }
      return true;
    }
    return true;
  }

  function send_billing_statement() {
    require_once($this->_construction_options['src_path'] .
      '/modules/class.red.correspondence.inc.php');
    // $body is an array keyed to the language.
    $body = $this->get_billing_statement();
    if (empty($body)) {
      // If there is nothing due, return rather than send an empty message.
      return TRUE;
    }
    // $email is an indexed array of contacts with an address key and a lang key.
    $emails = $this->get_billing_email($this->get_member_id(), $this->_sql_resource);
    foreach($emails as $email) {
      $lang = $email['lang'];
      $subject = $this->get_email_template('invoice.subject', $lang);
      $values = array(
        'member_id' => $this->get_member_id(),
        'correspondence_subject' => $subject,
        'correspondence_body' => $body[$lang],
        'correspondence_to' => $email['address']
      );
      
      $ret = $this->create_related_object('red_correspondence',$values);
      if(!$ret) {
        red_set_message($this->get_errors_as_string(), 'error'); 
        return FALSE;
      }
    }
    return TRUE;
  }

  // Get the email body listing all outstanding invoices.
  function get_billing_statement() {
    // We will return the $ret array filled out with the text to send keyed to each language.
    $ret = array();

    // Get all invoices due 14 days from now or before. Our default is 14
    // days form now because we generate invoices 14 days before they are 
    // due and we want all invoices shown except ones that might be made
    // far in advance.
    $days = -14;
    $invoice_ids  = $this->get_past_due_invoices($this->get_member_id(), $this->_sql_resource, $days);
    $total_amount = array('USD' => 0, 'MXN' => 0);
    $total_amount_mxn = 0;
    $invoice_count = count($invoice_ids);
    $statement_body = array();
    $statement_footer = array();
    // Build the list of outstanding invoices.
    foreach($invoice_ids as $invoice_id) {
      $sql = "SELECT * FROM red_invoice WHERE invoice_id = " . intval($invoice_id);
      $result = $this->_sql_query($sql);
      $co = $this->_construction_options;
      if(array_key_exists('id', $co)) {
        unset($co['id']);
      }
      $co['rs'] = red_sql_fetch_assoc($result);
      require_once($this->_construction_options['src_path'] . '/modules/class.red.invoice.inc.php');
      $invoice = new red_invoice($co);
      $invoice->set_total_paid();
      if($invoice->get_invoice_currency() == 'USD') {
        $total_amount['USD'] = $total_amount['USD'] + $invoice->get_invoice_amount() - $invoice->total_paid;
      }
      else {
        $total_amount['MXN'] = $total_amount['MXN'] + $invoice->get_invoice_amount() - $invoice->total_paid;
      }
      $invoice->set_invoice_body();
      foreach ($invoice->invoice_body as $lang => $body) {
        // Append each invoice found for each language.
        if (!array_key_exists($lang, $statement_body)) {
          $statement_body[$lang] = $body;
        }
        else {
          $statement_body[$lang] .= $body;
        }
      }
      // The statement footer is identical for every invoice for the same
      // member, so grab it here and we will tack it on to the end of our
      // email below.
      if(empty($statement_footer)) {
        $invoice->set_invoice_footer();
        foreach ($invoice->invoice_footer as $lang => $footer) {
          $statement_footer[$lang] = $invoice->invoice_footer[$lang];
        }
      }
    }

    // Now create a grand total string to include at the top of the statement.

    // Most likely we just have a list of invoices in one currency. But
    // in theory there could be more than one. So we have to convert
    // our array of totals in each currency into a single line that 
    // reads something like one of these three options:
    // $50 USD
    // $805 MXN
    // $50 USD and $805 MXN
    $currencies = array('USD', 'MXN');

    // We start by eliminating any currencies for which we have no total.
    // And converting the existing totals from a number to a more friendly
    // string that includes the currency itself.
    foreach($currencies as $cur) {
      if($total_amount[$cur] == 0) {
        unset($total_amount[$cur]);
      }
      else {
        $total_amount[$cur] = '$' . $total_amount[$cur] . " $cur";
      }
    }
    
    // Use a language neutral "+" instead of and or y.
    $total_amount_string = implode(' + ', $total_amount);

    // Tack on a header to the top of the $statement_body and a footer to the bottom of it,
    // in each language, of course.
    reset($statement_body);
    foreach($statement_body as $lang => $body) {
      $header = $this->get_email_template('invoice.header', $lang);

      $active_currencies = array_keys($total_amount);

      $payment_instructions = NULL;
      foreach($active_currencies as $cur) { 
        $template = $this->get_email_template('payment.' . strtolower($cur), $lang);
        $payment_instructions .= str_replace('{member_id}', $this->get_member_id(), $template); 
      }
      $find = array(
        '{member_friendly_name}',
        '{total_amount}',
        '{invoice_count}',
        '{payment_instructions}',
        '{date_printed}'
      );
      $replace = array(
        $this->get_member_friendly_name(),
        $total_amount_string,
        $invoice_count,
        $payment_instructions,
        date("Y-m-d")
      );

      $header = str_replace($find, $replace, $header);
      $ret[$lang] = $header . $body . $statement_footer[$lang];

    }
    return $ret;
  }

  static function get_billing_email($member_id = NULL, $sql_resource = NULL) {
    // if(is_null($member_id)) $member_id = $this->get_member_id();
    if(is_null($member_id)) {
      $member_id = $this->get_member_id();
    }
    if(is_null($sql_resource)) $sql_resource = $this->_sql_resource;
    $emails = array();
    $sql = "SELECT contact_email, contact_lang FROM red_contact WHERE ".
      "member_id = " . intval($member_id) .  " " .
      "AND contact_billing = 'y' AND contact_status = 'active'";
    $result = red_sql_query($sql, $sql_resource);
    while($row = red_sql_fetch_row($result)) {
      $lang = red_validate_lang($row[1]);
      $emails[] = array('address' => $row[0], 'lang' => $lang);
    }
    return $emails;
  }

  function get_edit_member_parent_id() {
    return $this->_html_generator->get_select('sf_member_parent_id',$this->member_parent_id_options,$this->get_member_parent_id(),false,array(),false);
  }
  
  function get_edit_member_benefits_level() {
    return $this->_html_generator->get_select('sf_member_benefits_level',$this->get_member_benefits_level_options(),$this->get_member_benefits_level(), false);
  }
  function get_edit_member_term() {
    return $this->_html_generator->get_select('sf_member_term',$this->get_member_term_options(),$this->get_member_term());
  }
  function get_edit_member_type() {
    return $this->_html_generator->get_select('sf_member_type',$this->get_member_type_options(),$this->get_member_type(), false);
  }
  function get_edit_member_currency() {
    return $this->_html_generator->get_select('sf_member_currency',$this->get_member_currency_options(),$this->get_member_currency(), false);
  }
  function get_edit_member_quota() {
    // Convert to human readable before displaying.
    $value = red_human_readable_bytes($this->get_member_quota());
    return $this->_html_generator->get_input('sf_member_quota',$value);
  }
  function get_read_member_quota() {
    // Convert to human readable before displaying.
    $value = red_human_readable_bytes($this->get_member_quota());
    if ($value == 0) {
      return red_t("Not set");
    }
    return $value;
  }
  function get_member_benefits_level_options() {
    return array('basic' => red_t('Basic'), 'standard' => red_t('Standard'), 'extra' => red_t("Extra"));
  }
  function get_member_term_options() {
    return array('month' => red_t('Month'),'year' => red_t('Year'));
  }
  function get_member_type_options() {
    return array('individual' => red_t('Individual'),'organization' => red_t('Organization'));
  }
  function get_member_currency_options() {
    return array('USD' => red_t('US Dollars'),'MXN' => red_t('Mexican Pesos'));
  }
  function get_edit_block_table_elements() {
    $elements = parent::get_edit_block_table_elements();
    if(!$this->exists_in_db()) {
      // pop off the last row
      $last = array_pop($elements);
      // add a special field to ask for the user login name

      // first login
      $row = array(0 => array(), 1 => array());
      $row[0]['value'] = red_t('Group short name');
      $row[0]['attributes'] = array('class' => 'red-edit-field-label');
      $row[1]['value'] = $this->_html_generator->get_form_element('sf_group_name',$this->get_unique_unix_group_name(),'text');
      $elements[] = $row;

      // email address for related contact
      $row = array(0 => array(), 1 => array());
      $row[0]['value'] = red_t('Email address for contact');
      $row[0]['attributes'] = array('class' => 'red-edit-field-label');
      $row[1]['value'] = $this->_html_generator->get_form_element('sf_contact_email',$this->get_contact_email(),'text');
      $elements[] = $row;

      // first name for related contact
      $row = array(0 => array(), 1 => array());
      $row[0]['value'] = red_t('First name');
      $row[0]['attributes'] = array('class' => 'red-edit-field-label');
      $row[1]['value'] = $this->_html_generator->get_form_element('sf_contact_first_name',$this->get_contact_first_name(),'text');
      $elements[] = $row;

      // last name for related contact
      $row = array(0 => array(), 1 => array());
      $row[0]['value'] = red_t('Last name');
      $row[0]['attributes'] = array('class' => 'red-edit-field-label');
      $row[1]['value'] = $this->_html_generator->get_form_element('sf_contact_last_name',$this->get_contact_last_name(),'text');
      $elements[] = $row;

      // lang preference for related contact
      $default = $this->get_contact_lang();
      if(empty($default)) $default = 'en_US';
      $lang_select = $this->_html_generator->get_select('sf_contact_lang',array('en_US' => red_t('English'),'es_MX' => red_t('Spanish')),$default);

      $row = array(0 => array(), 1 => array());
      $row[0]['value'] = 'Language';
      $row[0]['attributes'] = array('class' => 'red-edit-field-label');
      $row[1]['value'] = $lang_select;
      $elements[] = $row;

      // put the last element back on.
      $elements[] = $last;
    }
    return $elements;
  }

  // designed to be called statically
  static function get_dues_responsible_member_id($member_id = null, $sql_resource = null) {
    if(is_null($member_id)) $member_id = $this->get_member_id();
    if(is_null($sql_resource)) $sql_resource = $this->_sql_resource;

    $member_id = intval($member_id);

    $sql = "SELECT member_price, member_parent_id FROM red_member WHERE ".
      "member_id = " . $member_id;
    $result = red_sql_query($sql,$sql_resource);
    $row = red_sql_fetch_row($result);

    // if a dues price is set, then they are responsible for their
    // own dues
    if(!empty($row[0])) return $member_id;

    // If not, and they have a parent id, the parent is responsible
    if(empty($row[0]) && !empty($row[1])) return $row[1];

    // otherwise, they are not responsible for paying dues,
    // simply return their own member id
    return $member_id;

  }

  /**
   * Check to see if this membership has any hosting orders
   * that may need to be deleted before removing the membership.
   **/
  function still_has_hosting_order() {
    $member_id = $this->get_member_id();
    $sql = "SELECT count(*) FROM red_hosting_order WHERE ".
      "member_id = " . intval($member_id) . " AND " .
      "(hosting_order_status = 'active' OR hosting_order_status = 'disabled')";

    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    if($row[0] > 0) return TRUE;
    return FALSE;
  }

  // Pass in member_id, sql_resource and days - days determines how many days delinquent
  // we should search for. Defaults to 14 days older or more. 
  static function get_past_due_invoices($member_id = null, $sql_resource = null, $days = 14) {
    if(is_null($member_id)) $member_id = $this->get_member_id();
    if(is_null($sql_resource)) $sql_resource = $this->_sql_resource;

    $responsible_member_id = red_member::get_dues_responsible_member_id($member_id,$sql_resource);
    $responsible_member_id = intval($responsible_member_id);

    // only show reminder if they are $days days behind payment
    $ts = time() - ($days * 86400);

    $today = date('Y-m-d',$ts);
    $sql = "SELECT invoice_id FROM red_invoice WHERE ".
      "member_id = " . $responsible_member_id . " AND invoice_status = 'unpaid' ".
      "AND invoice_date <= '$today'";

    $result = red_sql_query($sql,$sql_resource);
    $ret = array();
    if(red_sql_num_rows($result) == 0) {
      return $ret;
    }
    while($row = red_sql_fetch_row($result)) {
      $ret[] = $row[0];
    }
    return $ret;
  }

  // designed to be called statically
  // Determine if a member has past due invoices, and if so, are they
  // behind in their payment plan? Return false if they are not delinquent
  // and an array of delinquent invoice ids if they are delinquent.
  static function get_delinquent_invoices($member_id = NULL, $sql_resource = NULL, $co = NULL) {
    if(is_null($member_id)) $member_id = $this->get_member_id();
    if(is_null($sql_resource)) $sql_resource = $this->_sql_resource;
    if(is_null($co)) $co = $this->_construction_options;

    // if no unpaid invoices, then dues are defininetly not owed
    $invoice_ids = red_member::get_past_due_invoices($member_id, $sql_resource);
    if(false === $invoice_ids) return false;

    // if they have unpaid invoices, determine if the member is behind in their
    // payment plan. Return array of delinquent invoice_ids
    $ret = array();
    foreach($invoice_ids as $invoice_id) {
      $sql = "SELECT * FROM red_invoice WHERE invoice_id = " . intval($invoice_id);
      $result = red_sql_query($sql, $sql_resource);
      $co['rs'] = red_sql_fetch_assoc($result);
      $invoice = new red_invoice($co);
      if(!$invoice->is_delinquent()) continue;
      $ret[] = $invoice_id;
    }
    if(count($ret) == 0) return false;
    return $ret;
  }

  // designed to be called statically
  static function get_unpaid_explanation($member_id = null, $sql_resource = null) {
    if(is_null($member_id)) $member_id = $this->member_id;
    if(is_null($sql_resource)) $sql_resource = $this->_sql_resource;

    $responsible_member_id = red_member::get_dues_responsible_member_id($member_id,$sql_resource);
    $responsible_member_id = intval($responsible_member_id);

    $today = date('Y-m-d');
    $amount = 'unknown';
    $paid = 0;
    $owed = 0;
    $sql = "SELECT sum(payment_amount) FROM red_payment JOIN red_invoice ".
      "USING(invoice_id) WHERE invoice_status = 'unpaid' AND member_id = ".
      "$responsible_member_id AND payment_status = 'active'";
    $result = red_sql_query($sql,$sql_resource);
    $row = red_sql_fetch_row($result);
    if(!empty($row[0])) $paid = $row[0];

    $sql = "SELECT sum(invoice_amount) FROM red_invoice ".
      "WHERE invoice_status = 'unpaid' AND member_id = ".
      "$responsible_member_id AND invoice_date < '$today'";
    $result = red_sql_query($sql,$sql_resource);
    $row = red_sql_fetch_row($result);
    if(!empty($row[0])) $owed = $row[0];
    if(!empty($row[0])) $owed = $row[0];

    $due = number_format($owed - $paid,0);
    $contact_emails = red_member::get_all_billing_contact_emails($responsible_member_id,$sql_resource);
    $contact_emails_string = implode(',',$contact_emails);

    $contact_label = 'contact is';
    if(count($contact_emails) > 1) $contact_label = 'contacts are';

    if($member_id == $responsible_member_id) {
      return red_t('Your membership is $@due behind in membership dues. Your on-record billing @contact_label @contact_emails. If you believe there is an error, please contact members@mayfirst.org.', array('@due' => $due, '@contact_label' => $contact_label, '@contact_emails' => $contact_emails_string));
    } else {
      $sql = "SELECT member_friendly_name FROM red_member WHERE member_id = $responsible_member_id";
      $result = red_sql_query($sql,$sql_resource);
      $row = red_sql_fetch_row($result);
      $member_friendly_name = $row[0];
      return red_t('The member responsible for paying your membership dues, @member_friendly_name, owes $@due in membership dues. Their @contact_label @contact_emails. If you believe there is an error, please contact members@mayfirst.org.', array('@member_friendly_name' => $member_friendly_name, '@due' => $due, '@contact_label' => $contact_label, '@contact_emails' => $contact_emails));
    }
  }

  // designed to be called statically
  static function get_all_billing_contact_emails($member_id = null, $sql_resource = null) {
    if(is_null($member_id)) $member_id = $this->member_id;
    if(is_null($sql_resource)) $sql_resource = $this->_sql_resource;

    $member_id = intval($member_id);

    $emails = array();
    $sql = "SELECT contact_email FROM red_contact WHERE member_id = $member_id " .
      "AND contact_billing = 'y' AND contact_status = 'active'";
    $result = red_sql_query($sql,$sql_resource);
    while($row = red_sql_fetch_row($result)) {
      $emails[] = $row[0];
    }
    return $emails;
  }

  function set_user_input($post) {
    // first load the db values
    $key_field = $this->get_key_field();
    $submit_key_var = 'sf_' . $key_field;
    $this->set_field_value($key_field,$post[$submit_key_var]);
    $this->reset_to_db_values();
    foreach ($post as $k => $v) {
      if(preg_match('/sf_(.*)$/',$k,$matches)) {
        $field = $matches[1];
        // This should not be set by the class - it's a
        // helper
        if($field == 'group_name') {
          // set the unique unix group - which will be created in _pre_commit_to_db
          $this->set_unique_unix_group_name($v);
        } elseif($field == 'contact_email') {
          $this->set_contact_email($v);
        } elseif($field == 'contact_first_name') {
          $this->set_contact_first_name($v);
        } elseif($field == 'contact_lang') {
          $this->set_contact_lang($v);
        } elseif($field == 'contact_last_name') {
          $this->set_contact_last_name($v);
        } else {
          // normal fields
          if($this->accept_user_input($field))
            $this->set_field_value($field,$v);
        }
      }
    }
  }
}
