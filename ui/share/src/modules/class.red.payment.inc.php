<?php

class red_payment extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'payment_id';
  var $_key_table = 'red_payment';
  var $is_admin = false;

  
  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_payment_id)) {
      if(array_key_exists('invoice_id',$construction_options)) {
        $this->set_invoice_id($construction_options['invoice_id']);
      }
      // member id
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_payment_date(date('Y-m-d'));
      $this->set_payment_status('active');
    }
    $invoice_id = intval($this->get_invoice_id());
    if(empty($this->_member_id) && !empty($invoice_id)) {
      $sql = "SELECT member_id FROM red_invoice WHERE invoice_id = $invoice_id";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      $this->_member_id = $row[0];
    }
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_payment_status('deleted');
  }

  // used to generate list of available invoices
  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }
  function get_member_id() {
    return $this->_member_id;
  }


  function get_delete_confirmation_message() {
    $attr = array('class' => 'red-message-variable');
    return red_t("Are you sure you want to delete the payment: ") . $this->_html_generator->get_tag('span',$this->get_payment_identifier(),$attr) . "?";
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_payment_id());
    return $this->_get_initialize_sql($id);
  }
  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_payment ". 
      "WHERE payment_id = " . $id;
    return $sql;
  }

  var $_payment_id;
  function set_payment_id($value) {
    $this->_payment_id = $value;
  }

  function get_payment_id() {
    return $this->_payment_id;
  }

  var $_payment_status;
  function set_payment_status($value) {
    $this->_payment_status = $value;
  }

  function get_payment_status() {
    return $this->_payment_status;
  }
  var $_invoice_id;
  function set_invoice_id($value) {
    $this->_invoice_id = $value;
  }

  function get_invoice_id() {
    return $this->_invoice_id;
  }

  var $bank_id;
  function set_bank_id($value) {
    $this->bank_id = $value;
  }

  function get_bank_id() {
    return $this->bank_id;
  }


  var $_payment_date;
  function set_payment_date($value) {
    $this->_payment_date = $value;
  }

  function get_payment_date() {
    return $this->_payment_date;
  }

  var $_payment_method;
  function set_payment_method($value) {
    $this->_payment_method = $value;
  }

  function get_payment_method() {
    return $this->_payment_method;
  }

  var $_payment_identifier;
  function set_payment_identifier($value) {
    $this->_payment_identifier = $value;
  }

  function get_payment_identifier() {
    return $this->_payment_identifier;
  }

  var $_payment_amount;
  function set_payment_amount($value) {
    $this->_payment_amount = $value;
  }

  function get_payment_amount() {
    return $this->_payment_amount;
  }
  
  var $_payment_notes;
  function set_payment_notes($value) {
    $this->_payment_notes = $value;
  }

  function get_payment_notes() {
    return $this->_payment_notes;
  }

  function _set_datafields() {
    $this->_datafields = array(
      'payment_id' => array(
        'fname' => red_t('Payment ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_payment',
        'req' => FALSE 
      ),
      'invoice_id' => array(
        'fname' => red_t('Invoice ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_payment',
        'req' => FALSE 
      ),
      'bank_id' => array(
        'fname' => red_t('Bank ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_payment',
        'text_length' => 50,
        'req' => TRUE
      ),
      'payment_date' => array(
        'fname' => red_t('Payment Date'),
        'type' => 'datetime',
        'pcre' => RED_DATE_MATCHER,
        'pcre_explanation' => RED_DATE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_payment',
        'text_length' => 10,
        'req' => TRUE
      ),
      'payment_method' => array(
        'fname' => red_t('Method'),
        'type' => 'varchar',
        'pcre' => RED_PAYMENT_METHOD_MATCHER,
        'pcre_explanation' => RED_PAYMENT_METHOD_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_payment',
        'text_length' => 50,
        'req' => FALSE 
      ),
      'payment_identifier' => array(
        'fname' => red_t('Identifier'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_payment',
        'text_length' => 50,
        'req' => FALSE 
      ),
      'payment_amount' => array(
        'fname' => red_t('Amount'),
        'type' => 'decimal',
        'pcre' => RED_MONEY_MATCHER,
        'pcre_explanation' => RED_MONEY_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE, 
        'text_length' => 5,
        'tblname' => 'red_payment',
      ),
      'payment_notes' => array(
        'fname' => red_t('Notes'),
        'type' => 'textarea',
        'pcre' => RED_ANYTHING_MATCHER,
        'pcre_explanation' => RED_ANYTHING_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'text_length' => 50,
        'tblname' => 'red_payment',
      ),
      'payment_status' => array(
        'fname' => red_t('Status'),
        'type' => 'textarea',
        'pcre' => RED_STATUS_MATCHER,
        'pcre_explanation' => RED_STATUS_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE,
        'text_length' => 50,
        'tblname' => 'red_payment',
      ),
    );
  }

  function get_brief_payment_info_as_string() {
    return red_t('$@payment_amount deposited on @payment_date with payment identifier: @payment_identifier',array(
        '@payment_amount' => $this->get_payment_amount(), '@payment_date' => $this->get_payment_date(),'@payment_identifier' => $this->get_payment_identifier()));
  }
  function get_edit_payment_method() {
    return $this->_html_generator->get_select('sf_payment_method',$this->get_payment_method_options(),$this->get_payment_method());
  }
  static function get_payment_method_options() {
    return array(
      'credit' => red_t('Credit Card'),
      'check' => red_t('Check'),
      'transfer' => red_t('Bank Transfer'),
      'cash' => red_t('Cash'),
      'other' => red_t('Other')
    );

  }
  function get_edit_bank_id() {
    return $this->_html_generator->get_select('sf_bank_id',$this->get_bank_id_options(),$this->get_bank_id());
  }
  static function get_bank_id_options() {
    return array(
      1 => red_t('Citibank'),
      2 => red_t('Paypal'),
      3 => red_t('Network For Good'),
      4 => red_t('Banamex'),
      5 => red_t('Stripe'),
    );

  }
  function get_edit_invoice_id() {
    return $this->_html_generator->get_select('sf_invoice_id',$this->get_invoice_id_options(),$this->get_invoice_id());
  }

  function get_invoice_id_options() {
    $member_id = intval($this->get_member_id());
    $sql = "SELECT invoice_id, invoice_date, invoice_amount FROM ".
      "red_invoice WHERE member_id = $member_id ".
      "AND ";
    if(!$this->exists_in_db()) {
      $or = array("invoice_status = 'unpaid' OR invoice_status = 'review'");
    } else {
      $invoice_id = intval($this->get_invoice_id());
      $or = array(
        "invoice_status = 'unpaid'", 
        "invoice_id = $invoice_id",
      );
    }
    $sql .= '(' . implode(' OR ', $or) . ') ';
    $result = $this->_sql_query($sql);
    $ret = array();
    require_once($this->_construction_options['src_path'] . 
      '/modules/class.red.invoice.inc.php');

    $co = $this->_construction_options;
    unset($co['rs']);
    $co['service_id'] = 17;

    while($row = $this->_sql_fetch_row($result)) {
      $invoice_id = $row[0];
      $co['id'] = $invoice_id;
      $invoice = new red_invoice($co);

      // Get the amount owed 
      $invoice->set_total_paid();
      $remaining = $invoice->get_invoice_amount() - $invoice->total_paid;
      $display = substr($row[1],0,10) . " (#${invoice_id}, \$$remaining remaining)";
      $ret[$invoice_id] = $display;
    }
    return $ret;
  }
  function _pre_commit_to_db() {
    // figure out if we are deleting or reducing amount and need to adjust the
    // payment status of the related invoice 
    $invoice_id = $this->get_invoice_id();
    if(empty($invoice_id)) return true;

    require_once($this->_construction_options['src_path'] . 
      '/modules/class.red.invoice.inc.php');

    $co = $this->_construction_options;
    unset($co['rs']);
    $co['service_id'] = 17;

    // see if we're modifying the invoice id, which means we have
    // to adjust the previous invoice
    $old_invoice_id = null;
    if($this->exists_in_db()) {
      $payment_id = intval($this->get_payment_id());
      $sql = "SELECT invoice_id, payment_amount FROM red_payment WHERE payment_id = $payment_id";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      $old_invoice_id = intval($row[0]);
      if($old_invoice_id != $invoice_id) {
        $old_amount = $row[1];

        $co['id'] = $old_invoice_id;
        $invoice = new red_invoice($co);

        // Get the amount owed 
        $invoice->set_total_paid();
        $total_paid = $invoice->total_paid - $old_amount;
        $owed = $invoice->get_invoice_amount();
        $status = $invoice->get_invoice_status();
        if($owed > $total_paid && $status == 'paid') {
          red_set_message(red_t("Balance is now unpaid on previous invoice, setting invoice status to 'unpaid'"));
          $invoice->set_invoice_status('unpaid');
          $invoice->commit_to_db();
        }
      }
    }

    // now adjust the currently set invoice id
    $co['id'] = $invoice_id;
    $invoice = new red_invoice($co);

    // Get the amount owed 
    $invoice->set_total_paid();
    $owed = $invoice->get_invoice_amount() - $invoice->total_paid + $this->get_payment_amount();
    $status = $invoice->get_invoice_status();
    if($owed > 0 && $status == 'paid') {
      red_set_message(red_t("Balance is now unpaid, setting invoice status to 'unpaid'"));
      $invoice->set_invoice_status('unpaid');
      $invoice->commit_to_db();
    }
    return true;
  }

  function _post_commit_to_db() {
    // figure out if we have paid off the full amount of the invoice
    $invoice_id = $this->get_invoice_id();
    require_once($this->_construction_options['src_path'] . 
      '/modules/class.red.invoice.inc.php');

    $co = $this->_construction_options;
    unset($co['rs']);
    $co['service_id'] = 17;
    $co['id'] = $invoice_id;
    $invoice = new red_invoice($co);

    // Get the amount owed 
    $invoice->set_total_paid();
    $owed = $invoice->get_invoice_amount() - $invoice->total_paid;
    $status = $invoice->get_invoice_status();
    if($owed <= 0 && $status != 'paid') {
      red_set_message(red_t("Balance is paid, setting invoice status to 'paid'"));
      $invoice->set_invoice_status('paid');
      $invoice->commit_to_db();
    }
    return true;
  }

  function additional_validation() {
    if(!$this->is_admin) {
      $this->set_error(red_t("Sorry, only administrators can create/edit/delete payment records."),'validation');
    }
  }
}
