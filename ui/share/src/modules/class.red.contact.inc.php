<?php

class red_contact extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'contact_id';
  var $_key_table = 'red_contact';

  // how to auto-subscribe contacts to email lists
  var $_list_subscriptions = array();

  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_contact_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_contact_status('active');
    }
    $this->_list_subscriptions[]  = array(
      'login' => 'list@leslie.mayfirst.org',
      'listname' => 'service-advisories',
      'match' => '/tech/' 
    );
    $this->_list_subscriptions[]  = array(
      'login' => 'list@leslie.mayfirst.org',
      'listname' => 'lowdown',
      'match' => '/.*/' 
    );
    $this->_human_readable_description = red_t("Contacts are individuals related to a member");
    $this->_human_readable_name = red_t('Contacts');

  }

  function _pre_commit_to_db() {
    if(!$this->_handle_list_subscriptions()) {
      red_set_message(red_t("Something went wrong during list subscriptions, the email address entered ".
          "has not been properly subscribe/unsubscribed from the service lists."));
    }
    // return true even if list subscription fails to ensure record is saved.
    return true;
  }

  function _handle_list_subscriptions() {
    // We no longer use these mailman lists. This cruft should probably be pulled
    // out.
    return TRUE; 

    // If the config file specifies not to do list subscriptions
    // then do nothing
    if(defined('RED_LIST_DONT_SUBSCRIBE') && RED_LIST_DONT_SUBSCRIBE == 1) {
      return TRUE;
    }
    // if no subscriptions are configured, do nothing
    if(empty($this->_list_subscriptions)) return true;
  
    // if we are deleting a record, remove from all lists
    if($this->_delete) return $this->_unsubscribe_from_lists();

    if(!$this->exists_in_db()) {
      // new record, subscribe
      return $this->_subscribe_to_lists();
    } else {
      // updated record, check if email address is changing...
      $new_email = $this->get_contact_email();

      // get existing email
      $sql = "SELECT contact_email FROM red_contact WHERE " .
        "contact_id = " . intval($this->get_contact_id());
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      $old_email = $row[0];
      if($new_email == $old_email) {
        // no change, nothing to do
        return true;
      } else {
        if(!$this->_unsubscribe_from_lists($old_email)) return false;
        return $this->_subscribe_to_lists();
      }
    }
  }

  function _subscribe_to_lists() {
    $email = $this->get_contact_email();
    reset($this->_list_subscriptions);
    foreach($this->_list_subscriptions as $key => $value) {
      if(preg_match($value['match'],$this->get_contact_description())) {
        $list =  $value['listname'];
        $login = $value['login'];
        red_set_message(red_t("Subscribing @email to @list", array('@email' => $email, '@list' => $list)));
        if(!red_subscribe_email_to_list($list, $email, $login)) { 
          return false; 
        }
      }
    }
    return true;
  }

  function _unsubscribe_from_lists($email = null) {
    if(is_null($email)) {
      $email = $this->get_contact_email();
    }
    // unsubscribing from a list you don't belong to is harmless
    // so we may as well try to unsubscribe from all of them
    reset($this->_list_subscriptions);
    foreach($this->_list_subscriptions as $value) {
      $list =  $value['listname'];
      $login =  $value['login'];
      red_set_message(red_t("Unsubscribing @email from @list", array('@email' => $email, '@list' => $list)));
      if(!red_unsubscribe_email_from_list($list, $email, $login)) { 
        return false; 
      }
    }
    return true;
  }

  function get_delete_confirmation_message() {
    return red_t("Are you sure you want to delete the contact: @email?", array('@email' => $this->get_contact_email()));
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_contact_id());
    return $this->_get_initialize_sql($id);
  }
  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_contact ". 
      "WHERE contact_id = " . $id;
    return $sql;
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_contact_status('deleted');
  }

  var $_contact_id;
  function set_contact_id($value) {
    $this->_contact_id = $value;
  }

  function get_contact_id() {
    return $this->_contact_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }

  function get_member_id() {
    return $this->_member_id;
  }

  var $_contact_first_name;
  function set_contact_first_name($value) {
    $this->_contact_first_name = $value;
  }

  function get_contact_first_name() {
    return $this->_contact_first_name;
  }


  var $_contact_last_name;
  function set_contact_last_name($value) {
    $this->_contact_last_name = $value;
  }

  function get_contact_last_name() {
    return $this->_contact_last_name;
  }

  var $_contact_status;
  function set_contact_status($value) {
    $this->_contact_status = $value;
  }

  function get_contact_status() {
    return $this->_contact_status;
  }

  var $_contact_description;
  function set_contact_description($value) {
    $this->_contact_description = $value;
  }

  function get_contact_description() {
    return $this->_contact_description;
  }

  var $_contact_email;
  function set_contact_email($value) {
    $this->_contact_email = $value;
  }

  function get_contact_email() {
    return $this->_contact_email;
  }
  
  var $_contact_billing;
  function set_contact_billing($value) {
    $this->_contact_billing = $value;
  }

  function get_contact_billing() {
    return $this->_contact_billing;
  }

  var $_contact_lang;
  function set_contact_lang($value) {
    $this->_contact_lang = $value;
  }

  function get_contact_lang() {
    return $this->_contact_lang;
  }
  function _set_datafields() {
    $this->_datafields = array(
      'contact_id' => array(
        'fname' => red_t('Member ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_contact',
        'req' => FALSE 
      ),
      'member_id' => array(
        'fname' => red_t('Member Parent ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_contact',
        'req' => FALSE 
      ),
      'contact_first_name' => array(
        'fname' => red_t('First Name'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_contact',
        'text_length' => 50,
        'req' => TRUE
      ),
      'contact_last_name' => array(
        'fname' => red_t('Last Name'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_contact',
        'text_length' => 50,
        'req' => TRUE
      ),
      'contact_description' => array(
        'fname' => red_t('Description (billing, tech, etc.)'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_contact',
        'text_length' => 50,
        'req' => FALSE 
      ),
      'contact_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_explanation' => RED_ACTIVE_DELETED_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE, 
        'tblname' => 'red_contact',
      ),
      'contact_email' => array(
        'fname' => red_t('Email address'),
        'type' => 'text',
        'pcre' => RED_EMAIL_MATCHER,
        'pcre_explanation' => RED_EMAIL_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => TRUE,
        'text_length' => 50,
        'tblname' => 'red_contact',
      ),
      'contact_billing' => array(
        'fname' => red_t('Billing Contact?'),
        'type' => 'text',
        'pcre' => RED_YES_NO_MATCHER,
        'pcre_explanation' => RED_YES_NO_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE, 
        'tblname' => 'red_contact',
      ),
      'contact_lang' => array(
        'fname' => red_t('Preferred language for notices'),
        'type' => 'text',
        'pcre' => RED_INTERFACE_LANG_MATCHER,
        'pcre_explanation' => RED_INTERFACE_LANG_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE, 
        'tblname' => 'red_contact',
      ),
    );
  }
  function get_edit_contact_billing() {
    return $this->_html_generator->get_select('sf_contact_billing',array('y' => 'y','n' => 'n'),$this->get_contact_billing());
  }
  function get_edit_contact_lang() {
    $default = $this->get_contact_lang();
    if(empty($default)) $default = 'en_US';
    return $this->_html_generator->get_select('sf_contact_lang',array('en_US' => red_t('English'),'es_MX' => red_t('Spanish')),$default);
  }
}
