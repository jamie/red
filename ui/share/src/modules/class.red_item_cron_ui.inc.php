<?php

if(!class_exists('red_item_cron_ui')) {
  class red_item_cron_ui extends red_item_cron {

    var $_javascript_includes = array('scripts/item_cron.js');

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // Set language dependent properties
    }

    // return a custom edit field for logins that only
    // allows certain logins 
    function get_edit_cron_login() {
      if(!$this->exists_in_db()) {
        $login = $this->get_cron_login();

        $user_accounts = $this->get_related_user_account_logins();
        return $this->_html_generator->get_select('sf_cron_login',$user_accounts,$login,false);
      }
      else {
        return $this->get_cron_login();
      }
    }

    function get_edit_cron_schedule() {
      $input = $this->get_auto_constructed_edit_field('cron_schedule');  

      // create links for user to generate predefined schedules 
      $daily_attr = array(
        'href' => '#',
        'onClick' => "pass=generateCronSchedule('daily');return false;"
      );
      $hourly_attr = array(
        'href' => '#',
        'onClick' => "pass=generateCronSchedule('hourly');return false;"
      );
      $weekly_attr = array(
        'href' => '#',
        'onClick' => "pass=generateCronSchedule('weekly');return false;"
      );
      $links = "Auto fill schedule field: " . 
        $this->_html_generator->get_tag('a',red_t('Daily'),$daily_attr) . ' | ' . 
        $this->_html_generator->get_tag('a',red_t('Hourly'),$hourly_attr) . ' | ' . 
        $this->_html_generator->get_tag('a',red_t('Weekly'),$weekly_attr);
      // Create tag to display the password when the users clicks it

      $explanation_attr = array('class' => 'red-explanation', 'id' => 'red-help-hidden-fieldset-message');
      $format_block = $this->_html_generator->get_tag('blockquote',red_t('minute hour day-of-month month day-of-week'));
      $hourly_block = $this->_html_generator->get_tag('blockquote','34 * * * *');
      $daily_block = $this->_html_generator->get_tag('blockquote','15 4 * * *');
      $weekly_block = $this->_html_generator->get_tag('blockquote','47 2 * * sun');
      $monthly_block = $this->_html_generator->get_tag('blockquote','32 18 17 * *');
        
      $help_message = $this->_html_generator->get_tag('div',red_t("Please enter schedules in five space separated fields: !format_block An asterisk means always run at the specified interval. For example, once an hour at 34 minutes after the hour would be: !hourly_block Once a day at 4:15 am would be: !daily_block Every sunday at 2:47 am would be !weekly_block And, on the 17th of the month at 6:32 pm would be: !monthly_block", array('!format_block' => $format_block, '!hourly_block' => $hourly_block, '!weekly_block' => $weekly_block, '!monthly_block' => $monthly_block)),$explanation_attr) . '<br />';

      $help_attr = array('href' => '#', 'onClick' => "toggleVisibilityFieldset('red-help-hidden-fieldset','red-help-hidden-fieldset-message'); return false");
      $help_link = $this->_html_generator->get_tag('a','Help',$help_attr);
      $help_legend = $this->_html_generator->get_tag('legend',$help_link);
      $fieldset_attr = array('id' => 'red-help-hidden-fieldset');
      $help_fieldset = $this->_html_generator->get_tag('fieldset',$help_legend . $help_message, $fieldset_attr);
      return $input . $links . $help_fieldset;

    }
  }

}

?>
