<?php

if(!class_exists('red_item_mysql_db_ui')) {
  class red_item_mysql_db_ui extends red_item_mysql_db {
    var $is_admin = FALSE;
    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the MySQL database " . 
        $this->_html_generator->get_tag('span',$this->get_mysql_db_name(),$attr) . 
        "? All tables will be dropped.";
    }
    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }
    function additional_validation() {
      parent::additional_validation();
      // Only applies to non-admins.
      if (!$this->is_admin) {
        // Max user disk usage is 3gb. This ensures that anyone who needs more space
        // has to notify an admin, who can make sure there is enough space on the partition
        // to support it.
        $max_quota = 3 * 1024 * 1024 * 1024;
        $user_set_value = red_machine_readable_bytes($this->get_item_quota());
        if ($user_set_value > $max_quota) {
          $error_message = red_t('Only administrators can set a quota higher then 3GB. This restriction ensures we have enough disk space on the database server to accomodate your needs. Please contact support and we can help.');
          // They have exceeded the limit. This is going to throw a
          // validation error unless they are editing the record and
          // the have not changed the value that was stored in the db
          // by an admin on an earlier save.
          if ($this->exists_in_db()) {
            $item_id = intval($this->get_item_id());
            $sql = "SELECT item_quota FROM red_item WHERE item_id = $item_id";
            $result = $this->_sql_query($sql);
            $row = $this->_sql_fetch_row($result);
            // Only if they are different do we have a problem. 
            if($row[0] != $user_set_value) {
              $this->set_error($error_message, 'validation');
            }
          }
          else {
            // It's a new record, always throw the error.
            $this->set_error($error_message, 'validation');
          }
        }
      }
    }
  }  
}

?>
