<?php

class red_map_user_server extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'map_user_server_id';
  var $_key_table = 'red_map_user_server';
  var $member_parent_id_options = array();
  var $server_options = array();

  function get_red_construction_options() {
    return $this->_construction_options;
  }

  function get_delete_confirmation_message() {
    return red_t("Are you sure you want to delete this member/server access record?");
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_map_user_server_id());
    return $this->_get_initialize_sql($id);
  }
  
  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_map_user_server ". 
      "WHERE map_user_server_id = " . $id;
    return $sql;
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_status('deleted');
  }

  var $_map_user_server_id;
  function set_map_user_server_id($value) {
    $this->_map_user_server_id = $value;
  }

  function get_map_user_server_id() {
    return $this->_map_user_server_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }

  function get_member_id() {
    return $this->_member_id;
  }

  var $_server;
  function set_server($value) {
    $this->_server = $value;
  }

  function get_server() {
    return $this->_server;
  }

  var $_status;
  function get_status() {
    return $this->_status;
  }

  function set_status($value) {
    $this->_status = $value;
  }

  var $_login;
  function get_login() {
    return $this->_login;
  }

  function set_login($value) {
    $this->_login = $value;
  }

  // constructor
  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    if(!$this->exists_in_db()) 
      $this->set_status('active');
  }


  function _set_datafields() {
    $this->_datafields = array(
      'map_user_server_id' => array(
        'fname' => red_t('Map user server id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_map_user_server',
        'req' => FALSE 
      ),
      'member_id' => array(
        'fname' => red_t('Member id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => TRUE,
        'tblname' => 'red_map_user_server',
      ),
      'status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_explanation' => RED_ACTIVE_DELETED_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE, 
        'tblname' => 'red_map_user_server',
      ),
      'server' => array (
        'req' => true,
        'pcre'   => RED_DOMAIN_MATCHER,
        'pcre_explanation'   => RED_DOMAIN_EXPLANATION,
        'type'  => 'varchar',
        'fname'  => red_t('Server'),
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'user_visible' => TRUE,
        'input_type' => 'text',
        'text_length' => 20,
        'text_max_length' => 128,
        'tblname'   => 'red_map_user_server'),
      'login' => array (
        'req' => true,
        'pcre'   => RED_LOGIN_MATCHER,
        'pcre_explanation'   => RED_LOGIN_EXPLANATION,
        'type'  => 'varchar',
        'fname'  => red_t('Login name'),
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'user_visible' => TRUE,
        'input_type' => 'text',
        'text_length' => 20,
        'text_max_length' => 128,
        'tblname'   => 'red_map_user_server'),

    );
  }

  function get_edit_member_id() {
    return $this->_html_generator->get_select('sf_member_id',$this->member_parent_id_options,$this->get_member_id());
  }
  function get_edit_server() {
    return $this->_html_generator->get_select('sf_server',$this->server_options,$this->get_server());
  }


}
