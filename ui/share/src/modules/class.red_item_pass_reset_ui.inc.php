<?php

class red_item_pass_reset_ui extends red_item {

  var $_reset_base_url = 'https://members.mayfirst.org/rp';

  var $_pass_reset_login;
  var $_pass_reset_hash;
  var $_pass_reset_expires;
  var $_pass_reset = '0000-00-00 00:00:00';

  var $default_expires = 5;
  var $email;

  function __construct($construction_options,$hash = null, $login = null) {
    // Call our various elders' construction options
    parent::__construct($construction_options);
    $this->_set_child_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_item_id)) {
      // attempt to initialize from the hash or login
      if(!is_null($login)) {
        $this->set_pass_reset_login($login);
        $hash = $this->get_hash_for_login($login);
      }
      if(!empty($hash)) {
        $item_id = $this->get_item_id_for_hash($hash);
        $this->_initialize_from_id($item_id);
      } else {
        // set hash to random value
        $this->set_pass_reset_hash();
      }
    }
    
  }

  function set_email($value) {
    $this->email = $value;
  }

  function get_email() {
    return $this->email;
  }

  function set_pass_reset_login($value) {
    $this->_pass_reset_login = $value;
  }

  function get_pass_reset_login() {
    return $this->_pass_reset_login;
  }

  function get_pass_reset_hash() {
    return $this->_pass_reset_hash;
  }
  function set_pass_reset_hash($value = null) {
    if(is_null($value)) {
      // read 256 random bits from the kernel, which will be digested into a pretty(ish) md5sum.
      $random = fopen('/dev/urandom', 'r');
      if (FALSE === $random)
        trigger_error(red_t('Could not open /dev/urandom'), E_USER_ERROR);
      $data = fread($random, 32); 
      if (FALSE === $data)
        trigger_error(red_t('Could not read entropy from the kernel'), E_USER_ERROR);
      fclose($random);
      $value = md5($data);
    }
    $this->_pass_reset_hash = $value;
  }

  function get_pass_reset_expires() {
    return $this->_pass_reset_expires;
  }

  function set_pass_reset_expires($value) {
    $this->_pass_reset_expires = $value;
  }

  function get_pass_reset() {
    return $this->_pass_reset;
  }

  function set_pass_reset($value) {
    $this->_pass_reset = $value;
  }

  function get_reset_base_url() {
    return $this->_reset_base_url;
  }

  function set_reset_base_url($value) {
    $this->_reset_base_url = $value;
  }

  function _set_child_datafields() {
    $this->_datafields = array_merge( 
      $this->_datafields,
      array('pass_reset_login' => array(
          'fname' => red_t('Login to reset'),
          'type' => 'varchar',
          'pcre' => RED_LOGIN_MATCHER,
          'pcre_explanation' => RED_LOGIN_EXPLANATION,
          'user_visible' => TRUE,
          'user_insert' => TRUE,
          'user_update' => FALSE,
          'tblname' => 'red_item_pass_reset',
          'req' => TRUE
        ),
        'pass_reset_hash' => array(
          'fname' => red_t('Hash'),
          'type' => 'varchar',
          'pcre' => RED_TEXT_MATCHER,
          'pcre_explanation' => RED_TEXT_EXPLANATION,
          'user_visible' => TRUE,
          'user_insert' => FALSE,
          'user_update' => FALSE,
          'tblname' => 'red_item_pass_reset',
          'req' => TRUE
        ),
        'pass_reset_expires' => array(
          'fname' => red_t('Expires'),
          'type' => 'varchar',
          'pcre' => RED_DATETIME_MATCHER,
          'pcre_explanation' => RED_DATETIME_EXPLANATION,
          'user_visible' => TRUE,
          'user_insert' => FALSE,
          'user_update' => FALSE,
          'tblname' => 'red_item_pass_reset',
          'text_length' => 50,
          'req' => FALSE 
        ),
        'pass_reset' => array(
          'fname' => red_t('Reset'),
          'type' => 'varchar',
          'pcre' => RED_DATETIME_MATCHER,
          'pcre_explanation' => RED_DATETIME_EXPLANATION,
          'user_visible' => TRUE,
          'user_insert' => FALSE,
          'user_update' => FALSE,
          'tblname' => 'red_item_pass_reset',
          'text_length' => 50,
          'req' => FALSE 
        ),
           ));
  }

  function hash_exists_for_login($login) {
    if($this->get_hash_for_login($login) === false)
      return false;
    return true;
  }

  function get_hash_for_login($login) {
    $login = addslashes($login);
    $sql = "SELECT pass_reset_hash FROM red_item_pass_reset JOIN ".
      "red_item USING(item_id) WHERE pass_reset_login = '$login' ".
      "AND pass_reset = '0000-00-00 00:00:00' AND ".
      "pass_reset_expires > NOW() AND item_status = 'active'";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    if(empty($row[0])) return false;
    return $row[0];
  }

  function get_item_id_for_hash($hash) {
    $hash = addslashes($hash);
    $sql = "SELECT item_id FROM red_item_pass_reset JOIN red_item ".
      "USING(item_id) WHERE pass_reset_hash = '$hash' AND ".
      "item_status = 'active'";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return $row[0];
  }

  function additional_validation() {
    if(!$this->_delete && !$this->login_exists()) {
      $this->set_error(red_t("That login does not exist or is not part of your hosting order."),'validation');
    }

    $email = $this->get_email();
    if(!empty($email) && !preg_match(RED_EMAIL_MATCHER,$email)) {
      $this->set_error(red_t("That didn't look like a valid email address."),'validation');
    }
    $login = $this->get_pass_reset_login();
    // if we're a new record and a valid hash already exists for this login
    // return an error
    if(!$this->exists_in_db() && $this->hash_exists_for_login($login)) {
      $this->set_error(red_t("A current hash already exists for that login - please resend the existing ".
          "password reset hash rather than creating a new one."),'validation');
    }
    if(count($this->get_errors('validate')) != 0) return false;
    return true;
  }

  function login_exists() {
    $login = addslashes($this->get_pass_reset_login());
    $hosting_order_id = $this->get_hosting_order_id();
    $sql = "SELECT user_account_login FROM red_item_user_account ".
      "INNER JOIN red_item USING (item_id) ".
      "WHERE user_account_login = '$login' AND item_status != ".
      "'pending-delete' AND item_status != 'deleted' AND ".
      "red_item.hosting_order_id = $hosting_order_id";
    $result = $this->_sql_query($sql);
    if($this->_sql_num_rows($result) > 0) return true;
    return false;
  }

  function expires_in_past() {
    if($this->get_pass_reset_expires() <= time()) return true;
    return false;
  }

  function convert_days_from_now_to_expires($days_from_now) {
    $ts = time();
    $offset = 0;
    switch($days_from_now) {
      case '1 hour':
        $offset = 60 * 60;
        break;
      case '12 hours':
        $offset = 60 * 60 * 12;
        break;
      case '1 day':
        $offset = 60 * 60 * 24;
        break;
      case '3 days':
        $offset = 60 * 60 * 24 * 3;
        break;
      case '5 days':
        $offset = 60 * 60 * 24 * 5;
        break;
      case '7 days':
        $offset = 60 * 60 * 24 * 7;
        break;
    }
    $ts += $offset;
    return date("Y-m-d H:i:s",$ts);
  }
  
  function convert_expires_to_friendly_date() {
    $expires = strtotime($this->get_pass_reset_expires());
    return date("F j, Y \a\\t g:i a T",$expires);
  }
    
  function get_email_subject() {
    $lang = red_get_lang();
    return file_get_contents($this->_construction_options['src_path'] . '/../email/' . $lang . '/pass_reset.subject');
  }

  function get_hash_link() {
    return $this->get_reset_base_url() . '/?h=' .  $this->get_pass_reset_hash();
  }

  function get_email_body() {
    $lang = red_get_lang();
    $template = file_get_contents($this->_construction_options['src_path'] . '/../email/' . $lang . '/pass_reset.body');
    $find = array(  '{login}',
            '{reset_url}',
            '{expires_on}',
                 );

    $replace = array(
      $this->get_pass_reset_login(),
      $this->get_hash_link(),
      $this->convert_expires_to_friendly_date(),
    );

    return str_replace($find,$replace,$template);
  }
  
  // can accept emails as single email, comma separated list of emails
  // or array of emails  
  function send_hash($emails) {
    if(!is_array($emails)) {
      if(preg_match('/,/',$emails)) {
        $emails = explode(',',$emails);
      } else {
        $emails = array($emails);
      }
    }

    require_once($this->_construction_options['src_path'] . 
      '/modules/class.red.correspondence.inc.php');
    $values = array(
      'member_id' => $this->get_member_id(),
      'correspondence_subject' => $this->get_email_subject(), 
      'correspondence_body' => $this->get_email_body(),
    );
    foreach($emails as $email) {
      $values['correspondence_to'] = $email;
      $ret = $this->create_related_object('red_correspondence',$values);
      if(!$ret) return $ret;
    }
    return true;
  }

  function _post_commit_to_db() {
    $email = $this->get_email();
    if(!empty($email)) {
      // send hash
      $ret = $this->send_hash($email);
      if(!$ret) return $ret;    
    }
    return true;
  }

  function _pre_commit_to_db() {
    $ret = parent::_pre_commit_to_db();
    if(!$ret) return $ret;
    // we always reset the expires - so that the mere act
    // of re-saving an existing object will always
    // extend the expiration
    $expires = $this->convert_days_from_now_to_expires($this->default_expires);
    $this->set_pass_reset_expires($expires);
    return true;
  }

  function get_edit_block_table_elements() {
    $elements = parent::get_edit_block_table_elements();
    // pop off the last row
    $last = array_pop($elements);
    // add a special field to ask for an email address to send the hash to 
  
    $row = array(0 => array(), 1 => array());
    $row[0]['value'] = 'Optionally, email hash to';
    $row[0]['attributes'] = array('class' => 'red-edit-field-label');
    $row[1]['value'] = $this->_html_generator->get_form_element('sf_email',$this->get_email(),'text');
    $elements[] = $row;

    // put the last element back on.
    $elements[] = $last;
    return $elements;
  }

  function set_user_input($post) {
    // first load the db values
    $key_field = $this->get_key_field();
    $submit_key_var = 'sf_' . $key_field;
    $this->set_field_value($key_field,$post[$submit_key_var]);
    $this->reset_to_db_values();
    foreach($post as $k => $v) {
      if(preg_match('/sf_(.*)$/',$k,$matches)) {
        $field = $matches[1];
        // This should not be set by the class - it's a  
        // helper
        if($field == 'email') {
          $this->set_email($v);
        } else {
          // normal fields
          if($this->accept_user_input($field)) 
            $this->set_field_value($field,$v);
        }
      } 
    }
  }
}
