<?php

class red_postal_address extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'postal_address_id';
  var $_key_table = 'red_postal_address';

  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_postal_address_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_postal_address_status('active');
    }

  }

  function get_delete_confirmation_message() {
    return red_t("Are you sure you want to delete the postal address: @postal_address?", array('@postal_address' => $this->get_one_line_postal_address()));
  }

  /**
   * Combine the street number with street name.
   **/
  function get_street_address() {
    $name = $this->get_postal_address_street_name();
    // If there is no street name, the number is meaningless.
    if(empty($name)) return '';
    $number = $this->get_postal_address_street_number();
    if(empty($number)) return $name;
    return "$number $name";
  }

  /**
   * Helper function to provide a readable one line representation
   * of the entire address.
   **/
  function get_one_line_postal_address() {
    $fields = array('city','province','country','code');
    $parts = array();
    $address = $this->get_street_address();
    if(!empty($address)) {
      $parts[] = $address;
    }
    foreach($fields as $field) {
      $function = 'get_postal_address_' . $field;
      $value = $this->$function();
      if(!empty($value)) {
        $parts[] = $value;
      }
    }
    return implode(', ', $parts);
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_postal_address_id());
    return $this->_get_initialize_sql($id);
  }
  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_postal_address ". 
      "WHERE postal_address_id = " . $id;
    return $sql;
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_postal_address_status('deleted');
  }

  var $_postal_address_id;
  function set_postal_address_id($value) {
    $this->_postal_address_id = $value;
  }
  function get_postal_address_id() {
    return $this->_postal_address_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }
  function get_member_id() {
    return $this->_member_id;
  }

  var $_postal_address_street_number;
  function set_postal_address_street_number($value) {
    $this->_postal_address_street_number = $value;
  }
  function get_postal_address_street_number() {
    return $this->_postal_address_street_number;
  }

  var $_postal_address_street_name;
  function set_postal_address_street_name($value) {
    $this->_postal_address_street_name = $value;
  }
  function get_postal_address_street_name() {
    return $this->_postal_address_street_name;
  }
  
  var $_postal_address_street_extra;
  function set_postal_address_street_extra($value) {
    $this->_postal_address_street_extra = $value;
  }
  function get_postal_address_street_extra() {
    return $this->_postal_address_street_extra;
  }
  var $_postal_address_city;
  function set_postal_address_city($value) {
    $this->_postal_address_city = $value;
  }
  function get_postal_address_city() {
    return $this->_postal_address_city;
  }

  var $_postal_address_province;
  function set_postal_address_province($value) {
    $this->_postal_address_province = $value;
  }
  function get_postal_address_province() {
    return $this->_postal_address_province;
  }

  var $_postal_address_code;
  function set_postal_address_code($value) {
    $this->_postal_address_code = $value;
  }
  function get_postal_address_code() {
    return $this->_postal_address_code;
  }

  var $_postal_address_country;
  function set_postal_address_country($value) {
    $this->_postal_address_country = strtolower($value);
  }
  function get_postal_address_country() {
    return strtolower($this->_postal_address_country);
  }

  var $_postal_address_status;
  function set_postal_address_status($value) {
    $this->_postal_address_status = $value;
  }
  function get_postal_address_status() {
    return $this->_postal_address_status;
  }

  var $_postal_address_is_primary;
  function set_postal_address_is_primary($value) {
    $this->_postal_address_is_primary = $value;
  }
  function get_postal_address_is_primary() {
    return $this->_postal_address_is_primary;
  }

  function _set_datafields() {
    $this->_datafields = array(
      'postal_address_id' => array(
        'fname' => red_t('Phone Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_postal_address',
        'req' => FALSE 
      ),
      'member_id' => array(
        'fname' => red_t('Member Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_postal_address',
        'req' => FALSE 
      ),
      'postal_address_street_number' => array(
        'fname' => red_t('Street Number'),
        'type' => 'varchar',
        'pcre' => RED_STREET_NUMBER_MATCHER,
        'pcre_explanation' => RED_STREET_NUMBER_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_postal_address',
        'text_length' => 5,
        'req' => FALSE 
      ),
      'postal_address_street_name' => array(
        'fname' => red_t('Street Name'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_postal_address',
        'text_length' => 30,
        'req' => FALSE 
      ),
      'postal_address_street_extra' => array(
        'fname' => red_t('Extra info (apt number, etc)'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_postal_address',
        'text_length' => 10,
        'req' => FALSE 
      ),
      'postal_address_city' => array(
        'fname' => red_t('City'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_postal_address',
        'text_length' => 20,
        'req' => TRUE 
      ),
      'postal_address_province' => array(
        'fname' => red_t('State/Province'),
        'type' => 'varchar',
        'pcre' => RED_PROVINCE_MATCHER,
        'pcre_explanation' => RED_PROVINCE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_postal_address',
        'text_length' => 20,
        'req' => FALSE 
      ),
      'postal_address_code' => array(
        'fname' => red_t('Postal Code'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_postal_address',
        'text_length' => 10,
        'req' => FALSE 
      ),
      'postal_address_country' => array(
        'fname' => red_t('Country (2 characters)'),
        'type' => 'varchar',
        'pcre' => RED_COUNTRY_CODE_MATCHER,
        'pcre_explanation' => RED_COUNTRY_CODE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_postal_address',
        'text_length' => 2,
        'req' => TRUE 
      ),
      'postal_address_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_explanation' => RED_ACTIVE_DELETED_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE, 
        'tblname' => 'red_postal_address',
      ),
      'postal_address_is_primary' => array(
        'fname' => red_t('Primary Address'),
        'type' => 'varchar',
        'options' => array('y' => ''),
        'input_type' => 'checkbox',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE, 
        'tblname' => 'red_postal_address',
      ),
    );
  }

  function additional_validation() {
    $user_input_state = $this->get_postal_address_province();
    if(!empty($user_input_state) && $this->get_postal_address_country() == 'us') { 
      // Make sure US uses existing two character state abbreviations.
      $us_states = $this->get_us_states();
      $user_input_state = $this->get_postal_address_province();
      if(!in_array($user_input_state, $us_states)) {
        // Maybe a capitlization problem.
        if(in_array(strtoupper($user_input_state),$us_states)) {
          // Silently convert to upper case.
          $this->set_postal_address_province(strtoupper($user_input_state));
        } 
        else {
          // Maybe they spelled out the full state name?
          $user_input_state_ucwords = ucwords($user_input_state);
          if(array_key_exists($user_input_state_ucwords, $us_states)) {
            // Silently replace with two letter abbreviation.
            $this->set_postal_address_province($us_states[$user_input_state_ucwords]);
          }
          else {
            $this->set_error(red_t("For US, please use two letter USPS abbreviations for state/province."), 'validation');
          }
        }
      }
    }
  } 

  function get_us_states() {
    return array (
      'Alabama' => 'AL', 
      'Alaska' => 'AK', 
      'Arizona' => 'AZ', 
      'Arkansas' => 'AR', 
      'California' => 'CA', 
      'Colorado' => 'CO', 
      'Connecticut' => 'CT', 
      'Delaware' => 'DE',
      'District Of Columbia' => 'DC',
      'Florida' => 'FL',
      'Georgia' => 'GA',
      'Hawaii' => 'HI',
      'Idaho' => 'ID',
      'Illinois' => 'IL',
      'Indiana' => 'IN',
      'Iowa' => 'IA',
      'Kansas' => 'KS',
      'Kentucky' => 'KY',
      'Louisiana' => 'LA',
      'Maine' => 'ME',
      'Maryland' => 'MD',
      'Massachusetts' => 'MA',
      'Michigan' => 'MI',
      'Minnesota' => 'MN',
      'Mississippi' => 'MS',
      'Missouri' => 'MO',
      'Montana' => 'MT',
      'Nebraska' => 'NE',
      'Nevada' => 'NV',
      'New Hampshire' => 'NH',
      'New Jersey' => 'NJ',
      'New Mexico' => 'NM',
      'New York' => 'NY',
      'North Carolina' => 'NC',
      'North Dakota' => 'ND',
      'Ohio' => 'OH',
      'Oklahoma' => 'OK',
      'Oregon' => 'OR',
      'Pennsylvania' => 'PA',
      'Rhode Island' => 'RI',
      'South Carolina' => 'SC',
      'South Dakota' => 'SD',
      'Tennessee' => 'TN',
      'Texas' => 'TX',
      'Utah' => 'UT',
      'Vermont' => 'VT',
      'Virginia' => 'VA',
      'Washington' => 'WA',
      'West Virginia' => 'WV',
      'Wisconsin' => 'WI',
      'Wyoming' => 'WY'
    );
  }

  /**
   * If the is_primary checkbox is absent, set value
   * to 'n'
   **/
  function set_user_input($post) {
    $key_field = $this->get_key_field();
    $submit_key_var = 'sf_' . $key_field;
    $this->set_field_value($key_field,$post[$submit_key_var]);
    $this->reset_to_db_values();  

    $is_primary = false;
    foreach($post as $k => $v) {
      if(preg_match('/sf_(.*)$/',$k,$matches)) {
        $field = $matches[1];
        if($field == 'postal_address_is_primary') {
          $is_primary = true;
        }
        if($this->accept_user_input($field))
          $this->set_field_value($field,$v);
      }
      if($is_primary) {
        $this->set_postal_address_is_primary('y');
      } 
      else {
        $this->set_postal_address_is_primary('n');
      }
    }
  } 

  /**
   * Ensure only one address is primary
   **/
  function _post_commit_to_db() {
    if($this->get_postal_address_is_primary() == 'y') {
      $member_id = intval($this->get_member_id());
      $postal_address_id = intval($this->get_postal_address_id());
      $sql = "SELECT postal_address_id FROM red_postal_address WHERE ".
        "member_id = $member_id AND postal_address_id != $postal_address_id ".
        "AND postal_address_is_primary = 'y' AND postal_address_status = 'active'";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      if(!empty($row)) {
        $other_postal_address_id = intval($row[0]);
        $sql = "UPDATE red_postal_address SET postal_address_is_primary = 'n' ".
          "WHERE postal_address_id = $other_postal_address_id";
        $this->_sql_query($sql);
      }
    }
    return true;
  }
}
