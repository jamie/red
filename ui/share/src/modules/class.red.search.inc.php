<?php

// simple class for displaying search result items
// it's a "virtual" class because there is no database
// table that backs it up - all items must be created from
// existing recordsets
class red_search extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'search_id';
  var $_key_table = '';
  var $_html_generator;

  function __construct($co) {
    parent::__construct($co);
    $this->_set_datafields();
    if(array_key_exists('rs',$co)) {
      $this->set_type($co['rs']['type']);
      $this->set_display($co['rs']['display']);
      $this->set_view_link($co['rs']['view_link']);
      $this->set_edit_link($co['rs']['edit_link']);
      $this->set_delete_link($co['rs']['delete_link']);
    }

  }
  var $_search_id;
  function set_search_id($value) {
    $this->_search_id = $value;
  }

  function get_search_id() {
    return $this->_search_id;
  }

  var $_type;
  function set_type($value) {
    $this->_type = $value;
  }

  function get_type() {
    return $this->_type;
  }

  var $_display;
  function set_display($value) {
    $this->_display = $value;
  }

  function get_display() {
    return $this->_display;
  }

  var $_view_link;
  function set_view_link($value) {
    $this->_view_link = $value;
  }
  function get_view_link() {
    return $this->_view_link;
  }

  var $_edit_link;
  function set_edit_link($value) {
    $this->_edit_link = $value;
  }
  function get_edit_link() {
    return $this->_edit_link;
  }

  var $_delete_link;
  function set_delete_link($value) {
    $this->_delete_link = $value;
  }
  function get_delete_link() {
    return $this->_delete_link;
  }

  function _set_datafields() {
    $this->_datafields = array(
      // dummy field because it's required by ado
      'type' => array(
        'fname' => red_t('Search ID'),
        'type' => 'int',
        'pcre' => RED_INT_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => '',
        'req' => FALSE 
      ),
      'type' => array(
        'fname' => red_t('Type'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => '',
        'req' => FALSE 
      ),
      'display' => array(
        'fname' => red_t('Description'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => '',
        'req' => FALSE 
      ),
      'view_link' => array(
        'fname' => red_t('View'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => '',
        'text_length' => 50,
        'req' => TRUE
      ),
      'edit_link' => array(
        'fname' => red_t('Edit'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => '',
        'text_length' => 50,
        'req' => TRUE
      ),
      'delete_link' => array(
        'fname' => red_t('Delete'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => '',
        'text_length' => 50,
        'req' => TRUE
      ),
    );
  }

  function get_read_view_link() {
    $attr = array('href' => "?" . $this->get_view_link());
    return $this->_html_generator->get_tag('a','view',$attr);
  }
  function get_read_edit_link() {
    if(empty($this->_edit_link)) return '';
    $attr = array('href' => "?" . $this->get_edit_link());
    return $this->_html_generator->get_tag('a','edit',$attr);
  }
  function get_read_delete_link() {
    if(empty($this->_delete_link)) return '';
    $attr = array('href' => "?" . $this->get_delete_link());
    return $this->_html_generator->get_tag('a','delete',$attr);
  }
}
