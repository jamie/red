<?php

if(!class_exists('red_item_email_address_ui')) {

  class red_item_email_address_ui extends red_item_email_address {

    var $_javascript_includes = array('scripts/item_email_address.js');
    var $_list_subscriptions = array();

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      $this->_list_subscriptions[] = array(
        'login' => 'list@leslie.mayfirst.org',
        'listname' => 'lowdown',
        'description' => red_t('Subscribe this address to the Lowdown, our low-volume members newsletter'),
        'optin' => true,
      );
    }

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the email address " . $this->_html_generator->get_tag('span',$this->get_email_address(),$attr) . "?";
    }

    // return a custom edit field for email addresses that only
    // allows certain domain names
    function get_edit_email_address() {
      if($this->exists_in_db()) return $this->get_auto_constructed_edit_field('email_address');

      // display the proper form
      $domains = $this->get_email_domains();
      $domain_options = array();
      foreach($domains as $domain) {
        $domain_options[$domain] = $domain;
      }
      $username_value = $this->get_username_portion_of_address(); 
      $domain_value = $this->get_domain_portion_of_address(); 
      $attributes = array('size' => '12', 'maxlength' => '100');
      $user_input = $this->_html_generator->get_input('sf_email_address_username',$username_value,'text',$attributes);
      $domain_input = $this->_html_generator->get_select('sf_email_address_domain',$domain_options,$domain_value);
      return $user_input . '@' . $domain_input;
    }
      
    // customize to ensure spaces between username and email address
    function get_read_email_address_recipient() {
      $recipient = $this->get_email_address_recipient();
      $array = explode(',',$recipient);
      return implode(', ',$array);
    }

    // customize to add a mailto: link 
    function get_read_email_address() {
      $email = $this->get_email_address();
      return '<a href="mailto:' . $email . '">' . $email . '</a>'; 
    }
    // Custom email address recipient that allows either an existing
    // user account or a free form email address
    function get_edit_email_address_recipient() {
      $recipient = $this->get_email_address_recipient();

      // Create the check box giving the option to forward to another
      // email address

      // recipient type radio button 
      $attributes = array('onClick' => 'displayRecipientInputOnRecipientTypeChange(this);');
      $recipient_type = 'radio_user_account';
      if(preg_match('/\@/',$recipient)) $recipient_type = 'radio_email_address';

      // Create the list of user accounts select drop down
      $user_accounts = $this->get_related_user_account_logins();
      $span_attributes = array('class' => 'red_explanation');
      $div_attributes = array(
        'id' => 'recipient_user_account',
        'class' => 'red_email_recipient_input'
      );
      $recipient_select = $this->_html_generator->get_select('sf_email_address_recipient_user_account',$user_accounts,$recipient,false);
      $recipient_select_div = $this->_html_generator->get_tag('div',$recipient_select,$div_attributes);

      // Create the optional text box for entering a free form email
      // account
      $input_attributes = array('size' => '40', 'maxlength' => '255');
      $div_attributes = array(
        'id' => 'recipient_email_address',
        'class' => 'red_email_recipient_input'
      );
      $span_attributes = array('class' => 'red_explanation');
      $recipient_input = $this->_html_generator->get_input('sf_email_address_recipient_email_address',$recipient,'text',$input_attributes);
      $recipient_input_div = $this->_html_generator->get_tag('div',$recipient_input,$div_attributes);

      // Create the radio buttons
      $radio_options = array(
        'radio_user_account' => 
        red_t("Choose from an existing user account") . "<br>\n".
        $recipient_select_div,
        'radio_email_address' => 
        red_t("Enter one or more comma separated email addresses/user".
          "accounts that all mail should be forwarded to") . "\n".
        $recipient_input_div
      );
      $recipient_type_radio = $this->_html_generator->get_radio('sf_recipient_type',$radio_options,$recipient_type,$attributes);

      $javascript = '<script type="text/javascript">setRecipientInputOnLoad();</script>';

      return $recipient_type_radio . $javascript;
    }

    // override parent to insert a new row to hold the subscription stuff
    function set_user_input($post) {
      // first load the db values
      $this->set_item_id($post['sf_item_id']);
      $this->reset_to_db_values();
      foreach($post as $k => $v) {
        if(preg_match('/sf_(.*)$/',$k,$matches)) {
          $field = $matches[1];
          // This should not be set by the class - they are  
          // helpers
          if($field == 'email_address_domain') continue;
          if($field == 'email_address_recipient_user_account') continue;
          if($field == 'email_address_recipient_email_address') continue;

          // Build the address based on username and domain
          if($field == 'email_address_username') {
            $v = $post['sf_email_address_username'] . '@' . $post['sf_email_address_domain'];
            // Change the field name so it is properly set below
            $field = 'email_address';
          }
          if($field == 'recipient_type') {
            // This is the checkbox that tells us whether to use
            // the free form email address entered or the value 
            // from the drop down
            if($v == 'radio_email_address') {
              $recipient = $post['sf_email_address_recipient_email_address'];
                
            } else {
              $recipient = $post['sf_email_address_recipient_user_account'];
            }
            // properly set the field and value that should be set
            // below
            $field = 'email_address_recipient';
            $v = $recipient;
          }
          if($this->accept_user_input($field))
            $this->set_field_value($field,$v);
        }
      }
    }

    function _pre_commit_to_db() {
      if(!parent::_pre_commit_to_db()) return false;
      // if we are deleting a record, remove from all lists
      return true;
    }

    // override parent function
    // Add an explanation for the mx_warning_indicator
    function get_enumerate_header_block() {
      $mx_explanation = $this->get_mx_warning_explanation();
      //return $mx_explanation . parent::get_enumerate_header_block();
      // not working yet...
      return parent::get_enumerate_header_block();
    }

    function get_mx_warning_explanation() {
      return "<p class=\"red_explanation\"><a name=\"mx_warning\">" .
        red_t("An @mx_warning_indicator  next to an email address indicates that the domain name used ".
          "by the email address is not configured to have email delivered ".
          "to May First/People Link servers.",array('@mx_warning_indicator' => $this->get_mx_warning_indicator() )) . 
        "</a></p>";
    }
    // override parent function 
    // this function is called when the values for this item are being
    // enumerated. We want to add a check to see if the domain is
    // pointing to us and set a warning if it is not
    function get_enumerate_data_block() {
      $row = $this->get_enumerate_data_block_row();
      $email_address = $row['email_address']['value'];
      $domain = strstr($email_address,'@');
      $domain = substr($domain,1);
      if(red_single_ping_result('1.1.1.1') && !$this->mx_set_to_primary_host($domain)) {
        $row['email_address']['value'] = $row['email_address']['value'] . 
          $this->get_mx_warning_indicator();
      }
      return $this->_html_generator->get_table_cells($row);
    }

    function get_mx_warning_indicator() {
      // not working yet
      return '';
      return '<span class="red_mx_warning"><a href="#red_mx_warning">'.
        '*</a></span>';
    }

    function get_email_optin_checkbox($override_optin = null) {
      $ret = '';
      if(count($this->_list_subscriptions) > 0) {
        foreach($this->_list_subscriptions as $k => $v) {
          if(is_null($override_optin)) {
            $optin = $v['optin'];
          } else {
            $optin = $override_optin;
          }
          $div_attributes = array( 'class' => 'red_email_address_subscription');
          $option = array($v['listname'] => $v['description']);
          $selected = $optin ? array($v['listname']) : array();
          $sub_checkbox = $this->_html_generator->get_checkbox('sf_subscription_checkbox',$option,$selected, array());
          $ret .= $this->_html_generator->get_tag('div',$sub_checkbox,$div_attributes);
        }
      }
      return $ret;
    }
  }  
    
}



?>
