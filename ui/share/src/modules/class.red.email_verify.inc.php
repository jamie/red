<?php

/**
 * Email Verify
 *
 * Handles email verification by managing and confirming the hash.
 *
 **/

class red_email_verify extends red_ado {
  var $_key_field = 'email_verify_id';
  var $_key_table = 'red_email_verify';

  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
  }

  // Given an email and item id, see if we already have a verify record.
  // Retun a full recordset or NULL;
  static function get_existing($email, $item_id) {
    global $globals;
    $item_id = intval($item_id);
    $email = addslashes($email);
    $sql = "SELECT * FROM red_email_verify WHERE item_id = $item_id AND email_verify_address = '$email'";
    $result = red_sql_query($sql, $globals['sql_resource']);
    $row = red_sql_fetch_assoc($result);
    if (empty($row)) return NULL;
    return $row;

  }
  // Given hash, confirm it's authenticate and update or return FALSE;
  static function validate_hash($hash) {
    global $globals;
    $hash = addslashes($hash);
    $sql = "SELECT email_verify_id, email_verify_expire, email_verify_status FROM red_email_verify WHERE email_verify_hash = '$hash'";
    $result = red_sql_query($sql, $globals['sql_resource']);
    $row = red_sql_fetch_assoc($result);
    if (empty($row)) {
      red_set_message(red_t("Something went wrong with the email confirmation hash - it seems to have been changed."));  
      return FALSE;
    }
    if ($row['email_verify_status'] == 1) {
      // Already validated.
      red_set_message(red_t("This email address has already been verified."));
      return TRUE;
    }
    if ($row['email_verify_expire'] < date('Y-m-d H:i:s')) {
      // Expired.
      red_set_message(red_t("Sorry! The email verification has expired."));
      return FALSE;
    }
    // It must be valid, update.
    $co = $globals['standard_construction_options'];
    $co['id'] = $row['email_verify_id'];
    $email_verify = new red_email_verify($co);
    $email_verify->set_email_verify_status("1");
    if (!$email_verify->commit_to_db()) {
      red_set_message(red_t("Woops, something went wrong saving the verified email to the database."));
      return FALSE;
    }
    red_set_message(red_t("Your email address has been verified."));
    return TRUE;
  }

  // When creating a new record, auto generate the hash and expiration.
  function autogenerate() {
    // The auto generated password is base64 encoded, which includes three
    // characters (+, = and /) that, if sent via an URL, will be urldecoded
    // resulting in a different string. So... replace those three characters
    // with three other characters.
    $find = array('+','=', '/');
    $replace = array('.', '_', '-');
    $this->set_email_verify_hash(str_replace($find, $replace, red_generate_random_password(25)));
    $this->autogenerate_expiration();  
  }

  function autogenerate_expiration() {
    // Set expiration to 24 hours.
    $this->set_email_verify_expire(date('Y-m-d H:i:s', time() + 86400));
  }

  var $_email_verify_id;
  function set_email_verify_id($value) {
    $this->_email_verify_id = $value;
  }

  function get_email_verify_id() {
    return $this->_email_verify_id;
  }

  var $_item_id;
  function set_item_id($value) {
    $this->_item_id = $value;
  }

  function get_item_id() {
    return $this->_item_id;
  }

  var $_email_verify_address;
  function set_email_verify_address($value) {
    $this->_email_verify_address = $value;
  }

  function get_email_verify_address() {
    return $this->_email_verify_address;
  }

  var $_email_verify_hash;
  function set_email_verify_hash($value) {
    $this->_email_verify_hash = $value;
  }

  function get_email_verify_hash() {
    return $this->_email_verify_hash;
  }

  var $_email_verify_expire;
  function set_email_verify_expire($value) {
    $this->_email_verify_expire = $value;
  }

  function get_email_verify_expire() {
    return $this->_email_verify_expire;
  }

  var $_email_verify_status;
  function set_email_verify_status($value) {
    $this->_email_verify_status = $value;
  }

  function get_email_verify_status() {
    return $this->_email_verify_status;
  }

  function _set_datafields() {
    $this->_datafields = array(
      'email_verify_id' => array(
        'fname' => red_t('Email Verify ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_email_verify',
        'req' => FALSE 
      ),
      'item_id' => array(
        'fname' => red_t('User account Item ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_email_verify',
        'req' => TRUE 
      ),
      'email_verify_address' => array(
        'fname' => red_t('Email address'),
        'type' => 'text',
        'pcre' => RED_EMAIL_MATCHER,
        'pcre_explanation' => RED_EMAIL_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => TRUE,
        'text_length' => 64,
        'tblname' => 'red_email_verify',
      ),
      'email_verify_hash' => array(
        'fname' => red_t('Hash'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_email_verify',
        'text_length' => 32,
        'req' => FALSE 
      ),
      'email_verify_expire' => array(
        'fname' => red_t('Expiration'),
        'type' => 'datetime',
        'pcre' => RED_DATETIME_MATCHER,
        'pcre_explanation' => RED_DATETIME_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_email_verify',
        'req' => TRUE 
      ),
      'email_verify_status' => array(
        'fname' => red_t('Verified status'),
        'type' => 'text',
        'pcre' => RED_ZERO_ONE_MATCHER,
        'pcre_explanation' => RED_ZERO_ONE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE, 
        'tblname' => 'red_email_verify',
      ),
    );
  }
  function _get_select_sql_statement() {
    $id = intval($this->get_email_verify_id());
    return $this->_get_initialize_sql($id);
  }

  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_email_verify ". 
      "WHERE email_verify_id = " . $id;
    return $sql;
  }
}
