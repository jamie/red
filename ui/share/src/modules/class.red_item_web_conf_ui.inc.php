<?php

if(!class_exists('red_item_web_conf_ui')) {
  class red_item_web_conf_ui extends red_item_web_conf {
    var $_max_allowed_user_set_processes = 12;
    var $_max_allowed_user_set_memory = 512;
    var $is_admin = false;
    var $_javascript_includes = array('scripts/text_field_split.js','scripts/item_web_conf.js');

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);
      $this->_delete_confirmation_message = red_t('Are you sure you want to delete this web configuration? All web files and configurations will be deleted.');

    }

    // We do this manually so we can run it through htmlentities.
    function get_edit_web_conf_settings() {
      $settings_value = $this->get_htmlentities($this->get_web_conf_settings());
      $attr = array('cols' => '100','rows' => '10','wrap' => 'nowrap', 'class' => 'form-control');
      $settings_input = $this->_html_generator->get_form_element('sf_web_conf_settings',$settings_value,'textarea',$attr);
      return $settings_input;
    }

    function get_edit_web_conf_login() {
      if(!$this->exists_in_db()) {
        $login = $this->get_web_conf_login();

        $user_accounts = $this->get_related_user_account_logins();
        // $extra = array('none' => 'Do not assign a user');
        // $user_accounts = array_merge($extra,$user_accounts);
        $attr = array('class' => 'form-control');
        $login_select = $this->_html_generator->get_select('sf_web_conf_login', $user_accounts, $login, false, $attr);
        return $login_select;
      }
      else {
        return $this->get_web_conf_login();
      }
    }

    function get_edit_web_conf_php_version() {
      // By default, $this->_web_conf_php_version_options contains every
      // possible option. Depending on the servers' default option, and whether
      // or not the user is an admin, we pair them down.
      $server = addslashes($this->get_item_host());
      $default_php_version = $this->get_default_php_version_for_host($server);

      // The 5.6 should always be displayed for admins and only
      // displayed for non-admins if the site is already running it.
      if (!$this->is_admin && $selected_php_version != '5.6') {
        // Hide the 5.6 options.
        unset($this->_web_conf_php_version_options['5.6']);
      }

      // Notify users that we recommend whatever is the current default.
      $this->_web_conf_php_version_options[$default_php_version] = "${default_php_version} Recommended";

      // Reset the options.
      $this->_datafields['web_conf_php_version']['options'] = $this->_web_conf_php_version_options;
      return $this->get_auto_constructed_edit_field('web_conf_php_version');
    }

    function get_edit_web_conf_cache_settings() {
      // Only display for admins
      if ($this->is_admin) {
        return $this->get_auto_constructed_edit_field('web_conf_cache_settings');
      }
      else {
        return red_t("Only available to administrators");
      }
    }
    function get_edit_web_conf_mountpoint() {
      // Only display for admins
      if ($this->is_admin) {
        return $this->get_auto_constructed_edit_field('web_conf_mountpoint');
      }
      else {
        return red_t("Only available to administrators");
      }
    }
    function additional_validation() {
      parent::additional_validation();
      // both max_processes and max_memory have limits that regular users
      // are not allowed to exceed.
      $user_limited_resources = array('processes', 'memory');
      // Only applies to non-admins.
      if (!$this->is_admin) {
        $item_id = intval($this->get_item_id());
        foreach($user_limited_resources as $limited_resource) {
          $getter = 'get_web_conf_max_' . $limited_resource;
          $parameter = '_max_allowed_user_set_' . $limited_resource;
          $user_set_value = $this->$getter();
          $max_allowed = $this->$parameter;
          if ($user_set_value > $max_allowed) {
            // They have exceeded the limit. This is going to throw a
            // validation error unless they are editing the record and
            // the have not changed the value that was stored in the db
            // by an admin on an earlier save.
            if ($this->exists_in_db()) {
              $field = 'web_conf_max_' . $limited_resource;
              $sql = "SELECT $field FROM red_item_web_conf WHERE item_id = $item_id";
              $result = $this->_sql_query($sql);
              $row = $this->_sql_fetch_row($result);
              // If they are the same, then it's no problem.
              if($row[0] == $user_set_value) {
                continue;
              }
            }
            $this->set_error(red_t('Only administrators are allowed to set @resource greater than @max.',
              array('@max' => $this->$parameter, '@resource' => $limited_resource)), 'validation');
          }
        }
      }

      // Also, only admins are allowed to change the cache_settings and mountpoint parameters.
      if (!$this->is_admin) {
        $item_id = intval($this->get_item_id());
        $cache_settings = $this->get_web_conf_cache_settings();
        $mountpoint = $this->get_web_conf_mountpoint();
        if (!empty($cache_settings)) {
          // We are going to throw an error if the submitted value is not
          // the same as the value in the database.
          $throw_error = TRUE;
          if ($this->exists_in_db()) {
            $sql = "SELECT web_conf_cache_settings FROM red_item_web_conf WHERE item_id = $item_id";
            $result = $this->_sql_query($sql);
            $row = $this->_sql_fetch_row($result);
            // If they are the same, then it's no problem.
            if($row[0] == $cache_settings) {
              // You are saved, no error.
              $throw_error = FALSE;
            }
          }
          if ($throw_error) {
            $this->set_error(red_t('Only administrators are allowed to set cache settings.'), 'validation');
          }
        }
        if (!empty($mountpoint)) {
          // We are going to throw an error if the submitted value is not
          // the same as the value in the database.
          $throw_error = TRUE;
          if ($this->exists_in_db()) {
            $sql = "SELECT web_conf_mountpoint FROM red_item_web_conf WHERE item_id = $item_id";
            $result = $this->_sql_query($sql);
            $row = $this->_sql_fetch_row($result);
            // If they are the same, then it's no problem.
            if($row[0] == $mountpoint) {
              // You are saved, no error.
              $throw_error = FALSE;
            }
          }
          if ($throw_error) {
            $this->set_error(red_t('Only administrators are allowed to set mountpoint.'), 'validation');
          }
        }
      }
    }     
  }  
}



?>
