<?php

class red_planned_payment extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'planned_payment_id';
  var $_key_table = 'red_planned_payment';
  var $is_admin = false;

  // not saved with the record, only recorded so we know
  // what invoice options to provide the user with
  var $_member_id = null;
  function set_member_id($value) {
    $this->_member_id = $value;
  }

  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following field must be set
    if(empty($this->_planned_payment_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->_member_id = $construction_options['member_id'];
      }
      $this->set_planned_payment_status('active');
      // set default date to one month from now
      $this->set_planned_payment_date(date('Y-m-d',time() + 30 * 86400));
    } else {
      // set the member_id from the invoice_id
      $sql = "SELECT member_id FROM red_invoice WHERE ".
        "invoice_id = " . intval($this->get_invoice_id());
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      $this->set_member_id($row[0]);
    }

  }

  function get_delete_confirmation_message() {
    $attr = array('class' => 'red-message-variable');
    $identifier = $this->get_planned_payment_date() . ' for $' . $this->get_planned_payment_amount();
    return "Are you sure you want to delete the planned payment dated " . $this->_html_generator->get_tag('span',$identifier,$attr) . "?";
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_planned_payment_id());
    return $this->_get_initialize_sql($id);
  }
  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_planned_payment ". 
      "WHERE planned_payment_id = " . $id;
    return $sql;
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_planned_payment_status('deleted');
  }

  var $_planned_payment_id;
  function set_planned_payment_id($value) {
    $this->_planned_payment_id = $value;
  }

  function get_planned_payment_id() {
    return $this->_planned_payment_id;
  }

  var $_invoice_id;
  function set_invoice_id($value) {
    $this->_invoice_id = $value;
  }

  function get_invoice_id() {
    return $this->_invoice_id;
  }

  var $_planned_payment_amount;
  function set_planned_payment_amount($value) {
    $this->_planned_payment_amount = $value;
  }

  function get_planned_payment_amount() {
    return $this->_planned_payment_amount;
  }

  var $_planned_payment_date;
  function set_planned_payment_date($value) {
    $this->_planned_payment_date = $value;
  }

  function get_planned_payment_date() {
    return substr($this->_planned_payment_date,0,10);
  }

  var $_planned_payment_status;
  function set_planned_payment_status($value) {
    $this->_planned_payment_status = $value;
  }

  function get_planned_payment_status() {
    return $this->_planned_payment_status;
  }

  var $_planned_payment_description;
  function set_planned_payment_description($value) {
    $this->_planned_payment_description = $value;
  }

  function get_planned_payment_description() {
    return $this->_planned_payment_description;
  }

  var $_planned_payment_email;
  function set_planned_payment_email($value) {
    $this->_planned_payment_email = $value;
  }

  function get_planned_payment_email() {
    return $this->_planned_payment_email;
  }
  
  var $_planned_payment_billing;
  function set_planned_payment_billing($value) {
    $this->_planned_payment_billing = $value;
  }

  function get_planned_payment_billing() {
    return $this->_planned_payment_billing;
  }

  function _set_datafields() {
    $this->_datafields = array(
      'planned_payment_id' => array(
        'fname' => red_t('Planned payment ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_planned_payment',
        'req' => FALSE 
      ),
      'invoice_id' => array(
        'fname' => red_t('Invoice'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_planned_payment',
        'req' => TRUE 
      ),
      'planned_payment_amount' => array(
        'fname' => red_t('Amount'),
        'type' => 'decimal',
        'pcre' => RED_MONEY_MATCHER,
        'pcre_explanation' => RED_MONEY_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_planned_payment',
        'text_length' => 5,
        'req' => TRUE
      ),
      'planned_payment_date' => array(
        'fname' => red_t('Date'),
        'type' => 'date',
        'pcre' => RED_DATE_MATCHER,
        'pcre_explanation' => RED_DATE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_planned_payment',
        'text_length' => 10,
        'req' => TRUE
      ),
      'planned_payment_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_explanation' => RED_ACTIVE_DELETED_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE, 
        'tblname' => 'red_planned_payment',
      ),
    );
  }  

  function get_edit_invoice_id() {
    $selected = $this->get_invoice_id();
    return $this->_html_generator->get_select('sf_invoice_id',$this->get_invoice_options($selected),$selected);
  }

  function get_invoice_options($selected = null) {
    $ret = array();
    $additional_where = null;
    if(!is_null($selected)) {
      $additional_where = " OR invoice_id = " . intval($selected);
    }
    $sql = "SELECT invoice_id, invoice_date, invoice_amount FROM red_invoice ".
      "WHERE member_id = " . intval($this->_member_id). " AND ".
      "( invoice_status = 'unpaid' $additional_where ) ORDER BY invoice_date DESC";
    $result = $this->_sql_query($sql);
    while($row = $this->_sql_fetch_row($result)) {
      $invoice_id = $row[0];
      $invoice_date = substr($row[1], 0, 10);
      $invoice_amount = $row[2];
      
      $accounted_for = $this->get_payment_for_invoice($invoice_id) +
        $this->get_planned_payments_for_invoice($invoice_id, $this->get_planned_payment_id());

      $remaining = $invoice_amount - $accounted_for;
      if($remaining > 0 || $invoice_id == $this->get_invoice_id()) {
        $ret[$invoice_id] = '#' . $invoice_id . ', '. $invoice_date . ', $' . $invoice_amount . ', $' . $remaining . ' remaining';
      }
    }
    if(count($ret) == 0) {
      $ret[''] = red_t('No outstanding invoices');
    }
    return $ret;
  }

  function get_payment_for_invoice($invoice_id) {
    $sql = "SELECT SUM(payment_amount) FROM red_payment WHERE invoice_id = " . intval($invoice_id) .
      " AND payment_status = 'active'";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return empty($row[0]) ? 0 : $row[0];
  }

  function get_planned_payments_for_invoice($invoice_id, $exclude_planned_payment_id = null) {
    $sql = "SELECT SUM(planned_payment_amount) FROM red_planned_payment WHERE invoice_id = " . 
      intval($invoice_id) . " AND planned_payment_status = 'active'";
    if(!empty($exclude_planned_payment_id)) {
      $sql .= " AND planned_payment_id != " . intval($exclude_planned_payment_id);
    }
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return empty($row[0]) ? 0 : $row[0];
  }

  function get_invoice_amount($invoice_id) {
    $sql = "SELECT invoice_amount FROM red_invoice WHERE invoice_id = " . intval($invoice_id);
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return $row[0];
  }

  function get_invoice_date($invoice_id) {
    $sql = "SELECT invoice_date FROM red_invoice WHERE invoice_id = " . intval($invoice_id);
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return $row[0];
  }

  function additional_validation() {
    if($this->_delete) return;

    $invoice_id = $this->get_invoice_id();
    $invoice_amount = $this->get_invoice_amount($invoice_id);
    $invoice_date = $this->get_invoice_date($invoice_id);

    // ensure the planned payments don't exceed the invoice amount
    $exclude = $this->get_planned_payment_id();
    $total = $this->get_planned_payments_for_invoice($invoice_id, $exclude) +
      $this->get_planned_payment_amount();

    if($invoice_amount < $total) {
      $this->set_error(red_t("The sum of your planned payments is greater than the invoice amount."),'validation');
    }

    if(!$this->is_admin) {
      // only admins can set a payment plan more than a year after the invoice date
      $cut_off = date('Y-m-d',strtotime($invoice_date) + (86400 * 365));
      if($this->get_planned_payment_date() > $cut_off) {
        $this->set_error(red_t("You cannot create a planned payment more than one year after the invoice date. Please contact members@mayfirst.org for assistance."),'validation');
      }
    }

  }
}
