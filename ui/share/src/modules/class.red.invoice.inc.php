<?php

// arbirtrary start date for invoices during switch to new
// system
define('RED_INVOICE_START_DATE','2011-08-23');
// number of days an invoice can be late before action is taken to
// disable resources
define('RED_INVOICE_GRACE_DAYS','90');
// number of days a planned payment can be late before action istaken
// to disable resources
define('RED_PLANNED_PAYMENT_GRACE_DAYS','30');
// number of days in advance a planned payment notification goes out
define('RED_PLANNED_PAYMENT_ADVANCED_NOTICE_DAYS','7');


class red_invoice extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'invoice_id';
  var $_key_table = 'red_invoice';
  var $display_view_payments_link = false;
  var $_javascript_includes = array('scripts/invoice.js');
  // number of days an invoice can be outstanding before we turn it off
  // start date is an arbitrary date to start the new
  // 30/60/90 day rule on late invoices

  // variables to hold defaults from the invoice's parent
  // member records
  var $member_friendly_name;
  var $member_start_date;
  var $member_price;
  var $member_benefits_price;
  var $member_benefits_level;
  var $member_term;
  var $member_currency;
  var $billing_emails = null;

  // variables to hold calculated data
  var $total_paid = null;
  var $invoice_text = array();
  var $invoice_body = array();
  var $invoice_footer = array();
  var $reminder_text = array();
  var $cutoff_date = null;
  // store the date of the first planned behind
  // that is owed/behind in dues
  var $planned_payment_overdue_date = null;
  // store the date of the next upcoming planned payment
  var $planned_payment_upcoming_date = null;
  // what's is the total they are supposed to have paid
  // by the next planned payment
  var $planned_payment_upcoming_running_total = null;

  // for testing - change today's date to
  // another date, should be timestemp
  var $as_of_timestamp = null;

  // for emailing
  var $email_subject = array();

  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_invoice_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
    }

    // Set default values.
    $this->initialize();
   }

  function initialize() {
    // set values specific to our parent member record
    $this->set_member_defaults();

    // if we're new, set invoice defaults - note: this must happen after setting the
    // member id
    if(empty($this->_invoice_id) && !empty($this->_member_id)) {
      $this->auto_set_date();
      $this->set_invoice_defaults();
    }

    // initialize as of date
    $this->as_of_timestamp = time();
  }

  function set_member_defaults() {
    $sql = "SELECT member_price, member_term,member_start_date, member_friendly_name, member_currency, member_benefits_price, member_benefits_level ".
      "FROM red_member WHERE member_id = ".  intval($this->get_member_id());
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    $this->member_price = $row[0];
    $this->member_term = $row[1];
    $this->member_start_date = $row[2];
    $this->member_friendly_name = $row[3];
    $this->member_currency = $row[4];
    $this->member_benefits_price = $row[5];
    $this->member_benefits_level = $row[6];
  }

  function auto_set_date() {
    $date = $this->get_next_invoice_date();
    $this->set_invoice_date($date);
  }

  function set_invoice_defaults() {
    $this->set_invoice_status('unpaid');
    $this->set_account_id(1001);
    $invoice_type = $this->get_invoice_type();
    if ($invoice_type == 'benefits') {
      $this->set_invoice_amount($this->member_benefits_price);
    }
    else {
      $this->set_invoice_amount($this->member_price);
    }
    $desc = $this->get_description();
    $this->set_invoice_description($desc);
    $this->set_invoice_currency($this->member_currency);
  }

  function get_description() {
    $date = $this->get_invoice_date();
    $term = $this->member_term;
    $params = array(
      '@date1' => $date, 
      '@date2' => date('Y-m-d',strtotime("$date + 1 $term"))
    );;
    $invoice_type = $this->get_invoice_type();
    if($invoice_type == 'benefits') {
      return red_t("Membership benefits for the period @date1 through @date2", $params);
    }
    return red_t("Membership dues for the period @date1 through @date2", $params);
  }

  function get_next_invoice_date() {
    require_once($this->_construction_options['src_path'] .
      '/class.red_collector.inc.php');

    $collector = new red_collector($this->_sql_resource);
    $include_past = FALSE;
    $invoice_type = $this->get_invoice_type();
    $next = $collector->get_next_invoice_date($this->get_member_id(), $include_past, $invoice_type);
    return $next;
  }
  function get_delete_confirmation_message() {
    $attr = array('class' => 'red-message-variable');
    return red_t("Are you sure you want to delete the invoice ") . $this->_html_generator->get_tag('span',$this->get_invoice_description(),$attr) . "?";
  }
  function _get_select_sql_statement() {
    $id = intval($this->get_invoice_id());
    return $this->_get_initialize_sql($id);
  }

  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_invoice ".
      "WHERE invoice_id = " . $id;
    return $sql;
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_invoice_status('void');
  }

  var $_invoice_id;
  function set_invoice_id($value) {
    $this->_invoice_id = $value;
  }

  function get_invoice_id() {
    return $this->_invoice_id;
  }

  var $_account_id;
  function set_account_id($value) {
    $this->_account_id = $value;
  }

  function get_account_id() {
    return $this->_account_id;
  }

  var $_invoice_date;
  function set_invoice_date($value) {
    $this->_invoice_date = $value;
  }

  function get_invoice_date() {
    return substr($this->_invoice_date,0,10);
  }

  var $_invoice_amount;
  function get_invoice_amount() {
    return $this->_invoice_amount;
  }
  function set_invoice_amount($value) {
    $this->_invoice_amount = $value;
  }

  var $_invoice_status;
  function set_invoice_status($value) {
    $this->_invoice_status = $value;
  }

  function get_invoice_status() {
    return $this->_invoice_status;
  }

  var $_invoice_description;
  function set_invoice_description($value) {
    $this->_invoice_description = $value;
  }

  function get_invoice_description() {
    return $this->_invoice_description;
  }

  var $_invoice_private_notes;
  function set_invoice_private_notes($value) {
    $this->_invoice_private_notes = $value;
  }

  function get_invoice_private_notes() {
    return $this->_invoice_private_notes;
  }

  var $_invoice_date_modified;
  function set_invoice_date_modified($value) {
    // This should always return the current date/time.
    $this->_invoice_date_modified = date("Y-m-d H:i:s");
  }
  function get_invoice_date_modified() {
    return $this->_invoice_date_modified;
  }

  var $_owed;
  function set_owed($value) {
    $this->_owed = $value;
  }

  function get_owed() {
    return $this->_owed;
  }

  var $_total_payment;
  function set_total_payment() {
    if(!empty($this->_invoice_id)) {
      $sql = "SELECT sum(payment_amount) FROM payment WHERE invoice_id = ".
        $this->get_invoice_id();
    }
    $this->_db->QueryField($sql,$total_payment);
    $this->_total_payment = $total_payment;
  }

  function get_total_payment() {
    return $this->_total_payment;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }

  function get_member_id() {
    return $this->_member_id;
  }

  var $_invoice_currency;
  function set_invoice_currency($value) {
    $this->_invoice_currency = $value;
  }

  function get_invoice_currency() {
    return $this->_invoice_currency;
  }

  var $_invoice_type;
  function set_invoice_type($value) {
    $this->_invoice_type = $value;
  }
  function get_invoice_type() {
    $invoice_type = $this->_invoice_type;
    if(!empty($invoice_type)) return $invoice_type;
    // Default to membership since all members have membership dues but
    // only standard/extra members have benefits.
    return 'membership';
  }

  function _set_datafields() {
    $this->_datafields = array(
      'invoice_id' => array(
        'fname' => red_t('Invoice ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_invoice',
        'req' => FALSE
      ),
      'member_id' => array(
        'fname' => red_t('Member ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_invoice',
        'req' => FALSE
      ),
      'account_id' => array(
        'fname' => red_t('Account ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_invoice',
        'req' => FALSE
      ),
      'invoice_date' => array(
        'fname' => red_t('Invoice Date'),
        'type' => 'date',
        'pcre' => RED_DATE_MATCHER,
        'pcre_explanation' => RED_DATE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_invoice',
        'req' => TRUE
      ),

      'invoice_amount' => array(
        'fname' => red_t('Amount'),
        'type' => 'decimal',
        'pcre' => RED_MONEY_MATCHER,
        'pcre_explanation' => RED_MONEY_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'text_length' => 5,
        'tblname' => 'red_invoice',
        'req' => TRUE
      ),
      'invoice_status' => array(
        'fname' => red_t('Status'),
        'type' => 'varchar',
        'pcre' => RED_INVOICE_STATUS_MATCHER,
        'pcre_explanation' => RED_INVOICE_STATUS_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_invoice',
        'req' => TRUE
      ),
      'invoice_description' => array(
        'fname' => red_t('Description'),
        'type' => 'text',
        'input_type' => 'textarea',
        'textarea_cols' => 70,
        'textarea_rows' => 5,
        'pcre' => RED_ANYTHING_MATCHER,
        'pcre_explanation' => RED_ANYTHING_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'text_length' => 50,
        'tblname' => 'red_invoice',
        'req' => TRUE
      ),
      'invoice_private_notes' => array(
        'fname' => red_t('Private notes'),
        'type' => 'text',
        'input_type' => 'textarea',
        'textarea_cols' => 70,
        'textarea_rows' => 5,
        'pcre' => RED_ANYTHING_MATCHER,
        'pcre_explanation' => RED_ANYTHING_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'text_length' => 50,
        'tblname' => 'red_invoice',
        'req' => FALSE
      ),
      'invoice_currency' => array(
        'fname' => red_t('Invoice Currency'),
        'type' => 'date',
        'pcre' => RED_CURRENCY_MATCHER,
        'pcre_explanation' => RED_CURRENCY_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => TRUE,
        'tblname' => 'red_invoice',
      ),
      'invoice_date_modified' => array(
        'fname' => red_t('Date modified'),
        'type' => 'datetime',
        'pcre' => RED_DATETIME_MATCHER,
        'pcre_explanation' => RED_DATETIME_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE,
        'tblname' => 'red_invoice',
      ),
      'invoice_type' => array(
        'fname' => red_t('Invoice Type'),
        'type' => 'text',
        'pcre' => RED_INVOICE_TYPE_MATCHER,
        'pcre_explanation' => RED_INVOICE_TYPE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => TRUE,
        'tblname' => 'red_invoice',
      ),

    );
  }

  var $_destination_email;
  function get_destination_email() {
    return $this->_destination_email;
  }

  function set_destination_email($value) {
    $this->_destination_email = $value;
  }

  function is_delinquent() {

    $as_of_date = date('Y-m-d',$this->as_of_timestamp);

    // basic sanity checking
    if($this->get_invoice_status() != 'unpaid') return false;
    if($this->get_invoice_date() > $as_of_date) return false;

    // Get the amount owed
    $this->set_total_paid();
    $owed = $this->get_invoice_amount() - $this->total_paid;

    // Now get total for planned payments scheduled after
    // $as_of_date
    $future_planned_payments_sum = $this->get_future_planned_payments_sum($as_of_date);
    if($owed > $future_planned_payments_sum) return true;
    return false;
  }

  function get_future_planned_payments_count($as_of_date = null) {
    if(is_null($as_of_date)) $as_of_date = date('Y-m-d');
    $as_of_date = addslashes($as_of_date);
    $sql = "SELECT count(planned_payment_id) FROM red_planned_payment ".
      "WHERE planned_payment_status = 'active' AND planned_payment_date ".
      "> '$as_of_date' AND invoice_id = " . intval($this->get_invoice_id());
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return $row[0];
  }
  function get_future_planned_payments_sum() {
    $as_of_date = date('Y-m-d', $this->as_of_timestamp);
    $as_of_date = addslashes($as_of_date);
    $sql = "SELECT sum(planned_payment_amount) FROM red_planned_payment ".
      "WHERE planned_payment_status = 'active' AND planned_payment_date ".
      "> '$as_of_date' AND invoice_id = " . intval($this->get_invoice_id()) .
      " GROUP BY invoice_id";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return $row[0];
  }

  function get_edit_block_table_elements() {
    // First, we want to add a new row that contains a field to enter
    // an email address that will get a copy of the invoice on submit.
    $elements = parent::get_edit_block_table_elements();
    // pop off the last row
    $last = array_pop($elements);

    // optional email for sending welcome email
    $billing_email_array = $this->get_billing_email();
    $emails = array();
    foreach($billing_email_array as $email) {
      $emails[] = $email['address'];
    }
    $default_emails = implode(',',$emails);
    $row = array(0 => array(), 1 => array());
    $row[0]['value'] = 'Send invoice to<br />(optional)';
    $row[0]['attributes'] = array('class' => 'red-edit-field-label');
    $row[1]['value'] = $this->_html_generator->get_form_element('sf_destination_email',$this->get_destination_email(),'text') . '<br />Billing email: ' . $default_emails;
    $elements[] = $row;

    // Next, we want to add hidden fields that include this organization's
    // benefits and dues amounts so javascript can default to the right
    // amount depending on their choice.
    $membership_dues = $this->member_price;
    $benefits_dues = $this->member_benefits_price;
    $membership_attr = array('id' => 'red_membership_dues');
    $benefits_attr = array('id' => 'red_benefits_dues');
    $row = array(0 => array(), 1 => array());
    $row[0]['value'] = '';
    $row[0]['attributes'] = array();
    $row[1]['value'] = $this->_html_generator->get_form_element('red_membership_dues', $membership_dues, 'hidden', $membership_attr) .
      $this->_html_generator->get_form_element('red_benefits_dues', $benefits_dues, 'hidden', $benefits_attr);

    $elements[] = $row;

    // put the last element back on.
    $elements[] = $last;

    return $elements;
  }
  function _post_commit_to_db() {
    $email = array();
    $email['address'] = $this->get_destination_email();
    $email['lang'] = red_get_lang();
    if(!empty($email['address'])) {
      // send
      $emails = array($email);
      if(!$ret = $this->send($emails)) return $ret;
    }
    return true;
  }
  function get_edit_invoice_status() {
    return $this->_html_generator->get_select('sf_invoice_status',$this->get_edit_invoice_options(),$this->get_invoice_status());
  }

  function get_edit_invoice_options() {
    return array(
      'unpaid' => 'unpaid',
      'paid' => 'paid',
      'void' => 'void',
      'eaten' => 'eaten',
      'review' => 'review'
    );
  }
  function get_edit_invoice_type() {
    return $this->_html_generator->get_select('sf_invoice_type',$this->get_invoice_type_options(),$this->get_invoice_type());
  }
  function get_edit_invoice_currency() {
    return $this->_html_generator->get_select('sf_invoice_currency',$this->get_invoice_currency_options(),$this->get_invoice_currency());
  }
  
  function get_invoice_type_options() {
    if($this->member_benefits_level == 'basic') {
      return array('membership' => red_t("Membership"));
    }
    return array('membership' => red_t('Membership'),'benefits' => red_t('Benefits'));
  }

  function get_invoice_currency_options() {
    return array('USD' => red_t('US Dollars'),'MXN' => red_t('Mexican Pesos'));
  }

  function set_total_paid() {
    if(!is_null($this->total_paid)) return;

    $sql = "SELECT sum(payment_amount) FROM red_payment WHERE ".
      "invoice_id = " . intval($this->get_invoice_id()) .
      " AND payment_status = 'active'";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    $this->total_paid = $row[0];
  }

  function printme() {
    $this->set_invoice_text();
    return nl2br($this->invoice_text);
  }

  function get_billing_email() {
    if(!is_null($this->billing_emails)) return $this->billing_emails;
    require_once($this->_construction_options['src_path'] .
      '/modules/class.red.member.inc.php');

    $emails = red_member::get_billing_email($this->get_member_id(), $this->_sql_resource);
    $this->billing_emails = $emails;
    return $emails;
  }

  function send($emails = null) {
    $this->set_invoice_text();
    if(is_null($emails)) {
      $emails = $this->get_billing_email();
    }
    require_once($this->_construction_options['src_path'] .
      '/modules/class.red.correspondence.inc.php');
    foreach ($emails as $email) {
      $lang = $email['lang'];
      $values = array(
        'member_id' => $this->get_member_id(),
        'correspondence_subject' => $this->email_subject[$lang],
        'correspondence_body' => $this->invoice_text[$lang],
      );
      $values['correspondence_to'] = $email['address'];
      $ret = $this->create_related_object('red_correspondence',$values);
      if(!$ret) return $ret;
    }
    return true;
  }

  function send_reminder() {
    $emails = $this->get_billing_email();
    require_once($this->_construction_options['src_path'] .
      '/modules/class.red.correspondence.inc.php');
    foreach($emails as $email) {
      $lang = $email['lang'];
      $values = array(
        'member_id' => $this->get_member_id(),
        'correspondence_subject' => $this->email_subject[$lang],
        'correspondence_body' => $this->reminder_text[$lang],
      );
      $values['correspondence_to'] = $email['address'];
      $ret = $this->create_related_object('red_correspondence',$values);
      if(!$ret) return $ret;
    }
    return true;
  }

  function set_invoice_body() {
    $this->set_cutoff_date();
    if(!empty($this->invoice_body)) return;
    $this->set_total_paid();

    $date_printed = date('m/d/Y');
    $find = array(
      '{member_friendly_name}',
      '{date_printed}',
      '{payment_due_date}',
      '{invoice_id}',
      '{invoice_description}',
      '{invoice_amount}',
      '{total_paid}',
      '{owed}',
      '{invoice_currency}',
      '{invoice_type}'
    );

    $total_paid = $this->total_paid;
    $invoice_amount = $this->get_invoice_amount();
    $owed = $invoice_amount - $total_paid;

    // any value set to 0 - should be replaced with string '0'
    // so it shows up in invoice
    if(empty($total_paid)) $total_paid = '0';
    if(empty($owed)) $owed = '0';
    if(empty($invoice_amount)) $invoice_amount = '0';

    if($this->has_planned_payment()) {
      $this->set_planned_payment_calculations();
      $payment_due_date = $this->planned_payment_overdue_date;
      if(empty($payment_due_date)) {
        // this will come back empty if they are not behind (e.g. if we are sending
        // and advance notice of the next planned payment date), so instead base it
        // on the upcoming date
        $payment_due_date = $this->planned_payment_upcoming_date;
      }
    } else {
      $payment_due_date = $this->get_invoice_date();
    }
    $payment_due_date = red_convert_to_friendly_date($payment_due_date);

    $replace = array(
      $this->member_friendly_name,
      $date_printed,
      $payment_due_date,
      $this->get_invoice_id(),
      wordwrap($this->get_invoice_description(),70),
      $invoice_amount,
      $total_paid,
      $owed,
      $this->get_invoice_currency(),
      $this->get_invoice_type()
   );

    $langs = red_allowed_languages();
    foreach ($langs as $lang) {
      $invoice_template = $this->get_email_template('invoice.body', $lang);
      $this->invoice_body[$lang] = str_replace($find,$replace,$invoice_template);
    }
  }
  
  function set_invoice_footer() {
    if(!empty($this->invoice_footer)) return;

    $langs = red_allowed_languages(); 
    foreach ($langs as $lang) {
      $invoice_template = $this->get_email_template('invoice.footer', $lang);
      $payment_instructions = $this->get_payment_instructions($lang);
      $this->invoice_footer[$lang] = str_replace('{payment_instructions}', $payment_instructions, $invoice_template);
    }
  }

  function get_payment_template_name() {
    $currency = $this->get_invoice_currency();
    return 'payment.' . strtolower($currency);
  }

  function get_payment_instructions($lang) {
    $currency = $this->get_invoice_currency();
    $member_id = $this->get_member_id();

    $payment_instructions_template_name = $this->get_payment_template_name();

    if($payment_instructions_template_name) {
      $payment_instructions = $this->get_email_template($payment_instructions_template_name, $lang);
      $payment_instructions = str_replace('{member_id}', $member_id, $payment_instructions);
    }
    return $payment_instructions;

  }
  function set_invoice_text() {
    $this->set_invoice_body();
    $this->set_invoice_footer();

    $langs = red_allowed_languages(); 
    foreach ($langs as $lang) {
      $this->invoice_text[$lang] = $this->invoice_body[$lang] . $this->invoice_footer[$lang]; 
      $this->email_subject[$lang] = $this->get_email_template('invoice.subject', $lang);
    }
  }

  function set_user_input($post) {
    // first load the db values
    $key_field = $this->get_key_field();
    $submit_key_var = 'sf_' . $key_field;
    $this->set_field_value($key_field,$post[$submit_key_var]);
    $this->reset_to_db_values();
    foreach ($post as $k => $v) {
      if(preg_match('/sf_(.*)$/',$k,$matches)) {
        $field = $matches[1];
        // This should not be set by the class - it's a
        // helper
        if($field == 'destination_email') {
          // set the email address - which will be sent to in _post_commit
          $this->set_destination_email($v);
        } else {
          if($this->accept_user_input($field))
            $this->set_field_value($field,$v);
        }
      }
    }
  }

  function set_cutoff_date() {
    if($this->has_planned_payment()) {
      $this->set_planned_payment_calculations();
      $date = $this->planned_payment_overdue_date;
      if(empty($date)) {
        // this will come back empty if they are not behind (e.g. if we are sending
        // and advance notice of the next planned payment date), so instead base it
        // on the upcoming date
        $date = $this->planned_payment_upcoming_date;
      }
      $pp_ts = strtotime($date);
      $this->cutoff_date = date('Y-m-d', $pp_ts + (RED_PLANNED_PAYMENT_GRACE_DAYS * 86400));

    } else {
      $effective_invoice_date = red_invoice::get_effective_invoice_date();
      $invoice_ts = strtotime($effective_invoice_date);
      $this->cutoff_date = date('Y-m-d',$invoice_ts + ( RED_INVOICE_GRACE_DAYS * 86400));
    }
  }

  // return the date of a planned payment due in
  // RED_PLANNED_PAYMENT_ADVANCED_NOTICE_DAYS or
  // false if there is none
  function get_planned_payment_amount_for_date($date){

    $sql = "SELECT planned_payment_amount FROM red_planned_payment ".
      "WHERE planned_payment_date = '$date' AND invoice_id = ".
      intval($this->get_invoice_id()) . " AND planned_payment_status = 'active'";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    if(empty($row[0])) return false;
    return $row[0];
  }

  static function get_planned_payment_advanced_notice_date() {
    $date_due_ts = time() + (RED_PLANNED_PAYMENT_ADVANCED_NOTICE_DAYS * 86400);
    return date('Y-m-d',$date_due_ts);
  }

  // used for sending email about upcoming planned payments
  function set_upcoming_planned_payment_text() {
    if(!empty($this->reminder_text)) return;

    // only operate if we have planned payments
    if(!$this->has_planned_payment()) return;

    $date_due = red_invoice::get_planned_payment_advanced_notice_date();
    $amount_due = $this->get_planned_payment_amount_for_date($date_due);

    $this->set_invoice_text();
    $find = array('{member_friendly_name}', '{planned_payment_amount}','{planned_payment_date}', '{invoice_currency}');
    $replace = array(  $this->member_friendly_name, $amount_due, $date_due, $this->get_invoice_currency());

    $langs = red_allowed_languages();
    foreach ($langs as $lang) {
      $template = $this->get_email_template('email_pp_advanced_notice.body', $lang);
      $this->reminder_text[$lang] = str_replace($find,$replace,$template) . $this->invoice_text[$lang];
      $this->email_subject[$lang] = $this->get_email_template('email_pp_advanced_notice.subject', $lang);
    }
  }

  function get_effective_invoice_date() {
    $invoice_date = $this->get_invoice_date();
    if(RED_INVOICE_START_DATE > $invoice_date) {
      return RED_INVOICE_START_DATE;
    } else {
      return $invoice_date;
    }
  }


  // determine whether a reminder of any type should be sent
  function reminder_due() {
    if(false === $this->get_reminder_template()) {
      return false;
    }
    return true;
  }

  // return true if this invoice has at least
  // one active planned payment
  function has_planned_payment() {

    $sql = "SELECT planned_payment_id FROM red_planned_payment " .
      "WHERE invoice_id = " . intval($this->get_invoice_id()) . " AND ".
      "planned_payment_status = 'active' ";
    $result = $this->_sql_query($sql);
    if($this->_sql_num_rows($result) > 0) return true;
    return false;
  }

  // determine what reminder template to use (or none)
  // depending on the date of the invoice
  // reminders are: reminder, cutoff-warning, cutoff-alert
  // cutoff-job
  function get_reminder_template() {
    $days = $this->get_number_of_days_behind();
    // if($this->get_invoice_id() == 4017) echo "days: $days\n";
    if($this->has_planned_payment()) {
      // determine planned payment notification
      if($days < RED_PLANNED_PAYMENT_GRACE_DAYS) return false;
      if($days == RED_PLANNED_PAYMENT_GRACE_DAYS ) return 'email_pp_cutoff_alert';
      if($days == 45) return 'email_pp_cutoff_job';
      return false;
    } else {
      // echo "days; $days\n";
      if($days < 30) return false;
      if($days == 30) return 'email_dues_cutoff_warning';
      if($days == 60) return 'email_dues_cutoff_warning';
      // if($days == 83) return 'email_dues_cutoff_alert';
      // if($days == 87) return 'email_dues_cutoff_alert';
      // if($days == 88) return 'email_dues_cutoff_alert';
      // if($days == 89) return 'email_dues_cutoff_alert';
      if($days == RED_INVOICE_GRACE_DAYS ) return 'email_dues_cutoff_alert';
      # grace period and to ensure we have been to the post office
      if($days == 104) return 'email_dues_cutoff_job';
      return false;
    }
  }

  function get_number_of_days_behind() {
    if($this->has_planned_payment()) {
      return $this->get_number_of_days_behind_planned_payment();
    } else {
      $effective_invoice_date = $this->get_effective_invoice_date();
      $invoice_ts = strtotime($effective_invoice_date);
      $today_ts = $this->as_of_timestamp;
      $ret = ceil(($today_ts - $invoice_ts)/86400);
      // echo "days: $ret\n";
      return $ret;
    }
  }

  function get_number_of_days_behind_planned_payment() {
    $this->set_planned_payment_calculations();

    if(is_null($this->planned_payment_overdue_date)) {
      // this means they are not behind
      return 0;
    }
    // calculate difference in days between now
    // and the date of the planned payment they failed
    // to heed.
    $ts_now = $this->as_of_timestamp;
    $ts_planned_payment = strtotime($this->planned_payment_overdue_date);
    return floor(($ts_now - $ts_planned_payment) / 86400);
  }

  // set various calculated values related to planned payments
  function set_planned_payment_calculations() {
    // don't run twice...
    if(!is_null($this->planned_payment_upcoming_running_total)) return;

    $this->set_total_paid();
    $planned_payment_running_total = 0;
    $date = null;
    // loop through all planned payments
    $sql = "SELECT planned_payment_date, planned_payment_amount ".
      "FROM red_planned_payment WHERE planned_payment_status = 'active' AND ".
      "invoice_id = " . intval($this->get_invoice_id()) . " ORDER BY ".
      "planned_payment_date";
    $result = $this->_sql_query($sql);
    while($row = $this->_sql_fetch_row($result)) {
      // build a running total of planned payments
      $planned_payment_running_total += $row[1];

      $current_date = date('Y-m-d');
      $pp_date = $row[0];

      // The first planned payment after now is the upcoming one
      // (used in calculations for advance notices)
      if($current_date <= $pp_date && is_null($this->planned_payment_upcoming_date)) {
        $this->planned_payment_upcoming_date = $pp_date;
      }

      // If the running total of planned payments is higher
      // then the total they have already paid, and the payment
      // was due in the past, then it means we've found the planned
      // payment that they are behind on and we can calculate the running
      // total that is due.
      if($current_date >= $pp_date &&
        $planned_payment_running_total > $this->total_paid) {

        $this->planned_payment_overdue_date = $pp_date;
        $this->planned_payment_upcoming_running_total = $planned_payment_running_total;
        return;
      }
    }
  }

  function get_read_invoice_id() {
    $ret = $this->get_auto_constructed_read_field('invoice_id');
    if($this->display_view_payments_link) {
      $attr = array(
        'onclick' => 'togglePayments('. $this->get_invoice_id() . ');return false;',
        'href' => '#',
        'id' => 'red-display-payment-link-' . $this->get_invoice_id(),
        'class' => 'red-display-payment-link',
        'title' => 'Click to see or hide your payments for this invoice',
      );
      $link = $this->_html_generator->get_tag('a','+',$attr);
      return $link . $ret;
    }
    return $ret;
  }

  static function is_paid($invoice_id, $sql_resource) {
    $invoice_id = intval($invoice_id);
    $sql = "SELECT invoice_id FROM red_invoice WHERE invoice_status = 'paid'
      AND invoice_id = $invoice_id";
    $result = red_sql_query($sql, $sql_resource);
    if(red_sql_num_rows($result) == 0) return FALSE;
    return TRUE;
  }
}
