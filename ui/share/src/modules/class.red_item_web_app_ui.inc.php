<?php

if(!class_exists('red_item_web_app_ui')) {
  class red_item_web_app_ui extends red_item_web_app {
    var $_delete_confirmation_message = "Deleting a web only deletes this record in the database. You must delete and re-create your web configuration to fully remove it.";

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);
    }
    
    function get_edit_web_app_name() {
      // We tag on to this field to add a checkbox allowing the user
      // to choose not to attempt installation when creating a new record.
      // This allows users to add a web app when their web app is already
      // installed so they can benefit from the upgrade system.
      $ret = $this->get_auto_constructed_edit_field('web_app_name');
      if ($this->exists_in_db()) {
        // No OP - if they are editing a record, we don't provide this option
        // because it would allow a user to change a web app from soft-error
        // to active without resolving the soft-error.
        return $ret;
      }
      $div_attributes = array( 'class' => 'red_web_app_install');
      $options = [
        'install_web_app_y' => red_t("Please download this web app into my web directory"),
        'install_web_app_n' => red_t("My app is already downloaded, just keep it up to date"),
      ];
      $selected = 'install_web_app_y';
      $install_radio = $this->_html_generator->get_radio('sf_install', $options, $selected);
      $ret .= $this->_html_generator->get_tag('div',$install_radio, $div_attributes);
      return $ret;
    }

    function additional_validation() {
      $ret = parent::additional_validation();
      if ($this->_delete) return TRUE;

      // You can't edit an app using drupal5, drupal6 or drupal7
      if ($this->exists_in_db()) {
        $item_id = intval($this->get_item_id());
        $sql = "SELECT web_app_name FROM red_item_web_app WHERE item_id = $item_id";
        $result = $this->_sql_query($sql);
        $row = $this->_sql_fetch_row($result);
        $disallow = [ 'drupal5', 'drupal6', 'drupal7' ];
        if (in_array($row[0], $disallow)) {
          $this->set_error(red_t("You can't edit old versions of Drupal."), 'validation');
          return FALSE;
        }
      }
      return $ret;
    }

    function _pre_commit_to_db() {
      if(!parent::_pre_commit_to_db()) return FALSE;

      // If we are deleting this record, don't make any adjustments.
      if ($this->_delete) return TRUE;

      // Set the status to active if they don't want to install the web app
      // and indicate that we don't want to notify the server.
      if (array_key_exists('sf_install', $_POST)) {
        if ($_POST['sf_install'] == 'install_web_app_n') {
          $this->set_item_status('active');
          $this->notify_host(FALSE);
        }
      }

      // A user may want to change the security setting of an existing item.
      // Normally, that will cause the control panel to try to install it,
      // which will result in an error because the files already exist. So..
      // If a record exists in the database AND the record's previous state was
      // active, then don't try to re-install or notify the host, just reset to
      // active.
      if ($this->exists_in_db()) {
        $item_id = intval($this->get_item_id());
        $sql = "SELECT item_status FROM red_item WHERE item_id = $item_id";
        $result = $this->_sql_query($sql);
        $row = $this->_sql_fetch_row($result);
        if ($row[0] == 'active') {
          $this->set_item_status('active');
          $this->notify_host(FALSE);
        }
      }
      return TRUE;
    }
  } 
}

?>
