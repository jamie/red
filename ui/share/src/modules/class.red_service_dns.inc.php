<?php

class red_service_dns extends red_area_hosting_order {
  function __construct($construction_options) {
    parent::__construct('hosting_order',$construction_options);
  }

  function get_footer() {
    $asterisk =  $this->html_generator->get_tag('span','*',
                 array('class' => 'red-non-local-mx'));
    $transfer_link =  $this->html_generator->get_tag('a',
                      red_t('transfer control of your domain to MFPL'),
                      array('href' => 'https://support.mayfirst.org/wiki/transfer_domain'));
    $msg = red_t('Records with a !asterisk do not appear to be assigned ' .
           'to our name servers and therefore the settings you are putting in place ' . 
           'on this page may not take effect until you !transfer_link.',array(
             '!asterisk' => $asterisk, '!transfer_link' => $transfer_link));
    $attr = array('class' => 'red-non-local-mx-explanation red-explanation');

    return $this->html_generator->get_tag('div',$msg,$attr);
      
  }
}

