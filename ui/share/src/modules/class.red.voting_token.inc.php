<?php

class red_voting_token extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'voting_token_id';
  var $_key_table = 'red_voting_token';

  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_voting_token_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_voting_token_status('active');
    }
  }

  function get_delete_confirmation_message() {
    return red_t("Are you sure you want to delete the voting token for the election: @election?", array('@election' => $this->get_election()));
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_voting_token_id());
    return $this->_get_initialize_sql($id);
  }
  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_voting_token ". 
      "WHERE voting_token_id = " . $id;
    return $sql;
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_voting_token_status('deleted');
  }

  var $_voting_token_id;
  function set_voting_token_id($value) {
    $this->_voting_token_id = $value;
  }
  function get_voting_token_id() {
    return $this->_voting_token_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }
  function get_member_id() {
    return $this->_member_id;
  }

  var $_voting_token;
  function set_voting_token($value) {
    $this->_voting_token = $value;
  }
  function get_voting_token() {
    return $this->_voting_token;
  }
  
  var $_voting_token_status;
  function set_voting_token_status($value) {
    $this->_voting_token_status = $value;
  }
  function get_voting_token_status() {
    return $this->_voting_token_status;
  }

  var $_voting_token_election;
  function set_voting_token_election($value) {
    $this->_voting_token_election = $value;
  }
  function get_voting_token_election() {
    return $this->_voting_token_election;
  }
  function _set_datafields() {
    $this->_datafields = array(
      'voting_token_id' => array(
        'fname' => red_t('Phone Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_voting_token',
        'req' => FALSE 
      ),
      'member_id' => array(
        'fname' => red_t('Member Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_voting_token',
        'req' => FALSE 
      ),
      'voting_token' => array(
        'fname' => red_t('Voting Token'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_voting_token',
        'text_length' => 16,
        'req' => TRUE 
      ),
      'voting_token_election' => array(
        'fname' => red_t('Election'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_voting_token',
        'text_length' => 16,
        'req' => TRUE 
      ),
      'voting_token_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_explanation' => RED_ACTIVE_DELETED_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE, 
        'tblname' => 'red_voting_token',
      ),
    );
  }
}
