<?php

class red_note extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'note_id';
  var $_key_table = 'red_note';

  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_note_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_note_status('active');
    }
    if(empty($this->_note_modified)) {
      $this->_note_modified = date('Y-m-d h:i:s');
    }
  }

  function get_delete_confirmation_message() {
    return red_t("Are you sure you want to delete the note: @note?", array('@note' => $this->get_note()));
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_note_id());
    return $this->_get_initialize_sql($id);
  }
  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_note ". 
      "WHERE note_id = " . $id;
    return $sql;
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_note_status('deleted');
  }

  var $_note_id;
  function set_note_id($value) {
    $this->_note_id = $value;
  }
  function get_note_id() {
    return $this->_note_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }
  function get_member_id() {
    return $this->_member_id;
  }

  var $_note;
  function set_note($value) {
    $this->_note = $value;
  }
  function get_note() {
    return $this->_note;
  }


  var $_note_status;
  function set_note_status($value) {
    $this->_note_status = $value;
  }
  function get_note_status() {
    return $this->_note_status;
  }

  var $_note_modified;
  function set_note_modified($value) {
    $this->_note_modified = $value;
  }
  function get_note_modified() {
    return $this->_note_modified;
  }

  function _set_datafields() {
    $this->_datafields = array(
      'note_id' => array(
        'fname' => red_t('Note Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_note',
        'req' => FALSE 
      ),
      'member_id' => array(
        'fname' => red_t('Member Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_note',
        'req' => FALSE 
      ),
      'note' => array(
        'fname' => red_t('Note'),
        'type' => 'varchar',
        'pcre' => RED_ANYTHING_MATCHER,
        'pcre_explanation' => RED_ANYTHING_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'input_type' => 'textarea',
        'textarea_cols' => 70,
        'textarea_rows' => 5,
        'tblname' => 'red_note',
        'text_length' => 50,
        'req' => TRUE
      ),
      'note_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_explanation' => RED_ACTIVE_DELETED_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE, 
        'tblname' => 'red_note',
      ),
      'note_modified' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_DATETIME_MATCHER,
        'pcre_explanation' => RED_DATETIME_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE, 
        'tblname' => 'red_note',
      ),
    );
  }
  
}
