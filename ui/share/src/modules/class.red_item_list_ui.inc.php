<?php

if(!class_exists('red_item_list_ui')) {
  class red_item_list_ui extends red_item_list {

    var $server_url_map;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the list " . 
        $this->_html_generator->get_tag('span',$this->get_list_name(),$attr) . 
        "? All list archives will be deleted.";
    }

    function get_read_list_name() {
      $list = $this->get_list_name();
      $domain = $this->get_list_domain();
      // set default URL
      $url = "https://$domain/";
      // Try to determine the URL preferred by this server
      if(!empty($this->server_url_map)) {
        $map = $this->choose_var_from_multi_dimensional_config($this->server_url_map);
        if(array_key_exists($this->get_item_host(),$map)) {
          $url = $map[$this->get_item_host()];
        }
      }
      $attributes = array('href' => $url . "mailman/admin/$list");
      return $this->_html_generator->get_tag('a',$list,$attributes);
    }

    function get_edit_list_domain() {
      if($this->exists_in_db()) return $this->get_auto_constructed_edit_field('list_domain');

      $selected_domain = $this->get_list_domain();
      $options = $this->get_list_domain_options();
      if(!empty($selected_domain) && !in_array($selected_domain,$options))
        $options[$selected_domain] = $selected_domain;
      $multiple = false;
      $attributes = array();
      $prepend_choose_one_option = false;
      return $this->_html_generator->get_select('sf_list_domain',$options,$selected_domain,$multiple,$attributes,$prepend_choose_one_option);
    }

    function get_edit_list_owner_email() {
      if($this->exists_in_db()) {
        $message = $this->_html_generator->get_tag('p',red_t("Note: when updating an existing list, the owner password will be reset and sent to the current owner email address (which may be different from the email address entered when the list was created, which is the email address shown above)."));
      }
      else {
        $message = $this->_html_generator->get_tag('p',red_t("Your email list password will be sent to the owner email address."));
      }
      return $this->get_auto_constructed_edit_field('list_owner_email') .
        $message;
    }
  }  
}

?>
