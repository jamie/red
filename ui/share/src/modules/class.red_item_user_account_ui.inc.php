<?php

if(!class_exists('red_item_user_account_ui')) {
  class red_item_user_account_ui extends red_item_user_account {
   var $is_admin = false;
   var $_javascript_includes = array('scripts/password_generator.js','scripts/item_user_account.js');

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);


    }


    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the user account " . 
        $this->_html_generator->get_tag('span',$this->get_user_account_login(),$attr) . 
        "? The user's home directory will be deleted.";
    }

    function get_edit_user_account_password() {
      // display confirm box
      $value = $this->get_user_account_password();

      $pass_input = $this->_html_generator->get_input('sf_user_account_password','','password', array('size' => 30,'id' => 'user_account_password')) . '<br />';

      // create link for user to generate random password
      $link_attr = array(
        'href' => '#',
        'onClick' => "pass=generateRandomPassword(12);parsePassword(pass);return false;"
      );
      $password_link = $this->_html_generator->get_tag('a','Create Random Pass',$link_attr);
      // Create tag to display the password when the users clicks it
      $display_attr = array('id' => 'red_display_password');
      $password_display = $this->_html_generator->get_tag('span','',$display_attr) . '<br />';

      // password confirm box
      $pass_confirm_input = $this->_html_generator->get_input('sf_user_account_password_confirm','','password', array('size' => 30,'id' => 'user_account_password_confirm')) . '<br />';

      // Directions for password confirm box
      $explanation_attr = array('class' => 'red_explanation');
      $message = $this->_html_generator->get_tag('span',red_t('Please confirm your password (or leave blank if not changing)'),$explanation_attr) . '<br />';
      return $pass_input . $password_link . $password_display .
        $pass_confirm_input . $message;
    }

    function set_user_input($post) {
      $this->set_item_id($post['sf_item_id']);
      $this->reset_to_db_values();
      foreach($post as $k => $v) {
        if(preg_match('/sf_(.*)$/',$k,$matches)) {
          $field = $matches[1];
          // This field should not be set by the class - it's a  
          // helper field
          if($field == 'user_account_password_confirm') continue;

          // We need to auto create/manipulate the password field 
          if($field == 'user_account_password') {
            // this is hacky - validation errors should happen
            // when validation... but then the post variable is not
            // available there and by then the password will be
            // encrypted ...
            $login = '';
            if(array_key_exists('sf_user_account_login',$post)) {
              // it's a new account
              $login = $post['sf_user_account_login'];
            }
            else {
              $login = $this->get_user_account_login();
            }
            if($v == '') {
              // Continue - if this record is being updated, then the
              // value from the db will be used. If it is a new record
              // then the empty password will be caught by the validate
              // script
              continue;
            }
            elseif($v != $post['sf_user_account_password_confirm']) {
              $this->set_error(red_t("Your passwords do not match."),'validation');
            }
            elseif(!red_is_good_password($v)) {
              $this->set_error(red_t('Please use a password that is at least 6 characters long and has both letters and non-letters - like: b@mzit5 or NuX@gwb or no$n1ch.'),'validation');
            }
            elseif($v == 'b@mzit5' || $v == 'NuX@gwb' || $v == 'no$n1ch') {
              $this->set_error(red_t("Use your imagination. Come up with your own password."),'validation');
            }
            elseif($v == $login) {
              $this->set_error(red_t("Please don't set your password to the same value as your username. That's one of the first guesses of any EvilGenius."),'validation');
            }
          }
          if($this->accept_user_input($field))
            $this->set_field_value($field,$v);
        }
      }
    }

    function get_edit_user_account_auto_response_action() {
      $action_value = $this->get_user_account_auto_response_action();
      $action_options = $this->_user_account_auto_response_action_options;
      $action_input = $this->_html_generator->get_select('sf_user_account_auto_response_action',$action_options,$action_value,false,array(),false);
      return $action_input;
    }
    function _pre_commit_to_db() {
      if(!parent::_pre_commit_to_db()) return FALSE;
      if($this->_delete) {
        // Replace the password for privacy reasons and to avoid login trouble
        // with usernames that have the same password.
        // See: https://support.mayfirst.org/ticket/7507
        $password = red_generate_random_password(25);
        if(FALSE === $password) {
            $this->set_error(red_t("Failed to generate random password."),'system');
            return FALSE;
        }
        $this->set_field_value('user_account_password', $password);

        // Remove the user from sitewide admins if they are a sitewide admin and being deleted.
        // See: https://support.mayfirst.org/ticket/7191
        $sql = "DELETE FROM red_sitewide_admin WHERE user_name = '" . addslashes($this->get_user_account_login()) . "'";
        $this->_sql_query($sql);
      }
      return TRUE;
    }

    function set_user_account_password($value) {
      // If there is a value and it does not begin with $1$ (md5) or $6$
      // (sha512), then encrypt it 
      if(!empty($value) && substr($value,0,3) != '$1$' && substr($value,0,3) != '$6$') {
        $value = $this->crypt($value);
      }
        return parent::set_user_account_password($value);
    }

    static function crypt($value,$salt = '') {
      // Auto generate the salt passed on the configuration parameter.
      if(empty($salt)) {
        if(!defined('RED_PASSWORD_HASH_TYPE') || RED_PASSWORD_HASH_TYPE == 'md5') {
          $prefix = '$1$';
        } elseif (RED_PASSWORD_HASH_TYPE == 'sha512') {
          // rounds= tells php how many times the hashing loop should be executed.
          // Should be an integer between 1000 and 999,999,999. Default is 5000.
          // We calculated that on dkg's machine, 100,000 rounds took 100 ms to
          // verify. We are using an odd number in case someone has created a
          // table already.
          $prefix = '$6$rounds=199999$';
        } else {
          // Use default
          $prefix = '$1$';
        }
        $salt = $prefix . red_generate_random_password(16);
      }
      return crypt($value,$salt);
    }

    function get_edit_user_account_mountpoint() {
      // Only display for admins
      if ($this->is_admin) {
        return $this->get_auto_constructed_edit_field('user_account_mountpoint');
      }
      else {
        return red_t("Only available to administrators");
      }
    }
    function additional_validation() {
      parent::additional_validation();
      // Only admins are allowed to change the mountpoint parameter.
      if (!$this->is_admin) {
        $item_id = intval($this->get_item_id());
        $mountpoint = $this->get_user_account_mountpoint();
        if (!empty($mountpoint)) {
          // We are going to throw an error if the submitted value is not
          // the same as the value in the database.
          $throw_error = TRUE;
          if ($this->exists_in_db()) {
            $sql = "SELECT user_account_mountpoint FROM red_item_user_account WHERE item_id = $item_id";
            $result = $this->_sql_query($sql);
            $row = $this->_sql_fetch_row($result);
            // If they are the same, then it's no problem.
            if($row[0] == $mountpoint) {
              // You are saved, no error.
              $throw_error = FALSE;
            }
          }
          if ($throw_error) {
            $this->set_error(red_t('Only administrators are allowed to set mountpoint.'), 'validation');
          }
        }
      }
    }
  }  
}

?>
