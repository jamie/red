<?php

if(!class_exists('red_item_server_access_ui')) {
  class red_item_server_access_ui extends red_item_server_access {

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete server access for " . 
        $this->_html_generator->get_tag('span',$this->get_server_access_login(),$attr) . 
        "?";
    }

    function get_edit_server_access_login() {
      if($this->exists_in_db()) return $this->get_auto_constructed_read_field('server_access_login');

      $login = $this->get_server_access_login();

      $user_accounts = $this->get_related_user_account_logins();
      $login_select = $this->_html_generator->get_select('sf_server_access_login',$user_accounts,$login);
      return $login_select;
    }

    function get_edit_server_access_public_key() {
      $field = $this->get_auto_constructed_edit_field('server_access_public_key');
      $explanation_attr = array('class' => 'red_explanation');
      $message = $this->_html_generator->get_tag('span',red_t('This form will add keys but not delete them. To delete keys please manually edit your .ssh/authorized_keys file.'),$explanation_attr) . '<br />';
      return $field . '<br />' . $message;
    }
  }  
}

?>
