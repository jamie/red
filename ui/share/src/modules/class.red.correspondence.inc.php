<?php

class red_correspondence extends red_ado {

  var $_key_field = 'correspondence_id';
  var $_key_table = 'red_correspondence';
  
  var $default_from = 'members@mayfirst.org';

  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_correspondence_id)) {
      if(array_key_exists('member_id',$construction_options) && empty($this->get_member_id())) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_correspondence_from($this->default_from);
      $this->set_correspondence_date(date('Y-m-d H:i:s'));
      if(!array_key_exists('correspondence_to',$construction_options) && empty($this->get_correspondence_to())) {
        $this->set_correspondence_to($this->get_default_to());
      }
    }
    $this->_human_readable_description = red_t("Correspondence is a record of email communication");
    $this->_human_readable_name = red_t('Correspondence');
  }

  function get_default_to() {
    $member_id = intval($this->get_member_id());
    if(empty($member_id)) return '';
    $sql = "SELECT contact_email FROM red_contact ".
      "WHERE member_id = $member_id AND contact_billing = 'y' AND contact_status = 'active' LIMIT 1";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    $email = NULL;
    if ($row) {
      $email = $row[0];
    }
    return $email; 
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_correspondence_id());
    return $this->_get_initialize_sql($id);
  }
  
  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_correspondence ". 
      "WHERE correspondence_id = " . $id;
    return $sql;
  }

  var $_correspondence_id;
  function set_correspondence_id($value) {
    $this->_correspondence_id = $value;
  }

  function get_correspondence_id() {
    return $this->_correspondence_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }

  function get_member_id() {
    return $this->_member_id;
  }

  var $_correspondence_to;
  function set_correspondence_to($value) {
    $this->_correspondence_to = $value;
  }

  function get_correspondence_to() {
    return $this->_correspondence_to;
  }

  var $_correspondence_from;
  function set_correspondence_from($value) {
    $this->_correspondence_from = $value;
  }

  function get_correspondence_from() {
    return $this->_correspondence_from;
  }

  var $_correspondence_subject;
  function set_correspondence_subject($value) {
    $this->_correspondence_subject = $value;
  }

  function get_correspondence_subject() {
    return $this->_correspondence_subject;
  }

  var $_correspondence_date;
  function set_correspondence_date($value) {
    $this->_correspondence_date = $value;
  }

  function get_correspondence_date() {
    return $this->_correspondence_date;
  }

  var $_correspondence_body;
  function set_correspondence_body($value) {
    $this->_correspondence_body = $value;
  }

  function get_correspondence_body() {
    return $this->_correspondence_body;
  }

  function _set_datafields() {
    $this->_datafields = array(
      'correspondence_id' => array(
        'fname' => red_t('Correspondence ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_correspondence',
        'req' => FALSE 
      ),
      'member_id' => array(
        'fname' => red_t('Member ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_correspondence',
        'req' => FALSE 
      ),
      'correspondence_to' => array(
        'fname' => red_t('To'),
        'type' => 'varchar',
        'pcre' => RED_EMAIL_MATCHER,
        'pcre_explanation' => RED_EMAIL_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_correspondence',
        'text_length' => 50,
        'req' => TRUE
      ),
      'correspondence_date' => array(
        'fname' => red_t('Date'),
        'type' => 'varchar',
        'pcre' => RED_DATETIME_MATCHER,
        'pcre_explanation' => RED_DATETIME_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_correspondence',
        'text_length' => 50,
        'req' => TRUE
      ),
      'correspondence_from' => array(
        'fname' => red_t('From'),
        'type' => 'varchar',
        'pcre' => RED_EMAIL_MATCHER,
        'pcre_explanation' => RED_EMAIL_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_correspondence',
        'text_length' => 50,
        'req' => TRUE
      ),
      'correspondence_subject' => array(
        'fname' => red_t('Subject'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_correspondence',
        'text_length' => 50,
        'req' => FALSE 
      ),
      'correspondence_body' => array(
        'fname' => red_t('Body'),
        'type' => 'text',
        'pcre' => RED_ANYTHING_MATCHER,
        'pcre_explanation' => RED_ANYTHING_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'input_type' => 'textarea',
        'textarea_cols' => 70,
        'textarea_rows' => 5,
        'req' => FALSE, 
        'tblname' => 'red_correspondence',
      ),
    );

  }

  function get_edit_block_table_elements() {
    $elements = parent::get_edit_block_table_elements();
    // pop off last one - the submit button
    $submit = array_pop($elements);
    // change Submit to Send and Record email
    $submit[0]['value'] = str_replace(red_t('Submit'),red_t("Send and Record Email"),$submit[0]['value']);
    $elements[] = $submit;
    return $elements;
  }
  function _pre_commit_to_db() {
    // you cannot change correspondence. 
    // anytime you are trying to "update" a record, instead we
    // want to insert a new one
    if($this->exists_in_db()) {
      $this->set_correspondence_id('');
      $this->_exists_in_db = false;
    }
    return true;
  }
  
  function _post_commit_to_db() {
    // send the email
    if($this->send()) return true;
    return false;
  }

  function send() {
    if(defined('RED_MAIL_DONT_SEND') && RED_MAIL_DONT_SEND) return true;
    $headers = "From: " . $this->get_correspondence_from() . "\r\n" .
      "Content-Type:text/plain;charset=utf-8";
    if(!mail($this->get_correspondence_to(),$this->get_correspondence_subject(),$this->get_correspondence_body(),$headers)) {
      $this->set_error(red_t("Failed to send email."),'system');
      return false;
    }
    return true;
  }
}
