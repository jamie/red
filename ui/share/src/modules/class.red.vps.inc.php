<?php

class red_vps extends red_ado {
  var $_key_field = 'vps_id';
  var $_key_table = 'red_vps';
  var $is_admin = FALSE;

  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_vps_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_vps_status('active');
    }

  }

  function get_delete_confirmation_message() {
    return red_t("Are you sure you want to delete the vps: @vps_server?", array('@vps_server' => $this->get_vps_server()));
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_vps_id());
    return $this->_get_initialize_sql($id);
  }
  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_vps ". 
      "WHERE vps_id = " . $id;
    return $sql;
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_vps_status('deleted');
  }

  var $_vps_id;
  function set_vps_id($value) {
    $this->_vps_id = $value;
  }
  function get_vps_id() {
    return $this->_vps_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }
  function get_member_id() {
    return $this->_member_id;
  }

  var $_vps_cpu;
  function set_vps_cpu($value) {
    $this->_vps_cpu = $value;
  }
  function get_vps_cpu() {
    return $this->_vps_cpu;
  }

  var $_vps_ram;
  function set_vps_ram($value) {
    $this->_vps_ram = $value;
  }
  function get_vps_ram() {
    return $this->_vps_ram;
  }
  
  var $_vps_hd;
  function set_vps_hd($value) {
    $this->_vps_hd = $value;
  }
  function get_vps_hd() {
    return $this->_vps_hd;
  }
  var $_vps_ssd = 0;
  function set_vps_ssd($value) {
    $this->_vps_ssd = $value;
  }
  function get_vps_ssd() {
    return $this->_vps_ssd;
  }

  var $_vps_server;
  function set_vps_server($value) {
    $this->_vps_server = $value;
  }
  function get_vps_server() {
    return $this->_vps_server;
  }

  var $_vps_status;
  function set_vps_status($value) {
    $this->_vps_status = $value;
  }
  function get_vps_status() {
    return $this->_vps_status;
  }

  function _set_datafields() {
    $this->_datafields = array(
      'vps_id' => array(
        'fname' => red_t('VPS Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_vps',
        'req' => FALSE 
      ),
      'member_id' => array(
        'fname' => red_t('Member Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_vps',
        'req' => TRUE 
      ),
      'vps_server' => array(
        'fname' => red_t('Server Domain Name'),
        'type' => 'varchar',
        'pcre' => RED_DOMAIN_MATCHER,
        'pcre_explanation' => RED_DOMAIN_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'text_length' => 30,
        'req' => TRUE 
      ),
      'vps_cpu' => array(
        'fname' => red_t('Number of CPUs'),
        'type' => 'int',
        'pcre' => RED_TINYINT_MATCHER,
        'pcre_explanation' => RED_TINYINT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'text_length' => 1,
        'req' => TRUE 
      ),
      'vps_ram' => array(
        'fname' => red_t('Amount of RAM (in GB)'),
        'type' => 'int',
        'pcre' => RED_TINYINT_MATCHER,
        'pcre_explanation' => RED_TINYINT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'text_length' => 2,
        'req' => TRUE 
      ),
      'vps_hd' => array(
        'fname' => red_t('Spinning Disk Size'),
        'type' => 'int',
        'pcre' => RED_BYTES_MATCHER,
        'pcre_explanation' => RED_BYTES_EXPLANATION,
        'description' => red_t("Valid values include: 10b, 100kb, 500mb, or 3gb"),
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'req' => TRUE 
      ),
      'vps_ssd' => array(
        'fname' => red_t('Solid State Disk Size'),
        'type' => 'int',
        'pcre' => RED_BYTES_MATCHER,
        'pcre_explanation' => RED_BYTES_EXPLANATION,
        'description' => red_t("Valid values include: 10b, 100kb, 500mb, or 3gb"),
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'req' => TRUE 
      ),
      'vps_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_explanation' => RED_ACTIVE_DELETED_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE, 
        'tblname' => 'red_vps',
      ),
    );
  }

  function additional_validation() {
    if(!$this->is_admin) {
      $this->set_error(red_t("Sorry, only administrators can create/edit/delete payment records."),'validation');
      return;
    }
    // If the member quota is set, we can't exceed it.
    $member_quota = red_get_member_quota($this->_sql_resource, $this->get_member_id());

    if ($member_quota > 0) {
      $total = red_get_quota_for_all_items($this->_sql_resource, $this->get_member_id()); 
      $total += red_get_quota_for_all_vps($this->_sql_resource, $this->get_member_id(), $this->get_vps_id());

      $diff = $total + intval($this->get_vps_hd()) - $member_quota;

      if ($diff > 0) {
        $quota_over = red_human_readable_bytes($diff);
        $this->set_error(red_t("Setting the hard disk for this VPS exceeds your membership quota by @quota_over", [ '@quota_over' => $quota_over ]),'validation');
      }
    }
  }

  function _pre_commit_to_db() {
    $this->set_vps_hd(red_machine_readable_bytes($this->get_vps_hd()));
    $this->set_vps_ssd(red_machine_readable_bytes($this->get_vps_ssd()));
    return parent::_pre_commit_to_db();
  }

  function get_edit_vps_ssd() {
    // Convert to human readable before displaying.
    $value = red_human_readable_bytes($this->get_vps_ssd());
    $attributes = [ 'class' => 'form-control', 'id' => 'vps_ssd' ];
    $type = 'text';
    return $this->_html_generator->get_input('sf_vps_ssd',$value, $type, $attributes);
  }

  function get_read_vps_ssd() {
    // Convert to human readable before displaying.
    return red_human_readable_bytes($this->get_vps_ssd());
  }

  function get_edit_vps_hd() {
    // Convert to human readable before displaying.
    $value = red_human_readable_bytes($this->get_vps_hd());
    $attributes = [ 'class' => 'form-control', 'id' => 'vps_hd' ];
    $type = 'text';
    return $this->_html_generator->get_input('sf_vps_hd',$value, $type, $attributes);
  }

  function get_read_vps_hd() {
    // Convert to human readable before displaying.
    return red_human_readable_bytes($this->get_vps_hd());
  }
}
