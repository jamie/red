<?php

class red_phone extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'phone_id';
  var $_key_table = 'red_phone';

  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_phone_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_phone_status('active');
      // default country code
      $this->set_phone_country_code(1);
    }

  }

  function get_delete_confirmation_message() {
    return red_t("Are you sure you want to delete the phone number: @phone_number?", array('@phone_number' => $this->get_phone_number()));
  }

  function _get_select_sql_statement() {
    $id = intval($this->get_phone_id());
    return $this->_get_initialize_sql($id);
  }
  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_phone ". 
      "WHERE phone_id = " . $id;
    return $sql;
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_phone_status('deleted');
  }

  var $_phone_id;
  function set_phone_id($value) {
    $this->_phone_id = $value;
  }
  function get_phone_id() {
    return $this->_phone_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }
  function get_member_id() {
    return $this->_member_id;
  }

  var $_phone_number;
  function set_phone_number($value) {
    $this->_phone_number = preg_replace('/\D/','',$value);
  }
  function get_phone_number() {
    return $this->_phone_number;
  }

  var $_phone_country_code;
  function set_phone_country_code($value) {
    $this->_phone_country_code = $value;
  }
  function get_phone_country_code() {
    return $this->_phone_country_code;
  }


  var $_phone_status;
  function set_phone_status($value) {
    $this->_phone_status = $value;
  }
  function get_phone_status() {
    return $this->_phone_status;
  }

  var $_phone_modified;
  function set_phone_modified($value) {
    $this->_phone_modified = $value;
  }
  function get_phone_modified() {
    return $this->_phone_modified;
  }

  var $_phone_type;
  function set_phone_type($value) {
    $this->_phone_type = $value;
  }
  function get_phone_type() {
    return $this->_phone_type;
  }

  function _set_datafields() {
    $this->_datafields = array(
      'phone_id' => array(
        'fname' => red_t('Phone Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_phone',
        'req' => FALSE 
      ),
      'member_id' => array(
        'fname' => red_t('Member Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_phone',
        'req' => FALSE 
      ),
      'phone_country_code' => array(
        'fname' => red_t('Country Code'),
        'type' => 'int',
        'pcre' => RED_INT_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_phone',
        'text_length' => 2,
        'req' => TRUE
      ),
      'phone_number' => array(
        'fname' => red_t('Phone Number (including area code)'),
        'type' => 'int',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_phone',
        'text_length' => 12,
        'req' => TRUE
      ),
      'phone_type' => array(
        'fname' => red_t('Type'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_phone',
        'text_length' => 10,
        'req' => FALSE 
      ),
      'phone_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_explanation' => RED_ACTIVE_DELETED_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE, 
        'tblname' => 'red_phone',
      ),
    );
  }

  function get_phone_number_display() {
    if(empty($this->_phone_number)) return '';

    $country_code = $this->get_phone_country_code();
    $parts = array();
    if($country_code == 1) {
      $parts[] = substr($this->_phone_number, 0,3);
      $parts[] = substr($this->_phone_number, 3,3);
      $parts[] = substr($this->_phone_number, 6,4);
    }
    if($country_code == 52) {
      // see: https://en.wikipedia.org/wiki/Telephone_numbers_in_the_Americas#Mexico
      if(preg_match('/55|81|33/', $this->_phone_number)) {
        $parts[] = substr($this->_phone_number, 0,2);
        $parts[] = substr($this->_phone_number, 2,4);
        $parts[] = substr($this->_phone_number, 6,4);
      } else {
        $parts[] = substr($this->_phone_number, 0,3);
        $parts[] = substr($this->_phone_number, 3,3);
        $parts[] = substr($this->_phone_number, 6,4);
      }
    }
    return implode('-',$parts);
  }
  
  function get_edit_phone_number() {
    return $this->_html_generator->get_input('sf_phone_number', $this->get_phone_number_display()); 
  }

  function get_read_phone_number() {
    return $this->get_phone_number_display(); 
  }
}
