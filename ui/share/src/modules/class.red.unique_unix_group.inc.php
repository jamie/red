<?php

class red_unique_unix_group extends red_ado {

  var $_unique_unix_group_id;
  var $_unique_unix_group_name;
  var $_unique_unix_group_status;
  var $_key_table = 'red_unique_unix_group';
  var $_key_field = 'unique_unix_group_id';

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_unique_unix_group_status('deleted');
  }
  function _get_select_sql_statement() {
    $id = intval($this->get_unique_unix_group_id());
    return $this->_get_initialize_sql($id);
  }

  function _get_initialize_sql($id) {
    $id = intval($id);
    $sql = "SELECT * FROM red_unique_unix_group ". 
      "WHERE unique_unix_group_id = " . $id;
    return $sql;
  }
  function set_unique_unix_group_id($value) {
    $this->_unique_unix_group_id = $value;
  }

  function get_unique_unix_group_id() {
    return $this->_unique_unix_group_id;
  }

  function get_unique_unix_group_name() {
    return $this->_unique_unix_group_name;
  }

  function set_unique_unix_group_name($value) {
    $this->_unique_unix_group_name = $value;
  }

  function set_unique_unix_group_status($value) {
    $this->_unique_unix_group_status = $value;
  }

  function get_unique_unix_group_status() {
    return $this->_unique_unix_group_status;
  }

  function __construct($construction_options) {
    parent::__construct($construction_options);
    $this->_set_datafields();
    if(empty($this->_unique_unix_group_id)) 
      $this->set_unique_unix_group_status('active');

  }

  function _set_datafields() {
    $this->_datafields = array(
      'unique_unix_group_id' => array(
        'type' => 'int',
        'fname' => red_t('Unique Unix Group ID'),
        'pcre' => RED_ID_MATCHER,
        'req' => FALSE,
        'user_insert' => FALSE,
        'user_visible' => TRUE,
        'user_update' => FALSE,
        'tblname' => 'red_unique_unix_group',
      ),
      'unique_unix_group_name' => array(
        'type' => 'varchar',
        'pcre' => RED_UNIX_GROUP_MATCHER,
        'pcre_explanation' => RED_UNIX_GROUP_EXPLANATION,
        'fname' => red_t('Login name'),  // group name is "login" to users
        'req' => TRUE,
        'user_insert' => TRUE,
        'user_visible' => TRUE,
        'user_update' => FALSE,
        'tblname' => 'red_unique_unix_group',
      ),
      'unique_unix_group_status' => array(
        'type' => 'varchar',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_EXPLANATION' => RED_ACTIVE_DELETED_EXPLANATION,
        'fname' => red_t('Status'),  // group name is "login" to users
        'req' => TRUE,
        'user_insert' => FALSE,
        'user_visible' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_unique_unix_group',
      )
    );
  }
  function additional_validation() {
    // skip validation on delete
    if($this->_delete) return true;

    if($this->unix_group_name_reserved()) {
      $this->set_error(red_t("That login is reserved."),'validation');
    }

    if(!$this->unix_group_name_unique()) {
      $this->set_error(red_t("The login is already taken."),'validation');
    }
    if(count($this->get_errors('validate')) != 0) return false;
    return true;
  }

  // Make sure the group name is not reserved 
  function unix_group_name_reserved() {
    $group = $this->get_unique_unix_group_name();
    $reserved = array('root','daemon','bin','sys','sync','games','man','lp',
                'mail','new','uucp','proxy','www-data','backup','list','irc','gnats',
                'nobody','identd','sshd','postfix','gdm','messagebus',
                'mysql','guest','postgres','ogo','fetchmail','dnslog','dnscache',
                'tinydns','axfrdns','otrs','zope','clamav','nagios','amavis',
                'ftp','mayfirst','vmail','asterisk','awstats');
    if(in_array($group,$reserved)) return true;
    return false;
  }

  function unix_group_name_unique() {
    // make sure it's not a unix group
    $unix_group_name = $this->get_unique_unix_group_name();
    $sql = "SELECT unique_unix_group_id FROM red_unique_unix_group ".
      "WHERE unique_unix_group_name = '$unix_group_name' AND ".
      "unique_unix_group_status = 'active'";
    $result =  $this->_sql_query($sql);
    if($this->_sql_num_rows($result) != 0) return false;

    // make sure it's not being used as a user_account
    $sql = "SELECT item_id FROM red_item_user_account ".
      "JOIN red_item USING(item_id) WHERE user_account_login ".
      "  = '$unix_group_name' AND red_item.item_status != ".
      "'deleted'";
    $result =  $this->_sql_query($sql);
    if($this->_sql_num_rows($result) != 0) return false;

    return true;
  }

  
}

?>
