<?php
  
class red_area_member extends red_area {
  var $default_service_id = 12; // hosting_order 
  var $unit_friendly_name;
  var $unit_key_field = 'member_id';
  var $unit_friendly_field = 'member_friendly_name';
  var $unit_table = 'red_member';
  var $service_key_field = 'hosting_order_id';

  function __construct($area,$construction_options) {
    parent::__construct($area, $construction_options);
    $this->unit_friendly_name = red_t('Members');
  }

  function &get_single_object($co) {
    // See if we are dealing with an "item" (which is lives in red_item)
    // or an object independent of the red_item table.
    $service_id = intval($co['service_id']);
    $sql = "SELECT service_item FROM red_service WHERE service_id = $service_id";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    $item = $row[0] == 1 ? TRUE: FALSE;
    if (!$item) {
      $file_name = 'class.' . str_replace('red_','red.',$this->service_table) . '.inc.php';
      $class_name = $this->service_table;
      require_once($co['src_path'] . "/modules/$file_name");
      $ret = new $class_name($co);
    }
    else {
      $ret =& red_item::get_red_object($co);
      if (!$ret) {
        print_r($co);
      }
      $ret->set_hosting_order_id(0);
    }
      
    // planned payments and payments and vps need to know if the user is an admin or not
    if(property_exists($ret, 'is_admin')) {
      $ret->is_admin = $this->is_admin;
    }

    return $ret;
  }

  function get_tags() {
    $member_id = intval($this->get_unit_id());
    $sql = "SELECT tag_id, tag FROM red_tag WHERE member_id = $member_id AND tag_status = 'active'";
    $result = $this->_sql_query($sql);
    $ret = array();
    while($row = $this->_sql_fetch_row($result)) {
      $tag_id = $row[0];
      $tag = $row[1];
      $ret[$tag_id] = $tag;
    }
    return $ret;
  }

  function get_area_navigation_items($top,$member,$hosting_order) {
    $ret = array();
      
    $items = array();
    if($top) {
      $items['top'] = array(
        'friendly' => 'Top',
      );
    }
    if($member) {
      $edit = null;
      if($this->is_admin) {
        $attr = array('href' => '?area=top&action=edit&member_id=' .  
                $this->get_unit_id(), 'class' => 'button tiny round');
        $content = red_t("edit");
        $edit = $this->html_generator->get_tag('a',$content,$attr);
        $edit = ' ' . $this->html_generator->get_tag('sup',$edit);
      }
      $items['member'] = array(
        'friendly' => $this->unit_active_friendly_name . $edit, 
        'field' => 'member_id',
        'id' => $this->get_unit_id(),
      );
    }
    if($hosting_order) {
      $items['hosting_order'] = array(
        'friendly' => $this->get_first_hosting_order_friendly_name(),
        'field' => 'hosting_order_id',
        'id' => $this->get_first_hosting_order_id(),
      );
    }

    return $items;
  }

  function get_first_hosting_order_friendly_name() {
    $sql = "SELECT hosting_order_name FROM red_member JOIN red_hosting_order " .
      "USING(member_id) WHERE member_id = " . intval($this->get_member_id()) . " AND " .
      "(hosting_order_status = 'active' OR hosting_order_status = 'disabled') ORDER BY hosting_order_identifier LIMIT 1";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    if(empty($row[0])) return 'Hosting Orders';
    return $row[0];
  }

  function get_first_hosting_order_id() {
    $sql = "SELECT red_hosting_order.hosting_order_id FROM red_member JOIN red_hosting_order " .
      "USING(member_id) WHERE member_id = " . intval($this->get_member_id()) . " AND " .
      "(hosting_order_status = 'active' OR hosting_order_status = 'disabled') ORDER BY hosting_order_identifier LIMIT 1";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    if(empty($row[0])) return null;
    return $row[0];
  }

  function add_new_item_link() {
    if($this->service_key_field == 'payment_id') {
      // Only admins can create payments, everyone else can go to the link.
      if(!$this->is_admin) {
        $this->template->set_add_new_item_link('inline-block', 'https://members.mayfirst.org/dues');
        return;
      }
    } 
    elseif($this->service_key_field == 'vps_id') {
      // Only admins can create vps.
      if(!$this->is_admin) {
        $this->template->set_add_new_item_link('none');
        return;
      }
    }
    parent::add_new_item_link();
  }

  function list_children($start = 0,$limit = 0) {
    $this->prepare_list();
    $this->template->set_file('child_items_file','child_items.ihtml');
    $this->template->set_var('list_block',$this->get_list_block($start,$limit));
    $this->template->parse('children','children_file');
    $this->template->parse('body_block','child_items_file');
  }

  function get_member_id(){
    return $this->unit_id;
  }

  function get_list_sql($start = false, $limit = false) {
    $member_id = $this->get_member_id();
    $order_by = '';
    if(!is_null($this->order_by)) $order_by = "ORDER BY " . $this->order_by;
    $status_field = str_replace('_id','_status',$this->service_key_field);
    $status_clause = "AND $status_field = 'active' ";

    $join_table = null;
    $select = $this->service_table . ".*";
    // a few anoying exceptions...
    if($this->service_key_field == 'invoice_id') {
      $status_clause = "AND invoice_status != 'eaten' AND invoice_status != 'void'";
    } elseif($this->service_key_field == 'correspondence_id') {
      $status_clause = '';
    } elseif($this->service_key_field == 'map_user_member_id') {
      $status_clause = "AND status = 'active'";
    } elseif($this->service_key_field == 'postal_address_id') {
      $status_clause = "AND postal_address_status = 'active'";
    } elseif($this->service_key_field == 'planned_payment_id') {
      $join_table = " LEFT JOIN red_invoice USING(invoice_id) ";
    } elseif($this->service_key_field == 'payment_id') {
      $join_table = " LEFT JOIN red_invoice USING(invoice_id) ";
    } elseif($this->service_key_field == 'hosting_order_id') {
      $status_clause = "AND ($status_field = 'active' OR $status_field = 'disabled') ";
    } elseif($this->service_key_field == 'item_id') {
      $join_table = " JOIN red_item USING(item_id)";
      $select = '*';
      $status_clause = "AND $status_field != 'deleted' ";
    }
    
    $sql = "SELECT " . $select . " FROM " . $this->service_table . $join_table . " " .
      "WHERE member_id = $member_id $status_clause $order_by ".
      "LIMIT $start, $limit";
    return $sql;
  }

  function post_user_input_modifications(&$obj) {
    if(empty($obj->member_id)) {
      $obj->set_member_id($this->unit_id);
    }
  }

  function get_table_row_for_single_object(&$object,$row_number) {
    $table_row = array();

    // see if this is an invoice, if so we will try to append payments
    if($this->service_id == 17) {
      $js_includes = $object->get_javascript_includes();
      $this->template->add_js($js_includes);

      $invoice_id = intval($object->get_invoice_id());
      $sql = "SELECT * FROM red_payment WHERE invoice_id = $invoice_id AND payment_status = 'active'";
      $result = $this->_sql_query($sql);
      if($this->_sql_num_rows($result) > 0) {
        $co = $this->_construction_options;
        require_once($co['src_path'] . '/modules/class.red.payment.inc.php');
        if(array_key_exists('id',$co)) unset($co['id']);
        $payment_info = '';
        while($row = $this->_sql_fetch_assoc($result)) {
          $co['rs'] = $row;
          // red_set_message($row);
          $payment = new red_payment($co);
          // set the view payment links proprety on the invoice object being passed in, so it displays with a plus sign
          // which, if clicked, will display the payments we are building now.
          $object->display_view_payments_link = true;
          if(!$payment) {
            red_set_message(red_t("Failed to create payment object for invoice: @invoice_id.",array('@invoice_id' => $invoice_id)));
          } else {
            $payment_info .= $payment->get_brief_payment_info_as_string() . "<br />\n";
              
          }
        }
        $rows = array(
          array('value' => '&nbsp;'),
          array('value' => '&nbsp;'),
          array('value' => '&nbsp;'), 
          array('value' => '&nbsp;'),
          array('value' => $payment_info)
        );
        $row = $this->html_generator->get_table_cells($rows);
        $attr = array('class' => 'red-payment-row','id' => 'red-payment-row-' . $object->get_invoice_id());
        $table_row[] = $this->html_generator->get_table_row($rows,$attr);
      }
    }

    // call parent to build the row
    $ret = parent::get_table_row_for_single_object($object,$row_number);
    if(!$ret) return $ret;
    return array_merge($ret,$table_row);
  }

  function get_list_links($key_field_name,$id, $status = null) {
    $ret = '';
    if($key_field_name == 'hosting_order_id')
      $ret .= $this->get_list_view_link('hosting_order',$key_field_name,$id);
    $ret .= $this->get_list_edit_link($key_field_name,$id);
    if($status == 'active') {
      $ret .= $this->get_list_disable_link($key_field_name,$id);  
    } elseif($status == 'disabled') {
      $ret .= $this->get_list_enable_link($key_field_name,$id);  
    }
    $ret .= $this->get_list_delete_link($key_field_name,$id);  
    if($key_field_name == 'invoice_id' || $key_field_name == 'payment_id') {
      $ret .= $this->get_list_print_link($key_field_name, $id);
    }
    if($key_field_name == 'invoice_id') {
      $co = $this->_construction_options;
      require_once($co['src_path'] . '/modules/class.red.invoice.inc.php');
      if(!red_invoice::is_paid($id, $co['sql_resource'])) {
        $ret .= $this->get_list_pay_link($key_field_name, $id); 
      }
    } 
    return $ret;
  }

  function get_list_pay_link($key_field_name, $id) {
    // $id is the invoice id but we want the member_id
    $invoice_id = intval($id);
    $sql = "SELECT member_id FROM red_invoice WHERE invoice_id = $invoice_id";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    $id = $row[0];
    $edit_url = 'https://members.mayfirst.org/dues?member_id=' . $id; 
    $edit_attr = array('href' => $edit_url, 'target' => '_blank', 'class' => 'btn btn-xs btn-info');
    return $this->html_generator->get_tag('a',red_t('pay online'),$edit_attr);
  }

  function get_list_print_link($key_field_name, $id) {
    $edit_url = $this->template->self_url . 
      "?area=reports&amp;$key_field_name=$id";
    $edit_attr = array('href' => $edit_url, 'target' => '_blank', 'class' => 'btn btn-xs btn-info');
    return $this->html_generator->get_tag('a',red_t('print'),$edit_attr);
  }

  function get_list_edit_link($key_field_name,$id) {
    // only show edit if you are an admin
    if($key_field_name == 'invoice_id' && !$this->is_admin) return '';
    if($key_field_name == 'payment_id' && !$this->is_admin) return '';
    if($key_field_name == 'vps_id' && !$this->is_admin) return '';
    if($key_field_name == 'correspondence_id') {
      // convert to a "resend" link  
      $edit_url = $this->template->constructed_self_url . 
        "&amp;action=edit&amp;$key_field_name=".$id;
      $edit_attr = array('href' => $edit_url, 'class' => 'btn btn-xs btn-info');
      return $this->html_generator->get_tag('a',red_t('view/resend'),$edit_attr);
    }

    return parent::get_list_edit_link($key_field_name,$id);
  }

  function get_list_delete_link($key_field_name,$id) {
    // only show edit if you are an admin
    if($key_field_name == 'invoice_id' && !$this->is_admin) return '';
    if($key_field_name == 'payment_id' && !$this->is_admin) return '';
    if($key_field_name == 'vps_id' && !$this->is_admin) return '';
    // nobody can delete correspondence
    if($key_field_name == 'correspondence_id') return ''; 
    return parent::get_list_delete_link($key_field_name,$id);
  }

  function set_available_units() {
    if(!is_null($this->available_units)) return;
    $ret = array();
    $sql = $this->get_available_units_sql();
    $result = $this->_sql_query($sql);
    if($this->_sql_num_rows($result) > 0) {
      $ret = $this->_get_members_for_sql_result($result);    
    }
    $this->available_units = $ret;
  }

  function get_available_units_sql() {
    $is_admin = $this->is_admin;
    $user_name = addslashes($this->user_name);
    $sql_limit = '';
    $where = '';
    $extra = '';

    // common sql statement
    $sql_start = "SELECT red_member.member_id,member_friendly_name, ".
      "member_status FROM red_member ";
    $sql_where = "WHERE member_status = 'active' ";

    $sql_server_union = '';
    $sql_access_where = '';
    if(!$is_admin) {
      $sql_limit = "JOIN red_map_user_member USING (member_id) ";
      $sql_access_where = " AND login = '$user_name' ";

      // build additional sql statement to pull in via server access - if you have access to a 
      // server, you should have access to all members coded to that server access record regardless of
      // whether you have explicit member access
      $sql_join_server_access = ",red_map_user_server  ";
      $sql_where_additional_access = "$sql_where AND login = '$user_name' AND status = 'active' AND ".
        "(red_map_user_server.member_id = red_member.member_parent_id OR red_map_user_server.member_id = red_member.member_id)";
      $sql_server_union = "UNION ($sql_start $sql_join_server_access $sql_where_additional_access) ";
    }

    $extra = "ORDER BY member_friendly_name";
    return "($sql_start $sql_limit $sql_where $sql_access_where) $sql_server_union $extra";

  }

  function _get_members_for_sql_result($result) {
    $ret = array();
    while($row = $this->_sql_fetch_row($result)) {
      $id = $row[0];  
      $member_friendly_name = $row[1];  
      $status = $row[2];  
      $ret[$id] = "$member_friendly_name ($status)";
    }
    return $ret;
  }

  function check_access($id = null, $action = null) {
    if(!$this->check_unit_access()) return false;

    // Local DNS, Tags and Notes can only be accessed by admins
    $admin_only_service_ids = array(33);
    if(!$this->is_admin && ($this->service_table == 'red_tag' || $this->service_table == 'red_note' || in_array($this->service_id, $admin_only_service_ids))) {
      red_set_message("You cannot access tags, notes, or local DNS.");
      return false;
    }

    if(is_null($id)) $id = $this->id;

    global $globals;
    $object = preg_replace('/^red_/', '', $this->service_table);
    if(!empty($action) && $action != 'print') {
      // They are trying to write an action. Make sure they have write access.
      if(!$globals['authz']->check_insert_perms($object, $this->unit_id)) {
        return FALSE;
      }
    }
    
    // If they're not checking an id, and they have unit access from above, then
    // let them in. They are just getting a listing. 
    if(empty($id)) {
      return TRUE;
    }

    // Make sure they have read-access to this id.
    $id = intval($id);
      
    // See if this is a service item (e.g. it has a record in the red_item table).
    $service_id = intval($this->service_id);
    $sql = "SELECT service_item FROM red_service WHERE service_id = $service_id";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    $service_item = $row[0];

    $sql = "SELECT member_id FROM " . $this->service_table . " WHERE " . $this->service_key_field . " = $id";
    if($this->service_table == 'red_planned_payment' || $this->service_table == 'red_payment') {
      $sql = "SELECT member_id FROM " . $this->service_table . " JOIN red_invoice USING(invoice_id) WHERE " . $this->service_key_field . " = $id";
    }
    if ($service_item == 1) {
      $sql = "SELECT member_id FROM " . $this->service_table . " JOIN red_item USING(item_id) WHERE " . $this->service_key_field . " = $id";
    }

    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    if($this->unit_id != $row[0]) {
      $this->errors[2] = "There is an error. The " . $this->service_key_field . " ($id) does not seem to match the member id " . $this->unit_id;
      return false;
    }

    return TRUE; 
  }
}


?>
