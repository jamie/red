<?php
  
class red_area_hosting_order extends red_area {
  var $default_service_id = 1; // user accounts
  var $unit_friendly_name;
  var $unit_key_field = 'hosting_order_id';
  var $unit_friendly_field = 'hosting_order_name';
  var $unit_table = 'red_hosting_order';
  var $service_key_field = 'item_id';

  function __construct($area, $construction_options) {
    parent::__construct($area, $construction_options);
    $this->unit_friendly_name = red_t('Hosting Orders');
  }

  function get_primary_host() {
    $sql = "SELECT hosting_order_host FROM red_hosting_order WHERE ".
      "hosting_order_id = " . $this->unit_id;
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return $row[0];
  }

  function get_tags() {
    $member_id = intval($this->get_member_id());
    $sql = "SELECT tag_id, tag FROM red_tag WHERE member_id = $member_id AND tag_status = 'active'";
    $result = $this->_sql_query($sql);
    $ret = array();
    while($row = $this->_sql_fetch_row($result)) {
      $tag_id = $row[0];
      $tag = $row[1];
      $ret[$tag_id] = $tag;
    }
    return $ret;
  }

  function get_member_friendly_name() {
    $sql = "SELECT member_friendly_name FROM red_member JOIN red_hosting_order " .
      "USING(member_id) WHERE hosting_order_id = " . intval($this->unit_id) . " AND " .
      "member_status = 'active'";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return $row[0];
  }

  function get_member_id() {
    $sql = "SELECT member_id FROM red_member JOIN red_hosting_order " .
      "USING(member_id) WHERE hosting_order_id = " . intval($this->unit_id) . " AND " .
      "member_status = 'active'";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return $row[0];
  }

  function get_member_quota() {
    $member_id = intval($this->get_member_id());
    $sql = "SELECT member_quota FROM red_member WHERE member_id = $member_id";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    if ($row[0] == 0) {
      return 'Not set';
    }
    return $row[0] . ' GB';
  }

  function get_member_benefits_level() {
    $sql = "SELECT member_benefits_level FROM red_member JOIN red_hosting_order " .
      "USING(member_id) WHERE hosting_order_id = " . intval($this->unit_id);
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return $row[0];
  }
  function get_area_navigation_items($top,$member,$hosting_order) {
    $ret = array();
      
    $items = array();
    if($top) {
      $items['top'] = array(
        'friendly' => 'Top',
      );
    }
    if($member) {
      $items['member'] = array(
        'friendly' => $this->get_member_friendly_name(), 
        'field' => 'member_id',
        'id' => $this->get_member_id(),
      );
    }
    if($hosting_order) {
      $items['hosting_order'] = array(
        'friendly' => $this->unit_active_friendly_name,
        'field' => 'hosting_order_id',
        'id' => $this->get_unit_id(),
      );
    }

    return $items;
  }

  function list_children($start = 0,$limit = 0) {
    $this->prepare_list();
    $this->template->set_file('child_items_file','child_items.ihtml');
    $this->template->set_file('area_navigation_file','area_navigation.ihtml');
    $this->template->set_var('lang_primary_host_is',red_t("Your primary host is:"));
    $this->template->set_var('lang_primary_email_address_is',red_t("Your primary email address is:"));
    $this->template->set_var('primary_user_account_login',$this->get_primary_user_account_login());
    $primary_host = $this->get_primary_host();
    $this->template->set_var('primary_host',$primary_host);
    $this->template->set_var('primary_host_ip',red_get_ip_for_host($primary_host,$this->_sql_resource));
    $this->template->set_var('lang_benefits_level_is',red_t("Your benefits level is:"));
    $benefits_level = $this->get_member_benefits_level();
    if($benefits_level != 'basic') {
      $this->template->set_var('display_if_basic_membership', 'display:none;');
    }
    $this->template->set_var('benefits_level',$benefits_level);

    $this->template->set_var('lang_member_quota_is',red_t("Your member quota is:"));
    $member_quota = $this->get_member_quota();
    $this->template->set_var('member_quota',$member_quota);
      
    $this->template->set_var('list_block',$this->get_list_block($start,$limit));
    $this->template->parse('children','children_file');
    $this->template->parse('body_block','child_items_file');
  }

  // For basic memberships, we should have only one user account login.
  // This function fetches it.
  function get_primary_user_account_login() {
    $hosting_order_id = intval($this->unit_id);
    $sql = "SELECT user_account_login FROM red_item JOIN red_item_user_account USING(item_id) ".
      "WHERE item_status = 'active' AND hosting_order_id = $hosting_order_id LIMIT 1";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    return $row[0];
  }

  function get_table_row_for_single_object(&$object,$row_number) {
    $table_row = array();

    $ret = parent::get_table_row_for_single_object($object,$row_number);
    if(!$ret) return $ret;

    $status = $object->get_item_status();
    $key_field = '_' . $object->_key_field;
    if(preg_match('/.*error$/',$status) > 0) {
      $table_row[] = $this->get_item_error_row($object->$key_field);
    }
    return array_merge($ret,$table_row);
  }

  function &get_single_object($co) {
    $ret =& red_item::get_red_object($co);
    // this is less than elegant - not sure how to do this better
    if(method_exists($ret,'set_default_hosting_order_id')) {
      $ret->set_default_hosting_order_id($this->config['list_default_hosting_order_id']);
    }
    if(array_key_exists('server_url_map',$this->config) && property_exists($ret,'server_url_map')) {
      $ret->server_url_map = $this->config['server_url_map'];
    }
    if(property_exists($ret, 'is_admin')) {
      $ret->is_admin = $this->is_admin;
    }
    return $ret;
  }

  function set_available_units() {
    if(!is_null($this->available_units)) return;
    $is_admin = $this->is_admin;

    $ret = array();
    $user_name = addslashes($this->user_name);

    // get list of all hosting orders
      
    // admin sql statement
    $sql_select ="SELECT member_friendly_name,".
      "red_hosting_order.hosting_order_id,hosting_order_identifier,".
      "hosting_order_status ";

    $sql_from = "FROM red_hosting_order JOIN red_member USING ".
      "(member_id) ";

    $sql_member_union = '';
    $sql_server_union = '';

    $sql_where = "WHERE (hosting_order_status = 'active' OR hosting_order_status = 'disabled') ";

    $sql_join = '';
    $sql_extra = '';
    $sql_access_where = '';
    if(!$is_admin)  {
      $sql_join = "JOIN red_item USING(hosting_order_id) JOIN ".
        "red_item_hosting_order_access USING(item_id) ";
      $sql_access_where = "AND hosting_order_access_login = '$user_name' AND ".
        "red_item.item_status != 'deleted'";

      // build additional sql statement to pull in via member access - if you have access to a 
      // member, you should have access to all hosting orders coded to that member regardless of
      // whether you have explicit hosting order access
      $sql_join_member = "JOIN red_map_user_member USING(member_id) ";
      $sql_access_where_member = "$sql_where AND login = '$user_name' AND status = 'active'";
      $sql_member_union = "UNION ($sql_select $sql_from $sql_join_member $sql_access_where_member) ";

      // build additional sql statement to pull in via server access - if you have access to a 
      // server, you should have access to all hosting orders coded to that member regardless of
      // whether you have explicit hosting order access
      $sql_join_server = ",red_map_user_server ";
      $sql_access_where_server = "$sql_access_where_member AND (red_member.member_id = red_map_user_server.member_id OR ".
        "red_member.member_parent_id = red_map_user_server.member_id) ";
      $sql_server_union = "UNION ($sql_select $sql_from $sql_join_server $sql_access_where_server) ";

    }

    $sql_extra = "ORDER BY member_friendly_name,hosting_order_identifier ";

    $sql = '(' . $sql_select . $sql_from . $sql_join . $sql_where . 
      $sql_access_where .  ') ' . $sql_member_union . $sql_server_union . $sql_extra;
    $result = $this->_sql_query($sql);
    if($this->_sql_num_rows($result) > 0) {
      $ret = $this->_get_hosting_orders_for_sql_result($result);    
    }

    // Now add hosting orders for members in which this person has member
    // access. If you have access to a membership, you have access to all
    // hosting orders in that membership
    if(!$is_admin) {
      $sql = $sql_select . "FROM red_member JOIN red_map_user_member USING " .
        "(member_id) WHERE login = '$user_name'";
      $result = $this->_sql_query($sql);
      if($this->_sql_num_rows($result) > 0) {
        $ret = array_merge($ret,$this->_get_hosting_orders_for_sql_result($result));  
      }
    }
    $this->available_units = $ret;
  }

  function _get_hosting_orders_for_sql_result($result) {
    $ret = array();
    while($row = $this->_sql_fetch_row($result)) {
      $member_friendly_name = $row[0];  
      $id = $row[1];  
      $identifier = $row[2];  
      $status = $row[3];  
      $ret[$id] = "$member_friendly_name: $identifier ($status)";
    }
    return $ret;
  }

  function check_access($id = null, $action = null) {
    if(!$this->check_unit_access()) return false;

    if(is_null($id)) $id = $this->id;

    global $globals;
    $object = preg_replace('/^red_/', '', $this->service_table);
    if(!empty($action) && $action != 'print') {
      // They are trying to write an action. Make sure they have write access.
      if(!$globals['authz']->check_insert_perms('item', $this->unit_id)) {
        // The only exception is if they are editing their own user account record.
        if($object == 'item_user_account' && $id == $globals['authz']->get_user_name_item_id()) {
          return TRUE;
        } 
        return FALSE;
      }
    }

    // if they're not checking an id, return true
    if(empty($id)) return true;

    // make sure this id reallly belongs to the hosting order
    $id = intval($id);
    $sql = "SELECT hosting_order_id FROM red_item WHERE item_id = $id";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    if($this->unit_id != $row[0]) {
      $msg = red_t("There is an error. The item id @id does not seem to match the " .
             "hosting order id @unit_id",array('@id' => $id, '@unit_id' => $this->unit_id));
      $this->errors[2] = $msg;
      return false;
    }
    return true;
  }
    
  function get_list_edit_link($key_field_name,$id) {
    // rename edit link for password reset
    if($this->service_id == '19') {
      $edit_url = $this->template->constructed_self_url . 
        "&amp;action=edit&amp;$key_field_name=".$id;
      $edit_attr = array('href' => $edit_url, 'class' => 'btn btn-primary btn-xs');
      return $this->html_generator->get_tag('a','resend',$edit_attr);
    }
    return parent::get_list_edit_link($key_field_name,$id);
  }  
}


?>
