<?php

class red_oauth {
  // NOTE: this works:
  private $server = NULL;
  // $consent, aka request_id, is the value assigned to this consent request.
  private $consent = NULL;
  private $consent_decision = NULL;
  private $consent_email = NULL;
  private $access_token = NULL;
  private $client_id = NULL;
  private $client_secret = NULL;
  private $redirect_url = NULL;
  private $ch = NULL;
  private $template = NULL;
  private $html_generator = NULL;
  private $user_name = NULL;

  function set_user_name($user_name) {
    $this->user_name = $user_name;
  }

  function set_credentials($client_id, $client_secret) {
    $this->client_id = $client_id;
    $this->client_secret = $client_secret;
  }

  function set_server($server) {
    $this->server = $server;
  }

  function set_template($template) {
    $this->template =& $template;
  }

  function set_html_generator($html_generator) {
    $this->html_generator =& $html_generator;
  }

  function set_user_input($user_input) {
    $possible_values = array(
      'consent',
      'consent_decision',
      'consent_email'
    );
    foreach ($possible_values as $value) {
      if (array_key_exists($value, $user_input)) {
        $this->$value = $user_input[$value];
      }
    }
  }

  function send_request($options) {
    $ch = curl_init(); 
    curl_setopt_array($ch, $options);
    $ret = curl_exec($ch);
    unset($ch);
    return $ret;
  }

  function run() {
    if (empty($this->consent)) {
      red_set_message(red_t("Failed to get the consent request id."), 'error');
      return FALSE;
    }
    if (!$this->set_token()) return FALSE;
    if (!$this->set_redirect_url()) return FALSE;
    if (empty($this->consent_decision)) {
      $this->prompt_user();
    }
    else {
      if ($this->consent_decision == 'accept') {
        $this->accept();
        header('Location: '. $this->redirect_url);
      }
      else {
        $this->reject();
        red_set_message(red_t("The request has been rejected."), 'error');
      }
    }
  }

  function prompt_user() {
    $this->template->set_file('oauth_file','oauth.ihtml');
    $this->template->set_var('lang_auth_request', red_t("Authorization request"));
    $this->template->set_var('lang_consent_explanation', red_t("The May First/People Link Discourse web site is asking you to confirm your identity. Do you accept?"));
    $this->template->set_var('lang_your_decision', red_t("Please indicate if you accept or reject this request."));

    $attributes = array('class' => 'form-control');
    $email_block = $this->html_generator->get_form_element('consent_email', '', 'input', $attributes);
    $accept_options = array(
      'accept' => red_t("Accept"),
    );
    $reject_options = array(
      'reject' => red_t("Reject"),
    );
    $consent_accept_input = $this->html_generator->get_radio('consent_decision', $accept_options, TRUE);
    $consent_reject_input = $this->html_generator->get_radio('consent_decision', $reject_options, NULL);
    $this->template->set_var('consent_email_input', $email_block);
    $this->template->set_var('consent_accept_input', $consent_accept_input);
    $this->template->set_var('consent_reject_input', $consent_reject_input);
    $this->template->set_var('session_id',session_id());
    $this->template->set_var('consent',$this->consent);
    $this->template->parse('body_block','oauth_file');
  }
  
  function set_token() {
    $data = 'scope=hydra.consent&grant_type=client_credentials&client_id=' . $this->client_id;
    $options = array(
      CURLOPT_URL => $this->server . 'oauth2/token',
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_SSL_VERIFYPEER => TRUE,
      CURLOPT_POST => TRUE,
      CURLOPT_USERPWD => $this->client_id . ':' . $this->client_secret,
      CURLOPT_POSTFIELDS => $data,
    );

    $info = json_decode($this->send_request($options));
    if (!is_object($info) || empty($info->access_token)) {
      red_set_message(red_t("Failed to set token when authorizing you."));
      return FALSE;
    }
    $this->access_token = $info->access_token;
    return TRUE;
  }

  function set_redirect_url() {
    $header = array('Authorization: Bearer ' . $this->access_token);

    $options = array(
      CURLOPT_URL => $this->server . 'oauth2/consent/requests/' . $this->consent,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_SSL_VERIFYPEER => TRUE,
      CURLOPT_HTTPHEADER => $header,
    );

    $info = json_decode($this->send_request($options));
    if (!is_object($info) || empty($info->redirectUrl)) {
      red_set_message(red_t("Failed to set redirect URL when authorizing you."));
      return FALSE;
    }
    red_set_message(print_r($info, TRUE));
    $this->redirect_url = $info->redirectUrl;
    return TRUE;
  }

  function accept() {
    $this->decide('accept');
  }

  function reject() {
    $this->decide('reject');
  }

  function decide($decision) {
    $header = array('Authorization: Bearer ' . $this->access_token);
    $data = json_encode(
      array(
        'subject' => $this->client_id,
        'idTokenExtra' => array(
          //'email' => $this->consent_email,
          //'email_verified' => TRUE,
          'id' => $this->user_name
        ),
      )
    );

    $options = array(
      CURLOPT_URL => $this->server . 'oauth2/consent/requests/' . $this->consent . '/' . $decision,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_SSL_VERIFYPEER => TRUE,
      CURLOPT_SSL_VERIFYHOST => 2,
      CURLOPT_CUSTOMREQUEST => 'PATCH',
      CURLOPT_HTTPHEADER => $header,
      CURLOPT_POSTFIELDS => $data,
    );
    $this->send_request($options); 
  }

  function simple() {
    $this->consent = $_REQUEST['consent'];

    // First get token.
    // `wget --post-data=grant_type=client_credentials --user consent-app --password consent-secret --auth-no-challenge  https://hydra.loc.cx:4444/oauth2/token`
    // `curl --data-raw grant_type=client_credentials --user consent-app:consent-secret https://hydra.loc.cx:4444/oauth2/token`

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->server . 'oauth2/token');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_USERPWD, $this->client_id . ':' . $this->client_secret);
    $data = 'scope=hydra.consent&grant_type=client_credentials&client_id=' . $this->client_id;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $info = json_decode(curl_exec($ch));
    $this->access_token = $info->access_token;

    // Now get info about the request so we know how to redirect when we are done.
    // curl -H "Authorization: Bearer <token>" https://hydra.loc.cx:4444/oauth2/consent/requests/<request-id>
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->server . 'oauth2/consent/requests/' . $this->consent); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    $header = array('Authorization: Bearer ' . $this->access_token);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $info = json_decode(curl_exec($ch));
    $this->redirect_url = $info->redirectUrl;

    // Now accept it.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->server . 'oauth2/consent/requests/' . $this->consent . '/accept'); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
    $header = array('Authorization: Bearer ' . $this->access_token);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $data = json_encode(
      array(
        'subject' => $this->client_id,
        'idTokenExtra' => array(
          'email' => 'joe@example..net',
          'email_verified' => TRUE,
          'id' => $this->user_name,
        ),
      )
    );
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_exec($ch);

    header('Location: '. $this->redirect_url);

  }
}
