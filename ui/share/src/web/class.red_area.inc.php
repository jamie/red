<?php

class red_area extends red_db {
  // general variables
  var $_construction_options;
  var $order_by = null;
  var $html_generator;
  var $template;
  var $area;
  var $config;
  var $filter_keywords;
  var $show_filter_form = true;

  // active ids of objects
  var $service_id = null;
  var $unit_id = null;
  var $id = null;

  // authorization related
  var $available_units = null;
  var $is_admin = false;
  var $user_name = null;

  // error handling
  var $errors = array();

  // override in children
  var $default_service_id;
  var $unit_friendly_name;
  var $unit_key_field;
  var $unit_friendly_field;
  var $unit_table;
  var $service_key_field;

  // auto generated
  var $service_table;
  var $unit_active_friendly_name;

  // used to hold service objects that don't pass validation
  // so the values can be used to populate the form after
  // the error 
  var $invalid_service_object;

  // factory method
  static function &get_red_object($area, $construction_options) {
    $obj = '';
    $service_id = $construction_options['service_id'];
    $sql_resource = $construction_options['sql_resource'];
      
    $src_path = $construction_options['src_path'];
    $resource = $construction_options['sql_resource'];

    if($area == 'hosting_order') {
      $service_table = red_get_service_table_for_service_id($service_id,$sql_resource);
      $construction_options['service_table'] = $service_table;
      require($src_path . '/web/class.red_area_hosting_order.inc.php');
      // Check for child class
      $short_table_name = substr($service_table,9);
      $child_class_name = "red_service_$short_table_name";
      $child_class_file = "$src_path/modules/class.$child_class_name.inc.php";
      if(file_exists($child_class_file)) {
        require_once($child_class_file);
        $ret = new $child_class_name($construction_options);
        return $ret;
      }
      $object_name = 'red_area_hosting_order';
    } else {
      $object_name = 'red_area_' . $area;
      require($src_path . '/web/class.'. $object_name . '.inc.php');
    }
    $obj = new $object_name($area,$construction_options);
    return $obj;
  }

  function __construct($area,$co) {
    $this->area = $area;
    $this->_construction_options = $co;
    $this->_sql_resource = $co['sql_resource'];
    $this->service_id = $co['service_id'];
    if(empty($this->service_id))
      $this->service_id = $this->default_service_id;
    $this->set_service_fields();
  }

  function set_service_fields() {
    $sql = "SELECT service_table, service_order_by FROM red_service ".
      "WHERE service_id = " . intval($this->service_id);
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    $this->service_table = $row[0];
    if(preg_match('/^red_item/',$this->service_table)) {
      // all tables starting with red_item use item_id as the key field
      $this->service_key_field = 'item_id';
    } else {
      // all others use the table name minus the initial red_item plus _id
      $this->service_key_field = substr($this->service_table,4) . '_id';
    }
    $this->order_by = $row[1];
  }

  function get_errors_as_string() {
    return implode(' ',$this->errors);
  }
  function list_children($start, $limit) {
    // override
    return false;

  }

  function set_available_units() {
    // should be overridden in child
    $this->available_units = array();
  }

  function set_unit_id($unit_id = null) {
    if(!empty($unit_id)) {
      $this->unit_id = $unit_id;
    } else {
      $this->auto_generate_unit_id();
      // if it's still empty... pop off the first available
      // unit_id
      if(empty($this->unit_id)) {
        if(!empty($this->available_units)) {
          $keys = array_keys($this->available_units);
          $this->unit_id = array_shift($keys);
        }
      }
    }
  }

  function auto_generate_unit_id() {
    // if the unit_id (member_id or hosting_order_id) is empty, and id 
    // is passed, auto generate the {member,hosting_order}_id 
    if(!empty($this->id)) {
      $id = intval($this->id);
      $service_field = addslashes($this->service_key_field);
      $service_table = addslashes($this->service_table);
      $unit_field = addslashes($this->unit_key_field);
      $unit_table = addslashes($this->unit_table);

      if(empty($service_field) || empty($service_table) || empty($unit_field) ||
        empty($unit_table)) return;

      // exception for red_items... we want to lookup in the red_item table,
      // not the red_item_user_account, etc. table
      if(preg_match('/^red_item/',$service_table)) $service_table = 'red_item';

      $sql = "SELECT $unit_field FROM $unit_table JOIN $service_table ".
        "USING($unit_field) WHERE $service_field = $id";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      $this->unit_id = $row[0];
    }
  }

  function check_access($id = null, $action = null) {
    //override in child
    return false;
  }

  function get_single_object($co) {
    // override
    return false;
  }

  function get_human_readable_tags($object_name = 'red_item') {

    $service_id = intval($this->service_id);
    $sql = "SELECT service_name, service_description, service_help_link FROM red_service WHERE service_id = $service_id";
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    $name = $row[0];
    $desc = $row[1];
    $help_link = $row[2];
    return array('description' => $desc, 'name' => $name, 'help_link' => $help_link);
  }

  function add_new_item_link() {
    $object = preg_replace('/_id$/', '', $this->service_key_field);
    global $globals;
    if(!$globals['authz']->check_insert_perms($object, $this->get_unit_id())) {
      // Disable this button.
      $this->template->set_add_new_item_link('none');
    }
    else {
      $this->template->set_add_new_item_link();
    }
  }
      
  function prepare_list() {
    $human_readable = $this->get_human_readable_tags();
    $this->template->set_file('children_file','children.ihtml');
    $this->add_new_item_link();
    $this->template->set_var('lang_add_new_item',red_t("Add a new item"));
    $this->template->set_var('lang_refresh',red_t("Refresh"));
    if($this->area == 'hosting_order') {
      $this->template->set_var('service_id',$this->service_id);
      $this->template->set_var('unit_key_field',$this->unit_key_field);
      $this->template->set_var('unit_key_value',$this->unit_id);
      if($this->show_filter_form) {
        $this->template->set_file('filter_file','filter.ihtml');
        $this->template->set_var('lang_filter',red_t("Filter"));
        $this->template->parse('filter_block','filter_file');
      }
    }
    $this->template->set_var('item_description',$human_readable['description']);
    $this->template->set_var('item_name',$human_readable['name']);
    if(!empty($human_readable['help_link'])) {
      $i_attr = array('class' => 'fa fa-question');
      $icon = $this->html_generator->get_tag('i', '', $i_attr);
      $attr = array('href' => $human_readable['help_link'], 'class' => 'red-help-link');
      $this->template->set_var('help_link', $this->html_generator->get_tag('a', $icon, $attr));
    }
    $this->set_area_navigation();
  }

  function get_list_block($start,$limit) {
    return $this->get_header() . $this->get_content($start,$limit) .
      $this->get_footer();
  }
  function get_header() {


  }

  function get_footer() {

  }

  function get_unit_id() {
    return $this->unit_id;
  }

  function set_unit_active_friendly_name() {
    if(is_null($this->unit_key_field) || 
      is_null($this->unit_friendly_field) || 
      empty($this->unit_id) || is_null($this->unit_table)) {
      $this->unit_active_friendly_name = $this->unit_friendly_name;
      return;
    }
    $sql = 'SELECT ' . $this->unit_friendly_field . ' FROM ' . 
      $this->unit_table . ' WHERE ' . $this->unit_key_field . 
      ' = ' . $this->unit_id;
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    if(empty($row[0])) {
      $this->unit_active_friendly_name = $this->unit_friendly_name;
    } else {
      $this->unit_active_friendly_name = $row[0];
    }
  }

  function set_area_navigation() {
    $this->set_unit_active_friendly_name();
    $this->template->set_file('area_navigation_file','area_navigation.ihtml');
    $this->template->set_var('lang_go',red_t('Go'));
    $this->template->set_var('area',$this->area);

    // fill units drop down
    $units = $this->available_units;
    if(count($units) == 0) {
      // no units to display, delete both blocks
      $this->template->set_block('area_navigation_file','multiple_unit_display','delete_me');
      $this->template->set_block('area_navigation_file','single_unit_display','delete_me');
    } elseif(count($units) == 1) {
      // one unit, display single unit
      list($k,$v) = each($units);
      $v = red_truncate_from_middle($v,60);
      $this->template->set_block('area_navigation_file','multiple_unit_display','delete_me');
      $this->template->set_var('unit_id_option',$k);
      $this->template->set_var('unit_name',$v);
    } else {
      // multiple units display drop down
      $this->template->set_block('area_navigation_file','single_unit_display','delete_me');
      $this->template->set_block('area_navigation_file','area_unit_row','area_unit_rows');
      foreach($units as $k => $v) {
        $v = red_truncate_from_middle($v,60);
        $this->template->set_var('unit_id_option',$k);
        $this->template->set_var('unit_name',red_htmlentities($v));
        if($k == $this->get_unit_id()) {
          $this->template->set_var('unit_selected','selected="selected"');
        } else {
          $this->template->set_var('unit_selected','');
        }
        $this->template->parse('area_unit_rows','area_unit_row',true);
      }
    }

    $this->template->set_var('unit_friendly_name',$this->unit_friendly_name);
    $this->template->set_var('unit_key_field',$this->unit_key_field);
    // fill services drop down
    $services = $this->get_available_services();
    $this->template->set_file('area_services','area_services.ihtml');
    $this->template->set_block('area_services','area_service_row','area_service_rows');
    foreach($services as $k => $v) {
      $this->template->set_var('service_id_option',$k);
      $this->template->set_var('service_name',str_replace(' ','&nbsp;',red_htmlentities($v)));
      $this->template->set_var('unit_id',$this->get_unit_id());
      if($k == $this->service_id) {
        $this->template->set_var('service_selected','active');
      } else {
        $this->template->set_var('service_selected','');
      }
      $this->template->parse('area_service_rows','area_service_row',true);
    }
    // Determine access levels
    $top = false;
    $member = false;
    $hosting_order = false;

    $top = $this->get_top_area_access();
    // if you access to the top level, you automatically have member level access
    if($top) {
      $member = true;
    } else {
      $member = $this->get_member_area_access(); 
    }
    if($member) {
      $hosting_order = true;
    } else {
      $hosting_order = $this->get_hosting_order_area_access();
    }
    $active = $this->area;

    $area_nav_items = $this->get_area_navigation_items($top,$member,$hosting_order);
    $member_id = null;
    if ($this->area != 'hosting_order') {
      $this->template->set_block('area_navigation_file', 'primary_host_block', 'delete_me');
    }

    if($this->area == 'member') {
      $member_id = $this->get_unit_id();
    }
    if($this->area == 'hosting_order') {
      $member_id = $this->get_member_id();
    }
    if(!empty($member_id) && $this->dues_are_owed($member_id)) {
      $this->template->set_dues_message($this->get_unpaid_explanation($member_id), $member_id);
    }
    if($member_id) {
      $member_id = intval($member_id);
      $sql = "SELECT member_benefits_level FROM red_member WHERE member_id = $member_id";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      $this->template->set_var('benefits_level', $row[0]);
      if($this->is_admin) {
        $this->template->set_var('user_auth', 'admin');
      }
      else {
        $this->template->set_var('user_auth', 'user');
      }
    }

    $this->template->set_var('delete_me', '');
    $this->template->set_area_navigation_list($area_nav_items,$active);
    $this->template->parse('area_navigation_block','area_navigation_file');
    $this->template->parse('area_services_block','area_services');

  }

  // Override in children.
  function get_tags() {
    return array();
  }

  function dues_are_owed($member_id) {
    if(empty($member_id)) return false;
    require_once($this->_construction_options['src_path'] . '/modules/class.red.member.inc.php');
    require_once($this->_construction_options['src_path'] . '/modules/class.red.invoice.inc.php');
    require_once($this->_construction_options['src_path'] . '/modules/class.red.planned_payment.inc.php');
    if(false === red_member::get_delinquent_invoices($this->get_member_id(),$this->_sql_resource, $this->_construction_options)) return false;
    return true;
  }

  function get_unpaid_explanation() {
    require_once($this->_construction_options['src_path'] . '/modules/class.red.member.inc.php');
    return red_member::get_unpaid_explanation($this->get_member_id(),$this->_sql_resource);
  }

  function get_area_navigation_links($top,$member,$hosting_order,$active) {
    // override in child
    return array();
  }

  function get_member_id() {
    //override in child class 
    return null;
  }

  function get_top_area_access() {
    if($this->is_admin) return true;
    $user_name = addslashes($this->user_name);
    $sql = "SELECT member_id FROM red_map_user_server ".
      "WHERE login = '$user_name' AND status = 'active'";
    $result = $this->_sql_query($sql);
    if($this->_sql_num_rows($result) == 0) return false;
    return true;
  }

  function get_member_area_access() {
    if(!$this->is_admin) {
      $user_name = addslashes($this->user_name);
      $sql = "SELECT member_id FROM red_map_user_member ".
        "WHERE login = '$user_name' AND status = 'active'";
      $result = $this->_sql_query($sql);
      if($this->_sql_num_rows($result) == 0) return false;
    }
    return true;
  }

  function get_hosting_order_area_access() {
    if($this->is_admin) return true;
    $user_name = addslashes($this->user_name);
    $sql = "SELECT hosting_order_id FROM red_item_hosting_order_access ".
      "JOIN red_item USING(item_id) WHERE hosting_order_access_login = ".
      "'$user_name' AND item_status = 'active'";
    $result = $this->_sql_query($sql);
    if($this->_sql_num_rows($result) == 0) return false;
    return true;
  }

  function get_table_row_for_single_object(&$object,$row_number) {
    $table_row = array();

    if(!$object) {
      red_set_message(red_t("Failed to create single object when listing!"),'error');
      return false;
    }
    else {
      $object->set_html_generator($this->html_generator);
      $key_field_name = $object->get_key_field();
      $func = 'get_' . $key_field_name;
      $id = $object->$func();
      if($row_number == 0)  {
        $row = $object->get_enumerate_header_block().
          $this->html_generator->get_table_cells("&nbsp;");
        $table_row[] = $this->html_generator->get_table_row($row);
      }
      if($this->area == 'hosting_order') {
        $status = $object->get_item_status();
      } elseif ($this->area == 'member' && $this->service_id == 12) {
        // service_id 12 is a hosting_order  
        $status = $object->get_hosting_order_status();
      } else {
        $status = null;
      }
        
      $links = $this->get_list_links($key_field_name,$id, $status);  
      $row = $object->get_enumerate_data_block() . 
        $this->html_generator->get_table_cells($links, array('class' => 'red-table-links'));
      $attr = array('class' => 'red-row-even');
      if(!is_int($row_number/2)) $attr = array('class' => 'red-row-odd');
      global $globals;
      if(array_key_exists($this->service_key_field,$globals['user_input'])) {
        if($id == $globals['user_input'][$this->service_key_field]) {
          $attr['class'] = "red-row-selected";
        }
        if(!is_null($status)) {
          $attr['class'] .= ' red-status-' . $status;
        }
      }
      $table_row[] = $this->html_generator->get_table_row($row,$attr);
      return $table_row;
    }
  }

  function get_list_links($key_field_name,$id, $status = null) {
    $links = $this->get_list_edit_link($key_field_name,$id);
    if($status == 'active') {
      $links .= $this->get_list_disable_link($key_field_name,$id);  
    } elseif($status == 'disabled') {
      $links .= $this->get_list_enable_link($key_field_name,$id);  
    }
    $links .=  $this->get_list_delete_link($key_field_name,$id);
    return $links;
  }

  function get_list_edit_link($key_field_name,$id) {
    $edit_url = $this->template->constructed_self_url . 
      "&amp;action=edit&amp;$key_field_name=".$id;
    $edit_attr = array('href' => $edit_url, 'class' => 'btn btn-xs btn-info');
    return $this->html_generator->get_tag('a', red_t('edit'),$edit_attr);
  }

  function get_list_delete_link($key_field_name,$id) {
    $del_url = $this->template->constructed_self_url . 
      "&amp;action=delete&amp;$key_field_name=".$id;
    $del_attr = array('href' => $del_url, 'class' => 'btn btn-xs btn-danger');
    return $this->html_generator->get_tag('a',red_t('delete'),$del_attr);
  }
  function get_list_disable_link($key_field_name,$id) {
    $dis_url = $this->template->constructed_self_url . 
      "&amp;action=disable&amp;$key_field_name=".$id;
    $dis_attr = array('href' => $dis_url, 'class' => 'btn btn-xs btn-warning');
    return $this->html_generator->get_tag('a', red_t('disable'),$dis_attr);
  }
  function get_list_enable_link($key_field_name,$id) {
    $en_url = $this->template->constructed_self_url . 
      "&amp;action=enable&amp;$key_field_name=".$id;
    $en_attr = array('href' => $en_url, 'class' => 'btn btn-xs btn-success');
    return $this->html_generator->get_tag('a', red_t('enable'),$en_attr);
  }
  function get_list_view_link($area,$key_field_name,$id) {
    $url_parts = $this->template->self_url_parts;
    $url_parts['area'] = $area;
    unset($url_parts['service_id']);
    $url_parts[$key_field_name] = $id;
    $url = $this->template->get_constructed_self_url($url_parts);
    $class = 'btn btn-xs btn-info';
    $attr = array('href' => $url, 'class' => $class);
    return $this->html_generator->get_tag('a','view',$attr);
  }

  function get_session_id() {
    return session_id();
  }

  function get_available_services() {
    $sql = "SELECT red_service.service_id,service_name FROM red_service " .
      "WHERE service_area = '" . addslashes($this->area) . "' AND ".
      "service_status = 'active'";
    $result = $this->_sql_query($sql);
    $ret = array();
    // local dns...
    $admin_only_service_ids = array(33);
    while($row = $this->_sql_fetch_row($result)) {
      // Only admins have access to tags and notes
      $id = $row[0];
      $name = $row[1];
      if($name == 'Tags' && !$this->is_admin) continue;
      if($name == 'Notes' && !$this->is_admin) continue;
      if(in_array($row[0], $admin_only_service_ids) && !$this->is_admin) continue;
      $ret[$id] = $name;
    }
    return $ret;
  }

  function get_filter_where($service_table,$keywords) {
    $ret = array();
    // create dummy object to get search fields 
    $dummy_co = $this->_construction_options;
    $dummy_co['service_id'] = $this->service_id;
    $dummy_co[$this->unit_key_field] = $this->unit_id;
    $dummy_item =& $this->get_single_object($dummy_co);
    $is_numeric = false;
    if(is_numeric($keywords)) {
      $is_numeric = true;
      $keywords = intval($keywords);
    } 
    $keywords = addslashes($keywords);
    foreach($dummy_item->_datafields as $k => $v) {
      if(array_key_exists('filter',$v) && $v['filter'] === true) {
        if($is_numeric && $v['type'] == 'int') {
          $ret[] = "$k = $keywords";
        } elseif($is_numeric && $v['type'] == 'varchar') {
          $ret[] = "$k = '$keywords'";
        } elseif(!$is_numeric && $v['type'] == 'varchar') {
          $ret[] = "$k LIKE '%$keywords%'";
        }
      }
    }
    return implode(' OR ',$ret);
  }
  function get_list_sql($start = 0, $limit = 50) {
    $service_table = red_get_service_table_for_service_id($this->service_id,$this->_sql_resource);
    $hosting_order_id = $this->unit_id;
    $order_by = '';
    if(!is_null($this->order_by)) $order_by = "ORDER BY " . $this->order_by;
    $service_id = $this->service_id;
    $filter = '';
    if(!empty($this->filter_keywords)) {
      $filter = " AND (" . $this->get_filter_where($service_table,$this->filter_keywords) . ")";
    }
    $sql = "SELECT * FROM red_item INNER JOIN $service_table ".
      "ON red_item.item_id = $service_table.item_id ".
      "WHERE hosting_order_id = $hosting_order_id AND ".
      "red_item.service_id = $service_id ".
      "AND red_item.item_status != 'deleted' $filter $order_by LIMIT $start, $limit";

    return $sql;
  }

  function check_unit_access() {
    // first make sure they have access to the unit (unit_id)
    if(count($this->available_units) == 0) {
      $this->errors[4] = "You do not have access to any units.";
      return false;
    }
    if(!array_key_exists($this->unit_id,$this->available_units)) {
      $this->errors[1] = red_t("You do not have access to the unit with id @unit_id", array('@unit_id' => $this->unit_id));
      return false;
    }
    return true;
  }

  function get_count_sql($sql) {
    // thanks drupal
    if(preg_match('/ UNION /',$sql)) {
      $count_sql ="SELECT COUNT(*) FROM (" . preg_replace('/LIMIT .*$/','', $sql) . ') AS count';
    } else {
      // thanks drupal
      $count_sql = preg_replace(array('/SELECT.*?FROM /', '/ORDER BY [^)]*/','/LIMIT [^)]*/'), array('SELECT COUNT(*) FROM ', '',''), $sql);
    }
    //echo "$count_sql";
    return $count_sql;
  }

  function get_content($start = 0, $limit = 50) {
    // Now display the existing items
    $sql = $this->get_list_sql($start, $limit);
    if(empty($sql)) return '';
    $result = $this->_sql_query($sql);
    $list_co = $this->_construction_options;
    $row_number = 0;
    $table_row = array();
    while($row = $this->_sql_fetch_assoc($result)) {
      $list_co['rs'] =& $row;
      $list_item =& $this->get_single_object($list_co);
      $ret = $this->get_table_row_for_single_object($list_item,$row_number);
      if(!$ret) return $ret;
      $table_row = array_merge($table_row,$ret);
      $row_number++;
    }
    $table_data = implode("\n",$table_row);
    $count_sql = $this->get_count_sql($sql);
    $result = $this->_sql_query($count_sql);
    $row = $this->_sql_fetch_row($result);
    $count = $row[0];
    $search_keywords = '';
    if(property_exists($this,'search_keywords')) $search_keywords = $this->search_keywords;
    $this->template->set_pager_block($count,$start,$limit,$search_keywords);
    return $this->html_generator->get_table($table_data,array('id' => 'red-items-table','class' => 'table table-striped red-table')); 
  }

  function get_item_error_row($item_id) {
    $item_id = intval($item_id);
    $sql = "SELECT error_log_message,error_log_datetime FROM red_error_log ".
      "WHERE item_id = $item_id ORDER BY error_log_id DESC LIMIT 25 ";
    $result = red_sql_query($sql,$this->_construction_options['sql_resource']);
    $table_rows = array();
    $error_data = '';
    while($error_row = $this->_sql_fetch_row($result)) {
      $span_attr = array('class' => 'red_error');
      $data = $this->html_generator->get_tag('span','Error!',$span_attr);
      $data .= ' ' . $error_row[1] . ' ' . 
        $error_row[0];
      $error_data .= $this->html_generator->get_tag('li',$data);
    }
    $ul_attr = array('class' => 'red_error_list');
    $error_data = $this->html_generator->get_tag('ul',$error_data,$ul_attr);
    $cell_attr = array('colspan' => '10');
    $cell = $this->html_generator->get_table_cells($error_data,$cell_attr);
    return $this->html_generator->get_table_row($cell);
  }

  function edit_child($id = false) {
    // When showing the form to edit a child, hide the links to create a new
    // child to avoid confusing the user.
    $this->template->set_var('hide_or_show_items_nav_links', 'hide');

    // $this->invalid_service_object will be populated if
    // we submitted a change that had a validation error.
    // this ensures that the user doesn't lose the values
    // they entered in the form.
    if(!empty($this->invalid_service_object)) {
      $edit =& $this->invalid_service_object;
    } else {
      // otherwise built it from the db values.
      $edit_co = $this->_construction_options;
      $edit_co['id'] = $id;
      $edit_co['service_id'] = $this->service_id;
      $edit =& $this->get_single_object($edit_co);
    }
      
    if(!$edit) {
      $message = red_t("Failed to create object for editing. Please check the ".
                 "red_error_log table for details.");
      red_set_message($message,'error');
    }
    else {
      $js_includes = $edit->get_javascript_includes();
      $this->template->add_js($js_includes);
      $edit->set_html_generator($this->html_generator);
      $this->template->set_file('edit_file','edit.ihtml');
      $this->template->set_var('edit_block',$edit->get_edit_block());
      $this->template->set_var('session_id',$this->get_session_id());
      $this->template->parse('edit_block','edit_file');
    }
    // Don't show filter option on edit - it clutters the ui
    $this->show_filter_form = false;

  }

  function print_child($id = false) {
    if(!$id) {
      $message = red_t("Failed to create object for printing. No id passed.");
      red_set_message($message,'error');
      return;
    }
    $co = $this->_construction_options;
    $co['id'] = $id;
    $co['service_id'] = $this->service_id;
    $obj =& $this->get_single_object($co);
      
    if(!$obj) {
      $message = red_t("Failed to create object for printing. Please check the ".
                 "red_error_log table for details.");
      red_set_message($message,'error');
    }
    else {
      $obj->set_html_generator($this->html_generator);
      echo $obj->printme();
      exit;
    }
  }
  function write_child($passed_session_id) {
    $standard_construction_options = $this->_construction_options;
    $write_co = $standard_construction_options;
    $write_co['service_id'] = $this->service_id;
    if($this->config['notify_host']) {
      $write_co['notify_host'] = $this->config['notify_host'];
      $write_co['notify_user'] = $this->config['notify_user'];
      if(array_key_exists('notify_cmd', $this->config)) {
        $co['notify_cmd'] = $this->config['notify_cmd'];
      }
    }

    $write_co[$this->unit_key_field] = $this->unit_id;

    $write_item =& $this->get_single_object($write_co);
    $write_item->set_html_generator($this->html_generator);

    if(!$write_item) {
      $message = red_t("Failed to create a red object when trying to write: @error_message",
                 array('@error_message' => $write_item->get_errors_as_string()));
      red_set_message($message,'error');
      return false;
    }

    $post = cred_un_magic_quotes($_POST);
    // check for valid session
    if(empty($passed_session_id) || $passed_session_id != $this->get_session_id()) {
      red_set_message(red_t("Missing or invalid session id. You are either experiencing a bug or someting sneaky is going on. If you really want to edit/insert this record, please try again. Otherwise, please contact your system administrator."),'error');
      $this->invalid_service_object = $write_item;
      return false;
    }
      
    $write_item->set_user_input($post);

    $this->post_user_input_modifications($write_item);

    // If the user makes a input error - return this item as the 
    // edit_item. with these values filled in
    if(!$write_item->validate()) {
      red_set_message($write_item->get_errors('validation'),'error');
      $this->invalid_service_object = $write_item;
      return false;
    }
    elseif(!$write_item->commit_to_db())  {
      red_set_message($write_item->get_errors('system'),'error');
      $this->invalid_service_object = $write_item;
      return false;
    }
    red_set_message(red_t('Record saved.'),'success');

    // Surprise, it all worked out
    // redirect to a non-write URL so if the user hits reload
    // on their browser it won't repeat the write action
    $redirect = $this->template->get_constructed_self_url(null,'raw');
    header("Location: $redirect");
    exit;
  }

  function post_user_input_modifications(&$obj) {
    // overrid

  }

  function new_child() {
    // When showing the form to create a new child, hide the links to create a new
    // child to avoid confusing the user.
    $this->template->set_var('hide_or_show_items_nav_links', 'hide');
    $standard_construction_options = $this->_construction_options;
    $new_co = $standard_construction_options;
    $new_co['service_id'] = $this->service_id;
    $unit_key_field = $this->unit_key_field;
    $new_co[$unit_key_field] = $this->unit_id;
    $new_item =& $this->get_single_object($new_co);
    $js_includes = $new_item->get_javascript_includes();
    $this->template->add_js($js_includes);

    if(!$new_item) {
      $message = red_t("Failed to create object for editing. Please check the ".
                 "red_error_log table for details.");
      red_set_message($message,'error');
      return false;
    }
    else {
      $new_item->set_html_generator($this->html_generator);
      $this->template->set_file('edit_file','edit.ihtml');
      $this->template->set_var('edit_block',$new_item->get_edit_block());
      $this->template->set_var('session_id',$this->get_session_id());
      $this->template->parse('edit_block','edit_file');
    }
    // Don't show filter option on new - it clutters the ui
    $this->show_filter_form = false;
    return true;
  }

  function delete_child($id,$session_id,$confirm, $cancel) {
    return $this->delete_or_disable_or_enable($id, $session_id, $confirm, $cancel, 'delete');
  }
  function disable_child($id,$session_id,$confirm, $cancel) {
    return $this->delete_or_disable_or_enable($id, $session_id, $confirm, $cancel, 'disable');
  }
  function enable_child($id,$session_id,$confirm, $cancel) {
    return $this->delete_or_disable_or_enable($id, $session_id, $confirm, $cancel, 'enable');
  }

  function delete_or_disable_or_enable($id, $session_id, $confirm, $cancel, $action ) {
    $co = $this->_construction_options;

    if(empty($id))
      trigger_error(red_t("You can't do this action without passing an id."),E_USER_ERROR);
    $co['id'] = $id;
    $co['service_id'] = $this->service_id;
    if($this->config['notify_host']) {
      $co['notify_host'] = $this->config['notify_host'];
      $co['notify_user'] = $this->config['notify_user'];
      if(array_key_exists('notify_cmd', $this->config)) {
        $co['notify_cmd'] = $this->config['notify_cmd'];
      }
    }
    $item =& $this->get_single_object($co);

    if(!$item) {
      $message = red_t("Failed to create a red object. ".
                 "Please check the red_error_log table for details.");
      red_set_message($message,'error');
      return false;
    } else {
      // make sure we're not deleting or disabling a resource providing our login
      if($action == 'disable' || $action == 'delete') {
        // hosting order
        $borking_yourself = false;
        if($this->service_id == 12) {
          $sql = "SELECT hosting_order_id FROM red_item JOIN red_item_user_account " .
            "WHERE item_status = 'active' AND ".
            "user_account_login = '" . addslashes($this->user_name) . "'"; 
          $result = $this->_sql_query($sql);
          $row = $this->_sql_fetch_row($result);
          if($row[0] == $item->get_hosting_order_id()) {
            $borking_yourself = true;
          }
        } elseif(method_exists($item, 'get_service_id') && $item->get_service_id() == 1) {
          // user account
          if($item->get_user_account_login() == $this->user_name) {
            $borking_yourself = true;
          }
        }
        if($borking_yourself) {
          red_set_message(red_t("You cannot @action a record providing your current login.", array('@action' => $action)));
          return false;
        }
      }
      $item->set_html_generator($this->html_generator);
      if($action == 'delete') {
        $confirm_message = $item->get_delete_confirmation_message();
      } elseif($action == 'disable') {
        $confirm_message = red_t("Are you sure you want to disable this record?");
      } elseif($action == 'enable') {
        $confirm_message = red_t("Are you sure you want to enable this record?");
      }

      if(!$cancel && !$confirm && !is_null($confirm_message)) {
        $this->template->display_confirm_message($id,$this->service_key_field,$confirm_message,$this->get_session_id(), $action);
      } elseif($session_id != $this->get_session_id()) {
        red_set_message(red_t("Missing or invalid session id. You are either experiencing a bug or someting sneaky is going on. If you really want to edit this record, please try again. Otherwise, please contact your system administrator."),'error');
        $this->template->cred_display_confirmation($id,$confirm_message,$this->get_session_id(), $action);

      }
      elseif($cancel) {
        red_set_message(red_t('The record was not affected.'),'info');
      } else {
        if($action == 'delete') {
          $item->set_delete_flag();
          $msg = red_t("Record deleted.");
        } elseif ($action == 'disable') {
          $item->set_disable_flag();
          $msg = red_t("Record disabled.");
        } elseif ($action == 'enable') {
          $item->unset_disable_flag();
          $msg = red_t("Record enabled.");
        }
        if(!$item->validate()) {
          red_set_message($item->get_errors('validation'),'error');
          return false;
        } elseif(!$item->commit_to_db()) {
          red_set_message($item->get_errors('system'),'error');
          return false;
        }

        red_set_message($msg,'success');

        // redirect to a non-write URL so if the user hits reload
        // on their browser it won't repeat the delete action
        $redirect = $this->template->get_constructed_self_url(null,'raw');
        header("Location: $redirect");
        exit;
      }
    }
  }
}
?>
