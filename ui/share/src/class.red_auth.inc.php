<?php

class cred_auth {
  var $_sql_resource;
  var $_user_name;
  var $_errors = array();

  var $_login_template = 'login.ihtml';
  var $_login_message = null;
  var $_login_timeout = 120; // in minutes
  var $_secure_cookie_flag = true;

  //var $_secure_cookie_flag = 0; 
    
  function set_secure_cookie_flag($value) {
    $this->_secure_cookie_flag = $value;
  }

  function _set_error($k,$v) {
    $this->_errors[$k] = $v;
  }

  function get_errors() {
    return $this->_errors;
  }

  function set_sql_resource(&$sql_resource) {
    $this->_sql_resource =& $sql_resource;
  }

  function get_user_name() {
    return addslashes($this->_user_name);
  }

  function get_uid() {
    $user_name = $this->get_user_name();
    $sql = "SELECT user_account_uid FROM red_item_user_account 
      JOIN red_item USING(item_id) WHERE
      user_account_login = '$user_name' AND item_status = 'active'";
    $result = red_sql_query($sql,$this->_sql_resource);
    $row = red_sql_fetch_row($result);
    return $row[0];
  }

  function get_item_id() {
    $user_name = $this->get_user_name();
    $sql = "SELECT red_item.item_id FROM red_item_user_account 
      JOIN red_item USING(item_id) WHERE
      user_account_login = '$user_name' AND item_status = 'active'";
    $result = red_sql_query($sql,$this->_sql_resource);
    $row = red_sql_fetch_row($result);
    return $row[0];
  }
  function set_user_name($user_name) {
    $this->_user_name = $user_name;
  }

  function set_template_object(&$template_object) {
    $this->_template_object =& $template_object;
  }

  // Return true, if:
  //  they already are logged in or
  //  they are passing valid credentials
  // Otherwise return false
  function login() {
    $this->_login_message = red_t('Please login.');
    if($this->already_logged_in()) return true;
    if($this->check_credentials()) return true;
    return false;
  }

  function already_logged_in() {
    if(!array_key_exists('red_hash',$_COOKIE)) return false;

    // They have the required cookie, now validate it 
    $hash = $_COOKIE['red_hash'];
    return $this->_validate_cookie($hash);
  }

  function check_credentials() {
    if(!array_key_exists('user_name',$_REQUEST) || !array_key_exists('user_pass',$_REQUEST)) return false;
    $user_name = $_REQUEST['user_name'];
    $user_pass = $_REQUEST['user_pass'];
    if($this->valid_user($user_name,$user_pass)) {
      $this->_initialize_user($user_name);
      $this->_login_message = null;
      return true;
    }
    $this->_login_message = red_t('Incorrect user name or password.');
    return false;
  }

  function get_login_message() {
    return $this->_login_message;
  }

  function _initialize_user($user_name) {
    $this->set_user_name($user_name);
    $this->_initialize_user_hash();
  }

  function _initialize_user_hash() {
    $user_name = $this->get_user_name();
    // see if the user already has a record
    $hash = $this->_get_user_hash($user_name); 
    if(empty($hash)) {
      $hash = md5(microtime() . rand());
      $sql = "INSERT INTO red_login_session SET user_name = '$user_name', ".
        "user_hash = '$hash', stamp = NOW()";
    } else {
      $sql = "UPDATE red_login_session SET user_hash = '$hash', ".
        "stamp = NOW() WHERE user_name = '$user_name'";
    }
    red_sql_query($sql,$this->_sql_resource);
    $this->_set_cookie($hash);
  }

  function _get_user_hash($user_name) {
    $sql = "SELECT user_hash FROM red_login_session WHERE user_name ".
      "= '$user_name'";
    $result = red_sql_query($sql,$this->_sql_resource);
    $row = red_sql_fetch_row($result);
    return $row[0];
  }

  function _set_cookie($hash) {
    $path = $this->_get_cookie_path(); 
    $secure_flag = $this->_secure_cookie_flag;
    // these fail when I try to specify the domain - not sure why
    // however, when I specify null, firefox at least seems to properly
    // add the right server
    setcookie('red_hash',$hash,0,$path,null,$secure_flag);
  }

  function _get_cookie_path() {
    $path = dirname($_SERVER['REQUEST_URI']);
    if ($path != '/') {
      $path .= '/';
    }
    return $path;
  }

  function logout() {
    $this->_set_cookie('');
    $user_name = $this->get_user_name();
    $sql = "DELETE FROM red_login_session WHERE user_name ".
      "= '$user_name'";
    red_sql_query($sql,$this->_sql_resource);
  }

  function _validate_cookie($hash) {
    $hash = addslashes($hash);
    $sql = "SELECT user_name, stamp FROM red_login_session ".
      "WHERE user_hash = '$hash'";
    $result = red_sql_query($sql, $this->_sql_resource);
    $row = red_sql_fetch_row($result);
    $user_name = $row[0];
    $stamp = $row[1];
    if(empty($user_name)) {
      $this->logout();
      $this->_login_message = red_t("Your login session is not valid. Please login again.");
      return false;
    }
    $current_ts = time();
    $session_ts = strtotime($stamp);
    $login_time = ($current_ts - $session_ts)/60;
    if($login_time > $this->_login_timeout) {
      $this->logout();
      $this->_login_message = red_t("Your login session has expired. Please login again.");
      return false;
    }
    $this->set_user_name($user_name);
    return true;
  }

  function set_login_template($file) {
    $this->_login_template = $file;
  }

  function valid_user($user_name,$user_pass) {
    // disabled users are allowed to login, the authz functions should
    // restrict what they can do
    $sql = "SELECT user_account_password FROM red_item_user_account ".
      "INNER JOIN red_item USING (item_id) WHERE user_account_login ".
      "= '$user_name' AND (item_status = 'active' OR item_status = ".
      "'pending-insert' OR item_status = 'pending-restore' OR ".
      "item_status = 'pending-update' OR item_status = 'pending-disable' ".
      "OR item_status = 'disabled')";
    $result = red_sql_query($sql,$this->_sql_resource);
    if(red_sql_num_rows($result) == 1) {
      $row = red_sql_fetch_row($result);
      $real_encrypted_hash = $row[0];
      $given_encrypted_hash = crypt($user_pass,$real_encrypted_hash);
      if($given_encrypted_hash == $real_encrypted_hash) return true;
      // echo "given: $given_encrypted_hash, real: $real_encrypted_hash";
    }
    return false;
  }

    
}

?>
