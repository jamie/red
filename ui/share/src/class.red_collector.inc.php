<?php

define('RED_MEMBERSHIP_INVOICES_IMPLEMENTATION_DATE', '2017-05-25');

class red_collector extends red_db {

  var $error;
  var $no_act = false;
  var $construction_options;
  var $timestamp = null;
  // an array with the key as the number of days old an invoice
  // is to trigger the notification and the value an array
  // of email addresses to send the notification
  // pp is for invoices with planned payments
  var $extra_notifications = array(
    'pp' => array(
      35 => array('support@lists.mayfirst.org')
    ),
    'regular' => array(
      100 => array('support@lists.mayfirst.org')
    ),
  );
  

  // how many days ahead should we generate an invoice
  var $invoice_days_ahead = '14';

  function __construct($db) {
    parent::__construct($db);
  }

  function set_no_act() {
    $this->no_act = true;
  }

  function set_timestamp($value) {
    $this->timestamp = $value;
  }

  // given a member id, get the date of the next invoice
  // if invoice creation has been skipped in the past and 
  // include_past is true, this could be a date in the past. 
  // If a member is all paid up, this could be a date in the future
  function get_next_invoice_date($member_id,$include_past = false, $type = 'membership') {
    if($type != 'membership' && $type != 'benefits' && $type != 'any') {
      red_set_message("Invoice type must be membership or benefits or any.");
      return FALSE;
    }
    $ret = null;
    $member_id = intval($member_id);
    $sql = "SELECT member_start_date, member_term FROM red_member WHERE ".
      "member_id = " . $member_id;
    $result = $this->_sql_query($sql);
    $row = $this->_sql_fetch_row($result);
    $member_date = substr($row[0],0,10);
    if ($type == 'membership') {
      // Term is always a year for memberships
      $member_term = 'year';
    }
    else {
      $member_term = $row[1];
    }

    // get an array of all existing invoices
    $existing = array();
    $sql = "SELECT invoice_date FROM red_invoice WHERE " .
      "member_id = $member_id AND (invoice_status = 'paid' ".
      "OR invoice_status = 'unpaid' OR invoice_status = 'review') ";

    if($type != 'any') {
      $sql .= "AND invoice_type = '$type' ";
    }
    
    $sql .= "ORDER BY invoice_date";
    $result = $this->_sql_query($sql);
    while($row = $this->_sql_fetch_row($result)) {
      $existing[] = substr($row[0],0,10);
    }

    if($include_past) {
      // get an array of dates that we expect to see based on the start
      // date and term
      $expected =& $this->get_expected_invoices($member_date,$member_term);

      // if we have more expected invoices than existing
      // create one based on the first mising date that we find
      //echo "expected: " . count($expected) . " and existing: " . count($existing);
      if(count($expected) > count($existing)) {
        foreach($expected as $date) {
          // return the first date that doesn't already exist
          // provided no other invoice has been dated during the period
          if(!in_array($date,$existing)) {
            if($this->no_invoice_during_term($date,$member_term, $member_id, $type)) 
              return $date;
          }
        }
      }
    }

    // if all the exected dates exist
    // return a date that is + 1 term from the
    // last existing date
    
    if(count($existing) == 0) {
      // this means there have been no invoices so far
      if($member_date <= RED_MEMBERSHIP_INVOICES_IMPLEMENTATION_DATE) {
        // This member started before the new regime, exception may be necessary.
        $year_passed = strtotime(RED_MEMBERSHIP_INVOICES_IMPLEMENTATION_DATE) + (86400 * 365);
        if(time() < $year_passed) {
          // An exception is necessary. We will generate the next date
          // that is in the future and matches their membership start date.
          if ($member_term == 'year') {
            // Construct a date using the same month and day as their 
            // start date, but with this year as the year. If it's in
            // the future, we use it, otherwise we have to advance one
            // year.
            $member_start_month_day = substr($member_date, 5);
            $try_this_year = date('Y') . '-' . $member_start_month_day;
            if (strtotime($try_this_year) > time()) {
              return $try_this_year;
            }
            else {
              $try_next_year = strtotime("$try_this_year + 1 year");
              return date('Y-m-d', $try_next_year);
            }
          }
          else {
            // Same principle, but on a month basis.
            $member_start_day = substr($member_date, 8);
            $try_this_month = date('Y-m') . '-' . $member_start_day;
            if (strtotime($try_this_month) > time()) {
              return $try_this_month;
            }
            else {
              $try_next_month = strtotime("$try_this_month + 1 month");
              return date('Y-m-d', $try_next_month);
            }
          }
        }
      }  
      else {
        // No exception necessary, just return their membership date which
        // will be the date of their first invoice.
        return $member_date;
      }
    }
    reset($existing);
    $last = array_pop($existing);
    // echo "last: $last";
    //print_r($existing);
    return date('Y-m-d',strtotime("$last + 1 $member_term"));

  }

  function no_invoice_on_date($date, $member_id, $type = 'membership') {
    if($type != 'membership' && $type != 'benefits' && $type != 'any') {
      echo "Invoice type must be membership or benefits or any.";
      return FALSE;
    }
    $sql = "SELECT invoice_date FROM red_invoice WHERE " .
      "member_id = $member_id AND invoice_date LIKE '$date %' " .
      "AND invoice_status != 'void'";

    // Limit by type of invoice if requested.
    if($type != 'any') {
      $sql .= " AND invoice_type = '$type'";
    }
    // echo "Called with date: $date, term: $term, member id $member_id\n";
    // echo $sql . "\n";
    $result = $this->_sql_query($sql);
    if($this->_sql_num_rows($result) > 0) return false;
    return true;
  }

  function no_invoice_during_term($date,$term,$member_id, $type = 'membership') {
    if($type != 'membership' && $type != 'benefits' && $type != 'any') {
      echo "Invoice type must be membership or benefits or any.";
      return FALSE;
    }
    $sql = "SELECT invoice_date FROM red_invoice WHERE ".
      "member_id = $member_id AND invoice_date BETWEEN ".
      "'$date' AND '" . date('Y-m-d',strtotime("$date + 1 ".
      "$term - 1 day")) ."' AND invoice_status != 'void'";

    // Limit by type of invoice if requested.
    if($type != 'any') {
      $sql .= " AND invoice_type = '$type'";
    }
    // echo "Called with date: $date, term: $term, member id $member_id\n";
    // echo $sql . "\n";
    $result = $this->_sql_query($sql);
    if($this->_sql_num_rows($result) > 0) return false;
    return true;
  }

  function &get_expected_invoices($start,$term) {
    $ret = array();
    $today = date('Y-m-d');
    $date = $start;
    while($today >= $date) {
      $ret[] = $date;
      $date = date('Y-m-d',strtotime("$date + 1 $term"));
    }
    return $ret;
  }

  // try to figure out if we've missed any previous invoices
  function troll() {
    $missed = $this->get_members_with_missed_invoices();
    print_r($missed);
    return true;
  }

  function get_members_with_missed_invoices() {
    $ret = array();
    $now = date('Y-m-d');
    $sql = "SELECT member_id,member_friendly_name FROM red_member ".
      "WHERE member_price > 0 AND member_status = 'active'";
    $result = $this->_sql_query($sql);
    while($row = $this->_sql_fetch_row($result)) {
      $member_id = $row[0];
      $member_friendly_name = $row[1];
      $date = $this->get_next_invoice_date($member_id,true);
      if($date < $now) {
        $ret[$member_id] = array(
          'date' => $date,
          'member_friendly_name' => $member_friendly_name,
        );
      }
    }
    return $ret;
  }

  function reinvoice() {
    $report = '';

    if(empty($this->timestamp)) {
      $time = time();
    } else {
      $time = $this->timestamp;
    }
    $time = $time + (86000 * $this->invoice_days_ahead);
    $month_day = date('m-d',$time);
    $day = date('d',$time);
    //$month_day = '08-22';
    //$day = '22';
    $sql = "SELECT member_id,member_term, member_benefits_price, member_start_date, member_price ".
     "FROM red_member WHERE ".
      "member_status = 'active' AND member_price > 0 AND ".
      "((member_term = 'year' AND member_start_date REGEXP('([0-9]){4}-$month_day.*')) OR ".
      "(member_term = 'month' AND member_start_date REGEXP('([0-9]){4}-([0-9]){2}-$day.*')))";
    $result = $this->_sql_query($sql);
    $date = date('Y-m-d',$time);
    // echo $sql . "\n";
    while($row = $this->_sql_fetch_row($result)) {
      $member_id = $row[0];
      $term = $row[1];
      $benefits_price = $row[2];
      $member_start_date = $row[3];
      $member_price = $row[4];

      // By default, don't re-invoice.
      $generate_benefits_invoice = FALSE;
      $generate_membership_invoice = FALSE;

      // The complicated process to determine if a member should be re-invoiced 
      // is further complicated by the fact that we institued member and benefits
      // invoices in May 2017. 
      //
      // Typically we simply check to see if an invoice of the given type has been
      // issued over the given term. However, prior to May 2017, invoices had no 
      // types.
      
      // So, we first determine if an exception to our rule should be made.
      $exception_to_rule = FALSE;
      if($member_start_date <= RED_MEMBERSHIP_INVOICES_IMPLEMENTATION_DATE) {
        // This member started before the new regime, exception may be necessary.
        $year_passed = strtotime(RED_MEMBERSHIP_INVOICES_IMPLEMENTATION_DATE) + (86400 * 365);
        if($time < $year_passed) {
          // It's been less than a year since we instituted the new
          // dues policy, so we need the exception.
          $exception_to_rule = TRUE;
        }
      }
      
      if($member_price > 0) {
        // First check if we need to generate a new membership invoice. 
        //
        // Our general approach is to only re-invoice if we don't have an existing
        // invoice in the last term.
        //
        // However, membership invoices should only be sent once a year regardless
        // of the term. 

        // If it's one year since we implemented separate invoices, then only
        // re-invoice if no membership invoices have been created in last year
        // (this is the code we will keep).
        if(!$exception_to_rule) {
          if(preg_match("/^[0-9]{4}-$month_day/", $member_start_date)) {
            if($this->no_invoice_on_date($date, $member_id, 'membership')) {
              $generate_membership_invoice = TRUE;
            }
          }
        }
        else {
          // After May 2018 kill this enter block please.
          // If the term is a year, then check for *any* invoice over last year.
          if($term == 'year') {
            if($this->no_invoice_during_term($date, 'year', $member_id, 'any')) {
              $generate_membership_invoice = TRUE;
            }
          }
          else {
            // If the term is a month... then we're guessing that they have not
            // yet been invoiced. Send an invoice if the start month and date
            // match the current month and date so they only get a membership invoice 
            // once per year, not once per month..
            if(preg_match("/^[0-9]{4}-$month_day/", $member_start_date)) {
              if($this->no_invoice_on_date($date, $member_id, 'membership')) {
                $generate_membership_invoice = TRUE;
              }
            } 
          }
        }
      }

      // And they might have a benefits invoice, which would be invoiced based
      // on their actual term.
      if($benefits_price > 0) {
        if(!$exception_to_rule) {
          // Be normal.
          if($this->no_invoice_during_term($date,$term,$member_id, 'benefits')) {
            $generate_benefits_invoice = TRUE;
          }
        }
        else {
          // Exception ville. Search for any invoice over previous term.
          if($this->no_invoice_during_term($date, $term, $member_id, 'any')) {
            $generate_benefits_invoice = TRUE;
          }
        }
      }

      if($generate_membership_invoice) {
        if($this->no_act) {
          $report .= "Would generate membership invoice for member id: $member_id, $date\n"; 
        } else {
          $invoice =& $this->create_invoice($date,$member_id, 'membership');
        }
      }
      if ($generate_benefits_invoice) {
        if($this->no_act) {
          $report .= "Would generate benefits invoice member id: $member_id, $date\n"; 
        } else {
          $invoice =& $this->create_invoice($date,$member_id, 'benefits');
        }
      }

      if(!$this->no_act) {
        $co = $this->construction_options;
        $co['id'] = $member_id;
        require_once($co['src_path'] .  '/modules/class.red.member.inc.php');
        $member = new red_member($co);
        $member->send_billing_statement();
      }
    }
    if($this->no_act) {
      echo "$report\n";
    }
  }

  function &create_invoice($date,$member_id, $type) {
    $co = $this->construction_options;
    $co['member_id'] = $member_id;
    $invoice = new red_invoice($co);    
    if(!is_null($this->timestamp)) $invoice->as_of_timestamp = $this->timestamp;
    $invoice->set_invoice_date($date);
    $invoice->set_invoice_type($type);
    // reset the invoice defaults now that we've set a specific date
    $invoice->set_invoice_defaults();
    $invoice->commit_to_db();
    return $invoice;
  }

  // Send reminder emails to people who have not paid their
  // invoice or to people who have upcoming planned payments. 
  function sendreminders() {
    // first - people with upcoming planned payments
    $invoice_ids = $this->get_upcoming_pp_invoice_ids();

    if(count($invoice_ids) == 0) {
      if($this->no_act) echo "No upcoming planned payments.\n";
    } else {
      $this->_send_upcoming_pp_notices($invoice_ids);;
    }

    // now people late on their invoices or planned payments
    $member_ids = $this->_get_sendreminders_member_ids();
    if($member_ids === false) return false;
    if(count($member_ids) == 0) {
      if($this->no_act) echo "No invoice or planned payment reminders to send.\n";
      return true;
    }

    if(!$this->_sendreminders($member_ids)) return false;

    return true;

  }

  function get_upcoming_pp_invoice_ids() {
    $date_due = red_invoice::get_planned_payment_advanced_notice_date();
    $sql = "SELECT invoice_id FROM red_planned_payment ".
      "WHERE planned_payment_date = '$date_due' ".
      "AND planned_payment_status = 'active'";
    $result = $this->_sql_query($sql);
    $ret = array();
    while($row = $this->_sql_fetch_row($result)) {
      if(!empty($row[0])) $ret[] = $row[0];
    }
    return $ret;
  }

  function _get_sendreminders_member_ids() {
    // Find members wth invoices that are older than the cutoff, unpaid,
    // delinquent (no payment plan or behind in the payment plan)
    // and with an invoice date that fits our notification schedule 

    $sql = "SELECT DISTINCT * FROM red_invoice ". 
      "WHERE invoice_status = 'unpaid' AND invoice_date <= '" . 
      $this->_get_invoice_reminder_cutoff_date() . "'";
    // echo $sql;  
    if(!$result = $this->_sql_query($sql))  {
      return false;
    }
    $ret = array();
    $co = $this->construction_options;
    while($row = $this->_sql_fetch_assoc($result)) {
      $co['rs'] = $row;
      $invoice = new red_invoice($co);
      if(!is_null($this->timestamp)) $invoice->as_of_timestamp = $this->timestamp;
      if($invoice->is_delinquent()) {
        $invoice_date = $invoice->get_invoice_date();
        $invoice_id = $invoice->get_invoice_id();
        // extra notifications might be sent even if a
        // regular notification is not sent
        $this->send_extra_notifications($invoice_id,$invoice_date);
        // check to see if a regular notification should
        // be sent and if not, continue
        if(!$invoice->reminder_due()) {
          continue;
        }
        // otherwise, put it in array of invoices to send
        // notifications
        $member_id = $invoice->get_member_id();
        if(!in_array($member_id, $ret)) {
          $ret[] = $member_id;
        }
      } 
    }
    return $ret;
  }
  function _send_upcoming_pp_notices($invoice_ids) {
    $co = $this->construction_options;
    foreach($invoice_ids as $invoice_id) {
      // fetch the invoice object
      $co['id'] = $invoice_id;
      $invoice = new red_invoice($co);
      if(!is_null($this->timestamp)) $invoice->as_of_timestamp = $this->timestamp;
      $invoice->set_upcoming_planned_payment_text();
      if(!$this->no_act) {
        if(!$invoice->send_reminder()) {
          echo "Failed to send to planned payment notice for invoice id  $invoice_id.\n";
        }
        
      } else {
        echo "Would send planned payment reminder text:\n\n";
        echo "Subject: " . print_r($invoice->email_subject) . "\n";
        echo "Body: \n" . print_r($invoice->reminder_text) . "\n\n";
      }
    }
    return true;
  }

  function _sendreminders($member_ids) {
    $co = $this->construction_options;
    require_once($co['src_path'] .  '/modules/class.red.member.inc.php');
    foreach($member_ids as $member_id) {
      // fetch the invoice object
      $co['id'] = $member_id;
      $member = new red_member($co);
      if(!$this->no_act) {
        if(!$member->send_billing_statement()) {
          echo "Failed to send billing statement for member_id $member_id.\n";
        }
      } else {
        echo "Would send billing statement to $member_id\n";
        print_r($member->get_billing_statement());
      }
    }
    return true;
  }
  
  function send_extra_notifications($invoice_id,$invoice_date) {
    $co = $this->construction_options;
    $co['id'] = $invoice_id;
    $invoice = new red_invoice($co);
    if(!is_null($this->timestamp)) $invoice->as_of_timestamp = $this->timestamp;
    $days = $invoice->get_number_of_days_behind();
    if(empty($days)) return;
    $days = intval($days);
    if($invoice->has_planned_payment()) {
      $index = 'pp';
    } else {
      $index = 'regular';
    }
    if(array_key_exists($days,$this->extra_notifications[$index])) {
      $subject = "Member termination alert (" . $invoice->get_invoice_currency() . 
        ") for: " . $invoice->member_friendly_name;
      $body = $invoice->member_friendly_name . " is " . $days . " days behind payment ".
        "for ".  "their invoice (#" . $invoice->get_invoice_id() . ")";
      $body = wordwrap($body);
      reset($this->extra_notifications[$index][$days]);
      foreach($this->extra_notifications[$index][$days] as $email) {
        if($this->no_act) {
          echo "Would send extra notification to: " . $email . " for " .
            $invoice->member_friendly_name . "\n";
        } else {
          mail($email,$subject,$body);    
        }
      }
    }
  }

  function _get_invoice_reminder_cutoff_date() {
    // 30 days before today (i.e. send reminders for unpaid
    // invoices dated 30 days ago or more
    if(empty($this->timestamp)) {
      $ts = strtotime('29 days ago');
    } else {
      $ts = $this->timestamp - (29 * 86400);
    }
    return date('Y-m-d',$ts);
  }

}
