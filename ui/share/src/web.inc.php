<?php

// $config array is in the config file, included by index.php

// session is only used for verifying form tokens,
// not for authentication
$secure_cookies = true;
session_set_cookie_params (0,null,null,$secure_cookies);

// start the session and first set language settings
session_start();
require_once($config['common_src_path'] . '/red.lang.utils.inc.php');

// Get required red libraries
require_once($config['common_src_path'] . '/red.utils.inc.php');
require_once($config['common_src_path'] . '/class.red_db.inc.php');
require_once($config['common_src_path'] . '/class.red_ado.inc.php');
require_once($config['common_src_path'] . '/class.red_item.inc.php');
require_once($config['src_path'] . '/web/class.html_generator.inc.php');
require_once($config['src_path'] . '/web/class.red_area.inc.php');

// Client Red functions 
require_once($config['src_path'] . '/functions.inc.php');


// This class handles html templating 
require_once($config['src_path'] . '/web/class.red_template.inc.php');
$template = new cred_template($config['ihtml_path']);
$template->set_file('main_file','main.ihtml');
$template->set_var('url_path', $config['url_path']);
$template->set_var('html_title',$config['org_name']);

// this is an html generator used by many of the objects 
// being called
$html_generator = new html_generator();

$sql_resource = red_db::init_db($config);  

if($sql_resource === FALSE) {
  // This is not recoverable - and should trigger output
  exit;
}

// This class handles users authentication
require_once($config['src_path'] . '/class.red_auth.inc.php');
$auth = new cred_auth();
$auth->set_template_object($template);
$auth->set_sql_resource($sql_resource);

$auth->set_secure_cookie_flag($secure_cookies);

$globals = array();
$globals['user_input'] = cred_sanitize_user_input($_REQUEST);

$globals['config'] = $config;  
$globals['template'] =& $template;
$globals['sql_resource'] = $sql_resource;
$globals['standard_construction_options'] = array(
  'mode' => 'ui',
  'notify_host' => $config['notify_host'],
  'sql_resource' => $sql_resource,
  'src_path' => $config['src_path'],
  'common_src_path' => $config['common_src_path'],
  'site_dir_template' => $config['site_dir_template'],
);
// Optional config items...
if(array_key_exists('notify_cmd', $config)) {
  $globals['standard_construction_options']['notify_cmd'] = $config['notify_cmd'];
}

$globals['html_generator'] =& $html_generator;


// sanity checking
if($secure_cookies && 
  (!array_key_exists('HTTPS',$_SERVER) || $_SERVER['HTTPS'] != 'on')) {
  red_set_message(red_t("Warning: secure_cookies are configured, but you are accessing the site via http. You may experience problems."),'error');
}

// Hash verification should take place prior to login in case they
// get logged out along the way or open in a different browser.
if(!empty($globals['user_input']['email_verify_hash'])) {
  require_once($globals['config']['src_path'] . '/modules/class.red.email_verify.inc.php');
  red_email_verify::validate_hash($globals['user_input']['email_verify_hash']);
}

if($auth->login()) {
  if(!empty($globals['user_input']['logout'])) {
    $auth->logout();
    cred_display_login_form($auth->get_login_message());
  }
  else {
    $globals['auth'] =& $auth;
    cred_main();
  }
}
else {
  cred_display_login_form($auth->get_login_message());
}

$template->display_messages();

cred_exit();

function cred_main() {
  global $globals;
  
  // store globals and user input in easier to reference variables
  $service_id = $globals['user_input']['service_id'];
  $action = $globals['user_input']['action'];
  $area = $globals['user_input']['area'];
  $session_id = $globals['user_input']['session_id'];
  $template =& $globals['template'];
  $auth =& $globals['auth'];
  $user_name = $auth->get_user_name();
  $co = $globals['standard_construction_options'];
  $co['service_id'] = $service_id;
  $search_keywords = $globals['user_input']['search_keywords'];
  $filter_keywords = $globals['user_input']['filter_keywords'];

  // authorize the user
  require_once($globals['config']['src_path'] . '/class.red_authz.inc.php');
  $authz = new cred_authz();
  $authz->set_sql_resource($globals['sql_resource']);
  $authz->set_user_name($user_name);

  $is_admin = $authz->is_admin();

  $globals['authz'] =& $authz;

  // set commonly used template variables
  $template->set_var('user_name',$user_name);
  $template->self_url = "index.php"; 

  // set the default area
  if(!$area) $area = 'hosting_order';

  // add area and service_id to self_url parts
  $template->self_url_parts['area'] = $area;
  if($service_id) $template->self_url_parts['service_id'] = $service_id;

  // set core navigation
  $template->set_file('core_navigation_file','core_navigation.ihtml');
  $template->set_var('lang_home',red_t('Home'));
  $template->set_var('lang_my_settings',red_t('My Settings'));
  $template->set_var('lang_reports',red_t('Reports'));
  if($is_admin) {
    // Show the accounting area menu link
    $template->set_var('lang_accounting',red_t('Accounting'));
  }
  else {
    // Hide the accounting menu link
    $template->set_block('core_navigation_file', 'red_accounting_link', 'red_accounting_link');
    $template->set_var('red_accounting_link', '');
  }

  $template->set_var('lang_help',red_t('Help'));
  $template->set_var('lang_logout',red_t('Logout'));
  $template->parse('core_navigation_block','core_navigation_file');

  // down for maintenance
  if(!$is_admin && array_key_exists('site_down_message',$globals['config'])) {
    $message = $globals['config']['site_down_message'];
    if($message !== false) {
      red_set_message($message,'error');
      $template->set_var('user_name',$user_name);
      return;
    }
  }
  // Check to see if we should be announcing an election
  if(array_key_exists('display_voting_token',$globals['config'])) {
    $election = $globals['config']['display_voting_token']['election'];
    $token = cred_get_voting_token($user_name, $election);
    $link = $globals['config']['display_voting_token']['link'] . '/' . $token;
    $template->announce_vote($election, $link, $token);
  }
  // logged in user is disabled
  if($authz->is_disabled()) {
    $message = red_t("You login is disabled. Disabled accounts are typically the result of failure to pay your membership dues. Please contact info@mayfirst.org immediately.");
    red_set_message($message,'error');
    $template->set_var('user_name',$user_name);
    return;
  }

  // capture help, reports, and settings request
  switch($area) {
    case 'help':
      cred_help();
      return;

    case 'settings':
      cred_settings($user_name);
      return;

    case 'reports':
      cred_reports();
      return;

    case 'accounting':
      cred_accounting();
      return;
    case 'sso':
      require_once($globals['config']['src_path'] . '/web/class.red_sso.inc.php');
      $sso = new red_sso();
      $sso->set_user_input($globals['user_input']);
      $sso->set_sso_secret($globals['config']['sso_secret']);
      $sso->set_redirect_url($globals['config']['sso_redirect_url']);
      $sso->set_user_name($user_name);
      $sso->set_item_id($auth->get_item_id());
      $sso->set_template($template);
      $sso->set_html_generator($globals['html_generator']);
      $sso->run();
      return;

    case 'oauth':
      require_once($globals['config']['src_path'] . '/web/class.red_oauth.inc.php');
      $oauth = new red_oauth();
      $oauth->set_user_input($globals['user_input']);
      $oauth->set_credentials($globals['config']['oauth_client_id'], $globals['config']['oauth_client_secret']);
      $oauth->set_user_name($user_name);
      $oauth->set_server($globals['config']['oauth_server']);
      $oauth->set_template($template);
      $oauth->set_html_generator($globals['html_generator']);
      $oauth->run();
      return;

  }

  // the list object controls how the page will be displayed
  $area_object = red_area::get_red_object($area,$co);

  // set authorization info
  $area_object->is_admin = $is_admin;
  $area_object->user_name = $user_name;

  // the area_object sets the default service id if it's empty 
  if(empty($service_id)) $service_id = $area_object->service_id;

  // unit_key_field - e.g. hosting_order_id (if we're in the 
  // hosting_order area), member_id (if in the member area), etc.
  $unit_key_field = $area_object->unit_key_field;

  // service_key_field - e.g. item_id (if we're in the 
  // hosting_order area), hosting_order_id or item_id (if we're in the
  // member area), etc.
  $service_key_field = $area_object->service_key_field;

  $unit_id = '';
  if(array_key_exists($unit_key_field,$globals['user_input']))
    $unit_id = $globals['user_input'][$unit_key_field];

  $id = '';
  if(array_key_exists($service_key_field,$globals['user_input'])) {
    $id = $globals['user_input'][$service_key_field];
  }

  $area_object->id = $id;

  // figure out what units are available to this user
  $area_object->set_available_units();

  // if no unit_id is selected, grab the first one from the
  // list of available ones.
  $area_object->set_unit_id($unit_id);
  $unit_id = $area_object->unit_id;

  if(!empty($search_keywords)) $area_object->parse_search_keywords($search_keywords);
  if(!empty($filter_keywords)) $area_object->filter_keywords = $filter_keywords;

  // If they are submitting a new item to be written, make sure the id
  // has not been tampered with. It should come through with the sf_
  // prefix.
  $sf_key = 'sf_' . $service_key_field;
  if(array_key_exists($sf_key, $_POST)) {
    $sf_id = $_POST[$sf_key];
    if(!empty($sf_id)) {
      // Ensure we use the actual key being written when validating.
      $id = $sf_id;
    }
  }

  // make sure the user has access to the area, unit_id, and id
  // they are trying to access
  if(!$area_object->check_access($id, $action)) {
    // error_id 4 simply means that don't have access to anything
    // so send them to their settings page
    if(array_key_exists(4,$area_object->errors) && $area == 'hosting_order') {
      cred_settings($user_name);
      return;
    } else {
      red_set_message(red_t("Sorry, you don't seem to have access to this area."),'error');
      red_set_message($area_object->errors);
      return false;
    }
  }

  // set globals for the area_object
  $area_object->template =& $template;
  $area_object->html_generator =& $globals['html_generator'];
  $area_object->config =& $globals['config'];
  if(!empty($unit_key_field)) $template->self_url_parts[$unit_key_field] = $unit_id;

  switch($action) {
    case 'delete':  
      $confirm = $globals['user_input']['confirm'];
      $cancel = $globals['user_input']['cancel'];
      $area_object->delete_child($id,$session_id,$confirm,$cancel);
      break;

    case 'disable':  
      $confirm = $globals['user_input']['confirm'];
      $cancel = $globals['user_input']['cancel'];
      $area_object->disable_child($id,$session_id,$confirm,$cancel);
      break;

      // Write the item specified to the database
    case 'enable':
      $confirm = $globals['user_input']['confirm'];
      $cancel = $globals['user_input']['cancel'];
      $area_object->enable_child($id,$session_id,$confirm,$cancel);
      break;

    case 'write':


      if($area_object->write_child($session_id)) break;
      // there was an error, don't break, fall through to edit
      // which will display what the user filled in

    case 'edit':
      $area_object->edit_child($id);
      break;

    case 'new':
      $area_object->new_child();
      break;

    case 'print':
      $area_object->print_child($id);
      break;

  }

  $start = $globals['user_input']['start'];
  $limit = $globals['user_input']['limit'];
  if(!$area_object->list_children($start,$limit)) return false;
  return true;

}

?>
