<?php

/**
 * Class for checking for and sending disk usage notifications.
 *
 * Instantiate with:
 *
 * require_once('/path/to/config.inc.php');
 * require_once($config['common_src_path'] . '/red.lang.utils.inc.php');
 * require_once($config['common_src_path'] . '/red.utils.inc.php');
 * require_once($config['common_src_path'] . '/class.red_db.inc.php');
 * require_once($config['src_path'] . '/class.red_disk_usage_notifier.inc.php');
 *
 * $sql_resource = red_db::init_db($config);
 * $notifier = new red_disk_usage_notifier($sql_resource);
 * $notifyier->notify();
 *
 */

class red_disk_usage_notifier extends red_ado{

  var $debug = FALSE;

  function log($val) {
    if ($this->debug) {
      if (is_array($val)) {
        print_r($val);
      }
      else {
        echo "$val\n";
      }
    }
  }
  function notify() {
    $array = $this->get_notification_array();
    // $this->log($items);
    foreach($array as $values ) {
      $this->send_notifications($values);
      $this->update_notify_date($values);
    }
  }

  function update_notify_date($values) {
    foreach($values['items'] as $item) {
      $item_id = intval($item['item_id']);
      $date = date('Y-m-d H:i:s', strtotime("+7 days"));
      $sql = "UPDATE red_item SET item_quota_notify_date = '$date' WHERE item_id = $item_id";
      $this->_sql_query($sql);
    }
  }
  function send_notifications($values) {
    //$this->log($values);
    $member_friendly_name = $values['member_friendly_name'];
    $items = $values['items'];
    foreach($values['contacts'] as $contact) {
      $lang = $contact['contact_lang'];
      $subject = $this->get_email_template('disk_quota.subject', $lang);
      $body = $this->get_email_template('disk_quota.body', $lang);
      $replace = [
        '{member_friendly_name}' => $member_friendly_name,
        '{contact_first_name}' => $contact['contact_first_name'],
        '{disk_usage_summary}' => $this->format_disk_usage_summary($items)
      ];
      $subject = str_replace(array_keys($replace), $replace, $subject );
      $body = str_replace(array_keys($replace), $replace, $body );
      $values = array(
        'member_id' => $values['member_id'],
        'correspondence_subject' => $subject,
        'correspondence_body' => $body,
        'correspondence_to' => $contact['contact_email']
      );
      
      $ret = $this->create_related_object('red_correspondence',$values);
      if(!$ret) {
        red_set_message($this->get_errors_as_string(), 'error'); 
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Get contacts for member.
   */
  function get_contacts_for_member($member_id) {
    $ret = [];
    $member_id = intval($member_id);
    $sql = "SELECT contact_first_name, contact_last_name, contact_email, contact_lang
      FROM red_contact WHERE contact_status = 'active' AND member_id = $member_id";
    $result = $this->_sql_query($sql);
    while ($row = $this->_sql_fetch_assoc($result)) {
      $ret[] = $row;
    }
    return $ret;
  }

  /**
   * Format array of items into a message
   *
   */
  function format_disk_usage_summary($items) {
    $ret = [];
    foreach($items as $item) {
      $quota = red_human_readable_bytes(($item['item_quota']));
      $disk_usage = red_human_readable_bytes(($item['item_disk_usage']));
      $ret[] = $item['service_name'] . ' (' . $item['identifier'] . '): ' . $disk_usage . ' (in use) ' . $quota . ' (quota)';  
    }
    return implode("\n", $ret);
  }

  /**
   * Return array of items that need notification.
   *
   * Create an array, indexed by the member_id, of each item
   * that is near quota.
   *
   **/
  function get_notification_array() {
    $ret = [];
    $now = date('Y-m-d H:i:s');
    $sql = "
      SELECT 
        red_member.member_id, member_friendly_name, red_service.service_id, 
        service_name, item_id, item_status, item_disk_usage, item_quota
      FROM red_member 
        JOIN red_hosting_order USING(member_id) 
        JOIN red_item USING(hosting_order_id)
        JOIN red_service USING(service_id)
      WHERE 
        item_status != 'deleted' AND
        (item_quota_notify_date IS NULL OR item_quota_notify_date < '$now') AND
        item_quota > 0 AND
        item_quota_notify_percent > 0 AND
        item_disk_usage / item_quota * 100 >= item_quota_notify_percent
    ";
    // $this->log($sql);
    $result = $this->_sql_query($sql);
    while ($row = $this->_sql_fetch_assoc($result)) {
      $member_id = $row['member_id'];
      if (!array_key_exists($member_id, $ret)) {
        $ret[$member_id] = [ 
          'member_id' => $member_id,
          'member_friendly_name' => $row['member_friendly_name'],
          'contacts' => $this->get_contacts_for_member($member_id),
          'items' => [],
        ];
      }

      if (count($ret[$member_id]['contacts']) == 0) {
        continue;
      }
      // Convert to friendlier units.
      $row['item_quota'] = red_human_readable_bytes($row['item_quota']);
      $row['item_disk_usage'] = red_human_readable_bytes($row['item_disk_usage']);

      // Depending on the service, add an identifier (e.g. database name, login name)
      $item_id = intval($row['item_id']);
      $sql = NULL;
      switch ($row['service_id']) {
        case 1:
          $sql = "SELECT user_account_login AS identifier FROM red_item_user_account WHERE item_id = $item_id";

          break;
        case 7:
          $sql = "SELECT hosting_order_identifier AS identifier FROM red_hosting_order JOIN red_item USING(hosting_order_id) WHERE item_id = $item_id";
          break;
        case 20:
          $sql = "SELECT mysql_db_name AS identifier FROM red_item_mysql_db WHERE item_id = $item_id";
          break;
        default:
          // If we somehow get another item just skip it.
          continue 2;
      }
      $id_result = $this->_sql_query($sql);
      $id_row = $this->_sql_fetch_assoc($id_result);
      $row['identifier'] = $id_row['identifier'];

      $ret[$member_id]['items'][] = $row; 
    }
    //$this->log($ret);
    return $ret;
  }
}


?>
