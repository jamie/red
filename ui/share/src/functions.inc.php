<?php

function cred_get_item_id_for_user_name($user_name) {
  global $globals;
  $user_name = addslashes($user_name);
  $sql = "SELECT red_item.item_id FROM red_item INNER JOIN ".
    "red_item_user_account USING (item_id) WHERE user_account_login ".
    "= '$user_name' AND (item_status = 'active' OR item_status = ".
    "'pending-insert' OR item_status = 'pending-update' OR item_status = ".
    "'pending-restore')";
  $sql_resource = $globals['sql_resource'];
  $result = red_sql_query($sql,$sql_resource);
  $row = red_sql_fetch_row($result);
  return $row[0];
}
function cred_get_voting_token($user_name, $election) {
  global $globals;
  $user_name = addslashes($user_name);
  $election = addslashes($election);
  $sql = "SELECT voting_token FROM red_voting_token JOIN red_member USING(member_id) ".
    "JOIN red_hosting_order USING(member_id) JOIN red_item USING(hosting_order_id) JOIN ".
    "red_item_user_account USING(item_id) " .
    "WHERE user_account_login = '$user_name' AND voting_token_election = '$election'";
  $sql_resource = $globals['sql_resource'];
  $result = red_sql_query($sql,$sql_resource);
  $row = red_sql_fetch_row($result);
  return $row[0];
}


function cred_write_settings($user_name) {
  global $globals;
  $standard_construction_options = $globals['standard_construction_options'];
  $template =& $globals['template'];

  $write_co = $standard_construction_options;
  $post = cred_un_magic_quotes($_POST);
  $item_id = cred_get_item_id_for_user_name($user_name);
  if($item_id != $post['sf_item_id']) {
    $args = array('@item_id' => $item_id, '@sf_item_id' => $post['sf_item_id']);
    $message = red_t("Failed to save settings - item_id mismatch ('@item_id' vs ".
               "'@sf_item_id').",$args);
    red_set_message($message,'error');
    return false;
  }
  $write_co['service_id'] = cred_get_service_id_for_item_id($item_id);
  $write_co['hosting_order_id'] = cred_get_hosting_order_id_for_item_id($item_id);
  $write_item =& red_item::get_red_object($write_co);
  if(!$write_item) {
    $message = red_t("Failed to create a red object when trying to write. ".
               "Please check the red_error_log table for details.");
    red_set_message($message,'error');
    return false;
  }

  cred_add_notify_user($write_item);
  $write_item->set_user_input($post);
  $session_id = $globals['user_input']['session_id'];
  if(empty($session_id) || $session_id != session_id()) {
    red_set_message(red_t("Missing or invalid session id. You are either experiencing a bug or someting sneaky is going on. If you really want to edit/insert this record, please try again. Otherwise, please contact your system administrator."),'error');
    return $write_item;
  }
  // If the user makes a input error - return this item as the
  // edit_item. with these values filled in
  if(!$write_item->validate()) {
    red_set_message($write_item->get_errors('validation'),'error');
    return $write_item;
  }
  elseif(!$write_item->commit_to_db()) {
    red_set_message($write_item->get_errors('system'),'error');
    return $write_item;
  }
  else {
    red_set_message(red_t('Recorded updated.'),'success');
    // Surprise, it all worked out, break
    return true;
  }
}

/**
  * Does the passed $user have access to the passed in $invoice_id?
  *
  **/
function cred_user_has_access_to_invoice_id( $user, $invoice_id) {
  $invoice_id = intval($invoice_id);
  $user = addslashes($user);
  $sql = "SELECT invoice_id FROM red_invoice  
    JOIN red_hosting_order USING(member_id) JOIN red_item USING(hosting_order_id) 
    JOIN red_item_user_account USING(item_id) WHERE invoice_id = $invoice_id
    AND user_account_login = '$user'";
  $ret = cred_sql_get_field($sql);
  if(empty($ret)) return FALSE;
  return TRUE;
}

/**
  * Does the passed $user have access to the passed in $payment_id?
  *
  **/
function cred_user_has_access_to_payment_id( $user, $payment_id) {
  $payment_id = intval($payment_id);
  $user = addslashes($user);
  $sql = "SELECT payment_id FROM red_payment JOIN red_invoice USING(invoice_id) 
    JOIN red_hosting_order USING(member_id) JOIN red_item USING(hosting_order_id) 
    JOIN red_item_user_account USING(item_id) WHERE payment_id = $payment_id
    AND user_account_login = '$user'";
  $ret = cred_sql_get_field($sql);
  if(empty($ret)) return FALSE;
  return TRUE;
}

function cred_get_member_parent_id_for_username($user_name) {
  $user_name = addslashes($user_name);
  $sql = "SELECT member_id FROM red_map_user_server WHERE login = '$user_name' AND status = 'active'";
  return cred_sql_get_field($sql);
}

function cred_accounting_process_submissions() {
  global $globals;
  $co = $standard_construction_options = $globals['standard_construction_options'];

  // Handle online payments form
  if(array_key_exists('members', $globals['user_input'])) {
    $members = $globals['user_input']['members'];
    reset($members);
    foreach($members as $online_payment_id => $member_id) {
      // Get details of the online payment
      $online_payment_id = intval($online_payment_id);
      $member_id = intval($member_id);

      // Don't act if no member_id is selected.
      if(empty($member_id)) continue;
      $sql = "SELECT online_payment_identifier, bank_id,
       online_payment_amount, online_payment_date FROM
       red_online_payment 
       WHERE online_payment_id = $online_payment_id";
      $result = red_sql_query($sql, $globals['sql_resource']);
      $row = red_sql_fetch_row($result);
      if(empty($row[0])) {
        $msg = red_t("Failed to find online payment id @id", array('@id' => $online_payment_id));
        red_set_message($msg);
        continue;
      }

      $payment_identifier = $row[0];
      $bank_id = $row[1];
      $payment_amount = $row[2];
      $date = $row[3];

      $apply_to_invoice_ids = cred_accounting_get_payable_invoices($member_id, $payment_amount);  

      if($apply_to_invoice_ids === FALSE) continue;

      foreach($apply_to_invoice_ids as $invoice_id => $amount) {
        $payment = new red_payment($co);
        $payment->set_invoice_id($invoice_id);
        $payment->set_payment_identifier($payment_identifier);
        $payment->set_bank_id($bank_id);
        $payment->set_payment_amount($amount);
        $payment->set_payment_date(substr($date,0, 10));
        $payment->set_payment_method('credit');
        $payment->set_payment_notes("Payment made via control panel bulk payment assignment form.");
        $payment->is_admin = TRUE;
        if($payment->commit_to_db()) {
          $msg = red_t("Committed payment for invoice id #@invoice_id.", array('@invoice_id' => $invoice_id));
          // Now we can safely delete from online_payment table
          $sql = "DELETE FROM red_online_payment WHERE online_payment_id = $online_payment_id";
          red_sql_query($sql, $globals['sql_resource']);
        }
        else {
          $msg = red_t("Failed to commit payment for invoice id #@invoice_id.", array('@invoice_id' => $invoice_id));
          $msg .= ' ' . $payment->get_errors_as_string('system');
        }
        red_set_message($msg);
      }
    }
  }
  // Handle check payments form.
  if(array_key_exists('member_id_check_payment', $globals['user_input'])) {
    $input = $globals['user_input'];
    // Check for invoices to apply this too.
    $member_id = intval($input['member_id_check_payment']);
    $payment_amount = $input['payment_amount'];

    $apply_to_invoice_ids = cred_accounting_get_payable_invoices($member_id, $payment_amount);  

    if($apply_to_invoice_ids === FALSE) return;

    foreach($apply_to_invoice_ids as $invoice_id => $amount) {
      $payment = new red_payment($co);
      $payment->set_invoice_id($invoice_id);
      $payment->set_payment_identifier($input['payment_identifier']);
      $payment->set_bank_id($input['bank_id']);
      $payment->set_payment_amount($amount);
      $payment->set_payment_date(substr($input['payment_date'], 0, 10));
      $payment->set_payment_method($input['payment_method']);
      $payment->set_payment_notes("Payment made via control panel bulk payment assignment form.");
      $payment->is_admin = TRUE;
      if($payment->commit_to_db()) {
        $msg = red_t("Committed payment for invoice id #@invoice_id.", array('@invoice_id' => $invoice_id));
      }
      else {
        $msg = red_t("Failed to commit payment for invoice id #@invoice_id.", array('@invoice_id' => $invoice_id));
        $msg .= ' ' . $payment->get_errors_as_string('system');
      }
      red_set_message($msg);
    }
  }
}

function cred_accounting_get_payable_invoices($member_id, $payment_amount) {
  global $globals;

  $amount_submitted = $remaining_amount = $payment_amount;
  $apply_to_invoice_ids = array();
  // Get list of unpaid invoices, with most recent first.
  $sql = "SELECT invoice_id, invoice_amount FROM red_invoice WHERE 
    invoice_status = 'unpaid' AND member_id = $member_id 
    ORDER BY invoice_date DESC";
  $result = red_sql_query($sql, $globals['sql_resource']);
  while($row = red_sql_fetch_row($result)) {
    $invoice_id = intval($row[0]);
    // Get amount already paid to this invoice.
    $sql = "SELECT SUM(payment_amount) FROM red_payment WHERE payment_status = 'active' AND invoice_id = $invoice_id";
    $presult = red_sql_query($sql, $globals['sql_resource']);
    $prow = red_sql_fetch_row($presult);
    $paid = $prow[0];
    $invoice_amount = $row[1] - $paid;
    if($remaining_amount == $invoice_amount) {
      $remaining_amount = 0;
      $apply_to_invoice_ids[$invoice_id] = $invoice_amount;
      break;
    }
    elseif($remaining_amount > $invoice_amount) {
      $remaining_amount = $remaining_amount - $invoice_amount;
      $apply_to_invoice_ids[$invoice_id] = $invoice_amount;
    }
    elseif($remaining_amount < $invoice_amount) {
      $apply_to_invoice_ids[$invoice_id] = $remaining_amount;
      $remaining_amount = 0;
      break;
    }
  }
  if(!empty($remaining_amount)) {
      $msg = red_t("Failed to commit payment for member_id @member_id because there would be money left over (@remaining)", 
        array('@member_id' => $member_id, '@remaining' => $remaining_amount));
      red_set_message($msg);
      return FALSE;
   }
   elseif(empty($apply_to_invoice_ids)) {
     $msg = red_t("Failed to commit payment for member_id @member_id because no open invoices.", 
       array('@member_id' => $member_id));
     red_set_message($msg);
     return FALSE;
   }

   return $apply_to_invoice_ids;

}

function cred_accounting() {
  global $globals;
  $sql_resource = $globals['sql_resource'];
  $template =& $globals['template'];
  $is_admin = $globals['authz']->is_admin();
  if($is_admin) {
    // Update any records in red_online_payments with an invoice_id but no member id
    $sql = "SELECT invoice_id FROM red_online_payment WHERE member_id IS NULL OR member_id = 0";
    $result = red_sql_query($sql, $globals['sql_resource']);
    while($row = red_sql_fetch_row($result)) {
      $invoice_id = intval($row[0]);
      $sql = "SELECT member_id FROM red_invoice WHERE invoice_id = $invoice_id";
      $mresult = red_sql_query($sql, $globals['sql_resource']);
      $mrow = red_sql_fetch_row($mresult);
      if($mrow[0]) {
        $member_id = intval($mrow[0]);
        $sql = "UPDATE red_online_payment SET member_id = $member_id WHERE invoice_id = $invoice_id";
        red_set_message(red_t("Updating online payment with invoice id @invoice_id to member id @member_id", array('@invoice_id' => $invoice_id, '@member_id' => $member_id)));
        red_sql_query($sql, $globals['sql_resource']);
      }
    }
    require_once($globals['config']['src_path'] . '/modules/class.red.payment.inc.php');
    cred_accounting_process_submissions();
    $template->set_file('accounting_file','accounting.ihtml');
    // Handle recent payments
    $after = date('Y-m-d', strtotime("3 weeks ago"));
    $sql = "SELECT payment_date, invoice_id, payment_amount, bank_id,
      member_friendly_name, payment_identifier FROM red_payment JOIN
      red_invoice USING(invoice_id) JOIN red_member USING(member_id) 
      WHERE payment_date > '$after' AND payment_status = 'active'
      ORDER BY payment_date"; 
    $result = red_sql_query($sql, $globals['sql_resource']);
    $template->set_block('accounting_file','accounting_payment','accounting_payments');
    $template->set_var('lang_recent_payments', red_t("Recent Payments"));
    $banks = red_payment::get_bank_id_options();
    $template->set_var('lang_invoice_id', 'Invoice ID');
    $template->set_var('lang_member_name', 'Member Name');
    $template->set_var('lang_payment_date', 'Payment Date');
    $template->set_var('lang_payment_amount', 'Payment Amount');
    $template->set_var('lang_bank', 'Bank');
    $template->set_var('lang_payment_identifier', 'Identifier');
    while($row = red_sql_fetch_row($result)) {
      $template->set_var('payment_date', $row[0]);
      $template->set_var('payment_invoice_id', $row[1]);
      $template->set_var('payment_amount', $row[2]);
      $template->set_var('payment_bank', $banks[$row[3]]);
      $template->set_var('member_friendly_name', $row[4]);
      $template->set_var('payment_identifier', $row[5]);
      $template->parse('accounting_payments', 'accounting_payment', TRUE);
    }

    $online_payments = cred_accounting_get_online_payments_info();
    $membership_options = cred_get_membership_options();
    $template->display_check_submission_form($membership_options);
    if(count($online_payments) > 0) {
      // red_set_message($online_payments);
      $membership_option = array(
        '' => red_t("Do not apply this payment")
      );
      $membership_options = $membership_option + $membership_options;
      $template->display_online_payments($online_payments, $membership_options);
    }
    else {
      $template->set_block('accounting_file','accounting_online_payment_form','deleteme');
      $template->set_var('deleteme', '');
      red_set_message(red_t("No online payments have been recorded."));
    }
    $template->parse('body_block','accounting_file');
  }
}
function cred_get_membership_options() {
  $sql = "SELECT DISTINCT member_id, member_friendly_name
    FROM red_member JOIN red_invoice USING(member_id)   
    WHERE invoice_status = 'unpaid' AND member_currency = 'USD' ORDER BY member_friendly_name";
  global $globals;
  $result = red_sql_query($sql, $globals['sql_resource']);
  $ret = array();
  while($row = red_sql_fetch_row($result)) {
    $member_id = $row[0];
    $member_friendly_name = $row[1];
    $ret[$member_id] = "$member_id $member_friendly_name";
  }
  return $ret;
}

function cred_accounting_get_online_payments_info() {
  $ret = array();
  $sql = "SELECT online_payment_id, online_payment_identifier, invoice_id,
    bank_id, online_payment_amount, online_payment_email, online_payment_notes,
    online_payment_date, member_id FROM red_online_payment";
  global $globals;
  $res = $globals['sql_resource'];
  $result = red_sql_query($sql,$res);
  global $globals;
  $banks = red_payment::get_bank_id_options();
  while($row = red_sql_fetch_row($result)) {
    $id = $row[0];
    $bank = NULL;
    if(!is_null($row[3])) {
      $bank_id = $row[3];
      $bank = $banks[$bank_id];
    }
    $ret[$id] = array(
      'identifier' => $row[1],
      'invoice_id' => $row[2],
      'bank' => $bank,
      'amount' => $row[4],
      'email' => $row[5],
      'notes' => $row[6],
      'date' => $row[7],
      'member_id' => $row[8],
    );
  }
  return $ret;
}

function cred_report_print_invoice($invoice_id, $sql_resource, $template) {
  $sql = "SELECT invoice_id, invoice_date, invoice_description, 
    invoice_amount, member_friendly_name, invoice_currency FROM 
    red_invoice JOIN red_member USING(member_id) 
   WHERE invoice_id = $invoice_id";
  $result = red_sql_query($sql,$sql_resource);
  $row = red_sql_fetch_row($result);
  if(empty($row[0])) {
    red_set_message(red_t("Failed to find that invoice id."));
    return;
  }
  $template->set_file('invoice_file', 'invoice.ihtml');
  $template->set_var('lang_invoice', red_t('Invoice')); 
  $template->set_var('lang_paid_to', red_t('Payment made to')); 
  $template->set_var('lang_payment_details', red_t('Payment Details')); 
  $template->set_var('lang_due_date', red_t('Due Date:')); 
  $template->set_var('lang_amount', red_t('Amount:')); 
  $template->set_var('lang_invoice_id', red_t('Invoice ID:')); 
  $template->set_var('lang_membership', red_t('Membership:')); 
  $template->set_var('lang_payment_for', red_t('For:')); 
  $template->set_var('invoice_id', $row[0]); 
  $template->set_var('invoice_date', substr($row[1], 0, 10)); 
  $template->set_var('invoice_description', $row[2]); 
  $template->set_var('invoice_amount', $row[3]); 
  $template->set_var('member_friendly_name', $row[4]); 
  $template->set_var('invoice_currency', $row[5]); 

  // Set remittance block depending on currency
  $template->set_var('lang_payment_info', red_t('Payment Information'));
  if($row[5] == 'USD') {
    $template->set_var('lang_pay_online', red_t("Pay Online"));
    $template->set_var('lang_make_checks_payable_to', red_t("Make checks payable to"));
    $template->set_file('payment.usd', 'payment.usd.ihtml');
    $template->parse('payment_instructions_block', 'payment.usd');
  }
  else {
    $template->set_var('lang_transfer_to', red_t("Transfer to the following bank account"));
    $template->set_file('payment.mxn', 'payment.mxn.ihtml');
    $template->parse('payment_instructions_block', 'payment.mxn');
  }

  $template->pparse('out','invoice_file');
  exit;
}

function cred_report_print_payment($payment_id, $sql_resource, $template) {
  $sql = "SELECT payment_id, payment_identifier, payment_date, i.invoice_id,
    invoice_description, payment_amount, member_friendly_name, invoice_currency
    FROM red_payment p JOIN red_invoice i USING(invoice_id) JOIN red_member
    USING(member_id) WHERE payment_id = $payment_id";
  $result = red_sql_query($sql,$sql_resource);
  $row = red_sql_fetch_row($result);
  if(empty($row[0])) {
    red_set_message(red_t("Failed to find that payment id."));
    return;
  }
  $template->set_file('receipt_file', 'receipt.ihtml');
  $template->set_var('lang_receipt', red_t('Payment Receipt')); 
  $template->set_var('lang_paid_to', red_t('Payment made to')); 
  $template->set_var('lang_payment_identifier', red_t('Payment identifier:')); 
  $template->set_var('lang_id', red_t('Payment id:')); 
  $template->set_var('lang_payment_details', red_t('Payment Details')); 
  $template->set_var('lang_date', red_t('Date:')); 
  $template->set_var('lang_amount', red_t('Amount:')); 
  $template->set_var('lang_invoice_id', red_t('Invoice ID:')); 
  $template->set_var('lang_membership', red_t('Membership:')); 
  $template->set_var('lang_payment_for', red_t('For:')); 
  $template->set_var('payment_id', $row[0]); 
  $template->set_var('payment_identifier', $row[1]); 
  $template->set_var('payment_date', $row[2]); 
  $template->set_var('invoice_id', $row[3]); 
  $template->set_var('invoice_description', $row[4]); 
  $template->set_var('payment_amount', $row[5]); 
  $template->set_var('member_friendly_name', $row[6]); 
  $template->set_var('invoice_currency', $row[7]); 
  $template->pparse('out','receipt_file');
  exit;
}

function cred_reports() {
  global $globals;
  $sql_resource = $globals['sql_resource'];
  $template =& $globals['template'];
  $user_name = $globals['auth']->get_user_name();
  $is_admin = $globals['authz']->is_admin();

  // If payment_id is passed it means we are printing a receipt
  if(!empty($globals['user_input']['payment_id'])) {
    $payment_id = intval($globals['user_input']['payment_id']);

    if($is_admin || cred_user_has_access_to_payment_id($user_name, $payment_id)) {
      cred_report_print_payment($payment_id, $sql_resource, $template);    
    }
    else {
      red_set_message(red_t("You don't have access to that payment."));
    }
  }
  // If invoice_id is passed it means we are printing an invoice
  if(!empty($globals['user_input']['invoice_id'])) {
    $invoice_id = intval($globals['user_input']['invoice_id']);

    if($is_admin || cred_user_has_access_to_invoice_id($user_name, $invoice_id)) {
      cred_report_print_invoice($invoice_id, $sql_resource, $template);    
    }
    else {
      red_set_message(red_t("You don't have access to that invoice."));
    }
  }

  $report_type = 'member-stats';
  if(!empty($globals['user_input']['report_type'])) {
    $report_type = $globals['user_input']['report_type'];
  }

  if ($report_type == 'member-stats') {
    // by default - set member_parent_id to null which show MFPL wide stats
    $member_parent_id = null;

    $allowed_member_parent_id = cred_get_member_parent_id_for_username($user_name);
    $passed_member_parent_id = $globals['user_input']['member_parent_id'];

    $template->set_file('reports_file','reports.ihtml');
    $template->set_var('lang_limit_results',red_t("Limit Results"));
    if(!$is_admin && !$allowed_member_parent_id) {
      // you only have access to mfpl - member wide stats
      $template->set_block('reports_file','red_stats_limit_form','red_stats_limit_form');
      $template->set_var('red_stats_limit_form','');
    } else {
      // you are an admin or you have your own server
      if (!$is_admin) {
        // you are only allowed MFPL and your own server...
        if($passed_member_parent_id == $allowed_member_parent_id) {
          $member_parent_id = $passed_member_parent_id;
        }
        $member_parents = cred_get_parent_members($allowed_member_parent_id);
      } else {
        // granted access to anything
        if($passed_member_parent_id != 1) $member_parent_id = $passed_member_parent_id;
        $member_parents = cred_get_parent_members();
      }
      $template->display_member_parent_menu($member_parents,$member_parent_id);
    }

    if(!is_null($member_parent_id)) {
      $sql = "SELECT member_friendly_name FROM red_member WHERE member_id = $member_parent_id";
      $result = red_sql_query($sql,$sql_resource);
      $row = red_sql_fetch_row($result);
      $title = "Results for " . $row[0];
    } else {
      $title = "Results for full MFPL membership";
    }

    $template->set_var('report_title',$title);
    $stats = cred_reports_get_stats($member_parent_id);
    $template->display_stats($stats);
  }
  else if ($is_admin) {
    // Display stats on members behind on payments.
   $template->set_file('reports_file','member-dues-report.ihtml');
   $title = "Members behind on payments";
   
   $data = cred_get_members_behind_on_dues(); 
  
   //$template->debug = TRUE;
   $template->set_block('reports_file','line','lines');
   $template->set_var('total', count($data));
   $member_count = 0;
   $disabled_count = 0;
   foreach($data as $row) {
     $template->set_var($row);
     $template->parse('lines', 'line', TRUE);
   }
  }
  $template->parse('body_block','reports_file');
}

function cred_get_members_behind_on_dues() {
  global $globals;
  $member_currency = $globals['user_input']['member_currency'];
  if ($member_currency == 'MXN') {
    $member_currency_clause = "AND member_currency = 'MXN'";
  }
  elseif ($member_currency == 'USD') {
    $member_currency_clause = "AND member_currency = 'USD'";
  }
  else {
    $member_currency_clause = 'AND member_currency IS NOT NULL';
  }

  $ret = [];
  $sql = "SELECT DISTINCT member_id, member_friendly_name, member_price + member_benefits_price as member_price, member_currency, invoice_date FROM red_member JOIN red_invoice USING(member_id) JOIN red_hosting_order USING(member_id) WHERE member_status = 'active' AND hosting_order_status = 'active' $member_currency_clause AND invoice_status = 'unpaid' AND invoice_date < NOW() - INTERVAL 3 MONTH GROUP BY member_id HAVING MAX(invoice_date)";
  $result = red_sql_query($sql, $globals['sql_resource']);
  while ($row = red_sql_fetch_assoc($result)) {
    $id = intval($row['member_id']);
    $total_sql = "SELECT SUM(COALESCE(invoice_amount,0)) FROM red_invoice WHERE invoice_status = 'unpaid' AND member_id = $id;";
    $count_sql = "SELECT COUNT(invoice_id) FROM red_invoice WHERE invoice_status = 'unpaid' AND member_id = $id;";
    $payment_sql = "SELECT payment_date FROM red_invoice JOIN red_payment USING(invoice_id) WHERE payment_status = 'active' AND member_id = $id ORDER BY payment_date DESC LIMIT 1";
    $total_result = red_sql_query($total_sql, $globals['sql_resource']);
    $count_result = red_sql_query($count_sql, $globals['sql_resource']);
    $payment_result = red_sql_query($payment_sql, $globals['sql_resource']);
    $count_row = red_sql_fetch_row($count_result);
    $total_row = red_sql_fetch_row($total_result);
    $payment_row = red_sql_fetch_row($payment_result);
    $row['owed'] = $total_row[0];
    $row['count'] = $count_row[0];
    $row['payment'] = substr($payment_row[0], 0, 10);
    $last_payment_index = strtotime($row['payment']);
    $ret[] = $row;
    
  }
  return $ret; 
}

function cred_get_member_dns_status($member_id) {
  global $globals;
  // Get an array of all IP addresses assigned to hosts.
  $sql = "SELECT dns_ip FROM red_item JOIN red_item_dns USING(item_id) WHERE item_status = 'active' AND dns_type = 'ptr'";
  $result = red_sql_query($sql, $globals['sql_resource']);
  $our_ips = [];
  while ($row = red_sql_fetch_row($result)) {
    $our_ips[] = $row[0]; 
  }
  $member_id = intval($member_id);
  // Collect all dns zones for this membership
  $sql = "SELECT DISTINCT dns_zone FROM red_item JOIN red_item_dns USING(item_id) WHERE item_status = 'active' AND member_id = $member_id AND dns_zone NOT REGEXP 'mayfirst\.(org|info|cx)' UNION DISTINCT SELECT DISTINCT dns_zone FROM red_hosting_order JOIN red_item USING(hosting_order_id) JOIN red_item_dns USING(item_id) WHERE item_status = 'active' AND red_hosting_order.member_id = $member_id AND dns_zone NOT REGEXP 'mayfirst\.(org|info|cx)'";
  $result = red_sql_query($sql, $globals['sql_resource']);
  $records = 0;
  while($row = red_sql_fetch_row($result)) {
    $records++;
    $domain = $row[0];

    // First try MX record.
    $mxhosts = [];
    if (getmxrr($domain, $mxhosts)) {
      foreach($mxhosts as $host) {
        if (preg_match('/\.mayfirst\.org$/', $host)) {
          return 'still with us';
        }
      }
    }
    // Now try A records.
    if ($records = @dns_get_record($domain, DNS_A)) {
      foreach($records as $host_values) {
        $ip = $host_values['ip'];
        if (in_array($ip, $our_ips)) {
          return 'still with us';
        }
      }
    }
    // Lastly try NS.
    if ($records = @dns_get_record($domain, DNS_NS)) {
      foreach($records as $host_values) {
        $host = $host_values['host'];
        if (preg_match('/\.mayfirst\.org$/', $host)) {
          return 'still with us';
        }
        if (preg_match('/\.cloudflare\.com$/', $host)) {
          return "can't tell (proxy)";
        }
        if (preg_match('/^adns(1|2).easydns\.com$/', $host)) {
          return "can't tell (proxy)";
        }
      }
    }
  }
  if ($records == 0) {
    return "can't tell (no domains)";
  }
  return 'probably gone';
}

function cred_get_parent_members($restrict = null) {
  global $globals;
  $ret = array(
    '1' => 'May First/People Link'
  );
  if(!is_null($restrict)) {
    $sql = "SELECT member_friendly_name FROM red_member WHERE member_id = " . intval($restrict);
    $result = red_sql_query($sql,$globals['sql_resource']);
    $row = red_sql_fetch_row($result);
    $ret[$restrict] = $row[0];
    return $ret;
  }
  $sql = "SELECT DISTINCT m1.member_parent_id, m2.member_friendly_Name FROM red_member m1 JOIN red_member m2 ON m1.member_parent_id = m2.member_id WHERE m1.member_status = 'active'";
  $result = red_sql_query($sql,$globals['sql_resource']);
  while($row = red_sql_fetch_row($result)) {
    $member_parent_id = $row[0];
    $member_friendly_name = $row[1];
    $ret[$member_parent_id] = $member_friendly_name;
  }
  return $ret;
}

function cred_reports_get_stats($member_id = null) {
  $ret = array();
  $ret['Members'] = cred_reports_get_members($member_id);
  if(is_null($member_id)) {
    // The following counts are only relevant membership-wide
    $ret['Members (parents)'] = cred_reports_get_parent_members();
    $ret['Members (children)'] = cred_reports_get_child_members();
    $ret['Members (independent)'] = cred_reports_get_independent_members();
    $ret['Members (organization)'] = cred_reports_get_org_members();
    $ret['Members (individuals)'] = cred_reports_get_ind_members();
    $ret['Members (USD)'] = cred_reports_get_usd_members();
    $ret['Members (MXN)'] = cred_reports_get_mxn_members();
    $ret['Members (delinquent)'] = cred_reports_get_delinquent_members();
    $ret['Members (not paying dues)'] = cred_reports_get_zero_dues_members();
    $ret['Members (paying dues)'] = cred_reports_get_nonzero_dues_members();
  }
  $ret['Hosting orders'] = cred_reports_get_hosting_orders($member_id);
  $items = cred_reports_get_items($member_id);
  $ret = array_merge($ret,$items);
  $ret['Unique, hosted Email accounts'] = cred_reports_get_unique_hosted_email($member_id);
  return $ret;
}

function cred_sql_get_field($sql) {
  global $globals;
  $res = $globals['sql_resource'];
  $result = red_sql_query($sql,$res);
  $row = red_sql_fetch_row($result);
  return $row[0];
}
function cred_reports_get_delinquent_members() {
  $sql = "SELECT count(DISTINCT red_member.member_id) FROM red_member JOIN red_invoice USING(member_id) WHERE member_status = 'active' AND invoice_status = 'unpaid' AND DATEDIFF(NOW(),invoice_date) > 30 ";
  return cred_sql_get_field($sql);
}
function cred_reports_get_mxn_members() {
  $sql = "SELECT count(member_id) FROM red_member WHERE member_status = 'active' AND member_currency = 'MXN'";
  return cred_sql_get_field($sql);
}
function cred_reports_get_org_members() {
  $sql = "SELECT count(member_id) FROM red_member WHERE member_status = 'active' AND member_type = 'organization'";
  return cred_sql_get_field($sql);
}
function cred_reports_get_ind_members() {
  $sql = "SELECT count(member_id) FROM red_member WHERE member_status = 'active' AND member_type = 'individual'";
  return cred_sql_get_field($sql);
}
function cred_reports_get_usd_members() {
  $sql = "SELECT count(member_id) FROM red_member WHERE member_status = 'active' AND member_currency = 'USD'";
  return cred_sql_get_field($sql);
}
function cred_reports_get_parent_members() {
  $sql = "SELECT count(DISTINCT a.member_id) FROM red_member a LEFT JOIN red_member b ON a.member_id = b.member_parent_id WHERE a.member_status = 'active' AND b.member_id IS NOT NULL";
  return cred_sql_get_field($sql);
}
function cred_reports_get_child_members() {
  $sql = "SELECT count(DISTINCT member_id) FROM red_member WHERE member_status = 'active' AND member_parent_id IS NOT NULL AND member_parent_id != 0";
  return cred_sql_get_field($sql);
}
function cred_reports_get_independent_members() {
  $sql = "SELECT count(DISTINCT member_id) FROM red_member WHERE member_status = 'active' AND (member_parent_id IS NULL OR member_parent_id = 0)";
  return cred_sql_get_field($sql);
}
function cred_reports_get_zero_dues_members() {
  $sql = "SELECT count(DISTINCT member_id) FROM red_member WHERE member_price = 0 AND member_status = 'active'";
  return cred_sql_get_field($sql);
}
function cred_reports_get_nonzero_dues_members() {
  $sql = "SELECT count(DISTINCT member_id) FROM red_member WHERE member_price != 0 AND member_status = 'active'";
  return cred_sql_get_field($sql);
}


function cred_reports_get_members($member_id) {
  $extra_where = '';

  if(!is_null($member_id)) {
    $member_id = intval($member_id);
    $extra_where = "AND (member_parent_id = $member_id OR member_id = $member_id)";
  }
  $sql = "SELECT count(member_id) FROM red_member WHERE member_status = 'active' $extra_where";
  return cred_sql_get_field($sql);
}

function cred_reports_get_hosting_orders($member_id) {
  $extra_where = '';
  $join = '';
  if(!is_null($member_id)) {
    $join = "JOIN red_member USING(member_id)";
    $member_id = intval($member_id);
    $extra_where = "AND (member_parent_id = $member_id OR member_id = $member_id)";
  }
  $sql = "SELECT count(hosting_order_id) FROM red_hosting_order $join WHERE (hosting_order_status = 'active' OR hosting_order_status = 'disabled') $extra_where";
  return cred_sql_get_field($sql);
}

function cred_reports_get_unique_hosted_email($member_id) {
  $extra_where = '';
  $extra_join = '';
  if(!is_null($member_id)) {
    $member_id = intval($member_id);
    $extra_where = "AND (member_parent_id = $member_id OR member_id = $member_id)";
    $extra_join = "JOIN red_hosting_order USING(hosting_order_id) JOIN red_member USING(member_id)";
  }
  $sql = "SELECT count(DISTINCT email_address_recipient) FROM red_item JOIN red_item_email_address USING(item_id) $extra_join WHERE item_status = 'active' AND email_address_recipient NOT LIKE '%@%' $extra_where";
  return cred_sql_get_field($sql);
}
function cred_reports_get_items($member_id) {
  $ret = array();
  $services = array('user_account' => 'User Accounts','list' => 'Email lists','dns' => 'DNS records','web_conf' => 'Web sites','server_access' => 'Shell accounts','email_address' => 'Email addresses');
  $sql_start = "SELECT count(item_id) FROM red_item JOIN";
  $extra_where = '';
  $extra_join = '';
  if(!is_null($member_id)) {
    $member_id = intval($member_id);
    $extra_where = "AND (member_parent_id = $member_id OR red_member.member_id = $member_id)";
    $extra_join = "JOIN red_hosting_order USING(hosting_order_id) JOIN red_member USING(member_id)";
  }
  foreach($services as $service => $label) {
    $sql = "$sql_start red_item_${service} USING(item_id) $extra_join WHERE item_status = 'active' $extra_where";
    $ret[$label] = cred_sql_get_field($sql);
  }
  return $ret;
}

function cred_settings($user_name) {
  global $globals;
  $edit_item = false;
  if(array_key_exists('session_id',$_POST)) {
    $result = cred_write_settings($user_name);
    if($result !== TRUE) $edit_item = $result;
  }
  $sql_resource = $globals['sql_resource'];
  $template =& $globals['template'];
  $item_id = cred_get_item_id_for_user_name($user_name);
  $edit_co = $standard_construction_options = $globals['standard_construction_options'];
  $edit_co['rs'] = cred_get_row_for_item_id($item_id,$sql_resource);
  if(!$edit_item)
    $edit_item =& red_item::get_red_object($edit_co);
  $js_includes = $edit_item->get_javascript_includes();
  $template->add_js($js_includes);
  $template->set_var('item_status',$edit_item->get_item_status());
  if(!$edit_item) {
    $message = red_t("Failed to create object for editing. Please check the ".
               "red_error_log table for details.");
    red_set_message($message,'error');
  }
  else {
    $edit_item->set_html_generator($globals['html_generator']);
    $template->set_file('settings_file','settings.ihtml');
    $template->set_var('lang_my_settings',red_t("My Settings"));
    $template->set_var('lang_status',red_t("Status:"));
    $template->set_var('lang_refresh_status',red_t("Refresh Status"));
    $template->set_var('edit_block',$edit_item->get_edit_block());
    $template->set_var('session_id',session_id());
    $template->parse('body_block','settings_file');
  }
}
function cred_help() {
  global $globals;
  $template =& $globals['template'];
  $template->set_file('help_file','help.ihtml');
  $template->parse('body_block','help_file');
}

// From Lightningbug
// This function checks to see of magic quotes are on. If so, it strips the added slashes
// thanks david at giffin dot org from php.net
function cred_un_magic_quotes(&$search_space) {
  if(get_magic_quotes_gpc()) {  // here magic quotes is on
    reset($search_space);
    if(is_array($search_space)) {
      $result = array();
      foreach($search_space as $k => $v) {
        if(is_array($v)) {
          $result[$k] = cred_un_magic_quotes($v);
        }
        else {
          $result[$k] = stripslashes($v);
        }
      }
      return $result;
    }
    else {
      return stripslashes($value);
    }
    return $value;
  }
  else {  // here we have magic quotes gpc off
    return $search_space;
  }
}

function cred_sanitize_user_input($user_input) {
  $user_input = cred_un_magic_quotes($user_input);

  // defaults
  $ret['area'] = 'hosting_order';
  $ret['action'] = '';
  $ret['hosting_order_id'] = '';
  $ret['member_id'] = '';
  $ret['service_id'] = '';
  $ret['payment_id'] = '';
  $ret['confirm'] = false;
  $ret['cancel'] = false;
  $ret['item_id'] = '';
  $ret['start'] = false;
  $ret['logout'] = false;
  $ret['limit'] = false;
  $ret['session_id'] = false;
  $ret['start'] = 0;
  $ret['limit'] = 50;
  $ret['search_keywords'] = '';
  $ret['filter_keywords'] = '';
  $ret['member_parent_id'] = 1;
  $ret['report_type'] = 'member-stats';
  $ret['member_currency'] = NULL;

  $tests = array(
    'area' => '/(hosting_order|member|top|settings|help|reports|accounting|oauth|sso)/',
    'action' => '/(new|write|edit|delete|print|disable|enable)/',
    'hosting_order_id' => '/[0-9]+/',
    'member_id' => '/[0-9]+/',
    'invoice_id' => '/[0-9]+/',
    'contact_id' => '/[0-9]+/',
    'tag_id' => '/[0-9]+/',
    'noteid' => '/[0-9]+/',
    'member_id_check_payment' => '/[0-9]+/',
    'payment_id' => '/[0-9]+/',
    'phone_id' => '/[0-9]+/',
    'postal_address_id' => '/[0-9]+/',
    'vps_id' => '/[0-9]+/',
    'note_id' => '/[0-9]+/',
    'correspondence_id' => '/[0-9]+/',
    'planned_payment_id' => '/[0-9]+/',
    'start' => '/[0-9]+/',
    'limit' => '/[0-9]+/',
    'map_user_member_id' => '/[0-9]+/',
    'map_user_server_id' => '/[0-9]+/',
    'item_id' => '/[0-9]+/',
    'service_id' => '/[0-9]+/',
    'start' => '/(false|[0-9]+)/',
    'limit' => '/(false|[0-9]+)/',
    'logout' => '/(true|1)/',
    'confirm' => '/(true|1|y|Y|yes|Yes)/',
    'cancel' => '/(false|0|n|N|no|No)/',
    'session_id' => '/[a-z0-9]+/',
    'search_keywords' => '/.*/',
    'filter_keywords' => '/.*/',
    'member_parent_id' => '/[0-9]+/',
    // These are all used in the accounting section
    'members' => '/([0-9]+|)/',
    'payment_amount' => RED_MONEY_MATCHER,
    'payment_identifier' => RED_TEXT_MATCHER,
    'payment_method' => RED_PAYMENT_METHOD_MATCHER,
    'payment_date' => RED_DATETIME_MATCHER,
    'bank_id' => RED_ID_MATCHER,
    // oauth 
    'consent_decision' => '/(accept|reject)/',
    'consent_email' => '/(' . RED_EMAIL_REGEXP . '|)/',
    'consent' => '/[A-Z0-0\-]+/',
    // sso
    'sso' => '/.*/',
    'sig' => '/.*/',
    'sso_confirm' => '/(accept|reject)/',
    'sso_email_address' => '/(' . RED_EMAIL_REGEXP . '|(other))/',
    'sso_email_address_other' => '/(' . RED_EMAIL_REGEXP . '|)/',
    'email_verify_hash' => '/.*/',
    'report_type' => '(member_stats|member-dues)',
    'member_currency' => '(MXN|USD)',
  );

  // indicate if a given variable should *only* come
  // from a particular input method (get, post, or cookie)
  $inputs = array(
    'session_id' => 'post',
    'members' => 'post',
    'payment_amount' => 'post',
    'payment_identifier' => 'post',
    'payment_method' => 'post',
    'payment_date' => 'post',
    'sso_confirm' => 'post'
  );
  foreach($tests as $key => $test) {
    if(array_key_exists($key,$user_input)) {
      if(array_key_exists($key,$inputs)) {
        if($inputs[$key] == 'post') {
          if(!array_key_exists($key,$_POST)) {
            trigger_error("Invalid $key - should only come via POST.",E_USER_ERROR);
          }
        }
      }
      if(is_array($user_input[$key])) {
        foreach($user_input[$key] as $inner_key => $value) {
          if(!preg_match($test,$value)) {
            trigger_error("Invalid $key for index $inner_key ($value).",E_USER_ERROR);
          }
        }
      }
      else {
        if(!preg_match($test,$user_input[$key])) {
          trigger_error("Invalid $key.",E_USER_ERROR);
        }
      }
      $ret[$key] = $user_input[$key];
    }
  }
  return $ret;
}

function cred_display_login_form($login_message) {
  global $globals;
  $template =& $globals['template'];
  $template->set_file('login_file','login.ihtml');
  if ($globals['user_input']['area'] == 'sso') {
    $template->set_var('lang_sso_login_explanation',red_t("The May First/People Link Discourse application would like you to login. You can login with <em>any</em> May First/People Link login that you have, e.g. the username and password you use to check your email, or copy files to your web site, or login via Nextcloud. If you need to reset your password, please follow this <a href=\"resetpass\">link</a>."));
  }

  $template->set_var('lang_user_name',red_t("User name:"));
  $template->set_var('lang_password',red_t("Password:"));
  $template->set_var('lang_login',red_t("Login"));
  $template->set_var('lang_help',red_t("Help"));
  $template->set_var('lang_forgot_pass',red_t("Forgot Password"));
  if (count($_REQUEST) > 0) {
    $req = $_REQUEST;
    // handle the case where someone has logged out, but needs to log in again.
    /// if we pass the the logout action, they will be logged right back out.
    if (array_key_exists('logout', $req) && $req['logout'] == 1) {
      $action = 'index.php';
    }
    // otherwise construct the request into a URL for the action
    else {
      // if the user and pass happen to be in the request, strip it out.
      if (array_key_exists('user_name', $req)) {
        unset($req['user_name']);
      }
      if (array_key_exists('user_pass', $req)) {
        unset($req['user_pass']);
      }
      $template->construct_self_url($req);
      $action = 'index.php'. $template->constructed_self_url;
    }
  }
  else {
    // since no request is supplied, just use the index file.
    $action ='index.php';
  }
  $template->set_var('action', $action);

  if(!empty($login_message)) red_set_message($login_message,'error');
  $template->parse('body_block','login_file');
}

function cred_exit() {
  global $globals;
  $globals['template']->parse_header_additions();
  $globals['template']->draw_theme();
  $globals['template']->parse_css($globals['config']);
  $globals['template']->parse_js();
  $globals['template']->pparse('out','main_file');
  exit;
}

function cred_get_hosting_order_id_for_item_id($item_id) {
  global $globals;
  $item_id = intval($item_id);
  $sql_resource = $globals['sql_resource'];
  $sql = "SELECT hosting_order_id FROM red_item WHERE item_id = $item_id";
  $result = red_sql_query($sql,$sql_resource);
  $row = red_sql_fetch_row($result);
  if(red_sql_num_rows($result) == 0) return false;
  return $row[0];
}

function cred_get_service_id_for_item_id($item_id) {
  global $globals;
  $item_id = intval($item_id);
  $sql_resource = $globals['sql_resource'];
  $sql = "SELECT service_id FROM red_item WHERE item_id = $item_id";
  $result = red_sql_query($sql,$sql_resource);
  $row = red_sql_fetch_row($result);
  if(red_sql_num_rows($result) == 0) return false;
  return $row[0];
}

function cred_add_notify_user(&$item) {
  // see if we should be adding a different notify user
  // This happens in testing mode - if you want to run
  // the notify script as a user other than the default roo
  // you can set $config['notify_user'] in the config file.
  global $globals;
  $config = $globals['config'];
  if(array_key_exists('notify_user',$config) && !empty($config['notify_user']))
    $item->set_notify_user($config['notify_user']);
}


function cred_get_row_for_item_id($item_id,$sql_resource) {
  $service_table = red_get_service_table_for_item_id($item_id,$sql_resource);
  $item_id = intval($item_id);
  $sql = "SELECT * FROM red_item INNER JOIN $service_table ".
    "ON red_item.item_id = $service_table.item_id ".
    "WHERE red_item.item_id = " . $item_id;
  $result = mysqli_query($sql_resource,$sql);
  return mysqli_fetch_assoc($result);
}

function cred_get_message_template($path, $name, $lang = 'en') {
  $p = "$path/$lang/$name";
  if(file_exists($p)) return file_get_contents($p);

  // try default
  $p = "$path/en/$name";
  if(file_exists($p)) return file_get_contents($p);
  return false;
}
?>
