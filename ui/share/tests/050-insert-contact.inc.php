<?php

function red_get_insert_contact_params() {
  return array(
    'object' => 'contact',
    'action' => 'insert',
    'set:contact_first_name' => RED_TEST_CONTACT_FIRST_NAME,
    'set:contact_last_name' => RED_TEST_CONTACT_LAST_NAME,
    'set:contact_email' => RED_TEST_CONTACT_EMAIL,
    'set:contact_billing' => 'y',
    'sub:member_friendly_name' => RED_TEST_MEMBER_FRIENDLY_NAME 
  );
}

function red_insert_contact_extra() {
  global $red_test_globals;
  $db = new red_db($red_test_globals['sql_resource']);

  $friendly_name = addslashes(RED_TEST_MEMBER_FRIENDLY_NAME);
  $sql = "SELECT * FROM red_member
    WHERE member_friendly_name = '$friendly_name' AND member_status = 'active'";
  $result = $db->_sql_query($sql);
  $rs = $db->_sql_fetch_assoc($result);
  $co = array(
    'rs' => $rs,
    'src_path' => $red_test_globals['config']['src_path'],
    'sql_resource' => $red_test_globals['sql_resource'],
  );
  $member = new red_member($co);
  $member->send_billing_statement();

  // Check for correspondence.
  $sql = "SELECT count(*) FROM red_correspondence JOIN red_member USING(member_id) 
    WHERE member_friendly_name = '$friendly_name' AND correspondence_subject LIKE '%invoice%' AND
    member_status = 'active' AND correspondence_body LIKE '%Total invoices listed below: 2%'";
  $result = $db->_sql_query($sql);
  $row = $db->_sql_fetch_row($result);
  $count = $row[0];
  if($count == 1) { 
    red_test_result(TRUE, "Member/contact resulted in one correspondence entries with subject line that includes the word invoice and 2 invoices in the body.");
  }
  else {
    red_test_result(FALSE, "Member/contact resulted in $count correspondence entries with subject line that includes the word invoice and 2 invoices in the body instead of one.");
  }

  // Set invoices to void so the membership will be deleted without errors.
  $sql = "UPDATE red_invoice SET invoice_status = 'void' WHERE member_id IN (SELECT member_id FROM red_member WHERE member_friendly_name = '$friendly_name')";
  $result = $db->_sql_query($sql);

}
