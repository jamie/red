<?php

function red_get_insert_standard_member_params() {
  global $red_test_globals;
  $red_test_globals['member_benefits_level'] = 'standard';
  return array(
    'object' => 'member',
    'action' => 'insert',
    'sub:unique_unix_group_name' => RED_TEST_UNIX_GROUP_NAME,
    'set:member_friendly_name' => RED_TEST_MEMBER_FRIENDLY_NAME,
    'set:member_start_date' => date('Y-m-d'),
    'set:member_type' => 'organization',
    'set:member_price' => '50',
    'set:member_benefits_price' => '150',
    'set:member_benefits_level' => $red_test_globals['member_benefits_level'],
    'set:member_currency' => 'USD',
  );
}

function red_insert_standard_member_extra() {
  global $red_test_globals;
  $db = new red_db($red_test_globals['sql_resource']);

  $friendly_name = addslashes(RED_TEST_MEMBER_FRIENDLY_NAME);
  $sql = "SELECT invoice_amount, invoice_description FROM red_invoice JOIN red_member USING(member_id) 
    WHERE member_friendly_name = '$friendly_name' AND invoice_type = 'membership' AND
    member_status = 'active'";
  $result = $db->_sql_query($sql);
  $row = $db->_sql_fetch_row($result);
  if($row[0] == '50.00') { 
    red_test_result(TRUE, "Standard member resulted in membership invoice with correct amount.");
  }
  else {
    red_test_result(FALSE, "Standard member resulted in membership invoice with amount: " . $row[0] . " instead of 50.00");
  }
  if(preg_match('/Membership dues/', $row[1]))  { 
    red_test_result(TRUE, "Standard member resulted in membership invoice with correct description.");
  }
  else {
    red_test_result(FALSE, "Standard member resulted in membership invoice with wrong description");
  }

  $sql = "SELECT invoice_amount, invoice_description FROM red_invoice JOIN red_member USING(member_id) 
    WHERE member_friendly_name = '$friendly_name' AND invoice_type = 'benefits' AND
    member_status = 'active'";
  $result = $db->_sql_query($sql);
  $row = $db->_sql_fetch_row($result);
  if($row[0] == '150.00') { 
    red_test_result(TRUE, "Standard member resulted in benefits invoice with correct amount.");
  }
  else {
    red_test_result(FALSE, "Standard member resulted in benefits invoice with amount: " . $row[0] . " instead of 150.00");
  }
  if(preg_match('/Membership benefits/', $row[1]))  { 
    red_test_result(TRUE, "Standard member resulted in benefits invoice with correct description.");
  }
  else {
    red_test_result(FALSE, "Standard member resulted in benefits invoice with wrong description");
  }



}
