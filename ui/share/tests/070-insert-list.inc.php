<?php

function red_get_insert_list_params() {
  return array(
    'object' => 'item',
    'action' => 'insert',
    'sub:hosting_order_identifier' => RED_TEST_HOSTING_ORDER_IDENTIFIER,
    'set:service_id' => 8,
    'set:list_name' => 'testo-listo',
    'set:list_domain' => 'lists.mayfirst.org',
    'set:list_owner_email' => 'jamie@example.org',
    'set:hosting_order_host' => RED_TEST_HOSTING_ORDER_HOST,
  );
}
