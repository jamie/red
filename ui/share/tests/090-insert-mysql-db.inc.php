<?php

function red_get_insert_mysql_db_params() {
  return array(
    'object' => 'item',
    'action' => 'insert',
    'sub:hosting_order_identifier' => RED_TEST_HOSTING_ORDER_IDENTIFIER,
    'set:service_id' => 20,
    'set:mysql_db_name' => 'testdb',
  );
}
