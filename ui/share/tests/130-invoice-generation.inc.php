<?php

function red_invoice_generation_extra() {
  global $red_test_globals;
  $usd_membership_price = '150.00';
  $mxn_membership_price = '1025.00';
  $usd_benefits_price = '50.00';
  $mxn_benefits_price = '615.00';

  $db = new red_db($red_test_globals['sql_resource']);

  // ensure we are starting fresh
  red_invoice_cleanup($db, 'red_invoice');
  red_invoice_cleanup($db, 'red_correspondence');

  // switch member to MXN
  red_switch_member_currency($db, 'MXN', $mxn_membership_price, $mxn_benefits_price);
  // test with MXN values
  red_invoice_auto_create($db, 'MXN', $mxn_membership_price, $mxn_benefits_price);
  red_invoice_cleanup($db, 'red_invoice');
  red_invoice_cleanup($db, 'red_correspondence');

  // switch to USD values
  red_switch_member_currency($db, 'USD', $usd_membership_price, $usd_benefits_price);
  // test with USD values
  red_invoice_auto_create($db, 'USD', $usd_membership_price, $usd_benefits_price);
  // clear out only correspondence table, leave invoice
  // for further testing
  red_invoice_cleanup($db, 'red_correspondence');
  
  red_invoice_late_notice($db, 30);
  red_invoice_cleanup($db, 'red_correspondence');

  red_invoice_late_notice($db, 60);
  red_invoice_cleanup($db, 'red_correspondence');

  red_invoice_late_notice($db, 90);
  red_invoice_cleanup($db, 'red_correspondence');

  red_invoice_late_notice($db, 104);
  red_invoice_cleanup($db, 'red_correspondence');

  // Clean up so we can properly delete the member
  red_invoice_cleanup($db, 'red_invoice');
}


function red_invoice_auto_create($db, $currency = 'USD', $membership_price, $benefits_price) {
  global $red_test_globals;
  // Invoices are generated 14 days before due.
  // We assume membership was created today, so calculate
  // the timestamp the collector should be run with 
  $ts = time() - 14 * 86400;
  $cmd = $red_test_globals['config']['src_path'] . '/../../sbin/collector';
  $config_file = $red_test_globals['config']['src_path'] . '/../../etc/config.inc.php';
  $args = array('--re-invoice', '--timestamp', $ts, '--config-file', $config_file);

  $test = "Re-invoice via collector command ($currency)";
  red_test_exec($cmd, $args, $test);

  // ensure the membership invoice was actually created
  $invoice_date = date("Y-m-d 00:00:00", time());
  $sql = "SELECT invoice_id FROM red_invoice JOIN red_member ".
    "USING(member_id) WHERE member_friendly_name = '" . 
    RED_TEST_MEMBER_FRIENDLY_NAME . "' AND member_status = 'active' ".
    "AND invoice_date = '$invoice_date' AND ".  "invoice_amount = '$membership_price' ".
    "AND invoice_status = 'unpaid' AND invoice_currency = '$currency' ".
    "AND invoice_type = 'membership'";
  $result = $db->_sql_query($sql);
  $test = "Membership invoice created by collector command ($currency)";
  $num_rows = $db->_sql_num_rows($result);
  if($num_rows == 1) {
    red_test_result(true, $test);
  } else {
    $test .= " ($num_rows rows returned instead of 1)";
    red_test_result(false, $test);
  }

  if($red_test_globals['member_benefits_level'] == 'standard') {
    // Ensure the benefits invoice was created.
    $sql = "SELECT invoice_id FROM red_invoice JOIN red_member ".
      "USING(member_id) WHERE member_friendly_name = '" . 
      RED_TEST_MEMBER_FRIENDLY_NAME . "' AND member_status = 'active' ".
      "AND invoice_date = '$invoice_date' AND ".  "invoice_amount = '$benefits_price' ".
      "AND invoice_status = 'unpaid' AND invoice_currency = '$currency' ".
      "AND invoice_type = 'benefits'";
    $result = $db->_sql_query($sql);
    $test = "Benefits invoice created by collector command ($currency)";
    $num_rows = $db->_sql_num_rows($result);
    if($num_rows == 1) {
      red_test_result(true, $test);
    } else {
      $test .= " ($num_rows rows returned instead of 1)";
      red_test_result(false, $test);
    }
  }
  // ensure mail was sent 
  $correspondence_date = date("Y-m-d", time());
  $likes = array();
  if($currency == 'USD') {
    $likes[] = 'PO BOX 205527';
  }
  elseif($currency == 'MXN') {
    $likes[] = 'Banorte';
  }
  $likes[] = "${membership_price}";
  if($red_test_globals['member_benefits_level'] == 'standard') {
    $likes[] = "${benefits_price}";
  }
  $sql = "SELECT correspondence_id FROM red_correspondence JOIN red_member ".
    "USING(member_id) WHERE member_friendly_name = '" . 
    RED_TEST_MEMBER_FRIENDLY_NAME . "' AND member_status = 'active' ".
    "AND correspondence_date LIKE '$correspondence_date%' AND ".
    "correspondence_to = '" . RED_TEST_CONTACT_EMAIL . "' ";
  foreach($likes as $like) {
    $sql .= " AND correspondence_body LIKE '%${like}%'";
  }
  $result = $db->_sql_query($sql);
  $test = "Correspondence sent by collector command ($currency)";
  $num_rows = $db->_sql_num_rows($result);
  if($num_rows == 1) {
    red_test_result(true, $test);
  } else {
    $test .= " ($num_rows rows returned instead of 1)";
    red_test_result(false, $test);
  }
}

function red_invoice_cleanup($db, $table) {
  $sql = "DELETE FROM $table WHERE member_id IN ".
    "(SELECT member_id FROM red_member WHERE member_friendly_name = '" . 
    RED_TEST_MEMBER_FRIENDLY_NAME . "' AND member_status = 'active')";
  $result = $db->_sql_query($sql);
}

function red_switch_member_currency($db, $currency, $membership_price, $benefits_price) {
  $sql = "UPDATE red_member SET member_currency = '$currency', ".
    "member_price = '$membership_price', " .
    "member_benefits_price = '$benefits_price' WHERE member_friendly_name = '" .
    RED_TEST_MEMBER_FRIENDLY_NAME . "' AND member_status = 'active'";
  $db->_sql_query($sql);
}

function red_invoice_late_notice($db, $days) {
  global $red_test_globals;
  $ts = time() + ( ( $days - 1) * 86400);
  $cmd = $red_test_globals['config']['src_path'] . '/../../sbin/collector';
  $config_file = $red_test_globals['config']['src_path'] . '/../../etc/config.inc.php';
  $args = array('--send-reminders', '--timestamp', $ts, '--config-file', $config_file);

  $test = "Send reminders via collector command ($days days)";
  red_test_exec($cmd, $args, $test);

  // ensure mail was sent 
  $correspondence_date = date("Y-m-d", time());
  $sql = "SELECT correspondence_id FROM red_correspondence JOIN red_member ".
    "USING(member_id) WHERE member_friendly_name = '" . 
    RED_TEST_MEMBER_FRIENDLY_NAME . "' AND member_status = 'active' ".
    "AND correspondence_date LIKE '$correspondence_date%' AND ".
    "correspondence_to = '" . RED_TEST_CONTACT_EMAIL . "'";
  $result = $db->_sql_query($sql);
  $test = "Late notice sent by collector command ($days days)";
  $num_rows = $db->_sql_num_rows($result);
  if($num_rows == 1) {
    red_test_result(true, $test);
  } else {
    $test .= " ($num_rows rows returned instead of 1)";
    red_test_result(false, $test);
  }
}
