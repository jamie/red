<?php

function red_get_delete_hosting_order_params() {
  return array(
    'object' => 'hosting_order',
    'action' => 'delete',
    'where:hosting_order_identifier' => RED_TEST_HOSTING_ORDER_IDENTIFIER,
  );
}
