<?php

function red_get_insert_web_conf_params() {
  return array(
    'object' => 'item',
    'action' => 'insert',
    'sub:hosting_order_identifier' => RED_TEST_HOSTING_ORDER_IDENTIFIER,
    'set:service_id' => 7,
    'set:web_conf_login' => RED_TEST_UNIX_GROUP_NAME,
    'set:web_conf_execute_as_user' => RED_TEST_UNIX_GROUP_NAME,
    'set:web_conf_port' => 'http',
    'set:web_conf_settings' => '# web config for testhostingorder.org
    DocumentRoot /home/members/testgroup/sites/testhostingorder.org/web
    CustomLog /home/members/testgroup/sites/testhostingorder.org/logs/web.log combined
    ErrorLog /home/members/testgroup/sites/testhostingorder.org/logs/error.log
ScriptAlias /cgi-bin /home/members/testgroup/sites/testhostingorder.org/cgi-bin',
    'set:web_conf_server_name' => 'roach.loc.cx',
    'set:web_conf_server_alias' => 'www.roach.loc.cx',
    'set:web_conf_server_max_processes' => '12',
  );
}
