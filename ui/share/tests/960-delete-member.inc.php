<?php

function red_get_delete_member_params() {
  return array(
    'object' => 'member',
    'action' => 'delete',
    'where:member_friendly_name' => RED_TEST_MEMBER_FRIENDLY_NAME 
  );
}
