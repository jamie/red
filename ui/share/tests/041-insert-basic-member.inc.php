<?php

function red_get_insert_basic_member_params() {
  global $red_test_globals;
  $red_test_globals['member_benefits_level'] = 'basic';
  return array(
    'object' => 'member',
    'action' => 'insert',
    'sub:unique_unix_group_name' => RED_TEST_UNIX_GROUP_NAME,
    'set:member_friendly_name' => RED_TEST_MEMBER_FRIENDLY_NAME,
    'set:member_start_date' => date('Y-m-d'),
    'set:member_type' => 'organization',
    'set:member_price' => '50',
    'set:member_benefits_price' => '',
    'set:member_benefits_level' => $red_test_globals['member_benefits_level'],
    'set:member_currency' => 'USD',
  );
}

function red_insert_basic_member_extra() {
  global $red_test_globals;
  $db = new red_db($red_test_globals['sql_resource']);

  $friendly_name = addslashes(RED_TEST_MEMBER_FRIENDLY_NAME);
  $sql = "SELECT invoice_amount, invoice_description FROM red_invoice JOIN red_member USING(member_id) 
    WHERE member_friendly_name = '$friendly_name' AND invoice_type = 'membership' AND
    member_status = 'active'";
  $result = $db->_sql_query($sql);
  $row = $db->_sql_fetch_row($result);
  if($row[0] == '50.00') { 
    red_test_result(TRUE, "Basic member resulted in membership invoice with correct amount.");
  }
  else {
    red_test_result(FALSE, "Basic member resulted in membership invoice with amount: " . $row[0] . " instead of 25.00");
  }
  if(preg_match('/Membership dues/', $row[1])) {
    red_test_result(TRUE, "Basic member resulted in membership invoice with correct description.");
  }
  else {
    red_test_result(FALSE, "Basic member resulted in membership invoice with the wrong description");
  }

  $sql = "SELECT invoice_amount FROM red_invoice JOIN red_member USING(member_id) 
    WHERE member_friendly_name = '$friendly_name' AND invoice_type = 'benefits' AND
    member_status = 'active'";
  $result = $db->_sql_query($sql);
  $row = $db->_sql_fetch_row($result);
  if(empty($row)) { 
    red_test_result(TRUE, "Basic member resulted in no benefits invoice.");
  }
  else {
    red_test_result(FALSE, "Basic member resulted in a benefits invoice.");
  }

  // Set invoices to void so the membership will be deleted without errors.
  $sql = "UPDATE red_invoice SET invoice_status = 'void' WHERE member_id IN (SELECT member_id FROM red_member WHERE member_friendly_name = '$friendly_name')";
  $result = $db->_sql_query($sql);

}
