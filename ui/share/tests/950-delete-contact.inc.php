<?php

function red_get_delete_contact_params() {
  return array(
    'object' => 'contact',
    'action' => 'delete',
    'where:contact_first_name' => RED_TEST_CONTACT_FIRST_NAME,
    'where:contact_last_name' => RED_TEST_CONTACT_LAST_NAME,
    'where:contact_email' => RED_TEST_CONTACT_EMAIL,
  );
}
