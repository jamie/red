<?php

function red_get_insert_web_app_params() {
  return array(
    'object' => 'item',
    'action' => 'insert',
    'sub:hosting_order_identifier' => RED_TEST_HOSTING_ORDER_IDENTIFIER,
    'set:service_id' => 23,
    'set:web_app_name' => 'drupal',
    'set:web_app_dir' => '',
    'set:web_app_initial_admin_email' => 'jamie@mail.turkey',
    'set:web_app_version' => '7',
  );
}
