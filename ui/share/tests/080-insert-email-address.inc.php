<?php

function red_get_insert_email_address_params() {
  return array(
    'object' => 'item',
    'action' => 'insert',
    'sub:hosting_order_identifier' => RED_TEST_HOSTING_ORDER_IDENTIFIER,
    'set:service_id' => 2,
    'set:email_address_recipient' => RED_TEST_UNIX_GROUP_NAME,
    'set:email_address' => RED_TEST_UNIX_GROUP_NAME . '@' . RED_TEST_HOSTING_ORDER_IDENTIFIER,
  );
}
