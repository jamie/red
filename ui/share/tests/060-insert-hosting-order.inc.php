<?php

function red_get_insert_hosting_order_params() {
  return array(
    'object' => 'hosting_order',
    'action' => 'insert',
    'set:hosting_order_identifier' => RED_TEST_HOSTING_ORDER_IDENTIFIER,
    'set:hosting_order_name' => 'test hosting order',
    'set:hosting_order_host' => RED_TEST_HOSTING_ORDER_HOST,
    'sub:unique_unix_group_name' => RED_TEST_UNIX_GROUP_NAME,
    'sub:member_friendly_name' => RED_TEST_MEMBER_FRIENDLY_NAME,
  );
}

function red_insert_hosting_order_extra() {
  global $red_test_globals;
  // Ensure we have a user account, server record, DNS and web configuration.
  $sql = "SELECT COUNT(*) FROM red_item JOIN red_hosting_order USING(hosting_order_id)
    WHERE hosting_order_identifier = '" . RED_TEST_HOSTING_ORDER_IDENTIFIER . "' AND
    hosting_order_status = 'active' AND item_status = 'active'";
  $db = new red_db($red_test_globals['sql_resource']);
  $result = $db->_sql_query($sql);
  $row = $db->_sql_fetch_row($result);
  if($red_test_globals['member_benefits_level'] == 'standard') {
    if($row[0] == 7) { 
      // We should have 3 DNS records, one user account, one server
      // access, one web conf and one hosting order access
       red_test_result(TRUE, "Standard member resulted in 7 items auto-created.");
    }
    else {
       red_test_result(FALSE, "Standard member resulted in " . $row[0] . " items auto-created instead of 7.");
    }
  } 
  else {
    if($row[0] == 3) { 
      // We should have 1 DNS records, one user account and one email address. 
       red_test_result(TRUE, "Basic member resulted in 3 items auto-created.");
    }
    else {
       red_test_result(FALSE, "Basic member resulted in " . $row[0] . " items auto-created instead of 3.");
    }
  }
}
