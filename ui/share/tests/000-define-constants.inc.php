<?php

define('RED_TEST_UNIX_GROUP_NAME', 'testgroup');
define('RED_TEST_MEMBER_FRIENDLY_NAME', 'Testo Member');
define('RED_TEST_CONTACT_FIRST_NAME', 'Testo Joe');
define('RED_TEST_CONTACT_LAST_NAME', 'Testo Smith');
define('RED_TEST_CONTACT_EMAIL', 'test@example.com');
define('RED_TEST_HOSTING_ORDER_IDENTIFIER', 'testhostingorder.org');
define('RED_TEST_HOSTING_ORDER_HOST', 'docker.local');
