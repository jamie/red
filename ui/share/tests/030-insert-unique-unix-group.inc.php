<?php

function red_get_insert_unique_unix_group_params() {
  return array(
    'object' => 'unique_unix_group',
    'action' => 'insert',
    'set:unique_unix_group_name' => RED_TEST_UNIX_GROUP_NAME,
  );
}
