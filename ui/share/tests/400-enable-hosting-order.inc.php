<?php

function red_get_enable_hosting_order_params() {
  // this action should enable the hosting order and all related
  // items
  return array(
    'object' => 'hosting_order',
    'action' => 'update',
    'where:hosting_order_identifier' =>  RED_TEST_HOSTING_ORDER_IDENTIFIER,
  );
}
