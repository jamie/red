<?php

function red_get_insert_password_reset_params() {
  return array(
    'object' => 'item',
    'action' => 'insert',
    'sub:hosting_order_identifier' => RED_TEST_HOSTING_ORDER_IDENTIFIER,
    'set:service_id' => 19,
    'set:pass_reset_login' => RED_TEST_UNIX_GROUP_NAME,
  );
}
