<?php

require('../etc/config.inc.php');
require($config['common_src_path'] . '/class.red_db.inc.php');
require($config['src_path'] . '/class.red_api.inc.php');
if(!$sql_resource = red_db::init_db($config)) {
  echo "Failed to initialize db\n";
}
$api = new red_api($sql_resource);
$api->set_output_format('json');
$api->parse_request_arguments($_REQUEST);
if($api->init($config) && $api->authenticate()) {
  $api->run();
}
print $api->print_output();
exit;
?>
