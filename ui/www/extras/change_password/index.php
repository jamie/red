<?php

/* 
This program allows a user to change their own user account password.
*/

require('../../../etc/config.inc.php');
session_start();
require($config['common_src_path'] . '/red.lang.utils.inc.php');
require_once($config['src_path'] . '/web/class.red_template.inc.php');

error_reporting(E_ALL);
// Create an instance of the Template class
$t = new Template('ihtml');
$t->set_file('header_file','header.ihtml');
$t->parse('header_block','header_file');
$t->set_file('footer_file','footer.ihtml');
$t->parse('footer_block','footer_file');

// Log into the database server
if (!$sql_resource = mysqli_connect($config['db_host'],$config['db_user'],$config['db_pass'])) 
{
  echo "<b>Yipes</b><br><br>";
  echo "Failed to connect to the database server.";
  exit;
}

if (!mysqli_select_db($sql_resource, $config['db_name'])) 
{
  echo "<b>Yipes</b><br><br>";
  echo "Failed to access " . $config['db_name'] . " database.";
  exit;
}
require_once($config['common_src_path'] . '/class.red_db.inc.php');
require_once($config['common_src_path'] . '/class.red_ado.inc.php');
require_once($config['common_src_path'] . '/class.red_item.inc.php');
require_once($config['common_src_path'] . '/modules/class.red_item_user_account.inc.php');
require_once($config['src_path'] . '/modules/class.red_item_user_account_ui.inc.php');
require_once($config['common_src_path'] . '/red.utils.inc.php');

$red_construction_options = array(
              'mode' => 'ui',
              'notify_host' => TRUE,
              'site_dir_template' => $config['site_dir_template'],
              'sql_resource' => $sql_resource,
              'src_path' => $config['src_path'],
              'common_src_path' => $config['common_src_path']);


// Start the main program conditionals
if(isset($_POST['change_password']))
{
  $username = $_POST['username'];
  $old_password = $_POST['old_password'];
  $new_password = $_POST['new_password'];
  $new_password_confirmed = $_POST['new_password_confirmed'];


  if($username == '') {
    $display_message = 'You must enter a username.';
  }
  elseif($old_password == '') {
    $display_message = 'You must enter your old password.';
  }
  elseif($new_password == '') {
    $display_message = 'You must enter your new password.';
  }
  elseif($new_password != $new_password_confirmed) {
    $display_message = 'Your passwords don\'t match.';
  }
  elseif(!red_is_good_password($new_password)) {
      $display_message = 'Sorry, but that password is too simple. Please make it at least 6 characters long and ensure that it has both letters and non-letters. Thanks.';
  }
  else { 
    $sql = "SELECT * FROM ".
        "red_item INNER JOIN red_item_user_account ON ".
        "red_item.item_id = red_item_user_account.item_id ".
        "WHERE user_account_login = '$username' ".
        "AND item_status = 'active'";
    $result = mysqli_query($sql_resource,$sql);
    if(mysqli_num_rows($result) == 0) {
      $display_message = 'Incorrect username or password.';
    }
    else {
      $red_construction_options['rs'] = mysqli_fetch_assoc($result);
      $red = red_item::get_red_object($red_construction_options);
      $existing_crypt = $red->get_user_account_password();
      $given_crypt = red_item_user_account_ui::crypt($old_password,$existing_crypt);
      if($given_crypt != $existing_crypt) {
        $display_message = "Incorrect old password.";
      }
      else {
        $red->set_user_account_password($new_password);
        if(!$red->validate()) {
          $display_message = "There was an error. Sorry, your password did not properly validate.<br>";
          $errors = $red->get_errors();
          foreach($errors as $v) {
            $display_message .= $v['error'] . "<br>";
          }
        }
        else {
          if(!$red->commit_to_db()) {
            $display_message = "There was an error updating the database.";
            $errors = $red->get_errors();
            foreach($errors as $v) {
              $display_message .= $v['error'] . "<br>";
            }
          }
          else {

            $display_success_message = 'Your password was changed.';
            $t->set_var('display_success_message','<div class="alert alert-success">' . $display_success_message . '</div>');
          }
        }
      }
    }
  }
}

if(!isset($success))
{
  $t->set_file('password_file','change_password.ihtml');
  $t->set_var('css_file',$config['url_path'].$config['css']);
  $t->set_var('url_path',$config['url_path']);
  if(isset($display_message)) {
    $t->set_var('display_message','<div class="alert alert-danger">' . $display_message . '</div>');
  }
    
    
}
  
// Make the substition and return
$substituted = $t->subst('password_file');
echo $t->finish($substituted);


?>
