<?php

/*
This program manages password reset hashes. It allows a user to generate
a reset hash and it processes reset hashes.
*/
require('../../../etc/config.inc.php');
session_start();
require($config['common_src_path'] . '/red.lang.utils.inc.php');
require_once($config['src_path'] . '/web/class.red_template.inc.php');
//error_reporting(E_ALL);

// $return_message are passed around by reference. Yuck.
$return_message = NULL;

// Create an instance of the Template class
$t = new Template('ihtml');
$t->set_file('header_file','header.ihtml');
$t->parse('header_block','header_file');
$t->set_file('footer_file','footer.ihtml');
$t->parse('footer_block','footer_file');
$t->set_file('reset_password_file','reset_password.ihtml');
if (array_key_exists('css', $config)) {
  $t->set_var('css_file',$config['url_path'].$config['css']);
}
$t->set_var('url_path',$config['url_path']);

// Log into the database server - this is required for red
if (!$sql_resource = mysqli_connect($config['db_host'],$config['db_user'],$config['db_pass']))
{
  echo "<b>Yipes</b><br><br>";
  echo "Failed to connect to the database server.";
  exit;
}

global $sql_resource;

if (!mysqli_select_db($sql_resource,$config['db_name']))
{
  echo "<b>Yipes</b><br><br>";
  echo "Failed to access " . $config['db_name'] . " database.";
  exit;
}

require_once($config['common_src_path'] . '/class.red_db.inc.php');
require_once($config['common_src_path'] . '/class.red_ado.inc.php');
require_once($config['common_src_path'] . '/class.red_item.inc.php');
require_once($config['common_src_path'] . '/red.utils.inc.php');
require_once($config['src_path'] . '/modules/class.red_item_pass_reset_ui.inc.php');

$red_construction_options = array(
              'mode' => 'ui',
              'notify_host' => TRUE,
              'sql_resource' => $sql_resource,
              'site_dir_template' => $config['site_dir_template'],
              'src_path' => $config['src_path'],
              'common_src_path' => $config['common_src_path']);

$action = false;

// most cases, action is passed in the post
if(array_key_exists('action',$_POST))
  $action = $_REQUEST['action'];

// if following a reset URL, no action is specified
// but h is in the get array
if((!$action) && array_key_exists('h',$_GET))
  $action = 'display_password_reset_form';

// If still not action, display new form to request
// password reset
if(!$action) $action = 'new';

switch($action)
{
  case 'submit_new_password':
    // A brand new password is being submitted
    // We still check to ensure the hash is correct.
    if(!is_valid_hash($_POST['h'],$return_message)) {
      $t->set_var('error_message',$return_message);
      break;
    }
    else {
      if(true === submit_new_password($return_message)) {
        $t->set_var('success_message',$return_message);
        break;
      }
      else {
        $t->set_var('error_message',$return_message);
        // fall through
      }
    }

  case 'display_password_reset_form':
    // The user is following a reset password link. Check if hash is valid.  h
    // might come from POST is we're falling through from an error so we check
    // $_REQUEST to cover both POST and GET.
    if(is_valid_hash($_REQUEST['h'],$return_message)) {
      // Display change password form
      display_password_reset_form($t);
    }
    else {
      $t->set_var('error_message',$return_message);
    }
    break;

  case 'submit_username':
    // The user is submitting the username of the user whose
    // password they want to reset - generate an email
    // to the billing contact
    //
    if(true === submit_username($return_message)) {
      $t->set_var('success_message',$return_message);
      break;
    }
    else {
      $t->set_var('error_message',$return_message);
      // fall through
    }
  case 'new':
    // The user is requesting the form to submit a password reset request
    $t->set_file('enter_username_file','enter_username.ihtml');
    $t->parse('body','enter_username_file');

}


// Make the substition and return
$substituted = $t->subst('reset_password_file');
echo $t->finish($substituted);


function get_reset_emails($username, &$return_message)
{
  global $sql_resource;
  $sql = "SELECT red_hosting_order.member_id FROM red_hosting_order JOIN ".
    "red_item USING(hosting_order_id) JOIN ".
    "red_item_user_account USING(item_id) WHERE ".
    "user_account_login = '$username' AND item_status = ".
    "'active'";
  $result = mysqli_query($sql_resource,$sql);
  if (!$result) {
  	$return_message .= "There was an error getting your membership account. ";
		return false;
  }
  $row = mysqli_fetch_row($result);
  $member_id = $row[0];
  if(empty($member_id)) return array();
  $sql = "SELECT contact_email FROM red_contact ".
    "WHERE member_id = $member_id AND ".
    "contact_billing = 'y' AND contact_email != '' ".
    "AND contact_status = 'active'";
  $result = mysqli_query($sql_resource,$sql);
  if (!$result) {
  	$return_message .= "There was an error getting your membership contacts. ";
		return false;
  }
  // Make the array easier to manage
  $ret = array();
  while($v = mysqli_fetch_row($result)) {
    if (!empty($v[0])) {
      $ret[] = $v[0];
    }
  }
  // Now check for a recovery email.
  $sql = "SELECT user_account_recovery_email FROM red_item_user_account JOIN ".
    "red_item USING(item_id) WHERE ".
    "user_account_login = '$username' AND item_status = ".
    "'active'";
  $result = mysqli_query($sql_resource,$sql);
  if (!$result) {
  	$return_message .= "There was an error getting your recovery email. ";
		return false;
  }
  $row = mysqli_fetch_row($result);
  if ($row) {
    if (!empty($row[0])) {
      $ret[] = $row[0];
    }
  }
  return $ret;
}

function get_username_for_hash($hash)
{
  global $sql_resource;
  $hash = addslashes($hash);
  $sql = "SELECT pass_reset_login FROM red_item_pass_reset WHERE pass_reset_hash = '$hash'";
  $result = mysqli_query($sql_resource,$sql);
  $row = mysqli_fetch_row($result);
  return $row[0];
}

function is_valid_hash($hash,&$return_message)
{
  global $sql_resource;
  $hash = addslashes($hash);
  $sql = "SELECT pass_reset_expires,pass_reset FROM red_item_pass_reset ".
    "JOIN red_item USING(item_id) WHERE pass_reset_hash = '$hash' ".
    "AND item_status = 'active'";
  $result = mysqli_query($sql_resource,$sql);
  if(mysqli_num_rows($result) == 0) {
    $return_message = "That is an invalid hash.";
    return false;
  }
  $row = mysqli_fetch_row($result);
  $expires = strtotime($row[0]);
  if($expires < time()) {
    $return_message = 'That hash has expired. You can <a href="index.php">generate a new one</a>.';
    return false;
  }
  if($row[1] != '0000-00-00 00:00:00') {
    $return_message = 'That hash has already been used. You can <a href="index.php">generate a new one</a>.';
    return false;
  }
  return true;
}

function is_current_hash($hash)
{
  global $sql_resource;
  $hash = addslashes($hash);
  $now = date("Y-m-h H:i:s");
  $sql = "SELECT password_reset_id FROM password_reset JOIN red_item ".
    "USING(item_id) WHERE hash = '$hash' ".
    "AND expires > '$now' AND reset = '0000-00-00 00:00:00'";
  $result = mysqli_query($sql_resource,$sql);
  if(mysqli_num_rows($result) == 0) return false;
  return true;
}
function submit_new_password(&$return_message)
{
  global $sql_resource;
  $hash = $_POST['h'];
  $username = get_username_for_hash($hash);
  $password = $_POST['password'];
  $password_confirm = $_POST['password_confirm'];
  if($password == '') {
    $return_message = 'You must enter your new password.<br><br>';
    return false;
  }
  elseif($password != $password_confirm) {
    $return_message = 'Your passwords don\'t match.<br><br>';
    return false;
  }
  elseif(!red_is_good_password($password)) {
    $return_message = "Sorry, but that password was too simple. ".
      "Please make it at least 6 characters long and ensure that it ".
      "has both letters and non-letters. Thanks.<br><br>";
    return false;
  }
  $sql = "SELECT * FROM red_item JOIN red_item_user_account ".
    "USING(item_id) WHERE user_account_login = '$username' AND ".
    "item_status = 'active'";
  $result = mysqli_query($sql_resource,$sql);
  if(mysqli_num_rows($result) == 0) {
    $return_message = 'That username no longer exists.<br><br>';
    return false;
  }
  global $red_construction_options;
  $co = $red_construction_options;
  $co['rs'] = mysqli_fetch_assoc($result);
  $red = red_item::get_red_object($co);
  $red->set_user_account_password($password);
  if(!$red->validate()) {
    $return_message = "There was an error. Sorry, your password did not ".
      "properly validate.<br>";
    $errors = $red->get_errors();
    foreach($errors as $v) {
      $return_message .= $v['error'] . "<br>";
    }
    return false;
  }
  if(!$red->commit_to_db()) {
    $return_message = "There was an error updating the database.";
    $errors = $red->get_errors();
    foreach($errors as $v) {
      $return_message .= $v['error'] . "<br>";
    }
    return false;
  }
  $hosting_order_id = $red->get_hosting_order_id();
  // update the reset time in the db
  $co = $red_construction_options;
  $co['service_id'] = 19;
  $co['child_table'] = 'red_item_pass_reset';
  $co['hosting_order_id'] = $hosting_order_id;

  $pass_reset = new red_item_pass_reset_ui($co,$hash);
  // record time it was reset
  $reset = date("Y-m-d H:i:s",time());
  $pass_reset->set_pass_reset($reset);
  if(!$pass_reset->commit_to_db()) {
    $return_message = "The password was updated, however, the hash was ".
      "not. ".  $pass_reset->get_errors_as_string();
    return false;
  }
  $return_message = "The password has been updated.";
  return true;
}

function submit_username(&$return_message)
{
  $username = $_POST['username'];
  // make sure it's valid syntaix
  if(!preg_match(RED_LOGIN_MATCHER,$username)) {
    $return_message = "That username is not valid.";
    return false;
  }

  // create hash
  global $red_construction_options;
  $co = $red_construction_options;
  $co['service_id'] = 19;
  $hosting_order_id = get_hosting_order_id($username);
  if(!$hosting_order_id) {
    $return_message = "Sorry - I could not locate that username.";
    return false;
  }
  $co['hosting_order_id'] = $hosting_order_id;
  $co['child_table'] = 'red_item_pass_reset';
  $pass_reset = new red_item_pass_reset_ui($co,null,$username);
  $emails = get_reset_emails($username, $return_message);
  if (false === $emails) {
		return false;
  }
  if(!$pass_reset->validate()) {
    $return_message = "There was an error. Sorry, your password reset link did not ".
      "properly validate.<br>";
    $errors = $pass_reset->get_errors();
    foreach($errors as $v) {
      $return_message .= $v['error'] . "<br />";
    }
    return false;
  }
  if(!$pass_reset->commit_to_db()) {
    $return_message = "There was an error updating the database.";
    $errors = $pass_reset->get_errors();
    foreach($errors as $v) {
      $return_message .= $v['error'] . "<br />";
    }
    return false;
  }
  if(!$pass_reset->send_hash($emails)) {
    $return_message = "Failed to send email. ".
    $pass_reset->get_errors_as_string();
    return false;
  }
  $return_message = '';
  $print_emails = array();
  reset($emails);
  foreach($emails as $email) {
    $print_emails[] = obscure_email($email);
  }
  $return_message .= "<p>An email was sent to ".  implode(' ',$print_emails) . " ".
    "with a link. Please check your email and click on the link to ".
    "reset your password.</p>";
  $return_message .= "<p>The link will expire on " .
    $pass_reset->convert_expires_to_friendly_date() . ".</p>";
  return true;
}

// Take an email and partially obscure it for privacy.
// e.g. joe@example.com => j**@ex****.com
function obscure_email($email) {
  if(empty($email)) return NULL;

	$parts = explode('@', $email);
	$user = array_key_exists(0, $parts) ? $parts[0] : NULL;
	$domain = array_key_exists(1, $parts) ? $parts[1] : NULL;

  if(empty($user) || empty($domain)) return NULL;

	$user_len = strlen($user);
	$domain_len = strlen($domain);

  $pos = 2;

  $start = $end = '';
	for($i = 0; $i <= ($user_len/$pos) - 1; $i++) {
		$start .= 'x';
	}
	for($i = 0; $i <= ($domain_len/$pos) - 1; $i++)
	{
		$end .= 'x';
	}

	return substr_replace($user, $start, 2, $user_len/$pos) .
		'@' . substr_replace($domain, $end, 2, $domain_len/$pos);
}

function display_password_reset_form(&$t)
{
  $hash = $_REQUEST['h'];
  $username = get_username_for_hash($hash);
  $t->set_file('enter_password_file','enter_password.ihtml');
  $t->set_var('username',$username);
  $t->set_var('hash',$hash);
  $t->parse('body','enter_password_file');
}

function get_hosting_order_id($login) {
  global $sql_resource;
  $login = addslashes($login);
  $sql = "SELECT hosting_order_id FROM red_item JOIN ".
    "red_item_user_account USING(item_id) WHERE user_account_login = ".
    "'$login' AND item_status = 'active'";
  $result = mysqli_query($sql_resource,$sql);
  $row = mysqli_fetch_row($result);
  return $row[0];
}
?>
