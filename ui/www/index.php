<?php
  $config_file = '../etc/config.inc.php';
  if(!file_exists($config_file))
    die("Please modify your index.php file, changing the value of the config variable so that it refers to a file that exists.");

  require_once($config_file);
  require_once($config['src_path'] . '/web.inc.php');

?>
