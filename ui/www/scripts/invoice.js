$(document).ready(function () {
  $("#invoice_type").change(function() {
    // Automatically update the invoice description based on a change in
    // invoice type. Also, unset the amount to force the user to enter the
    // correct amount. I don't know what amount it should be - and am 
    // tring to avoid setting constants in multiple places in the code.
    // Perhaps someday we can set the right amounts somewhere and pull them
    // in everywhere. Until then you have to enter an amount manually.
    var type = $("#invoice_type").val();
    var description = $('#invoice_description').val();
    if (type == 'membership') {
      $('#invoice_description').val(description.split("benefits").join("dues"));
      $('#invoice_amount').val($('#red_membership_dues').val());
    }
    else if (type == 'benefits') {
      $('#invoice_description').val(description.split("dues").join("benefits"));
      $('#invoice_amount').val($('#red_benefits_dues').val());
    }
  });
});

/* This function show or hides the payment information displayed under an
 * invoice based on whether the user clicks the + or - link.
 */
function togglePayments(id) {
  var classElements = new Array();
  var payment_row = document.getElementById('red-payment-row-' + id);
  var payment_link = document.getElementById('red-display-payment-link-' + id);
  if(payment_row.style.display == 'table-row') {
    payment_row.style.display = 'none';
    payment_link.innerHTML = '+';
  } else {
    payment_row.style.display = 'table-row';
   payment_link.innerHTML = '-';
  } 
}

