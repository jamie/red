function setRecipientInputOnLoad()
{
	var type = 'radio_user_account';
	var email_address_el = document.getElementById('radio_email_address');
	if(email_address_el.checked == true) type = 'radio_email_address';
	setRecipientInput(type);
}

function displayRecipientInputOnRecipientTypeChange(element) 
{
	setRecipientInput(element.value);
}

function setRecipientInput(type)
{
	var login_el = document.getElementById('recipient_user_account');
	var email_el = document.getElementById('recipient_email_address');

	if (type == 'radio_user_account') {
		login_el.style.display = 'block';
		email_el.style.display = 'none';
	} else {
		login_el.style.display = 'none';
		email_el.style.display = 'block';
	}
}
