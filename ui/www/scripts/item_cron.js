function generateCronSchedule(type)
{
	// we randomize by choosing the current date, hour, min
	var now = new Date();
	var min = now.getMinutes();
	var hour = now.getHours();
	var day = now.getDay();

	var schedule = '';
	if(type == 'hourly') {
		schedule = min + ' * * * *';
	} else if (type == 'daily') {
		schedule = min + ' ' + hour + ' * * *';
	} else if (type == 'weekly') {
		schedule = min + ' ' + hour + ' * * ' + day;
	}
	if(schedule != '') {
		document.getElementById('cron_schedule').value = schedule;
	}
}
