$(document).ready(function () {
  // As soon as the document is ready, set the version option values
  // based on the web app selected, and ensure that if the web app
  // selected changes, the options values are updated.
  setWebAppVersion();
  $("#sf_web_app_name").change(function() {
    setWebAppVersion();
  });
});

function setWebAppVersion() {
	var name = $("#sf_web_app_name").val();
	if(name == 'drupal') {
    $('#sf_web_app_version').parent().parent().show();
    $("option.drupal").show();
    $("option.mediawiki").hide();
	} else if (name == 'mediawiki') {
    $('#sf_web_app_version').parent().parent().hide();
    $("#sf_web_app_version").val("");
  }
}	
