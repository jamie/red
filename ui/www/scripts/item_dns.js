/* Only show DNS fields relevant to the type selected. */ 

// Get the value of the type field.
$(document).ready(function() {

  // Set available fields based on current value of dns type.
  var dns_type = $("[name|='sf_dns_type'] option:selected").val();
  update_fields_based_on_dns_type(dns_type);
  
  // If the dns type changes, update the fields available.
  $("[name|='sf_dns_type']").change(function () {
    dns_type = $("[name|='sf_dns_type'] option:selected").val();
    update_fields_based_on_dns_type(dns_type);
  });

  function update_fields_based_on_dns_type(dns_type) {
    if ( dns_type == '' ) {
      // Initially hide all fields we might not need.
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").hide();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
    }
    else if ( dns_type == 'mx' ) {
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").show();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").show();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
    }
    else if ( dns_type == 'text' ) {
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").hide();
      $("#red-dns-text-table-row").show();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
    }
    else if ( dns_type == 'srv' ) {
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").show();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").show();
      $("#red-dns-weight-table-row").show();
      $("#red-dns-port-table-row").show();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
    }
    else if ( dns_type == 'a' || dns_type == 'aaaa' || dns_type == 'ptr') {
      $("#red-dns-ip-table-row").show();
      $("#red-dns-server-name-table-row").hide();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
    }
    else if ( dns_type == 'cname' ) {
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").show();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
    }
    else if ( dns_type == 'mailstore' ) {
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").show();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
    }
    else if ( dns_type == 'sshfp' ) {
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").hide();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").show();
      $("#red-dns-sshfp-type-table-row").show();
      $("#red-dns-sshfp-fpr-table-row").show();
    }

  }
});

