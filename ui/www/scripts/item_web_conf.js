$(document).ready(function() {
  split('#web_conf_domain_names');

  function toggle_advanced_settings() {
    var advanced = [ "web-conf-tls-cert", "web-conf-tls-key", "web-conf-logging", "web-conf-tls-redirect", "web-conf-cgi", "web-conf-max-memory", "web-conf-max-processes", "web-conf-cache-settings", "web-conf-settings", "web-conf-document-root", "web-conf-php-version", "web-conf-mountpoint", "item-quota", "item-quota-notify-percent", "item-quota-notify-date" ];
    var action = "hide";
    if ($("#red-web-conf-toggle-advanced").data("visibility-state") == "hidden") {
      action = "show"
    }
    for (var i = 0, size = advanced.length; i < size ; i++) {
      if (action == 'hide') {
        $("#red-" + advanced[i] + "-table-row").hide();
        $("#red-web-conf-toggle-advanced").val("Show Advanced Settings");
        $("#red-web-conf-toggle-advanced").data("visibility-state", "hidden");
      }
      else {
        $("#red-" + advanced[i] + "-table-row").show();
        $("#red-web-conf-toggle-advanced").val("Hide Advanced Settings");
        $("#red-web-conf-toggle-advanced").data("visibility-state", "visible");
      }
    }
  }

  // Create a button that will toggle between showing and hiding the advanced options.
 	$("#item_id").parent().append('<input id="red-web-conf-toggle-advanced" class="btn btn-warning" type="button" value="See Advanced Settings" title="Toggle advanced display">');

  // Store a data attribute on it, set to hidden.
  $("#red-web-conf-toggle-advanced").data("visibility-state", "visible");

  // Now toggle it to hidden.
  toggle_advanced_settings();

  // Lastly, add a click function to toggle it again.
  $("#red-web-conf-toggle-advanced").click(function() {
    toggle_advanced_settings();
  });

});
