function split(target){
	var sourceInput = $(target);
	var splitString = sourceInput.val().split(' ');
	var wrapperName = target.slice(1) + "_wrapper";
	var className = target.slice(1) + "_child";
	var classDelete = target.slice(1) + '_delete_btn';
	var classAdd = target.slice(1) + '_add';
	var totalInputs = 0;
	
	sourceInput.hide();

	//build wrapper
	sourceInput.parent().append('<div id="' + wrapperName + '" /><input id="' + classAdd + '" class="btn btn-success" type="button" value="+ Add" title="add field">');

	//print inputs from split string
	for (i = totalInputs;i <= splitString.length; i++){
		addInput(i,splitString[i]);
		totalInputs++;
	}

	$('#' + classAdd).click(function(){
		addInput(totalInputs);
		totalInputs++;
	})

	function addInput(num,value){
		var input = new singleInput(value,num);
		$('#' + wrapperName).append(input.print());
		input.setInput();
	}

	function singleInput(value,num){

		this.value = (typeof value === 'undefined') ? '' : value ;
		this.id = className + '_' + num;
		this.last = false;
		var wrapperId = this.id + '_wrapper';
		var deleteSelector = '#' + wrapperId + ' > .' + classDelete;

		//child input wrapper
		var out =  '<div id="' + wrapperId + '" class="form-inline form-group ' + className + '_wrapper">';
		//child input, with split data value
		out += '<input id="' + this.id + '" class="form-control ' + className + '" type="text" value="' + this.value + '">';
		//child input delete button
		out += '<input type="button" class="' + classDelete + ' form-control btn btn-warning" data-parent="#' + wrapperId + '" value="-" title="remove">';
		out += '</div>';

		function refreshMainInput(){
			var string = new Array();
				$('.' + className).each(function(i){
					string[i] = $(this).val();
				});
				$(target).val(string.join(' '));
		}

		this.setInput = function(){
			$('#' + this.id).blur(function(){
				refreshMainInput()
			});

			$(deleteSelector).click(function(){
				$('#' + wrapperId).remove();
				refreshMainInput()
			});
		}

		this.print = function(){
			console.log('this works');
			return out;
		}

	}

}
