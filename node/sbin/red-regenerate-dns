#!/usr/bin/php
<?php

/**
  * This file will re-create all dns zones.
  * It can be used when we upgrade our dns servers. 
  **/

red_regenerate_main();
return true;

function red_regenerate_main() {
  // Make sure our permissions are properly set
  $me = $_SERVER['argv'][0];
  if(!red_regenerate_check_file($me))
    red_regenerate_exit("If running as root, the '$me' file must be owned by root and only writable by root.");

  // Include the config file
  $config = red_regenerate_get_config_values($_SERVER);
  $lock_file = $config['lock_file'];

  // Create a lock file to ensure that we only run one instance at a time
  red_regenerate_initialize_lock_file($lock_file);

  // Grab the utils file and the main red_item class file
  red_regenerate_include_files($config['common_src_path'],$lock_file);

  // Check for any requests
  $flags = MYSQL_CLIENT_SSL;
  $sql_resource = mysqli_init();
  if (!($sql_resource->options(MYSQLI_READ_DEFAULT_FILE, $config['mysql_cnf']))) {
    red_regenerate_exit(sprintf("failed to set the default file (MYSQLI_READ_DEFAULT_FILE: %d)\n", MYSQLI_READ_DEFAULT_FILE), $lock_file);
  }

  if (!($sql_resource->options(MYSQLI_READ_DEFAULT_GROUP, 'red'))) {
    red_regenerate_exit(sprintf("failed to set the group for the default file (MYSQLI_READ_DEFAULT_GROUP: %d)\n", MYSQLI_READ_DEFAULT_GROUP), $lock_file);
  }
  if (!($sql_resource->real_connect($config['db_host'],$config['db_user'],$config['db_pass']))) {
    return FALSE;
  }

  if(!mysqli_select_db($sql_resource,$config['db_name'])) {
    red_regenerate_exit(mysqli_error($sql_resource),$lock_file);
  }

  $node = addslashes($config['node']);

  $sql = "SELECT * FROM red_item WHERE  ".
		"item_status = 'active' AND service_id = 9";

  if(!$result = mysqli_query($sql_resource,$sql)) 
    red_regenerate_exit(mysqli_error($sql_resource),$lock_file);

  $construction_options = array(  
    'mode' => 'node', 
    'src_path' => $config['src_path'],
    'common_src_path' => $config['common_src_path'],
    'conf_path' => $config['conf_path'],
    'site_dir_template' => $config['site_dir_template'],
    'sql_resource' => $sql_resource,
    'backends' => $config['red_backends']
  );

  $exit_code = 0;
  while($row = mysqli_fetch_assoc($result)) {
    $exit_code = 0;
    if(is_array($row)) {
      $construction_options['rs'] =& $row;
      $item =& red_item::get_red_object($construction_options);
      if(!isset($item) || empty($item))  {
        red_regenerate_exit('Failed to create red_item object!',$lock_file);
      }
      if(!$item->regenerate()) {
				echo "Failed!\n";
        $exit_code = 1;
      };
			echo ".";
    }
  }
  $msg = '';
  if($exit_code != 0) {
    $msg = "One or more sites had an error during regeneration.";
  }
  echo "\n";
  red_regenerate_exit($msg,$lock_file);
}

function red_regenerate_get_config_values($node) {
  $config_file = '/usr/local/etc/red/red_node.conf';
  /// Overwrite default if passed in
  if($node['argc'] == 2) $config_file = $node['argv'][1];

  if(!file_exists($config_file))
    red_regenerate_exit("The config file, '$config_file', does not exist.");

  if(!red_regenerate_check_file($config_file,'read-restrict'))
    red_regenerate_exit("When running as root the file '$config_file' must be owned by root and only readable by root.\n");

  require_once($config_file);
  $config = array(
    'red_backends' => '',
    'src_path' => '',
    'common_src_path' => '',
    'conf_path' => '',
    'node' => '',
    'lock_file' => '',
    'db_host' => '',
    'db_user' => '',
    'db_pass' => '',
    'db_name' => '',
    'site_dir_template' => '',
    'mysql_cnf' => '',
  );
  foreach($config as $k => $v) {
    if(empty($$k)) red_regenerate_exit("The variable $k is required in your config file.");
    $config[$k] = $$k;
  }
    
  return $config;
}
  
function red_regenerate_include_files($common_src_path,$lock_file) {
  if(!red_regenerate_check_file($common_src_path . '/red.utils.inc.php'))
    red_regenerate_exit("Incorrect permssions or file does not exists. When running as root the file ${common_src_path}/red.utils.inc.php must be owned by root and only writable by root.\n",$lock_file);
  require_once($common_src_path . '/red.lang.utils.inc.php');
  require_once($common_src_path . '/red.utils.inc.php');
  if(!red_regenerate_check_file($common_src_path . '/class.red_db.inc.php'))
    red_regenerate_exit("Incorrect permissions or file does not exist. When running as root the file ${common_src_path}/class.red_db.inc.php must be owned by root and only writable by root.\n",$lock_file);
  require_once($common_src_path . '/class.red_db.inc.php');

  if(!red_regenerate_check_file($common_src_path . '/class.red_ado.inc.php'))
    red_regenerate_exit("Incorrect permissions or file does not exist. When running as root the file ${common_src_path}/class.red_ado.inc.php must be owned by root and only writable by root.\n",$lock_file);
  require_once($common_src_path . '/class.red_ado.inc.php');
  if(!red_regenerate_check_file($common_src_path . '/class.red_item.inc.php'))
    red_regenerate_exit("Incorrect permissions or file does not exist. When running as root the file ${common_src_path}/class.red_item.inc.php must be owned by root and only writable by root.\n",$lock_file);
  require_once($common_src_path . '/class.red_item.inc.php');

}

function red_regenerate_initialize_lock_file($lock_file) {
  // Check for a lock file
  $begin_ts = time();
  $end_ts = $begin_ts + (60);

  while(file_exists($lock_file))   {
    // Loop for a minute before giving up
    echo "Lock file exists: '$lock_file'! Trying again in 10 seconds.\n";
    sleep(10);
    if(time() > $end_ts) die("Lock file exists!\n");
  }

  if(!touch($lock_file))
    red_regenerate_exit("Failed to create the lock file: $lock_file",$lock_file);
}

function red_regenerate_check_file($file,$restriction = 'write-restrict') {
  if(!file_exists($file)) return false;
  if(posix_getuid() == 0) {
    // If we're be run with root privs, make sure the config 
    // file is only writable by root
    if(fileowner($file) != 0) {
      return false;
    }
    $perms = fileperms($file);
    $group_octal_perms = intval(substr(sprintf('%o',$perms),-2,1));
    $user_octal_perms = intval(substr(sprintf('%o',$perms),-1,1));

    if($group_octal_perms > 5 || $user_octal_perms > 5) return false;

    if($restriction == 'read-restrict') {
      if($group_octal_perms > 3 || $user_octal_perms > 3) return false;
    }
  }
  return true;
}

function red_regenerate_exit($message = '',$lock_file = '', $exit_code = 0) {
  if($message != '') echo $message . "\n";
  if(file_exists($lock_file))
    unlink($lock_file);
  exit($exit_code);
}
?>
