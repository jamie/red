<?php
if(!class_exists('red_item_dns_node_knot')) {
  class red_item_dns_node_knot extends red_item_dns {
    var $_knot_zone_dir = '/var/lib/knot';
    var $_knot_etc_dir = '/etc/knot';
    var $quick = FALSE;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to FALSE on error
      if(!$this) return;
      
    }

    // Ensure our conf and zone file directories exist.
    function node_sanity_check() {
      if(!is_dir($this->_knot_zone_dir))  {
        $message = 'Knot root directory does not exist. Trying: ' . $this->_knot_zone_dir;
        $this->set_error($message,'system');
        return FALSE;
      }
      if(!is_dir($this->_knot_etc_dir))  {
        $message = 'Knot root directory does not exist. Trying: ' . $this->_knot_zone_dir;
        $this->set_error($message,'system');
        return FALSE;
      }
      return TRUE;

    }

    /**
     * Standard red operations.
     */
    function delete() {
      if ($this->get_dns_type() == 'mailstore') {
        // No-op
        return TRUE;
      }
      if(!$this->_generate_zone_file()) return FALSE;
      if ($this->quick) {
          return TRUE;
      }
      if(!$this->_reload_knot()) return FALSE;
      if(!$this->_transfer_zone()) return FALSE;
      return TRUE;
    }

    function disable() {
      if ($this->get_dns_type() == 'mailstore') {
        // No-op
        return TRUE;
      }
      return $this->delete();  
    }

    function insert() {
      if ($this->get_dns_type() == 'mailstore') {
        // No-op
        return TRUE;
      }
      if(!$this->_generate_zone_file()) return FALSE;
      if ($this->quick) {
          return TRUE;
      }
      if(!$this->_reload_knot()) return FALSE;
      if(!$this->_transfer_zone()) return FALSE;
      return TRUE;
    }

    function update() {
      if ($this->get_dns_type() == 'mailstore') {
        // No-op
        return TRUE;
      }
      if(!$this->_generate_zone_file()) return FALSE;
      if ($this->quick) {
          return TRUE;
      }
      if(!$this->_reload_knot()) return FALSE;
      if(!$this->_transfer_zone()) return FALSE;
      return TRUE;
    }

    function restore() {
      if ($this->get_dns_type() == 'mailstore') {
        // No-op
        return TRUE;
      }
      if(!$this->_generate_zone_file()) return FALSE;
      if ($this->quick) {
          return TRUE;
      }
      if(!$this->_reload_knot()) return FALSE;
      if(!$this->_transfer_zone()) return FALSE;
      return TRUE;
    }

    function regenerate() {
      if(!$this->_generate_zone_file()) return FALSE;
      return TRUE;
    }

    /**
     * rsync the zones to our secondary servers.
     *
     * After every change all files should be copied over.
     * Using rsync is more secure and simpler than relying
     * on AXFR, etc.
     */
    function _transfer_zone() {
      if(FALSE === red_fork_exec_wait('/usr/local/sbin/mf-knot-zone-transfer', array(), array('HOME' => '/root'))) {
        $this->set_error("Failed to transfer zone to secondary servers.", 'system');
        return FALSE;
      }
      return TRUE;
    }

    /**
     * Generate a zone file for the given record.
     *
     * Regenerate the complete zone file for the given record's
     * zone. 
     */
    function _generate_zone_file() {
      $zone = strtolower($this->get_dns_zone());

			// _populate_zone_file should return the path to a file with a
			// properly populated zone file for the given record.
			$temp = $this->_populate_zone_file($zone);
			if(!$this->_process_new_zone_file($temp, $zone)) {
				return FALSE;
			}
			return TRUE;
    }

    /**
     * Process new zone file.
     *
     * Take the path to the new zone file and return true or false
     * based on whether we handled it successfully or not.
     *
     * We need to deal with three scenarios for a new zone file:
     * An error occured - report it and do nothing OR
     * The zone file is empty, ensure it is deleted OR
     * We have a new zone file, move it into place.
     */
     function _process_new_zone_file($temp, $zone) {
       if(FALSE === $temp) {
        // Something went wrong populating the zone file.
        $this->set_error("Failed to generate zone file.",'system');
        return FALSE;
      }
      if(is_null($temp)) {
        // This mean there are no records in this zone. We should ensure
        // this zone file is deleted.
        $paths = array(
          $this->get_zone_file_path($zone),
          $this->get_zone_conf_file_path($zone)
        );
        foreach($paths as $delete_path) {
          if(file_exists($delete_path)) {
            if(!unlink($delete_path)) {
              red_set_error("Failed to delete $delete_path.", 'system');
              return FALSE;
            }
          }
        }
        return TRUE;
      }
      

      // Success. Now all we have to do is make the zone file live by
      // moving the zone file into place and ensuring that a conf file 
      // pointing to the zone is also in place.
      if(!$this->_make_zone_file_live($zone, $temp)) return FALSE;
      return TRUE;
    }
    
    function get_zone_file_path($zone) {
      return $this->_knot_zone_dir . '/' . $zone . '.zone';
    }

    function get_zone_conf_file_path($zone) {
      return $this->_knot_etc_dir . '/' . 'knot.conf.d/knot.zones.conf.d/' .
        $zone . '.conf'; 
    }

    function _make_zone_file_live($zone, $temp) {
      $dest = $this->get_zone_file_path($zone);
      if(!rename($temp, $dest)) {
        $message = "Failed to move zone file into place.";
        $this->set_error($message,'system');
        return FALSE;
      }
  
      if(!chmod($dest, 0644)) {
        $message = "Failed to chmod  zone file.";
        $this->set_error($message,'system');
        return FALSE;
      }

      // Ensure /etc/knot/knot.conf.d/knot.zones.conf.d/$zone exists.
      $dest = $this->get_zone_conf_file_path($zone);
      $content = "zone:\n  - domain: ${zone}.\n";
      if(!file_put_contents($dest, $content)) {
        $this->set_error("Failed to move zone config file into place.",'system');
        return FALSE;
      }
      return TRUE;
    }

    /*
     * Populate a zone file.
     *
     * Take the name of the zone and type (reverse, forward)
     * Return: FALSE if there is an error, filename populated with the
     * records if it went well, or NULL if the zone is empty and the file
     * should be deleted.
     */
    function _populate_zone_file($zone) {
			// Handle temp file creation.
      $filename = tempnam(sys_get_temp_dir(), 'red');
      $handle = fopen($filename, 'w');
      if(!$handle) {
        $this->red_set_error("Failed to open temp file for zone file.", 'system');
        return FALSE;
      }
      // Write out the beginning of the zone file.
      if(!fwrite($handle, $this->get_header($zone))) {
        $this->set_error("Failed to write out header.", 'system');
        return FALSE;
      }
      if(!fwrite($handle, $this->get_soa($zone))) {
        $this->set_error("Failed to write out soa.", 'system');
				return FALSE;
			}
      if(!fwrite($handle, $this->get_ns($zone))) {
        $this->set_error("Failed to write out ns.", 'system');
				return FALSE;
			}

    	$result = $this->_get_zone_data_result_set($zone);

      if(!$result) {
        $this->set_error("Failed to get result set when populating zone file.", 'system');
        // Something went wrong.
        return FALSE;
      }
      if($this->_sql_num_rows($result) == 0) {
        // No records in this zone, return NULL so we can delete the zone.
        return NULL;
      }

      if(!$this->_add_zone_records_to_file($result, $handle)) return FALSE;

      fclose($handle);
      return $filename;
    }
    
    function _reload_knot() {
      if(FALSE === red_fork_exec_wait('/bin/systemctl', array('reload','knot.service'))) {
        $this->set_error("Failed to reload knot.", 'system');
        return FALSE;
      }
      return TRUE;
    }
      
    function _add_zone_records_to_file($result, &$handle) {
      while($row = $this->_sql_fetch_row($result)) {
        $type = $row[0];
        $fqdn = $this->append_dot($row[1]);
        $ip = $row[2];
        $ttl = $row[3];
        $server_name = $this->append_dot($row[4]);
        $text = $row[5];
        $dist = $row[6];
        $port = $row[7];
        $weight = $row[8];
        $sshfp_algorithm = $row[9];
        $sshfp_type = $row[10];
        $sshfp_fpr = $row[11];
        switch($type) {
          case 'ptr':
            if ($type == 'mailstore') {
              // This is not supposed to be written out. It's internal only.
              return TRUE;
            }
            if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
              if(!fwrite($handle, $this->get_ipv6_ptr($ip, $fqdn, $ttl))) {
                $this->set_error("Failed to write out ipv6 ptr record.", 'system');
                return FALSE;
              }
            }
            else {
              if(!fwrite($handle, $this->get_ptr($ip, $fqdn, $ttl))) {
                $this->set_error("Failed to write out ipv4 ptr record.", 'system');
                return FALSE;
              }
            }
            break;
          case 'mx':
            if(!fwrite($handle, $this->get_mx($fqdn, $server_name, $ttl, $dist))) { 
        			$this->set_error("Failed to write out mx record.", 'system');
							return FALSE;
						}
            break;
          case 'a':
            if(!fwrite($handle, $this->get_a($fqdn, $ip, $ttl))) { 
        			$this->set_error("Failed to write out a record.", 'system');
							return FALSE;
						}
            break;
					case 'aaaa':
            if(!fwrite($handle, $this->get_aaaa($fqdn, $ip, $ttl))) { 
        			$this->set_error("Failed to write out aaaa record.", 'system');
							return FALSE;
						}
            break;

          case 'text':
            if(!fwrite($handle, $this->get_text($fqdn, $text, $ttl))) { 
        			$this->set_error("Failed to write out text record.", 'system');
							return FALSE;
						}
            break;
          case 'cname':
            if(!fwrite($handle, $this->get_cname($fqdn, $server_name, $ttl))) { 
        			$this->set_error("Failed to write out cname record.", 'system');
							return FALSE;
						}
            break;
          case 'srv':
            if(!fwrite($handle, $this->get_srv($fqdn, $server_name, $port, $ttl, $dist, $weight))) { 
        			$this->set_error("Failed to write out srv record.", 'system');
							return FALSE;
						}
            break;
          case 'sshfp':
            if(!fwrite($handle, $this->get_sshfp($fqdn, $ttl, $sshfp_algorithm, $sshfp_type, $sshfp_fpr))) { 
        			$this->set_error("Failed to write out sshfp record.", 'system');
							return FALSE;
						}
            break;

        }      
      }
			return TRUE;
    }

    function get_ptr($ip, $fqdn, $ttl) {
      // We only want last part of IP.
      $ip_parts = explode('.', $ip);
		  $last = array_pop($ip_parts);
      return "${last}    $ttl    IN    PTR    $fqdn\n";
    }
    function get_ipv6_ptr($ip, $fqdn, $ttl) {
      $unpacked = unpack("H*hex", inet_pton($ip));

      // Chop off the first 64 bits - that's the zone.
      $significant_part = substr($unpacked['hex'], 16);

      // Reverse it (zone files want it backwards, what can I say?).
      $reversed = strrev($significant_part);

      // Now separate with periods.
      $ip = substr(preg_replace("/([A-f0-9]{1})/", "$1.", $reversed), 0, -1);

      return "${ip}    $ttl    IN    PTR    $fqdn\n";
    }
    function get_ns($zone) {
      // FIXME - how does a user modify the ttl for a nameserver?
      $ret = '';
      $nameservers = $this->get_nameservers();

      foreach($nameservers as $nameserver) {
        $ret .= "${zone}.    86400    IN    NS    ${nameserver}.\n";
      }
      return $ret;
    }
    function get_a($fqdn, $ip, $ttl) {
      return "${fqdn}    $ttl    IN    A    $ip\n";
    }
		function get_aaaa($fqdn, $ip, $ttl) {
      return "${fqdn}    $ttl    IN    AAAA    $ip\n";
    }
    function get_mx($fqdn, $server_name, $ttl, $dist) {
      return "${fqdn}    $ttl    IN    MX    ${dist}    $server_name\n";
    }
    function get_text($fqdn, $text, $ttl) {
      return "${fqdn}    $ttl    IN    TXT    \"$text\"\n";
    }
    function get_cname($fqdn, $server_name, $ttl) {
      return "${fqdn}    $ttl    IN    CNAME    $server_name\n";
    }
    function get_srv($fqdn, $server_name, $port, $ttl, $dist, $weight) {
      return "${fqdn}    $ttl    IN    SRV    $dist    $weight   $port   $server_name\n";
    }
    function get_sshfp($fqdn, $ttl, $sshfp_algorithm, $sshfp_type, $sshfp_fpr) {
      return "${fqdn}    $ttl    IN    SSHFP    $sshfp_algorithm    $sshfp_type   $sshfp_fpr\n";
    }
    function get_header($zone) {
      return '$TTL 86400' . "\n"
        . '$ORIGIN ' . $zone . '.' . "\n";
    }

    function append_dot($value) {
      // Ensure server_name end in a dot so we don't append $ORIGIN.
      if(!preg_match('/\.$/', $value)) {
        $value .= '.';
      }
      return $value;
    }

    function get_soa($zone) {
      $name_servers = $this->get_nameservers();
      // Get the first one as the authority.
      $ns = array_shift($name_servers) . '.';
      $email = 'hostmaster.' . $zone . '.';
      // For serial, uses a timestamp in ISO format for readability.
      // Include microtime so we can have multiple updates in less than one
      // second.
      $serial = time();
      // Time between refresh from the secondary server (not really used
      // since the primary server notifies the secondary when there is an 
      // update).
      $refresh = '2h';
      // How often for the secondary to retry if first attempt fails.
      $retry = '3m';
      // How long secondary values are valid if we can't reach the primary
      $expire = '4w';
      // How long the secondary should cache a "no response for this domain"
      $negative_response_ttl = '1h'; 
      return "@    IN    SOA    $ns    $email    (\n" .
        "    $serial\n" .  
        "    $refresh\n" .
        "    $retry\n" .
        "    $expire\n" .
        "    $negative_response_ttl\n" .
        ")\n";
    }

		/*
     * For a given zone, return all data necessary to build a forward
     * zone.
     */
    function _get_zone_data_result_set($zone) {
      $zone_file = addslashes($zone);
      // FIXME - limit to just the records matching the active domain.
      $sql = "SELECT dns_type,dns_fqdn,dns_ip,dns_ttl,".
        "dns_server_name,dns_text,dns_dist,dns_port,dns_weight,".
        "dns_sshfp_algorithm, dns_sshfp_type, dns_sshfp_fpr FROM ".
        "red_item_dns INNER JOIN red_item ON red_item_dns.item_id = ".
        "red_item.item_id WHERE item_status != 'pending-delete' AND ".
        "item_status != 'deleted' AND item_status != 'transfer-limbo' ".
        "AND item_status != 'disabled' AND item_status != 'pending-disable' ".
        "AND dns_zone = '$zone_file' ORDER BY dns_type";
      return $this->_sql_query($sql);
    }
  }  
}

?>
