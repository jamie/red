<?php
if(!class_exists('red_item_list_node_mailman')) {
  class red_item_list_node_mailman extends red_item_list {
    // If you want to extend this class in a way the requires an
    // addition to the config file, then add a value to this
    // array in your constructor
    var $_config_variables = array('mailman_bin_dir',
                             'owner_email_template',
                             'web_url',
                             'postmap_cmd',
                             'from_email',
                             'mailman_list_dir',
                             'postfix_transport_file',
                             'extra_conf_file');
    var $_mailman_bin_dir;
    var $_owner_email_template;
    var $_extra_conf_file = false;
    var $_mailman_list_dir = '/var/lib/mailman/lists';
    var $_postfix_disabled_email_file = '/etc/postfix/disabled-email';
    var $_web_url;
    var $_postfix_transport_file;
    var $_postmap_cmd;
    var $_from_email;
    var $_initial_password;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;

      $conf_file = $construction_options['conf_path'] .
        '/red_list.mailman.conf';
      if(!$this->_set_config_values($conf_file)) {
        return False;
      }
    }

    function node_sanity_check() {
      if(!is_dir($this->_mailman_bin_dir)) {
        $message = 'Mailman bin directory does not exist. Trying: ' . $this->_mailman_bin_dir;
        $this->set_error($message,'system');
        return false;
      }
      $message = '';
      if($this->_extra_conf_file !== false) {
        if(!$this->variable_conf_files_exists($this->_extra_conf_file,$message)) {
          $this->set_error($message,'system');
          return false;
        }
      }
      if(!$this->variable_conf_files_exists($this->_owner_email_template,$message)) {
        $this->set_error($message,'system');
        return false;
      }
      if(!is_writable($this->_postfix_transport_file))  {
        $message = 'Transport file not writable or does not '.
          'exist. Trying: ' . $this->_postfix_transport_file;
        $this->set_error($message,'system');
        return false;
      }
      if(!file_exists($this->_postmap_cmd)) {
        $message = 'Postmap command command does not exist. Trying: ' .
          $this->_postmap_cmd;
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    function delete() {

      if(!$this->remove_email_from_disabled_file()) return false;
      if(!$this->delete_list()) return false;
      return true;
    }

    function disable() {
      return $this->add_email_to_disabled_file();
    }

    function insert() {
      // See if the user exists
      if($this->list_exists()) {
        $message = 'I was asked to add a new Mailman list, but '.
          'a list with the same name already exists.';
        $this->set_error($message,'system','soft');
        return false;
      }
      if(!$this->insert_list()) return false;
      if(!$this->extra_configuration()) return false;
      if(!$this->notify_owner()) return false;
      return true;
    }

    function update() {
      if(!$this->remove_email_from_disabled_file()) return false;
      // reset the list password
      if(!$this->reset_password()) return false;
      return true;
    }

    function restore() {
      if(!$this->remove_email_from_disabled_file()) return false;

      // See if the list exists
      if(!$this->list_exists()) {
        return $this->insert();
      }
      else {
        return $this->update();
      }
    }

    function list_exists() {
      $dir = $this->_mailman_list_dir . '/' . $this->get_list_name();
      if(file_exists($dir)) {
        return true;
      }
      return false;
    }

    function add_email_to_disabled_file() {
      $file = $this->_postfix_disabled_email_file;
      $list_email = $this->get_list_name() . '@' . $this->get_list_domain();
      if(!red_key_exists_in_file($list_email," ",$file)) {
        $action = "REJECT This email list has been disabled. If you are the list owner, please contact support at https://support.mayfirst.org/newticket.";
        $map_line = "$list_email $action\n";
        if(!red_key_exists_in_file($list_email," ",$file)) {
          if(!red_append_to_file($file,$map_line)) {
            $message = "Failed to add email to disabled email list.";
            $this->set_error($message,'system');
            return false;
          }
        }
        if(!$this->postmap_file($file)) return false;
      }
      return true;
    }

    function remove_email_from_disabled_file() {
      $file = $this->_postfix_disabled_email_file;
      $list_email = $this->get_list_name() . '@' . $this->get_list_domain();
      if(red_key_exists_in_file($list_email," ",$file)) {
        if(!red_delete_from_file($file,$list_email," ")) {
          $message = "Failed to remove list email from disabled file.";
          $this->set_error($message,'system');
          return false;
        }
        if(!$this->postmap_file($file)) return false;
      }
      return true;
    }

    function reset_password() {
      $cmd = $this->_mailman_bin_dir . "/change_pw";
      $new_password = red_generate_random_password();
      if(FALSE === $new_password) {
        $message = "Failed to generate random password.";
        $this->set_error($message,'system');
        return FALSE;
      }
      $args = array('-l', $this->get_list_name(), '-p', $new_password);
      if(0 == red_fork_exec_wait($cmd, $args)) {
        return TRUE;
      }
      $message = "Failed to change the password.";
      $this->set_error($message,'system');
      return false;
    }

    function insert_list()  {
      if(!$this->configure_domain_if_necessary()) return false;
      $domain = $this->get_list_domain();
      $cmd = $this->_mailman_bin_dir . "/newlist";
      $this->_initial_password = red_generate_random_password();
      if(FALSE === $this->_initial_password) {
        $message = "Failed to generate random password.";
        $this->set_error($message,'system');
        return FALSE;
      }
      $args = array('-q',
        $this->get_list_name() . '@' . $domain,
        $this->get_list_owner_email(),
        $this->_initial_password
      );
      if(0 == red_fork_exec_wait($cmd, $args)) {
        return TRUE;
      }

      $message = "Failed to create the list.";
      $this->set_error($message,'system');
      return false;
    }

    function delete_list() {
      if(!$this->delete_domain_if_necessary()) return false;
      $cmd = $this->_mailman_bin_dir . "/rmlist";
      $args = array('-a', $this->get_list_name());
      if(0 == red_fork_exec_wait($cmd, $args)) {
        return TRUE;
      }
      $message = "Failed to delete the list.";
      $this->set_error($message,'system');
      return false;

    }

    function configure_domain_if_necessary() {
      // if this is the first time this domain name has been
      // used, make sure postfix is configured to use it
      $domain = $this->get_list_domain();
      if(!red_key_exists_in_file($domain,"\t",$this->_postfix_transport_file)) {
        $map_line = "$domain\tmailman:\n";
        if(!red_append_to_file($this->_postfix_transport_file,$map_line)) {
          $message = "Failed to create transport account for the domain.";
          $this->set_error($message,'system');
          return false;
        }

        if(!$this->postmap_file($this->_postfix_transport_file)) return false;
      }
      return true;
    }

    function postmap_file($file) {
      $cmd = $this->_postmap_cmd;
      $args = array($file);
      if(0 == red_fork_exec_wait($cmd,$args)) {
        return TRUE;
      }
      $message = "Failed to run postmap.";
      $this->set_error($message,'system');
      return false;
    }

    function delete_domain_if_necessary() {
      // if the domain name is not used by any other lists,
      // remove it from the postfix conf files.
      $domain = $this->get_list_domain();
      if(red_key_exists_in_file($domain,"\t",$this->_postfix_transport_file)) {
        $item_id = $this->get_item_id();
        $sql = "SELECT item_id FROM red_item_list JOIN red_item USING(item_id) ".
          "WHERE list_domain = '$domain' AND red_item.item_id != $item_id ".
          "AND (item_status = 'active' OR item_status LIKE 'pending-%')";
        $result = $this->_sql_query($sql);
        if($this->_sql_num_rows($result) == 0) {
          // remove
          $file = $this->_postfix_transport_file;
          if(!red_delete_from_file($file,$domain,"\t")) {
            $message = "Failed to delete transport account for the domain.";
            $this->set_error($message,'system');
            return false;
          }
          if(!$this->postmap_file($file)) return false;
        }
      }
      return true;
    }
    function extra_configuration() {
      $extra_conf_file = $this->choose_var_from_multi_dimensional_config($this->_extra_conf_file);
      // only run if _extra_conf_file is not null
      if($extra_conf_file !== false) {
        $cmd = $this->_mailman_bin_dir . "/config_list";
        $args = array('-vi', $extra_conf_file, $this->get_list_name());
        if(0 == red_fork_exec_wait($cmd, $args)) {
          return TRUE;
        }
        $message = "Failed to apply extra configuration to the list.".
        $this->set_error($message,'system');
        return false;
      }
      return true;

    }

    function notify_owner() {
      $template = $this->choose_var_from_multi_dimensional_config($this->_owner_email_template);
      $content = file_get_contents($template);
      $list_name = $this->get_list_name();
      $find = array(
        '{list_name}',
        '{initial_password}',
        '{web_url}',
        '{list_owner}',
        '{list_address}'
      );
      $list_owner = $this->get_list_owner_email();
      $domain = $this->get_list_domain();
      $list_address = "$list_name@$domain";
      $replace = array(
        $list_name,
        $this->_initial_password,
        $this->_web_url . 'admin/' . $list_name,
        $list_owner,
        $list_address,
      );
      $content = str_replace($find,$replace,$content);
      $subject = 'New email list instructions';
      $from_email = $this->choose_var_from_multi_dimensional_config($this->_from_email);
      $headers = null;
      if($from_email) {
        $headers = "From: $from_email";
      }
      if(!red_mail($list_owner,$subject,$content,$headers)) {
        $message = "Failed to send email.";
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }
  }
}


?>
