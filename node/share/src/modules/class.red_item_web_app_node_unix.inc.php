<?php
if(!class_exists('red_item_web_app_node_unix')) {
  class red_item_web_app_node_unix extends red_item_web_app {
    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;
    }

    function node_sanity_check() {
      return true;
    }

    function delete() {
      // No-op. You have to delete the web conf to delete the web app.
      return TRUE; 
    }

    function disable() {
      // No-op. You have to disable the web conf to disable the web app;
      return TRUE;
    }

    function insert() {
      $name = $this->get_web_app_name();
      switch ($name) {
        case 'drupal9':
          return $this->install_drupal9();
        case 'wordpress':
          return $this->install_wordpress();
        case 'backdrop':
          return $this->install_backdrop();
      }
      $this->set_error("Unknown web app name.", 'validation');
      return FALSE;
    }

    function update() {
      // FIXME - if you try to change the security settings you will
      // get an error. But if you are re-submitting after a soft
      // error, you'll want to re-run insert.
      return $this->insert(); 
    }

    function restore() {
      // FIXME - is this right?
      return $this->insert(); 
    }

    function get_web_conf_execute_as_user() {
      $sql = "SELECT web_conf_execute_as_user FROM red_item JOIN red_item_web_conf " .
        "USING (item_id) WHERE item_status = 'active' " .
        "AND hosting_order_id = " . intval($this->get_hosting_order_id());
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      if(empty($row)) return false;
      return $row[0];
    }

    function install_drupal9() {
      return $this->install_web_app('drupal9');
    }
    function install_wordpress() {
      return $this->install_web_app('wordpress');
    }
    function install_backdrop() {
      return $this->install_web_app('backdrop');
    }

    function install_web_app($name) {
      $user = $this->get_web_conf_execute_as_user();
      if (empty($user)) {
        $this->set_error("I failed to get the execute as user. Is your web configuration properly enabled?", 'validation');
        return FALSE;
      }
      $args = [
        $name,
        $this->get_site_dir() . '/web',
      ];
      $cmd = '/usr/local/share/red/node/sbin/red-install-web-app';
      $env = [];
      $exit_status = red_fork_exec_wait($cmd, $args, $env, $user);
      if($exit_status == 0) {
        return TRUE;
      }
      elseif ($exit_status == 1) {
        $this->set_error("The web directory is not empty. Please delete and re-create your web configuration and try again.", 'validation');
        return FALSE;
      }
      else {
        $this->set_error("There was an error installing the web app (error: $exit_status). Please contact support for help.", 'validation');
        return FALSE;
      }
      return TRUE;
    }
  }  
}


?>
