<?php
if(!class_exists('red_item_cron_node_vixie')) {
  class red_item_cron_node_vixie extends red_item_cron {

    // If you want to extend this class in a way the requires an
    // addition to the config file, then add a value to this
    // array in your constructor
    var $_config_variables = array('crontab_cmd');
    var $_crontab_cmd;

    /*
     * MAIN FUNCTIONS
     */

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;

      $conf_file = $construction_options['conf_path'] . 
        '/red_cron.vixie.conf';
      if(!$this->_set_config_values($conf_file)) {
        return False;
      }
    }

    function node_sanity_check() {
      if(!file_exists($this->_crontab_cmd)) {
        $message = 'crontab command does not exist. '.
          'Trying: ' . $this->_crontab_cmd;
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    // either delete, insert, restore, disable, or update is always called
    function delete() {
      return $this->_regenerate_crontab();
    }
    function disable() {
      return $this->_regenerate_crontab();
    }
    function insert() {
      return $this->_regenerate_crontab();
    }

    function update() {
      return $this->_regenerate_crontab();
    }

    function restore() {
      return $this->_regenerate_crontab();
    }
      
    function _regenerate_crontab() {
      $red_data = $this->_get_red_crontab();
      $server_data = $this->_get_server_crontab();
      if(false === $server_data) return false;
      $final_data = 
        $this->_merge_red_and_server_crontabs($red_data,$server_data);
      return $this->_set_crontab($final_data);
    }

    function _set_crontab($data) {
      $login = escapeshellarg($this->get_cron_login());
      $data = escapeshellarg($data);
      $cmd = "echo $data | " . $this->_crontab_cmd . " -u $login -";
      exec($cmd,$ouptut,$return);
      if($return != 0) {
        $this->set_error("There was an error setting the crontab on the server for $login.",'system','soft');
        return false;
      }
      return true;
    }

    function _merge_red_and_server_crontabs($red,$server) {
      $red_begin = '# BEGIN red generated lines';
      $red_end = '# END red generated lines';
      $ret = array();
      // remove red generated lines from server crontab
      reset($server);
      $red_generated = false;
      foreach($server as $line) {
        if(trim($line) == $red_begin) {
          $red_generated = true;
        } elseif(trim($line) == $red_end) {
          $red_generated = false;
        } elseif(!$red_generated) {
          $ret[] = $line;
        }
      }
      // Now add red generated content
      $ret[] = $red_begin; 
      $ret = array_merge($ret,$red);
      $ret[] = $red_end;
      return implode("\n",$ret);
    }

    // get existing server set crontab (could contain non
    // red entries)
    function _get_server_crontab() {
      $login = escapeshellarg($this->get_cron_login());
      $cmd = $this->_crontab_cmd . " -u $login -l 2> /dev/null";
      exec($cmd,$output,$return);
      if($return == 1) {
        // if the user has no crontab, crontab will return 1 - same error as no user :(
        $output = '';
      } elseif($return != 0) {
        $this->set_error("There was an error retreiving the crontab on the server for $login.",'system','soft');
        return false;
      }
      if(empty($output)) $output = array();
      return $output;
    }


    // get all crontab records for this user
    function _get_red_crontab() {
      $login = $this->get_cron_login();
      $login = addslashes($login);
      $sql = "SELECT cron_schedule,cron_cmd FROM ".
        "red_item_cron INNER JOIN red_item USING(item_id)  WHERE ".
        "(item_status = 'pending-update' OR item_status = 'pending-insert' ".
        "OR item_status = 'active')  AND cron_login = '$login'";
      $result = $this->_sql_query($sql);
      $ret = array();
      while($row = $this->_sql_fetch_row($result)) {
        $ret[] = $row[0] . ' ' . $row[1];
      }
      return $ret;
    }
  }  
}


?>
