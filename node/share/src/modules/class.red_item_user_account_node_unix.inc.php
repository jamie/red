<?php
if(!class_exists('red_item_user_account_node')) {
  class red_item_user_account_node_unix extends red_item_user_account {
    // If you want to extend this class in a way the requires an
    // addition to the config file, then add a value to this
    // array in your constructor
    var $_config_variables = array('useradd_cmd',
                             'groupadd_cmd',
                             'groupdel_cmd',
                             'userdel_cmd',
                             'usermod_cmd',
                             'auto_responder_cmd',
                             'group_file',
                             'passwd_file',
                             'home_dir_template'
                                  );
    var $_usermod_cmd;
    var $_useradd_cmd;
    var $_userdel_cmd;
    var $_groupadd_cmd;
    var $_auto_responder_cmd;
    var $_groupdel_cmd;
    // used for testing for the existance of users and groups
    var $_passwd_file = '/etc/passwd';
    var $_group_file = '/etc/group';
    var $_home_dir_template;
    var $_auto_response_db_name = '.red.auto_response.db';
    var $_db;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;

      $conf_file = $construction_options['conf_path'] . 
        '/red_user_account.unix.conf';
      if(!$this->_set_config_values($conf_file)) {
        return FALSE;
      }
    }

    function disable() {
      // See if the user exists 
      if(!$this->unix_account_exists() ) {
        $message = 'I was asked to delete the account, but the account '.
          'does not exist in the unix config files. '.
          'I will continue without a hard stop '.
          'but thought you would like to know.';
        $this->set_error($message,'system','soft');
      }
      return $this->disable_unix_account();
    }

    function delete() {
      // See if the user exists  
      if(!$this->unix_account_exists() ) {
        $message = 'I was asked to delete the account, but the account '.
          'does not exist in the unix config files. '.
          'I will continue without a hard stop '.
          'but thought you would like to know.';
        $this->set_error($message,'system','soft');
      }
      if(!$this->delete_unix_account()) return false;
        
      return true;
    }

    function insert() {
      // Make sure this user does not already exist
      if($this->unix_account_exists() && posix_getuid() == 0) {
        $message = 'Cannot insert new account, an account with '.
          'the same login already exists.';
        $this->set_error($message,'system');
        return false;
      }
        
      if(is_dir($this->get_home_dir())) {
        $message = 'Cannot insert new account, the home directory '.
          'already exists. Please manually delete this '.
          "user's home directory and try again.";
        $this->set_error($message,'system','soft');
        return false;
      }

      // create the unix group first (in case it hasn't been created)
      // because the new user will be set with the group id as their
      // primary group - so if groupadd fails, we want to stop rather
      // than create a user with an invalid group
      // this function returns true if the group already exists
      if(!$this->create_unix_group()) return false; 
      if(!$this->create_unix_account()) return false;
      if(!$this->update_auto_response_settings()) return false;

      return true;
    }

    function update() {
      // See if the user exists 
      if(!$this->unix_account_exists() && posix_getuid() == 0) {
        $message = 'I was asked to update the account, but the account '.
          'does not exist in the unix config files. ';
        $this->set_error($message,'system');
        return false;
      }
      if(!$this->update_unix_account()) return false;
      if(!$this->update_auto_response_settings()) return false;
      return true;
    }

    function restore() {
      if(!$this->unix_account_exists()) {
        if(!$this->insert()) return false;
      }
      else {
        if(!$this->update()) return false;
      }
      return true;  
    }

    function get_home_dir() {
      $login = $this->get_user_account_login();
      $member_name = $this->get_member_unix_group_name();
      $hosting_order_identifier = $this->get_hosting_order_identifier();
      $find = array('{member_name}','{identifier}','{login}');
      $replace = array($member_name,$hosting_order_identifier,$login);
      $home_dir = str_replace($find,$replace,$this->_home_dir_template);
      return $home_dir;
    }

    function get_user_dir() {
      $home_dir = $this->get_home_dir();
      // shave off last item
      $home_dir = rtrim($home_dir,'/');
      $parts = explode('/',$home_dir);
      // pop off the last element
      array_pop($parts);
      // put it back together
      return implode('/',$parts);
    }

    function unix_account_exists() {
      return red_key_exists_in_file($this->get_user_account_login(),':',$this->_passwd_file);
    }

    function unix_group_exists() {
      if(!red_key_exists_in_file($this->get_hosting_order_unix_group_name(),':',$this->_group_file)) return false;
      // fixme if it does exist, make sure it's not a group id less than 1500
      return true;
    }

    function node_sanity_check() {
      $parent = red_get_parent_directory($this->get_home_dir(), TRUE);
      if(!is_writable($parent)) {
        $message = 'Home directory (or nearest parent) is not writable. '.
          'Trying: ' . $parent;
        $this->set_error($message,'system');
        return false;
      }
      // we need to read the passwd and group files in order to 
      // no if a group or user already exists. Technically we need
      // to be able to write to them as well - however, since we use
      // the useradd and groupadd commands, we should leave open the
      // possibility that the user being invoked can't write to these
      // files but can execute useradd and groupadd suid.
      if(!is_readable($this->_passwd_file)) {
        $message = 'Password file not readable. '.
          'Trying: ' . $this->_passwd_file;
        $this->set_error($message,'system');
        return false;
      }
      if(!is_readable($this->_group_file)) {
        $message = 'Group file not readable. '.
          'Trying: ' . $this->_passwd_file;
        $this->set_error($message,'system');
        return false;
      }

      $cmds_to_check = array('groupadd','groupdel','useradd','userdel','usermod','auto_responder');
      foreach($cmds_to_check as $cmd) {
        $cmd_variable_name = "_{$cmd}_cmd"; 
        if(!file_exists($this->$cmd_variable_name)) {
          $message = "$cmd command does not exist. ".
            'Trying: ' . $this->$cmd_variable_name;
          $this->set_error($message,'system');
          return false;
        }
      }  
      return true;
    }

    function create_unix_account() {
      if(!$this->unix_account_exists()) {
        $user_dir = $this->get_user_dir();
        if(!red_create_directory_recursively($user_dir)) {
          $message = "Failed to create site user directory.";
          $this->set_error($message,'system');
          return false;
        }
        $login = $this->get_user_account_login();
        $uid = $this->get_user_account_uid();
        $gid = $this->get_hosting_order_unix_group_id();
        $real_home = $this->get_home_dir();
        $symlink_home = NULL;
        $mountpoint = $this->get_user_account_mountpoint();
        if ($mountpoint) {
          // We have to put the home directory in the mount point and
          // create a symlink to the user facing home dir.
          $mountpath = "/media/${mountpoint}";
          if (!is_dir($mountpath)) {
            // Something is wrong, mountpoint is not mounted.
            $this->set_error("Mountpoint: /media/${mountpoint} is not mounted.", "system", "soft");
            return FALSE;
          }
          $mount_user_dir = "${mountpath}/members/users";
          if(!red_create_directory_recursively($mount_user_dir)) {
            $message = "Failed to create mount path site user directory.";
            $this->set_error($message, 'system', 'soft');
            return FALSE;
          }
          $real_home = "${mount_user_dir}/${login}";
          $symlink_home = $this->get_home_dir();
        }
        $pass = $this->get_user_account_password();
        $cmd = $this->_useradd_cmd;  
        $args = array(
          '-u', $uid,
          '-g', $gid,
          '-d', $real_home,
          // default to shell of false - this is changed by the
          // node access module and the email module
          '-s', '/bin/false',
          '-p', $pass,
          // create home directory
          '-m', 
          $login
        );

        if (red_fork_exec_wait($cmd, $args) != 0) {
          $error = "Failed to create the new user.";
          $this->set_error($error,'system','hard');
          return false;
        }
        
        if ($symlink_home) {
          // We want the user to think their home directory is in /home/members/users/<login>
          // not in /media/<mountoint/users/login, so we give them a symlink and update their
          // user account to point to the symlink.
          if (!symlink($real_home, $symlink_home)) {
            $this->set_error("Failed to symlink real home to mount home.", 'system', 'soft');
            return FALSE;
          }
          $cmd = $this->_usermod_cmd;  
          $args = array(
            "-d", $symlink_home,
            $login
          );
          if (red_fork_exec_wait($cmd, $args) != 0) {
            $error = "Failed to update home directory.";
            $this->set_error($error,'system','soft');
            return false;
          }
        }
      }

      return true;
    }

    function create_unix_group() {
      // all users have their primary group set to the hosting order group
      if(!$this->unix_group_exists()) {
        // make sure the group id is not already taken
        $group_id = $this->get_hosting_order_unix_group_id();
        $groups = file($this->_group_file);
        $preg = '/^'.RED_LOGIN_REGEXP.":x:$group_id:/";
        if(preg_grep($preg,$groups)) {
          // somehow this group id is being used by another group
          $message = "Group ID: $group_id is already in use by another ".
            "group on this server.";
          $this->set_error($message,'system');
          return false;
        }
        $group_name = $this->get_hosting_order_unix_group_name();
        $group_id = $this->get_hosting_order_unix_group_id();
        $cmd = $this->_groupadd_cmd;
        $args = '-g ' . escapeshellarg($group_id) . ' ' .
          escapeshellarg($group_name) .
          " 2>&1";
        exec("$cmd $args",$output,$return_value);
        if($return_value != 0) {
          $output = $this->get_htmlentities(implode(' ',$output));
          $error = "Failed to create the new group. The output was: ".
            "$output.";
          $this->set_error($error,'system','hard');
          return false;
        }
      }  
      return true;
    }

    function delete_unix_account() {
      $login = $this->get_user_account_login();
      $cmd = $this->_userdel_cmd;  
      $mountpoint = $this->get_user_account_mountpoint();
      $args = array();
      if (empty($mountpoint)) {
        // remove home directory
        $args[] = '-r';
      }
      $args[] = $login;
      if (red_fork_exec_wait($cmd, $args) != 0) {
        $error = "Failed to delete the user.";
        $this->set_error($error,'system','hard');
        return false;
      }

      if (!empty($mountpoint) && !empty($login)) {
        // The command above only deletes the symlink. We have to delete the actual directory.
        $mountpoint_dir = "/media/${mountpoint}/members/users/${login}";
        if (is_dir($mountpoint_dir)) {
          if (!red_delete_directory_recursively($mountpoint_dir)) {
            $this->set_error("Failed to delete mountpoint directory.", 'system', 'hard');
            return FALSE;
          }
        }
        // And the symlik
        unlink($this->get_home_dir());
      }

      return true;
    }

    function disable_unix_account() {
      // set expiration date to 1  
      $login = $this->get_user_account_login();
      $cmd = $this->_usermod_cmd;  
      $args = '-e 1 ' .
        escapeshellarg($login).
        " 2>&1";
      exec("$cmd $args",$output,$return_value);
      if($return_value != 0) {
        $output = $this->get_htmlentities(implode(' ',$output));
        $error = "Failed to disable the user. The output was: ".
          "$output.";
        $this->set_error($error,'system','hard');
        return false;
      }
      return true;
    }

    function update_unix_account() {
      // the only field that can be updated:  user_account_password
      // not sure what has changed, so update everything
      // also, set expiration date to '' which means no
      // expiration date (needed in case the account had
      // previously been disabled, this will re-enable it)
      $login = $this->get_user_account_login();
      $pass = $this->get_user_account_password();
      $cmd = $this->_usermod_cmd;  
      $args = '-p ' . escapeshellarg($pass) . ' '.
        '-e "" ' .
        escapeshellarg($login).
        " 2>&1";
      exec("$cmd $args",$output,$return_value);
      if($return_value != 0) {
        $output = $this->get_htmlentities(implode(' ',$output));
        $error = "Failed to modify the user. The output was: ".
          "$output.";
        $this->set_error($error,'system','hard');
        return false;
      }
      return true;
    }

    function update_auto_response_settings() {
      // If the autoresponder_action is empty, remove the auto responder
      // reference in the .forward file and delete the auto_response_db.
      // If it is not empty, add to the forward file and initialize db
      $action = $this->get_user_account_auto_response_action();
      if(empty($action)) {
        if(!$this->unset_auto_response()) return false;
      } else {
        if(!$this->set_auto_response()) return false;
      }
      return true;
    }

    function set_auto_response() {
      if(!$this->initialize_auto_response_db()) return false;
      if(!$this->update_auto_response_db()) return false;
      // first delete from forward file to avoid adding the same
      // line twice
      if(!$this->delete_from_forward_file()) return false;
      if(!$this->update_forward_file()) return false;
      return true;
    }

    function unset_auto_response() {
      if(!$this->unlink_auto_response_db()) return false;
      if(!$this->delete_from_forward_file()) return false;
      return true;
    }

    function unlink_auto_response_db() {
      $home = $this->get_home_dir() . '/';
      $db = $home . $this->_auto_response_db_name;
      if(file_exists($db)) {
        if(!unlink($db)) {
          $message = "Unable to delete auto response db. Trying '$db'.";
          $this->set_error($message,'system');
          return false;
        }
      }
      return true;
    }

    function delete_from_forward_file() {
      $home = $this->get_home_dir() . '/';
      $forward = $home . '.forward';
      if(file_exists($forward)) {
        $lines = file($forward);
        foreach($lines as $k => $v) {
          if(preg_match("#$this->_auto_responder_cmd#",$v)) {
            unset($lines[$k]);
          }
        }
        if(count($lines) == 0)  {
          if(!unlink($forward)) {
            $this->set_error("Failed to unlink '$forward'",'system');
            return false;
          }
        } else {
          if(!file_put_contents($forward,implode("\n",$lines))) {
            $this->set_error("failed to resave '$forward'",'system');
            return false;
          }
        }
      }
      return true;
    }

    function update_auto_response_db() {
      $message = $this->get_user_account_auto_response();
      $reply_from = $this->get_user_account_auto_response_reply_from();
      // Delete existing settings
      $sql = "DELETE FROM settings";
      if(!$this->_db->query($sql)) {
        $this->set_error("Failed to delete existing settings.",'system');
        return false;
      }
      // Insert new settings
      $message = $this->_db->quote($message);
      $sql = "INSERT INTO settings (name,value) VALUES('message',$message)";
      if(!$this->_db->query($sql)) {
        $this->set_error("Failed to insert message.",'system');
        return false;
      }
      $reply_from = $this->_db->quote($reply_from);
      $sql = "INSERT INTO settings (name,value) VALUES('reply_from',$reply_from)";
      if(!$this->_db->query($sql)) {
        $this->set_error("Failed to insert message.",'system');
        return false;
      }
      return true;
    }

    function update_forward_file() {
      $action = $this->get_user_account_auto_response_action();
      $parts = array();
      if($action == 'respond_and_deliver') {
        $parts[] = $this->get_user_account_login();
      }
      $parts[] = '"|' . $this->_auto_responder_cmd . '"';
      $line = implode(', ',$parts);
      $file = $this->get_home_dir() . '/.forward';
      if(!red_append_to_file($file,$line)) {
        $this->set_error("Failed to append to '$file'",'system');
        return false;
      }
      return true;
    }

    function initialize_auto_response_db() {
      $home = $this->get_home_dir() . '/';
      $db_path = $home . $this->_auto_response_db_name;

      // see if the db already exists
      $exists = false;
      // if it does exists, we will still initialize...
      if(file_exists($db_path)) $exists = true;
      if(!$this->_db = new PDO("sqlite:$db_path")) {
        $this->set_error("Failed to initialize sqlite from '$db_path'",'system');
        return false;
      }
      // ... but we won't re-create the tables.
      if($exists) return true;

      $sql = 'CREATE TABLE settings (name varchar(20), value text)';
      $this->_db->query($sql);
      $sql = 'CREATE TABLE log (email varchar(128), ts timestamp)';
      $this->_db->query($sql);

      // chown to the current user
      $username = $this->get_user_account_login();
      $group = $this->get_hosting_order_unix_group_name();
      if(!red_chown($db_path,$username)) {
        $this->set_error("Failed to chown '$db_path' to '$username'",'system');
        return false;
      }
      if(!red_chgrp($db_path,$group)) {
        $this->set_error("Failed to chgrp '$db_path' to '$group'",'system');
        return false;
      }
      if(!chmod($db_path,0600)) {
        $this->set_error("Failed to chmod '$db_path' to 0600",'system');
        return false;
      }
      return true;
    }
  }
}


?>
