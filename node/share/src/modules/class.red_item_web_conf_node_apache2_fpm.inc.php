<?php
/**
 * Use php-fpm for all PHP scripts.
 */
require_once('class.red_item_web_conf_node_apache2.inc.php');
if(!class_exists('red_item_web_conf_node_apache2_fpm')) {
  class red_item_web_conf_node_apache2_fpm extends red_item_web_conf_node_apache2 {
    // Available versions of PHP on this server.
    var $php_versions = array();

    // Chose PHP configuration info for this item.
    var $chosen_php_version = NULL;

    function node_sanity_check() {
      if (!$this->set_php_version()) {
        // Don't return FALSE - that triggers a hard error.
        $this->set_error("Failed to set the PHP version.", 'system', 'soft');
      }
      return parent::node_sanity_check();
    }

    /**
     * Choose the right PHP version for this site. 
     *
     * We store all available versions in the php_versions array, in the form
     * like 5.6, 7.0, 7.1, 8.0, etc.
     **/
    function set_php_version() {
      // Collect all available versions on this server.
      $files = glob('/usr/bin/php*');
      foreach($files as $candidate) {
        if (preg_match('/php([0-9.]+)/', $candidate, $matches)) {
            $this->php_versions[] = $matches[1];
        }
      }

      if (count($this->php_versions) == 0) {
        $this->set_error('Cannot find any versions of PHP on the server', 'system', 'hard');
        return FALSE;
      }

      // Reverse sort so the highest number is in position 0.
      rsort($this->php_versions);

      // Get user selection.
      $php = $this->get_web_conf_php_version();

      if ($php == '0.0') {
        // No user choice, pick the highest one.
        $this->chosen_php_version = $this->php_versions[0];
      }
      else {
        $this->chosen_php_version = $php;
      }

      // Make sure the user chosen version is available.
      if (!in_array($this->chosen_php_version, $this->php_versions)) {
        $error = "User chosen PHP version is not available on the server.";
        $this->set_error($error,'system','soft');
        return FALSE;
      }

      return TRUE;
    }

    function get_php_info($type, $version = NULL) {
      if (is_null($version)) {
        $version = $this->chosen_php_version;
      }

      if ($type == 'etc_path') {
        return '/etc/php/' . $version;
      }
      elseif ($type == 'run_path') {
        return '/var/run/php';
      }
      elseif ($type == 'service_name') {
        return 'php' . $version . '-fpm';
      }
    }

    // overridden functions
    function get_apache_conf_file_children_additions() {
      // If PHP is enabled, add directive that matches PHP files.
  
      $ret = '';
      if($this->get_web_conf_max_processes() != '0') {
        $socket_path = $this->get_socket_path();
        $ret .= "\t" . '<FilesMatch \.php$>' . "\n";
        $ret .= "\t\t" . 'SetHandler "proxy:unix:' . $socket_path . '|fcgi://localhost"' . "\n";
        $ret .= "\t". '</FilesMatch>'. "\n";
      }
      return $ret;
    }

    function get_web_related_directory_names() {
      $core = parent::get_web_related_directory_names();
      // add include/php5
      $core[] = 'include/php5';
      return $core;
    }

    /**
     * Override delete_apache_files()
     *
     * We need to delete the php-fpm configuration.
     */
    function delete_apache_files() {
      if(!parent::delete_apache_files()) return FALSE;
      
      // Delete the php-fpm configution file.
      if(!$this->delete_fpm_config()) return FALSE;
      return TRUE;
    }

    function create_and_enable_apache_conf_files($port = NULL) {
      if(!parent::create_and_enable_apache_conf_files($port)) return false;

      // This function sometimes gets called with $port = https in addition
      // to the normal call without specifying a port. We don't need to run
      // more than once, so we only operate when port is null.
      if ($port == 'https') {
        // Nothing to do.
        return TRUE;
      }
      if($this->get_web_conf_max_processes() == '0') {
        // No PHP support.
        if(!$this->delete_fpm_config()) return FALSE;
        if(!$this->delete_php_ini_symlink()) return FALSE;
        // We leave behind any php ini file to avoid deleting user data.
      }
      else {
        if(!$this->create_fpm_config()) return false;
        if(!$this->create_php_ini()) return false;
        if(!$this->create_php_ini_symlink()) return false;
      }
      return true;
    }

    /**
     * Override reload apache so we reload php too.
     *
     */
    function reload_apache() {
      if (!parent::reload_apache()) return FALSE;
      if (!$this->reload_fpm()) return FALSE;
      return TRUE;
    }

    /**
     * After making any changes to the web directories,
     * reload fpm to ensure the changes to into effect.
     */
    function reload_fpm() {
      $cmd = "/bin/systemctl";
      $args = array('reload', $this->get_php_info('service_name') . '.service');  
      $exit_status = red_fork_exec_wait($cmd, $args);
      if ($exit_status != 0) {
        $error = "Failed to reload php-fpm.";
        $this->set_error($error,'system','hard');
        return FALSE;
      }

      // php-fpm can't handle being reloaded quickly, (it seems to crash if
      // you do it), so we have to sleepto ensure it has been properly
      // reloaded. This is only relevant if we are handling multiple web
      // configs on the same run.
      sleep(2);

      return TRUE;
    }

    /**
     * There's no way to create a php.ini file on a per-pool basis with
     * fpm. Boo. But, we can use a .user.ini file. See:
     * https://secure.php.net/manual/en/configuration.file.per-user.php
     **/
    function create_php_ini_symlink() {
      $dir = $this->get_php_ini_dir();
      $target = $dir . '/php.ini';
      $name = $web_path = $this->get_site_dir() . '/web/.user.ini';
      if(file_exists($name)) {
        // We assume it's either the link or the user knows what they are doing.
        return true;
      }
      symlink($target, $name);
      $user = $this->get_web_conf_login();
      $group = $this->get_hosting_order_unix_group_name();
      if(!red_chown_symlink($name, $user)) return false;
      if(!red_chgrp_symlink($name, $group)) return false;
      return true;
    }

    function delete_php_ini_symlink() {
      $path = $this->get_site_dir() . '/web/.user.ini';
      // Only delete if it is a symlink - otherwise we might be deleting
      // user data.
      if(file_exists($path) && is_link($path)) {
        if(!unlink($path)) {
          $message = "Unable to delete php ini file. Trying $path";
          $this->set_error($message,'system');
          return FALSE;
        }
      }
      return TRUE;
    }

    function get_socket_path() {
      $version = $this->chosen_php_version;
      return $this->get_php_info('run_path') . '/' . $version . '-site' . $this->get_item_id() . '.sock';
    }

    /**
     * Override the parent so we also kill the old php config files.
     *
     */
    function remove_old_sites_paths() {
      if (!parent::remove_old_sites_paths()) return FALSE;
      if (!$this->delete_fpm_config()) return FALSE;
      return TRUE;
    }

    function delete_fpm_config($versions = array()) {
      // If not set, delete the fpm config file from all available versions.
      if (empty($versions)) {
        $versions = $this->php_versions;
      }
      // Ensure we get the old pool name out.
      $old_pool = $this->get_hosting_order_identifier();
      $new_pool = 'site' . $this->get_item_id();
      foreach($versions as $version) {
        $paths = array(
          $this->get_php_info('etc_path', $version) . '/fpm/pool.d/' . $old_pool . '.conf',
          $this->get_php_info('etc_path', $version) . '/fpm/pool.d/' . $new_pool . '.conf',
        );
        foreach ($paths as $path) {
          if(file_exists($path)) {
            if(!unlink($path)) {
              $message = "Unable to delete fpm config file. Trying $path";
              $this->set_error($message,'system');
              return FALSE;
            }
          }
        }
      }
      return true;
    }

    function create_fpm_config() {
      $pool = 'site' . $this->get_item_id();
      $path = $this->get_php_info('etc_path') . '/fpm/pool.d/' . $pool . '.conf';
      $contents = $this->get_fpm_config_contents();
      if(!file_put_contents($path,$contents)) {
        $message = "Unable to create fpm config file. Trying $path";
        $this->set_error($message,'system');
        return false;
      }
      // Now, iterate over all other php versions - if we are upgrading
      // to a different version, we don't want the old config file
      // sitting around.
      $delete_versions = array();
      foreach($this->php_versions as $version) {
        if ($version != $this->chosen_php_version) {
	        $delete_versions[] = $version;
	      }
      }
      if (count($delete_versions) > 0) {
        if (!$this->delete_fpm_config($delete_versions)) {
          return FALSE;
        }
      }
      return true;
    }

    function get_fpm_config_contents() {
      $pool = 'site' . $this->get_item_id();
      $user = $this->get_web_conf_execute_as_user();
      $group = $this->get_hosting_order_unix_group_name();
      $socket_path = $this->get_socket_path();
      $max_processes = $this->get_web_conf_max_processes();
      $max_memory = $this->get_web_conf_max_memory();
      $contents = "[$pool]\n" .
        "user = $user\n".
        "group = $group\n".
        "listen = $socket_path\n".
        "listen.owner = www-data\n".
        "listen.group = www-data\n".
        "pm = ondemand\n".
        "pm.max_children = $max_processes\n".
        "php_admin_value[memory_limit] = ${max_memory}M\n".
        "pm.max_requests = 500\n";
      return $contents;
    }
  }
}
