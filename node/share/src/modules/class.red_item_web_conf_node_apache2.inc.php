<?php
if(!class_exists('red_item_web_conf_node_apache2')) {
  class red_item_web_conf_node_apache2 extends red_item_web_conf {

    // If you want to extend this class in a way the requires an
    // addition to the config file, then add a value to this
    // array in your constructor
    var $_config_variables = array('apache_cmd',
                             'sites_enabled_dir',
                             'sites_available_dir',
                             'path_to_default_index_template',
                             'reload_apache_cmd');
    var $_site_dir_template;
    var $_sites_enabled_dir;
    var $_sites_available_dir;
    var $_path_to_default_index_template;
    var $_apache_cmd;
    var $_reload_apache_cmd;
    var $_apache_server_wide_conf_file = '/etc/apache2/apache2.conf';

    /*
     * MAIN FUNCTIONS
     */

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;

      $conf_file = $this->_conf_path .
        '/red_web_conf.apache2.conf';
      if (!$this->_set_config_values($conf_file)) {
        return FALSE;
      }
    }

    function node_sanity_check() {
      if(!is_writable($this->_sites_enabled_dir)) {
        $message = 'Apache sites enabled directory not writable. '.
          'Trying: ' . $this->_sites_enabled_dir;
        $this->set_error($message,'system');
        return false;
      }
      if(!is_writable($this->_sites_available_dir)) {
        $message = 'Apache sites available directory not writable. '.
          'Trying: ' . $this->_sites_available_dir();
        $this->set_error($message,'system');
        return false;
      }
      if(!is_writable($this->_sites_enabled_dir)) {
        $message = 'Apache sites enabled directory not writable. '.
          'Trying: ' . $this->_sites_enabled_dir();
        $this->set_error($message,'system');
        return false;
      }

      // Probably has arguments, only check the cmd itself
      $cmd_parts = explode(' ',$this->_reload_apache_cmd);
      $cmd = $cmd_parts[0];
      if(!file_exists($cmd)) {
        $message = 'Apache reload_apache command does not exist. '.
          'Trying: ' . $cmd;
        $this->set_error($message,'system');
        return false;
      }
      // Probably has arguments, only check the cmd itself
      $cmd_parts = explode(' ',$this->_apache_cmd);
      $cmd = $cmd_parts[0];
      if(!file_exists($cmd)) {
        $message = 'Apache check syntax command does not exist. '.
          'Trying: ' . $cmd;
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    // either delete, insert, restore, or update is always called
    function delete() {
      // set delete flag - so we run less rigorous sanity checking
      if(!$this->delete_apache_files()) return false;
      if(!$this->delete_web_directories()) return false;
      if(!$this->reload_apache()) return false;
      if(!$this->purge_letsencrypt_files()) return false;
      return true;

    }

    function disable() {
      if(!$this->disable_site()) return false;
      if(!$this->reload_apache()) return false;
      if(!$this->purge_letsencrypt_files()) return false;
      return true;
    }

    function insert() {
      // Make sure any included ssl key files and crts exist and are not
      // password protected. This is only relevant for sites providing
      // their own cert and keys.
      if(!$this->ssl_key_and_crt_file_exists()) return false;
      if(!$this->ssl_key_file_not_password_protected()) return false;

      // Create web directories (if they don't exist) and, only if they
      // don't exist, chowns them.
      if(!$this->create_web_directories()) return false;
      
      if(!$this->create_and_enable_apache_conf_files()) return FALSE;

      $tls_key = $this->get_web_conf_tls_key();
      if ($this->get_web_conf_tls() == 1 && empty($tls_key)) {
        // If this is a new site or a site with expired certificates,
        // we will present a site on port 443 that will give tls errors.
        // In our tests, letsencrypt responds to those errors by attempting
        // to validate via port 80.
        // Reload apache so our newly generated config files are active.
        if(!$this->reload_apache()) return false;

        // Now generate files.
        if(!$this->generate_letsencrypt_files()) return false;

        // Regenerate the web configuration - now that we have a letsencrypt
        // certificate, we have to regenerate to ensure they are included.
        if (!$this->create_and_enable_apache_conf_files('https')) return FALSE;
      }
      if ($this->get_web_conf_tls() == 0) {
        // Ensure any left over ssl files or letsencrypt files are cleaned up.
        if(!$this->purge_letsencrypt_files()) return false;
      }

      if(!$this->reload_apache()) return false;
      return true;
    }

    /**
     * Create an enable apache configuration files.
     *
     * But don't reload apache (yet).
     */
    function create_and_enable_apache_conf_files($port = NULL) {
      if (is_null($port)) {
        $ports = $this->get_ports();
      }
      else {
        $ports = array($port);
      }
      foreach($ports as $port) {
        // Create the http configuration.
        $tmp = tempnam(sys_get_temp_dir(), 'red-apache-conf');
        if(!$this->create_apache_conf_file($port, $tmp)) {
          unlink($tmp);
          return false;
        }
        if(!$this->check_apache_syntax($tmp)) {
          unlink($tmp);
          return false;
        }
        if (!$this->move_apache_conf_file_into_place($tmp, $port)) {
          unlink($tmp);
          return false;
        }
        if(!$this->enable_site($port)) return false;
      }
      if ($this->get_web_conf_tls() == 0) {
        // Ensure we don't leave behind a .ssl.conf file.
        $available = $this->get_apache_sites_path('available', 'https');
        $enabled = $this->get_apache_sites_path('enabled', 'https');
        if (file_exists($available)) {
          if (!unlink($available)) {
            $message = 'Failed to unlink un-needed apache ssl configuration file from available directory.';
            $this->set_error($message,'system', 'soft');
            return FALSE;
          }
        }
        // file_exists fails on symlinks if they point to a file that doesn't exist. We want
        // to kill this symlink no matter what.
        if (is_link($enabled)) {
          if (!unlink($enabled)) {
            $message = 'Failed to unlink un-needed apache ssl configuration file from enabled directory.';
            $this->set_error($message,'system', 'soft');
            return FALSE;
          }
        }
      }
      return true;
    }

    /**
     * Regnerate web configuration.
     *
     * Sometimes (when we change the backend) we have to
     * regenerate all web configurations so they include the
     * right directives for the given backend. However, if we
     * reload the web server before all of them are regenerated
     * we may get apache errors. So - this function will just
     * regenerate the web configuration without testing them.
     */
    function regenerate() {
      if(!$this->node_sanity_check()) return false;
      if(!$this->remove_old_sites_paths()) return false;
      if(!$this->create_web_directories()) return false;
      if(!$this->ssl_key_and_crt_file_exists()) return false;
      if(!$this->ssl_key_file_not_password_protected()) return false;
      if (!$this->create_and_enable_apache_conf_files()) return false;
      return true;
    }

    function update() {
      // We do exactly the same thing as insert, however, we first
      // disable the apache configuration files, otherwise the apache
      // syntax check will always fail because it will detect duplicate
      // directives.
      if (!$this->disable_site()) return false;
      if (!$this->insert()) return false;
      return true;
    }

    function restore() {
      // if sites available file exists, create it, otherwise, update it
      if(!file_exists($this->get_apache_sites_path('available', 'http'))) {
        return $this->insert();
      }
      else {
        return $this->update();
      }
    }

    /*
     * HELPER FUNCTIONS
     */

    /**
     * Refactoring function.
     *
     * We are switching from naming apache conf files after the hosting order
     * identifier to naming them after the item id. This function ensures
     * that the old style files are deleted.
     */
    function remove_old_sites_paths() {
      $types = array('available', 'enabled');
      $ports = array('http', 'https');
      foreach($types as $type) {
        foreach($ports as $port) {
          $path = $this->get_old_apache_sites_path($type, $port);
          if (is_link($path)) {
            if (!unlink($path)) {
              $msg = "Failed to remove old style path ($path)";
              $this->set_error($msg, 'system', 'soft');
              return FALSE;
            }
          }
        }
      }
      return TRUE;
    }

    /**
     * When deleting or removing https remove letsencrypt.
     */
    function purge_letsencrypt_files() {
      $cert_name = 'site' . $this->get_item_id();
      $base = '/etc/letsencrypt';
      $renewal = "${base}/renewal/${cert_name}.conf";
      $live = "${base}/live/${cert_name}";
      $archive = "${base}/archive/${cert_name}";

      if (file_exists($renewal)) {
        if (!unlink($renewal)) {
          $message = 'Cannot delete renewal LE configuration.';
          $this->set_error($message,'system', 'soft');
          return FALSE;
        }
      }
      if (file_exists($live)) {
        if (!red_delete_directory_recursively($live)) {
          $message = 'Cannot delete live LE dir.';
          $this->set_error($message,'system', 'soft');
          return FALSE;
        }
      }
      if (file_exists($archive)) {
        if (!red_delete_directory_recursively($archive)) {
          $message = 'Cannot delete archive LE dir.';
          $this->set_error($message,'system', 'soft');
          return FALSE;
        }
      }
      return TRUE;
    }

    function generate_letsencrypt_files() {
      // Use http - if we are inserting, we will only have an
      // http config. In any event, it only provides the server names
      // and path to web config.
      $file = $this->get_apache_sites_path('enabled', 'http');
      if(!file_exists($file)) {
        $message = 'Cannot find apache configuration file when running mf-certbot.';
        $this->set_error($message,'system');
        return FALSE;
      }
      $cert_name = 'site' . $this->get_item_id();
      $env = array('MFCB_CERT_NAME' => $cert_name);
      // First run with --dry-run to see if we get any errors.
      $env['MFCB_DRY_RUN'] = 1;
      $exit_status = red_fork_exec_wait('/usr/local/sbin/mf-certbot', array($file), $env);
      if($exit_status !== 0) {
        $msg = "Generating your https certificate failed. Please ensure that ".
          "all the domains listed for this configuration are properly assigned ".
          "the IP address for the server. You can always add more domains at a ".
          "later date. Or you can change your site to http only for now.";
        $this->set_error($msg, 'system', 'soft');

        // Ensure we don't leave behind a broken configuration
        if (!$this->cleanup_after_failed_letsencrypt()) return FALSE;
        return FALSE;
      }
      // Now run without --dry-run.
      unset($env['MFCB_DRY_RUN']);
      $exit_status = red_fork_exec_wait('/usr/local/sbin/mf-certbot', array($file), $env);
      if($exit_status !== 0) {
        // We still return a soft error - we want the user to be able to adjust/recover.
        $this->set_error("Running mf-certbot returned an error (exit status: $exit_status). You can always change your site to http only for now.", 'system', 'soft');
        if (!$this->cleanup_after_failed_letsencrypt()) return FALSE;
        return FALSE;
      }
      return TRUE;
    }

    // If letsencrypt fails, clean up the apache configurations that aren't
    // working.
    function cleanup_after_failed_letsencrypt() {
      // Ensure we remove the https apache configuration files.
      $types = array('available','enabled');
      foreach($types as $type) {
        $file = $this->get_apache_sites_path($type, 'https');
        if ($type == 'enabled') {
          $exists = is_link($file);
        }
        else {
          $exists = file_exists($file);
        }
        if($exists) {
          if(!unlink($file)) {
            $message = "Unable to delete sites $type file. Trying: " . $file;
            $this->set_error($message,'system');
            return false;
          }
        }
      }
      if(!$this->reload_apache()) return false;
      return TRUE;
    }

    // Getters
    function get_web_related_directory_names() {
      // These are the names of the directories created in the
      // site directory when web is enabled
      return array('web','logs','cgi-bin','include');
    }

    function get_apache_sites_path($type, $port) {
      $path = '';
      if($type == 'enabled') {
        $path = $this->_sites_enabled_dir . '/';
      }
      elseif($type == 'available') {
        $path = $this->_sites_available_dir . '/';
      }

      $path .= 'site' . $this->get_item_id();
      if($port == 'https') $path .= '.ssl';

      $path .= '.conf';
      return $path;
    }

    function get_old_apache_sites_path($type, $port) {
      $path = '';
      if($type == 'enabled') {
        $path = $this->_sites_enabled_dir . '/';
      }
      elseif($type == 'available') {
        $path = $this->_sites_available_dir . '/';
      }

      $path .= $this->get_hosting_order_identifier();
      if($port == 'https') $path .= '.ssl';

      $path .= '.conf';
      return $path;
    }

    function get_apache_conf_file_shared_content() {
      $domain_names = explode(' ', $this->get_web_conf_domain_names());
      $server_name = array_shift($domain_names); 
      $server_aliases = trim(implode(' ', $domain_names));
      $base = $this->get_site_dir();

      $content = "\tServerName $server_name\n";
      if (!empty($server_aliases)) {
        $content .= "\tServerAlias $server_aliases\n";
      }
      $root = ltrim($this->get_web_conf_document_root(), '/');
      $content .= "\tDocumentRoot ${base}/web/${root}\n";
      $logging = $this->get_web_conf_logging();
      $error_log = '/dev/null';
      $web_log = '/dev/null';
      if ($logging > 0) {
        $error_log = "${base}/logs/error.log";
      }
      if ($logging > 1) {
        $web_log = "${base}/logs/web.log";
      }
      $content .= "\tErrorLog ${error_log}\n";
      $content .= "\tCustomLog ${web_log} combined\n";
      $content .= "\t<Directory " . $this->get_site_dir() . '/web' . '>' . "\n";
      $content .= "\t\tOptions FollowSymLinks\n";
      $content .= "\t\tAllowOverride  All\n";
      $content .= "\t\tRequire all granted\n";
      $content .= "\t\tRewriteEngine On\n";
      $content .= "\t\tRewriteCond %{REQUEST_FILENAME} !-f\n";
      $content .= "\t\tRewriteRule ^robots\.txt$ /default-robots.txt [PT]\n";
      $content .= "\t</Directory>\n";
      $content .= "\t<Location />\n";
      $content .= "\t\tOptions +IncludesNoExec -ExecCGI\n";
      $content .= "\t</Location>\n";
      return $content;
    }

    function get_apache_conf_file_content($port) {
      $port_number = 80;
      if ($port == 'https') $port_number = 443;
      $content = "<VirtualHost *:$port_number>\n";
      $content .= $this->get_apache_conf_file_shared_content();
      if($this->get_web_conf_cgi()) {
        $content .= "\t<Directory " . $this->get_site_dir() . '/cgi-bin' . '>' . "\n";
        $content .= "\t\tOptions FollowSymLinks ExecCGI\n";
        $content .= "\t\tAllowOverride  All\n";
        $content .= "\t\tRequire all granted\n";
        $content .= "\t</Directory>\n";
        $content .= "\tScriptAlias /cgi-bin " . $this->get_site_dir() . "/cgi-bin\n";
        $group_name = $this->get_hosting_order_unix_group_name();
        $user = $this->get_web_conf_execute_as_user();
        $content .= "\tSuexecUserGroup $user $group_name\n";
      }
      $settings = explode("\r\n",$this->get_web_conf_settings());
      foreach($settings as $line) {
        $content .= "\t$line\n";
      }
      $content .= $this->get_tls_settings();
      $content .= $this->get_apache_conf_file_children_additions();
      $content .= '</VirtualHost>'."\n";
      return $content;
    }

    function get_tls_settings() {
      if ($this->get_web_conf_tls() != 1) {
        // Return empty string if tls is not configured for this site.
        return '';
      }

      // Check if the user wants to supply their own certs.
      $tls_key = $this->get_web_conf_tls_key();
      $tls_cert = $this->get_web_conf_tls_cert();
      
      if (empty($tls_key)) {
        // No user supplied certs, it's lets encrypt.
        $le_base = '/etc/letsencrypt/live/';
        $domain_names = explode(' ', $this->get_web_conf_domain_names());

        // This is the old location.
        $le_cert_name_legacy = array_shift($domain_names); 
        $le_cert_dir_legacy = $le_base . $le_cert_name_legacy; 
        // This is the new location.
        $le_cert_name_new = 'site' . $this->get_item_id();
        $le_cert_dir_new = $le_base . $le_cert_name_new;

        // Get tricky. We want to move away from the legacy system to the new
        // system without breaking the legacy system.
        // First check if the new style is available.
        $le_cert_dir = NULL;
        if (is_dir($le_cert_dir_new)) {
          // Hooray. Use the new system.
          $le_cert_dir = $le_cert_dir_new;

          // And clean up the old.
          $this->delete_legacy_letsencrypt($le_cert_name_legacy);
        }
        elseif (is_dir($le_cert_dir_legacy)) {
          // Continue using for now.
          $le_cert_dir = $le_cert_dir_legacy;
        }
        if ($le_cert_dir) {
          $tls_key = "${le_cert_dir}/privkey.pem";
          $tls_cert = "${le_cert_dir}/fullchain.pem";
        }
      }

      // Ensure both cert and key exist.
      if(file_exists($tls_key) && file_exists($tls_cert)) {
        $ret = "\tSSLEngine On\n";
        $ret .= "\tSSLCertificateKeyFile ${tls_key}\n";
        $ret .= "\tSSLCertificateFile ${tls_cert}\n";
        return $ret;
      }
      return NULL;
    }

    function delete_legacy_letsencrypt($cert_name) {
      $cert_name = trim($cert_name);
      if (empty($cert_name)) {
        return;
      }
      $base = "/etc/letsencrypt/";
      $renewal = $base . "renewal/${cert_name}.conf";

      // If there is no renewal file, something may be wrong, so let's
      // bail before we start deleting directories recursively.
      if (!is_file($renewal)) return;

      // Fire away.
      unlink($renewal);

      $live = $base . "live/${cert_name}";
      $archive = $base . "archive/${cert_name}";
      red_delete_directory_recursively($live);
      red_delete_directory_recursively($archive);
    }

    function get_apache_conf_file_children_additions() {
      // override in children class
      return '';

    }

    function get_php_ini_dir() {
      return $this->get_site_dir() . "/include/php5";
    }

    function get_bin_dir() {
      return $this->get_site_dir() . '/bin';
    }

    // not used by core mod_php class, but used by both
    // suexec/fcgid and suphp
    function create_php_ini() {
      $dir = $this->get_php_ini_dir();
      $file = $dir . '/php.ini';

      // The file might exist - if we are creating an ssl version of an
      // existing site.
      if(file_exists($file)) return true;

      $contents = "; place your own custom php.ini settings here\n" .
        "; only add settings you wish to change from the server defaults\n";
      if(!file_put_contents($file,$contents)) {
        $message = "Unable to create php ini file. Trying $file";
        $this->set_error($message,'system');
        return false;
      }

      $unix_user_name = $this->get_web_conf_login();
      // the enclosing directory won't by default be properly
      // chown'ed, so do it here
      if(!red_chown($dir,$unix_user_name)) {
        $this->set_error("Failed to chown $file.",'system');
        return false;
      }
      if(!red_chown($file,$unix_user_name)) {
        $this->set_error("Failed to chown $file.",'system');
        return false;
      }
      if(!$this->set_group_permission($file)) {
        $this->set_error("Failed to chgrp $file.",'system');
        return false;
      }
      return true;
    }

    // Action functions
    function delete_apache_files() {
      // Delete both sites available file and sites enabled file
      $types = array('available','enabled');
      $ports = $this->get_ports();
      foreach($types as $type) {
        foreach($ports as $port) {
          $file = $this->get_apache_sites_path($type, $port);
          if ($type == 'enabled') {
            $exists = is_link($file);
          }
          else {
            $exists = file_exists($file);
          }
          if($exists) {
            if(!unlink($file)) {
              $message = "Unable to delete sites $type file. Trying: " . $file;
              $this->set_error($message,'system');
              return false;
            }
          }
        }
      }
      return true;
    }

    function check_apache_syntax($file = false) {
      // If $file is not passed, test the configuration files
      // currently in place. If $file is passed, also test
      // the file passed.
      $cmd = $this->_apache_cmd;
      // for testing
      $args = "-t";
      if($file) {
        // Only check the relevant file. However - we need
        // to include the server wide config file so that
        // we can read in any module conf file that might be
        // referenced in this particular file (and would
        // otherwise fail the syntax check).
        $args .= " -f $file -C 'Include " .
          $this->_apache_server_wide_conf_file . "'";

      }

      // redirect standard error to standard output
      $args .= " 2>&1";
      exec("$cmd $args",$output,$return_value);
      if($return_value != 0) {
        // Failed syntax check - this is a soft error - the user should
        // be allowed to fix it.
        $output = $this->get_htmlentities(implode(' ',$output));
        $error = "Failed apache check syntax command! There is probably ".
          "a typo or other mistake in your apache settings. Output: ".
          "$output.";
        $this->set_error($error,'system','soft');
        return false;
      }
      return true;
    }


    function create_apache_conf_file($port, $target) {
      $tls = $this->get_web_conf_tls();
      $tls_redirect = $this->get_web_conf_tls_redirect();
      if ($port == 'http' && $tls == 1 && $tls_redirect == 1) {
        $content = $this->get_http_redirect_content();
      }
      else {
        $content = $this->get_apache_conf_file_content($port);
      }
      if(!red_write_to_file($target, $content)) {
        $message = "Failed to create apache conf file. Trying: ".
          $target;
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    function get_http_redirect_content() {
      $content = "<VirtualHost *:80>\n";
      
      $content .= $this->get_apache_conf_file_shared_content();
      $content .= "\tRewriteEngine On\n";
      // Allow letsencrypt acme challenge to access via http or else
      // adding new domains to existing auto sites will fail because
      // letsencrypt authorization will get redirected to https and
      // the https certificate will not be valid.
      $content .= "\tRewriteCond %{REQUEST_URI} !/.well-known/acme-challenge/.*\n";
      $content .= "\tRewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}\n";
      $content .= "</VirtualHost>\n";
      return $content;
    }

    function move_apache_conf_file_into_place($source, $port) {
      if(!file_exists($source)) {
        $message = "Unable to move apache conf file: $source. It doesn't exist.";
        $this->set_error($message);
        return false;
      }
      $target = $this->get_apache_sites_path('available', $port);
      if(!rename($source,$target)) {
        $message = "Failed to move apache conf file into place. Trying: ".
          "$source -> $target";
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    function delete_web_directories() {
      // delete all the web related site directories
      $names = $this->get_web_related_directory_names();
      $site_dir = $this->get_site_dir();
      $site_dir_symlink = NULL;
      $mountpoint = $this->get_web_conf_mountpoint();

      if ($mountpoint) {
        $site_dir_symlink = rtrim($site_dir, "/");
        $site_dir = "/media/${mountpoint}/members/sites/site" . $this->get_item_id();
      }
      foreach($names as $name) {
        $full_path = $site_dir . '/' . $name;
        if(!red_delete_directory_recursively($full_path)) {
          $message = "Unable to delete $full_path";
          $this->set_error($message,'system');
          return false;
        }
      }
      // Attempt to rmdir it. The directory might not be empty if we are using old style
      // path which would include the users directory, in which case this will fail
      // and we ignore the error.
      @rmdir($site_dir);

      if ($site_dir_symlink) {
        // Also, clean up the symlink.
        if (!unlink($site_dir_symlink)) {
          $this->set_error("Failed to remove site dir symlink.", 'system');
          return FALSE;
        }
      }
      return TRUE;
    }

    function create_web_directories() {
      // Create all the web related site directories
      $names = $this->get_web_related_directory_names();
      $site_dir = $this->get_site_dir();
      $site_dir_symlink = NULL;
      $mountpoint = $this->get_web_conf_mountpoint();

      if ($mountpoint) {
        // A mountpoint means we actually store the site in /media/<mountpoint> and
        // create a symlink in /home/members.
        $site_dir_symlink = $site_dir;
        // Ensure the enclosing directory for the symlink exists.
        $site_dir_symlink_parts = explode('/', rtrim($site_dir_symlink, '/'));
        array_pop($site_dir_symlink_parts);
        $site_dir_symlink_parent = implode('/', $site_dir_symlink_parts);
        if(!red_create_directory_recursively($site_dir_symlink_parent))  {
          $this->set_error("Failed to create symlink parent.", 'system');
          return FALSE;
        }
        $site_dir = "/media/${mountpoint}/members/sites/site" . $this->get_item_id();
      }

      // Create/make sure site dir is there
      $group_writable = false;
      if(!red_create_directory_recursively($site_dir, $group_writable))  {
        $message = "Failed to create the site directory. Trying: $site_dir";
        $this->set_error($message,'system');
        return false;
      }

      // Also make sure symlink is there if we are using a mountpoint.
      if ($mountpoint) {
        if (!symlink($site_dir, $site_dir_symlink)) {
          $this->set_error("Failed to create site dir symlink.", 'system');
          return FALSE;
        }
      }
     
      foreach($names as $name) {
        $full_path = $site_dir . '/' . $name;
        if(!file_exists($full_path)) {
          $group_writable = false;
          // make include, and web writable by the group, they will remain owned by root.
          if($name == 'include' || $name == 'web') {
            $group_writable = true;
          }
            
          if(!red_create_directory_recursively($full_path,$group_writable)) {
            $message = "Unable to create ".
              $full_path;
            $this->set_error($message,'system');
            return false;
          }
          if ($name == 'cgi-bin') {
            $user = $this->get_web_conf_login();
            if (!red_chown($full_path, $user)) {
             $message = "Unable to chown ".
                $full_path;
              $this->set_error($message,'system');
              return false;
            }
          }

          // make the log file non-world readable
          elseif($name == 'logs') {
            if(!chmod($full_path,0750)) {
              $message = "Unable to chmod the web directory. Trying ".
                $full_path;
              $this->set_error($message,'system');
              return false;
            }
          }
          // chgrp the folder
          if(!$this->set_group_permission($full_path)) {
            $message = "Unable to chown/chgrp the web directory. Trying ".
              $full_path;
            $this->set_error($message,'system');
            return false;
          }

          // only create default index file if the web directory did
          // not exist
          if($name == 'web') {
            if(!$this->create_default_index_file()) return false;
          }
        }

        // Always ensure web symlink is present in user directory.
        if($name == 'web') {
          if(!$this->create_user_symlink()) return false;
        }
      }
      return true;
    }

    function set_group_permission($path) {
      // Use group_id to avoid problems if the user
      // has not yet been created on the server
      $unix_group_id = $this->get_hosting_order_unix_group_id();

      if(!red_chgrp($path,$unix_group_id)) {
        $this->set_error("Failed to chgrp $path.",'system');
        return false;
      }
      return true;
    }
    function create_user_symlink() {
      // Get target path.
      $site_dir = $this->get_site_dir();
      $target = $site_dir . '/web';

      // Get link path.
      $user = $this->get_web_conf_login();
      $home_dir = $this->get_user_home_dir($user);
      if (!$home_dir) {
        $message = "Failed to get user home dir when symlinking web folder.";
        $this->set_error($message,'system', 'soft');
        return FALSE;
      }

      $link = "${home_dir}/web";

      // The file might exist.
      if(file_exists($link)) {
        return TRUE;
      }

      if (!symlink($target, $link)) {
        $message = "Failed to symlink from user home directory.";
        $this->set_error($message,'system', 'soft');
        return FALSE;
      }

      // Update permissions.
      $unix_group_name = $this->get_hosting_order_unix_group_name();
      if(!red_chown_symlink($link, $user)) {
        $this->set_error("Failed to chown $link.",'system');
        return false;
      }
      if(!red_chgrp_symlink($link, $unix_group_name)) {
        $this->set_error("Failed to chgrp $link.",'system');
        return false;
      }
      return true;
    }

    function get_user_home_dir($user) {
      // Hackey, but there doesn't seem to be a getent in php.
      $file = file("/etc/passwd");
      foreach($file as $line) {
        if (preg_match('/^' . $user . ':x:[0-9]+:[0-9]+:[^:]*:([^:]+):/', $line, $matches)) {
          $homedir = $matches[1];
          if (is_dir($homedir)) {
            return $homedir;
          }
        }
      }
      return NULL;
    }

    function create_default_index_file() {
      $content = file_get_contents($this->_path_to_default_index_template);
      $hosting_order_identifier = $this->get_hosting_order_identifier();
      $content = str_replace('{hosting_order_identifier}',$hosting_order_identifier,$content);

      $site_dir = $this->get_site_dir();
      $file = $site_dir . '/web/index.html';

      // The file might exist.
      if(file_exists($file)) return true;

      if(!red_append_to_file($file,$content)) {
        $message = "Unable to create default index page. Trying ".
          $file;
        $this->set_error($message,'system');
        return false;
      }

      // fix permissions
      if(!chmod($file,0664)) {
        $message = "Unable to chmod the default index file. Trying ".
          $file;
        $this->set_error($message,'system');
        return false;
      }

      $unix_user_name = $this->get_web_conf_login();
      $unix_group_name = $this->get_hosting_order_unix_group_name();
      if(!red_chown($file,$unix_user_name)) {
        $this->set_error("Failed to chown $file.",'system');
        return false;
      }
      if(!red_chgrp($file,$unix_group_name)) {
        $this->set_error("Failed to chgrp $file.",'system');
        return false;
      }
      return true;
    }

    // Return an array with http or http and https in it.
    function get_ports() {
      $tls = $this->get_web_conf_tls();
      // There is always an http version.
      $ports = array('http');
      if ($tls == 1) {
        // Add https version as well.
        $ports[] = 'https';
      }
      return $ports;

    }
    function disable_site($port = NULL) {
      if (is_null($port)) {
        $ports = $this->get_ports();
      }
      else {
        $ports = array($port);
      }
      foreach($ports as $port) {
        // delete the sites enabled file
        $target = $this->get_apache_sites_path('enabled', $port);
        // We should only need is_link, but for legacy resaons we seem
        // to have some real files in some places and we want to be sure
        // we get rid of any file with this name here. Also file_exists
        // won't work because it returns FALSE if the target is missing
        // and we want to blow away any file whether or not the target
        // exists.
        if(is_link($target) || is_file($target)) {
          if(!unlink($target)) {
            $message = "Failed to disabled the site.";
            $this->set_error($message,'system');
            return false;
          }
        }
      }
      return true;
    }

    function enable_site($port = NULL) {
      if (is_null($port)) {
        $ports = $this->get_ports();
      }
      else {
        $ports = array($port);
      }
      foreach($ports as $port) {
        $target = $this->get_apache_sites_path('enabled', $port);
        $source = $this->get_apache_sites_path('available', $port);
        if(is_link($target) && readlink($target) == $source) {
          // This is a noop - our link exists and is pointing to the right place.
          continue;
        }
        // If a file exists, then delete it. We have some legacy cruft with old enabled
        // files that are real files, not symlinks. NOTE: file_exists returns FALSE if the
        // file is a symlink and the symlink target does not exist. So, we check for is_link
        // to ensure we delete whatever is here.
        if (file_exists($target) || is_link($target)) {
          if (!unlink($target)) {
            $this->set_error("Unable to remove pre-existing enabled file: '$target'.",'system');
            return false;
          }
        }
        // Now create the symlink.
        if(!symlink($source,$target)) {
          $this->set_error("Unable to create symlink source '$source' to target '$target'.",'system');
          return false;
        }
      }
      return true;
    }

    function reload_apache() {
      // First check syntax and sanity
      if(!$this->check_apache_syntax() || !$this->check_apache_sanity())  {
        return false;
      }

      // attempt reload
      $args = explode(' ', $this->_reload_apache_cmd);
      $cmd = array_shift($args);

      $return_value = red_fork_exec_wait($cmd, $args);
      if($return_value != 0) {
        // Failure! This is potentially very bad. Apache might be shutdown
        $this->set_error("Failed apache reload command despite passing syntax check!",'system');
        return false;
      }
      return true;
    }

    // Place any additional checks we should run that can only
    // be run after the other steps have completed and just before
    // reloading apache.
    function check_apache_sanity() {
      if(!$this->_delete && !file_exists($this->get_site_dir() . '/logs') && $this->get_web_conf_logging() > 0) {
        $this->set_error("Your logs directory does not exist. Please create a directory in " . 
          $this->get_site_dir() . " called 'logs'. Your web site is being disabled until this change is made.",
          'system','soft');
        $this->disable_site();
        return false;
      }
      return true;
    }
    
    function ssl_key_and_crt_file_exists() {
      $tls_key_file = trim($this->get_web_conf_tls_key());
      $tls_cert_file = trim($this->get_web_conf_tls_cert());
      foreach(array($tls_key_file, $tls_cert_file) as $file) {
        if(!empty($file) && !file_exists($file)) {
          $this->set_error("The SSL Certificate file you specified does ".
            "not exist. Trying: '" . $parts[1] ."'",'system','soft');
          return false;
        }
      }
      return true;
    }

    function ssl_key_file_not_password_protected() {
      $file = trim($this->get_web_conf_tls_key());
      if (empty($file)) {
        return TRUE;
      }
      $command = "/usr/bin/openssl";
      $args = array('rsa', '-passin', 'pass:', '-noout', '-in', $file);
      $return = red_fork_exec_wait($command, $args);
      // should return 0 on success, 1 on error
      if($return == 1) {
        $this->set_error("The SSL Key file you specified appears ".
          "to be password protected.",'system','soft');
        return false;
      }
      return true;
    }

  }
}


?>
