<?php
if(!class_exists('red_item_server_access_node_ssh')) {
  class red_item_server_access_node_ssh extends red_item_server_access {
    // If you want to extend this class in a way the requires an
    // addition to the config file, then add a value to this
    // array in your constructor
    var $_config_variables = array('home_dir_template',
                             'addgroup_cmd',
                             'deluser_cmd',
                             'ssh_group',
                             'usermod_cmd',
                                  );
    var $_home_dir_template;
    var $_passwd_file = '/etc/passwd';
    var $_addgroup_cmd;
    var $_deluser_cmd;
    var $_usermod_cmd;
    var $_ssh_group;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;

      $conf_file = $construction_options['conf_path'] . 
        '/red_server_access.ssh.conf';
      if(!$this->_set_config_values($conf_file)) {
        return False;
      }
    }

    function delete() {
      if(!$this->remove_user_from_ssh_group()) return false;
      return true;
    }

    function disable() {
      return $this->delete();
    }

    function insert() {
      // See if the user exists 
      if(!$this->unix_account_exists() && posix_getuid() == 0) {
        $message = 'I was asked to add server access for a user, but '.
          'the account does not exist in the unix config files. ';
        $this->set_error($message,'system');
        return false;
      }
      if(!$this->set_shell()) return false;
      if(!$this->add_user_to_ssh_group()) return false;
      if(!$this->create_public_key()) return false;
      return true;
    }

    function update() {
      // See if the user exists 
      if(!$this->unix_account_exists() && posix_getuid() == 0) {
        $message = 'I was asked to update server access for a user, but '.
          'the account does not exist in the unix config files. ';
        $this->set_error($message,'system');
        return false;
      }
      if(!$this->set_shell()) return false;
      if(!$this->add_user_to_ssh_group()) return false;
      if(!$this->create_public_key()) return false;
      return true;
    }

    function restore() {
      // See if the user exists 
      if(!$this->unix_account_exists() && posix_getuid() == 0) {
        $message = 'I was asked to add server access for a user, but '.
          'the account does not exist in the unix config files. ';
        $this->set_error($message,'system');
        return false;
      }
      if(!$this->set_shell()) return false;
      if(!$this->add_user_to_ssh_group()) return false;
      if(!$this->create_public_key()) return false;
      return true;
    }

    function node_sanity_check() {
      if(!is_readable($this->_passwd_file))  {
        $message = 'Passwd file not readable or does not exist. Trying: ' . $this->_passwd_file;
        $this->set_error($message,'system');
        return false;
      }
      $cmds_to_check = array('addgroup','deluser','usermod');
      foreach($cmds_to_check as $cmd) {
        $cmd_variable_name = "_{$cmd}_cmd"; 
        if(!file_exists($this->$cmd_variable_name)) {
          $message = "$cmd command does not exist. ".
            'Trying: ' . $this->$cmd_variable_name;
          $this->set_error($message,'system');
          return false;
        }
      }
      return true;
    }

    function unix_account_exists() {
      $login = $this->get_server_access_login();
      return red_key_exists_in_file($login,':',$this->_passwd_file);
    }

    function set_shell() {
      $cmd = $this->_usermod_cmd;  
      $user = $this->get_server_access_login();
      $args = '-s /bin/bash ' . escapeshellarg($user) .  " 2>&1";
      exec("$cmd $args",$output,$return_value);
      if($return_value != 0) {
        $output = $this->get_htmlentities(implode(' ',$output));
        $error = "Failed to update the user shell. The output was: ".
          "$output.";
        $this->set_error($error,'system','hard');
        return false;
      }
      return true;
    }

    function get_home_dir() {
      $login = $this->get_server_access_login();
      $member_name = $this->get_member_unix_group_name();
      $hosting_order_identifier = $this->get_hosting_order_identifier();
      $find = array('{member_name}','{identifier}','{login}');
      $replace = array($member_name,$hosting_order_identifier,$login);
      $home_dir = str_replace($find,$replace,$this->_home_dir_template);
      return $home_dir;
    }

    function create_public_key() {
      $home_dir = $this->get_home_dir();
      $ssh_dir = "$home_dir/.ssh";
      $auth_keys = "$ssh_dir/authorized_keys";

      // iterate over every line, only add lines 
      // that do not already exist
      $existing_lines = array();
      if(file_exists($auth_keys)) {
        $existing_lines = file($auth_keys);
      }
      $user_keys = $this->get_server_access_public_key();  
      if(!empty($user_keys)) {
        if(!file_exists($ssh_dir)) {
          if(!mkdir($ssh_dir)) {
            $this->set_error("Failed to create ssh directory. Trying: $ssh_dir.",'system');
            return false;
          }
          if(!$this->set_file_ownership($ssh_dir)) return false;
        }

        if(!file_exists($auth_keys)) {
          if(!touch($auth_keys)) {
            $this->set_error("Failed to create your auth keys file. Trying: $auth_keys.",'system');
            return false;
          }
          if(!$this->set_file_ownership($auth_keys)) return false;
        }
        $user_keys = array_map('trim', explode("\n",$user_keys));
        foreach($user_keys as $user_key) {
          $user_key = trim($user_key);
          if(!in_array($user_key,$existing_lines)) {
            // we can't be sure if the preceding line has a line break
            // so add one in the beginning.
            $append = "\n# added by mfpl members control panel\n" . $user_key . "\n";
            if(!red_append_to_file($auth_keys,$append)) {
              $this->set_error("Failed to append to authorized_keys file. Trying: $auth_keys.",'system');
              return false;
            }
          }
        }
        red_fork_exec_wait('/usr/sbin/monkeysphere-authentication', array('update-users',$this->get_server_access_login()));
      }
      return true;
    }

    function set_file_ownership($file) {
      $unix_user = $this->get_server_access_login();
      $unix_group_id = $this->get_hosting_order_unix_group_id();
      if(!red_chown($file,$unix_user)) {
        $this->set_error("Failed to chown $file.",'system');
        return false;
      }
      if(!red_chgrp($file,$unix_group_id)) {
        $this->set_error("Failed to chgrp $file.",'system');
        return false;
      }
      return true;
    }

    function get_login_uid() {
      $unix_user_name = $this->get_server_access_login();
      $sql = "SELECT user_account_uid FROM red_item_user_account WHERE ".
        "user_account_login = '$unix_user_name'";
      if(!$result = $this->_sql_query($sql) || $this->_sql_num_rows($result) == 0) {
        $message = "Unable to get user uid from user name";
        $this->set_error($message,'system');
        return false;
      }
      $row = $this->_sql_fetch_row($result);
      return $row[0];
    }
    function add_user_to_ssh_group() {
      $user = $this->get_server_access_login();
      $group = $this->_ssh_group;
      $cmd = $this->_addgroup_cmd;  
      $args = escapeshellarg($user) . ' ' . escapeshellarg($group) . " 2>&1";
      exec("$cmd $args",$output,$return_value);
      if($return_value != 0) {
        $output = $this->get_htmlentities(implode(' ',$output));
        $error = "Failed to add the use to the ssh group. The output was: ".
          "$output.";
        $this->set_error($error,'system','hard');
        return false;
      }
      return true;
    }

    function remove_user_from_ssh_group() {
      $user = $this->get_server_access_login();
      $group = $this->_ssh_group;
      $cmd = $this->_deluser_cmd;  
      $args = escapeshellarg($user) . ' ' . escapeshellarg($group) . " 2>&1";
      exec("$cmd $args",$output,$return_value);
      // Don't check for error. If the user does not exist, this will
      // return an error, which is ok.  
      return true;
    }
  }
}


?>
