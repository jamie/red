<?php
if(!class_exists('red_item_dns_node_tinydns')) {
  class red_item_dns_node_tinydns extends red_item_dns {
    // If you want to extend this class in a way the requires an
    // addition to the config file, then add a value to this
    // array in your constructor
    var $_config_variables = array('tinydns_root_dir',
                             'make_cmd',
                             'tinydns_data_file'
                                  );
    var $_tinydns_root_dir;
    var $_tinydns_data_file;
    var $_make_cmd;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;

      $conf_file = $construction_options['conf_path'] . 
        '/red_dns.tinydns.conf';
      if(!$this->_set_config_values($conf_file)) {
        return False;
      }
    }

    function node_sanity_check() {
      if(!is_dir($this->_tinydns_root_dir))  {
        $message = 'Tinydns root directory does not exist. Trying: ' . $this->_tinydns_root_dir;
        $this->set_error($message,'system');
        return false;
      }
      if(!file_exists($this->_make_cmd))  {
        $message = "The make command does not exist. ".
          "Trying: " .  $this->_make_cmd . ".";
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    function delete() {
      if(!$this->_regenerate_data_file()) return false;
      if(!$this->_make_database()) return false;
      return true;
    }

    function disable() {
      return $this->delete();  
    }

    function insert() {
      if(!$this->_regenerate_data_file()) return false;
      if(!$this->_make_database()) return false;
      return true;
    }

    function update() {
      if(!$this->_regenerate_data_file()) return false;
      if(!$this->_make_database()) return false;
      return true;
    }

    function restore() {
      if(!$this->_regenerate_data_file()) return false;
      if(!$this->_make_database()) return false;
      return true;
    }

    function _regenerate_data_file() {
      $temp = tempnam('/tmp', 'red');
      $content = $this->_get_data_file();
      $file = $this->_tinydns_root_dir . '/' . $this->_tinydns_data_file;
      if(!red_write_to_file($temp,$content)) {
        $message = "Failed to write to data file. Trying: '$temp'.";
        $this->set_error($message,'system');
        return false;
      }
      if(!rename($temp, $file)) {
        $message = "Failed to move data file into place.";
        $this->set_error($message,'system');
        return false;
      }

      return true;
    }

    function _make_database() {
      $cmd1 = "cd " . $this->_tinydns_root_dir;
      $cmd2 = $this->_make_cmd;
      $args = " 2>&1";
      exec("$cmd1 $args;$cmd2 $args",$output,$return_value);
      if($return_value != 0) {
        $output = $this->get_htmlentities(implode(' ',$output));
        $error = "Failed to create the tinydns database. Output: ".
          "$output.";
        $this->set_error($error,'system');
        return false;
      }
      return true;
    }
      
    function _get_data_file() {
      $ret = '';
      $data_array = $this->_get_data_array();
      foreach($data_array as $row) {
        $type = $row['type'];
        switch($type) {
          case 'host':
            $ret .= $this->_get_host_line($row);
            break;
          case 'a':
            $ret .= $this->_get_a_line($row);
            break;
          case 'ns':
            $ret .= $this->_get_ns_line($row);
            break;
          case 'mx':
            $ret .= $this->_get_mx_line($row);
            break;
          case 'text':
            $ret .= $this->_get_text_line($row);
            break;
          case 'cname':
            $ret .= $this->_get_cname_line($row);
            break;

        }
        $ret .= "\n";
      }
      return $ret;
    }

    function _get_data_array() {
      $sql = "SELECT red_item.item_id,dns_type,dns_fqdn,dns_ip,dns_ttl,".
        "dns_timestamp,dns_server_name,dns_text,dns_dist FROM ".
        "red_item_dns INNER JOIN red_item ON red_item_dns.item_id = ".
        "red_item.item_id WHERE item_status != 'pending-delete' AND ".
        "item_status != 'deleted' AND item_status != 'transfer-limbo' ".
        "AND item_status != 'disabled' AND item_status != 'pending-disable' ".
        "ORDER BY hosting_order_id,dns_type";
      $result = $this->_sql_query($sql);
      $return = array();
      while($row = $this->_sql_fetch_row($result)) {
        $item_id = $row[0];
        $return[$item_id]['type'] = $row[1];
        $return[$item_id]['fqdn'] = $row[2];
        $return[$item_id]['ip'] = $row[3];
        $return[$item_id]['ttl'] = $row[4];
        // disable timestamp usage until it can be properly
        // debugged. See:
        // http://lists.mayfirst.org/pipermail/service-advisories/2010-August/000200.html
        $timestamp = '';

        $return[$item_id]['timestamp'] = $timestamp;
        $return[$item_id]['server_name'] = $row[6];
        $return[$item_id]['text'] = $row[7];
        $return[$item_id]['dist'] = $row[8];
      }
      return $return;
    }

    function _get_host_line($data_array) {
      extract($data_array);
      return "=$fqdn:$ip:$ttl:$timestamp";
    }
      
    function _get_ns_line($data_array) {
      extract($data_array);
      return ".$fqdn:$ip:$server_name:$ttl:$timestamp";
    }
    function _get_mx_line($data_array) {
      extract($data_array);
      return "@$fqdn:$ip:$server_name:$dist:$ttl:$timestamp";
    }
    function _get_a_line($data_array) {
      extract($data_array);
      return "+$fqdn:$ip:$ttl:$timestamp";
    }
    function _get_text_line($data_array) {
      extract($data_array);
      return "'$fqdn:" . $this->djbdns_escape_string($text) . ":$ttl:$timestamp";
    }
    function _get_cname_line($data_array) {
      extract($data_array);
      return "C$fqdn:$server_name:$ttl:$timestamp";
    }

    function djbdns_escape_string($string) {
      // Thanks: http://www.anders.com/projects/sysadmin/djbdnsRecordBuilder/buildRecord.txt
      $out = '';
      $len = strlen($string);
      $index = 0;
      $replace = array(
        "\r","\n","\t",":","\\","/"," ",
      );
      while($index < $len) {
        $char = substr($string,$index,1);
        if(in_array($char,$replace)) {
          $out .= sprintf("\\%03o",ord($char)); 
        } else {
          $out .= $char;
        }
        $index++;
      }
      return $out;
    }
  }  
}


?>
