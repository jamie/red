<?php
if(!class_exists('red_item_email_address_node_postfix')) {
  class red_item_email_address_node_postfix extends red_item_email_address {

    // If you want to extend this class in a way the requires an
    // addition to the config file, then add a value to this
    // array in your constructor
    var $_config_variables = array('virtual_alias_maps_file',
                             'virtual_alias_domains_file',
                             'maildir_template',
                             'maildirmake_cmd',
                             'usermod_cmd',
                                  );
    var $_virtual_alias_maps_file;
    var $_virtual_alias_domains_file;
    var $_maildirmake_cmd;
    var $_usermod_cmd;
    var $_unix_recipients = array();

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;

      $conf_file = $construction_options['conf_path'] . 
        '/red_email_address.postfix.conf';
      if(!$this->_set_config_values($conf_file)) {
        return False;
      }

      // the recipients field could be a lot of things! It will be 
      // inserted as is into the /etc/postfix/virtual_alias_maps.
      // In addition, for every non-email address, we need to possibly 
      // create a Maildir. So - collect all the non-email
      // addresses into an array.
      $recipients_array = explode(',',$this->get_email_address_recipient());
      foreach($recipients_array as $single_recipient) {
        if(!preg_match('/\@/',$single_recipient)) {
          $this->_unix_recipients[] = trim($single_recipient);
        }
      }
    }

    function delete() {
      // See if the email address exists 
      if(!$this->email_address_exists()) {
        $message = 'I was asked to delete the email address, but the ' .
          'account does not exist in the postfix config files. '.
          'I will continue without a hard stop '.
          'but thought you would like to know.';
        $this->set_error($message,'system','soft');
      }

      // Kill the postfix settings
      if(!$this->delete_virtual_alias_domains_account()) return false;
      if(!$this->delete_virtual_alias_maps_account()) return false;
      // we don't delete the maildir because there may be other email
      // items that use the same maildir. The maildir (and entire home dir)
      // will be deleted if you delete the user
      return true;
    }

    function disable() {
      return $this->delete();
    }

    function insert() {
      // Make sure this email address does not already exist
      if($this->email_address_exists()) {
        $message = 'Cannot insert new email account, an email account with '.
          'the same address already exists.';
        $this->set_error($message,'system','soft');
        return false;
      }

      // if it's a unix user...
      if(count($this->_unix_recipients) > 0) {
        foreach($this->_unix_recipients as $single_recipient) {
          // set shell to /bin/bash - maildrop requires valid shell
          if(!$this->set_shell($single_recipient)) return false;

          // create_maildir() returns true if the maildir already exists. 
          // This is ok because they could be adding an alias that should 
          // go to an existing maildir. We have no way of knowing the
          // difference between this situation and a maildir that was
          // left lying around after a user was deleted.
          if(!$this->create_maildir($single_recipient)) return false;
        }
      }

      if(!$this->create_virtual_alias_maps_account()) return false;
      if(!$this->create_virtual_alias_domains_account()) return false;

      return true;

    }

    function update() {
      // we have to be able to restore from being disabled.
      return $this->restore();
    }

    function restore() {
      // if it's a unix user...
      if(count($this->_unix_recipients) > 0) {
        foreach($this->_unix_recipients as $single_recipient) {
          if(!$this->maildir_exists($single_recipient)) {
            if(!$this->create_maildir($single_recipient)) return false;
          }
          if(!$this->set_shell($single_recipient)) return false;
        }
      }
      if($this->email_address_exists()) {
        if(!$this->update_virtual_alias_maps_account()) return false;
      }
      else {
        if(!$this->create_virtual_alias_maps_account()) return false;
        if(!$this->create_virtual_alias_domains_account()) return false;
      }
      return true;
    }
    function unix_account_exists($recipient) {
      return red_key_exists_in_file($recipient,':',$this->_passwd_file);
    }

    function email_address_exists() {
      return red_key_exists_in_file($this->get_email_address(),'\s',$this->_virtual_alias_maps_file);
    }
      
    function get_maildir($recipient) {
      $maildir_template = $this->_maildir_template;
      $find = array('{member_name}',
              '{identifier}',
              '{login}');
      $replace = array($this->get_member_unix_group_name(),
                 $this->get_hosting_order_identifier(),
                 $recipient);
      $maildir = str_replace($find,$replace,$maildir_template);
      if(substr($maildir,-1) == '/') $maildir = substr($maildir,0,-1);
      return $maildir;
    }

    function maildir_exists($recipient) {
      return is_dir($this->get_maildir($recipient));
    }

    function node_sanity_check() {
      if(!is_writable($this->_virtual_alias_maps_file))  {
        $message = 'Virtual alias maps file not writable or does not '.
          'exist. Trying: ' . $this->_virtual_alias_maps_file;
        $this->set_error($message,'system');
        return false;
      }
      if(!is_writable($this->_virtual_alias_domains_file))  {
        $message = 'Virtual alias domains file not writable or does not '.
          'exist. Trying: ' . $this->_virtual_alias_domains_file;
        $this->set_error($message,'system');
        return false;
      }
        
      if(!file_exists($this->_maildirmake_cmd)) {
        $message = 'Maildirmake command does not exist. Trying: ' . 
          $this->_maildirmake_cmd;
        $this->set_error($message,'system');
        return false;
      }
        
      if(!file_exists($this->_usermod_cmd)) {
        $message = 'usermod command command does not exist. Trying: ' . 
          $this->_usermod_cmd;
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    function create_maildir($recipient) {
      if(!$this->maildir_exists($recipient)) {
        $cmd = $this->_maildirmake_cmd;
        $args = escapeshellarg($this->get_maildir($recipient)) .
          ' 2>&1';
        exec("$cmd $args",$output,$return_value);
        if($return_value != 0) {
          $output = $this->get_htmlentities(implode(' ',$output));
          $this->set_error("The command '$cmd' failed. Output was: $output",'system');
          return false;
        }
        if(!$this->maildir_exists($recipient)) {
          $this->set_error("Failed to create maildir.",'system');
          return false;
        }
        if(!$this->chown_maildir($recipient)) return false;
      }
      return true;
    }

    function chown_maildir($recipient) {
      $group = $this->get_hosting_order_unix_group_name();
      $maildir = $this->get_maildir($recipient);
      $dirs = array($maildir,"$maildir/new","$maildir/cur","$maildir/tmp");
      foreach($dirs as $dir) {
        if(!red_chown($dir,$recipient)) {
          $this->set_error("Failed to chown $dir .",'system');
          return false;
        }
        if(!red_chgrp($dir,$group)) {
          $this->set_error("Failed to chgrp $dir.",'system');
          return false;
        }
      }
      return true;
    }

    function create_virtual_alias_maps_account() {
      if(!$this->email_address_exists()) {
        $address = $this->get_email_address();
        $login = $this->get_email_address_recipient();
        $map_line = "$address\t$login\n";
        if(!red_append_to_file($this->_virtual_alias_maps_file,$map_line)) {
          $message = "Failed to create virtual alias maps account";
          $this->set_error($message,'system');
          return false;
        }
      }
      return true;
    }

    function update_virtual_alias_maps_account() {
      if($this->email_address_exists()) {
        $address = $this->get_email_address();
        $login = $this->get_email_address_recipient();
        $map_line = "$address\t$login\n";
        if(!red_update_file($this->_virtual_alias_maps_file,$address,"\t",$map_line)) {
          $message = "Failed to update virtual alias maps account";
          $this->set_error($message,'system');
          return false;
        }
      }
      return true;
    }

    function delete_virtual_alias_maps_account() {
      if(!red_delete_from_file($this->_virtual_alias_maps_file,$this->get_email_address(),'\t')) {
        $message = "Failed to delete virtual alias maps account";
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    function create_virtual_alias_domains_account() {
      $domain_line = $this->get_domain_portion_of_address() . "\t-\n";
      if(red_key_exists_in_file($this->get_domain_portion_of_address(),'\t',$this->_virtual_alias_domains_file)) return true;
      if(!red_append_to_file($this->_virtual_alias_domains_file,$domain_line)) {
        $message = "Failed to create virtual alias domains account";
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    function delete_virtual_alias_domains_account() {
      $domain = $this->get_domain_portion_of_address();
      if(!$this->domain_used_by_other_accounts()) {
        if(!red_delete_from_file($this->_virtual_alias_domains_file,$domain,'\t')) {
          $message = "Failed to delete virtual alias maps account";
          $this->set_error($message,'system');
          return false;
        }
      }
      return true;
    }
      
    function domain_used_by_other_accounts() {
      $domain = $this->get_domain_portion_of_address();
      $address = $this->get_email_address();
      $addresses = red_get_all_keys_in_file('\t',$this->_virtual_alias_maps_file);
      // Remove the current address
      reset($addresses);
      foreach($addresses as $k => $v) {
        if($v == $address) unset($addresses[$k]);
      }
      $regexp = "/@$domain$/";
      return red_preg_match_array($regexp,$addresses);
    }

    function set_shell($user) {
      // shell must be set to a valid shell or maildrop won't work
      $cmd = $this->_usermod_cmd;  
      $args = '-s /bin/bash ' . escapeshellarg($user) .  " 2>&1";
      exec("$cmd $args",$output,$return_value);
      if($return_value != 0) {
        $output = $this->get_htmlentities(implode(' ',$output));
        $error = "Failed to update the user shell. The output was: ".
          "$output.";
        $this->set_error($error,'system','hard');
        return false;
      }
      return true;
    }
  }  
}


?>
