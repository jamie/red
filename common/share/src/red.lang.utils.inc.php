<?php

// language functions - must be included before anything else
$lang = red_get_lang();
setlocale(LC_MESSAGES, $lang);
if(isset($config)) {
  $locale_path = $config['common_src_path'] . '/../../locale';
} else {
  $locale_path = './../../locale';
}
bindtextdomain('red', $locale_path );
bind_textdomain_codeset('red', 'UTF-8');
textdomain('red');

function red_get_default_lang() {
  return 'en_US';
}

function red_get_lang() {
  $lang = red_get_default_lang(); 
  // if no session variable is set, then we are probably operating
  // in a node context, so just return english
  if(!isset($_SESSION)) return $lang;

  // If the red_lang session variable is set (from clicking a language
  // option via the UI, then use that as the first choice).
  if(array_key_exists('red_lang',$_SESSION)) {
    $lang = $_SESSION['red_lang'];
  } elseif (array_key_exists('red_pref_lang',$_SESSION)) {
    // otherwise, if a saved langage preference has been loaded
    // when they logged in, use that one (this has not yet been 
    // implemented).
    $lang = $_SESSION['red_pref_lang'];
  } else {
    // todo... check for browser language preference
    if(array_key_exists('HTTP_ACCEPT_LANGUAGE', $_SERVER)) {
      $lang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
      if(preg_match('/^es/',$lang)) $lang = 'es_MX';
      if(preg_match('/^en/',$lang)) $lang = 'en_US';
    }
  }
  return red_validate_lang($lang);
}

// All allowed languages (and variants)
function red_allowed_languages() {
  return array('en_US','es_MX','es_MX.utf8');
}

// sanitize language input
function red_validate_lang($lang) {
  $default_lang = red_get_default_lang();
  if($lang == $default_lang) return $lang;

  $allowed = red_allowed_languages();
  if(in_array($lang, $allowed)) {
    // we need to ensure the codeset is configured properly
    if(preg_match('/^es_MX/',$lang)) $lang = 'es_MX.utf8';
    return $lang;
  }
  // don't use red_t here - to avoid recursive calls...
  // can't use red_set_message yet - not sure how to communicate
  // this to the end-user...
  // red_set_message("Unknown language selection. Using default language.");
  return $default_lang;
}

/*
 * Shameless stolen from drupal 7
 */
function red_t($string, array $args = array()) {
  $string = gettext($string); 
  if (empty($args)) {
    return $string;
  }
  else {
    // Transform arguments before inserting them.
    foreach ($args as $key => $value) {
      switch ($key[0]) {
        case '@':
          // Escaped only.
          $args[$key] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
          break;

        case '%':
        default:
          // Escaped and placeholder.
          $args[$key] = '<em class="red-placeholder">' . 
            htmlspecialchars($value, ENT_QUOTES, 'UTF-8') . '</em>';
          break;

        case '!':
          // Pass-through.
      }
    }
    return strtr($string, $args);
  }
}
