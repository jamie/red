<?php
/*
 * This is the red db class 
 */

if(!class_exists('red_db')) {
  class red_db {
    var $_sql_resource;  // the mysql connection resource to be used
    // constructor
    function __construct($sql_resource) {
      $this->_sql_resource = $sql_resource;
    }

    function _sql_query($sql) {
      return red_sql_query($sql,$this->_sql_resource);
    }

    function _sql_fetch_row($result) {
      return red_sql_fetch_row($result);
    }

    function _sql_fetch_assoc($result) {
      return red_sql_fetch_assoc($result);
    }

    function _sql_insert_id() {
      return red_sql_insert_id($this->_sql_resource);
    }

    function _sql_num_rows($result) {
      return red_sql_num_rows($result);
    }

    function _sql_error() {
      return red_sql_error($this->_sql_resource);
    }

    static function init_db($config) {
      $flags = NULL;
      $sql_resource = mysqli_init();
      // We have two ways of initializing the sql connection. One way is designed for php
      // 5 (jessie) and the other way is for php7 (stretch). At this time, neither way
      // seems to reliably stop a downgrade attack. We will need to test and re-configure
      // after we upgrade hay to stretch and get mariadb-server.
      // The code selects a way based on the presence of configuration keys in red-node.conf.
      // If mysql_cert_file exists (and it should point to /etc/mysql/red-server.pem) then
      // we use the new php7 method. Otherwise, we use the old php5 method.
      if(array_key_exists('mysql_cert_file', $config)) {
        if (!file_exists($config['mysql_cert_file'])) {
          trigger_error("Could not find the mysql_cert_file.");
          return FALSE;
        }
        // Uncomment the line below after we have upgraded to mariadb.
        $sql_resource->ssl_set(null, null, $config['mysql_cert_file'], null, null);
        if (!($sql_resource->real_connect($config['db_host'],$config['db_user'],$config['db_pass'], NULL, NULL, NULL, MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT))) {
          trigger_error(sprintf("failed to really connect\n"). mysqli_connect_errno() . ': ' . mysqli_connect_error());
          return FALSE;
        }
      }
      elseif(array_key_exists('mysql_cnf', $config)) {
        if (!($sql_resource->options(MYSQLI_READ_DEFAULT_FILE, $config['mysql_cnf']))) {
          trigger_error(sprintf("failed to set the default file (MYSQLI_READ_DEFAULT_FILE: %d)\n", MYSQLI_READ_DEFAULT_FILE));
          return FALSE;
        } 
        if (!($sql_resource->options(MYSQLI_READ_DEFAULT_GROUP, 'red'))) {
          trigger_error(sprintf("failed to set the group for the default file (MYSQLI_READ_DEFAULT_GROUP: %d)\n", MYSQLI_READ_DEFAULT_GROUP));
          return FALSE;
        }
        if (!($sql_resource->real_connect($config['db_host'],$config['db_user'],$config['db_pass']))) {
          trigger_error(sprintf("failed to really connect\n"));
          return FALSE;
        }
      }
      else {
        // NOTE: the ui does use or need encryption.
	      if (!($sql_resource->real_connect($config['db_host'],$config['db_user'],$config['db_pass']))) {
          trigger_error(sprintf("failed to really connect\n"));
          return FALSE;
        }
      }

      if(!$sql_resource) return false;
      if(!mysqli_select_db($sql_resource, $config['db_name'])) return false;
      return $sql_resource;
    }
  }
}

?>
