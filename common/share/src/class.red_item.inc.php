<?php
if(!class_exists('red_item')) {
  class red_item extends red_ado {
    var $_key_field = 'item_id';
    var $_key_table = 'red_item';

    // Variables common to ui and node classes
    var $_src_path;
    var $_common_src_path;
    var $_mode;   // either ui or node 
    var $_hosting_order_unix_group_id;
    var $_hosting_order_unix_group_name;
    var $_site_dir_template;
    var $_member_parent_id;
    var $_track_disk_usage = FALSE;
      
    // These are from the red_item database:
    var $_item_id;
    var $_hosting_order_id;
    var $_member_id;
    var $_service_id;
    var $_item_status;
    var $_item_host;
    var $_item_disk_usage = 0;
    var $_item_quota = 0;
    var $_item_quota_notify_date;
    var $_item_quota_notify_percent = 85;
    var $_item_modified;

    // Server only variables
    var $_hosting_order_identifier;    // usually domain name, from 
    // red_hosting_order table
    var $_conf_path; // path to node configuration files
    var $_member_unix_group_name;  // usually same as hosting_order, 
    // but can be different. This value
    // is used when building directories, e.g.
    // /home/members/$member_group_name/

    // Client only variables
    var $_child_table;
    var $_notify_host = TRUE;            // Tell the ui whether or not 
    // to notify the host that there is
    // a pending change
    var $_notify_user = 'root';          // username to ssh in as when
    // notifying host  
    var $_notify_cmd = '/usr/bin/ssh';
    var $_notify_background_process = true;  // Whether to background the
    // the notification process

    var $_minimum_unix_user_id = 500;  // never create unix UID less than 
    // this number
    static function &get_red_object($construction_options) {
      $stock_check_file_error = "The file either does not exist or does not ".
        "have the proper permissions set. When running as root, all ".
        "included files must be owned by root and only writable by the ".
        "owner. Please check the file permissions and try again.\n";

      // what are we, ui or node?
      $mode = $construction_options['mode'];

      // common_src_path is required by both ui and node
      $common_src_path = $construction_options['common_src_path'];
      // src_path is either the path to the ui code or node code
      // depending on whether we're in ui or node mode.
      $src_path = $construction_options['src_path'];

      $resource = $construction_options['sql_resource'];

      $backend_name = '';
        
      // If pulling from the database - get the service id 
      if(array_key_exists('rs',$construction_options)) {
        // extract the service_id so we know which child
        // class to call. Server mode MUST pass a recordset.
        $service_id = $construction_options['rs']['service_id'];
      }
      else {
        // Otherwise service_id must be passed 
        // This is ok with ui mode
        $service_id = $construction_options['service_id'];
      }
      if(empty($service_id))  {
        $ret = false;
        return $ret;
      }
      $child_table = red_get_service_table_for_service_id($service_id,$resource);

      if($mode == 'node') {
        // The backends array is something like 'postfix' or 'apache'
        // prepend - so it can be combined in the require statement
        if(!array_key_exists('backends',$construction_options) ||
          !array_key_exists($child_table,$construction_options['backends'])) {
          echo("Backend not defined for $child_table.\n");
          $ret = false;
          return $ret;
        }

        $backend_name = '_' . $construction_options['backends'][$child_table];
      }

      $child_class = $child_table . '_' . $mode . $backend_name;

      // Include the appropriate classes
      // First the generic class
      // Not all children have a common file - only require if it exists
      $child_common_class = $common_src_path . "/modules/class.$child_table.inc.php";
      if(red_check_file($child_common_class)) {
        require_once($child_common_class);
      }

      // Now the specific ui/node class
      $child_specific_class = $src_path . "/modules/class.$child_class.inc.php";

      if(!red_check_file($child_specific_class)) {
        echo ("Failed loading: $child_specific_class. $stock_check_file_error.");
        $ret = false;
        return $ret;
      }
      require_once($child_specific_class);

      $construction_options['child_table'] = $child_table;
      $object = new $child_class($construction_options);
      return $object;

    }
    // constructor
    function __construct($construction_options) {
      // call parent
      parent::__construct($construction_options);

      // required by both ui and node
      $this->set_site_dir_template($construction_options['site_dir_template']);

      // Set default value - this will be overridden when
      // the object is commited to the db
      if(empty($this->_item_modified)) $this->set_item_modified(date('Y-m-d H:i:s'));

      // what are we, ui or node?
      $this->_mode = $construction_options['mode'];
      $this->_common_src_path = $construction_options['common_src_path'];
      $this->_src_path = $construction_options['src_path'];

      $this->_set_common_datafields();

      $this->_child_table = $construction_options['child_table'];
      if($this->_mode == 'node') {
        $this->_conf_path = $construction_options['conf_path'];

        // Now - replace the recordset passed in with a new record set
        // that includes all fields from the child table
        $rs = $construction_options['rs'];

        // Set the item id in case it's needed for logging before we are
        // initialized.
        $this->set_item_id($rs['item_id']);
        $sql = $this->_get_select_sql_statement();
        if(false === $result = $this->_sql_query($sql)) {
          $message = "SQL execution error while constructing the object. ".
            'The mysql error is: ' . $this->_sql_error();
          $this->set_error($message,'system');
          return false;
        }
        elseif(false == $full_rs = $this->_sql_fetch_assoc($result)) {
          $message = "SQL error fetching the assoc from the result while ".
            "constructing the object. The mysql error ".
            'is: ' . $this->_sql_error();
          $this->set_error($message,'system');
          return false;
        }
        elseif(!is_array($full_rs)) {
          $this->set_error('SQL error in the constructor! Result is not an array.','system');
          return false;
        }
        elseif(count($full_rs) == 0) {
          // Yipes - orphan
          $this->set_error('Orphaned item!','system');
          return false;
        }
        // build the object from the recordset
        $this->_initialize_from_recordset($full_rs);
      }
      else {
        // mode is ui
        // see if we should be notifying the host (default is true)
        if(array_key_exists('notify_host',$construction_options)) 
          $this->_notify_host = $construction_options['notify_host'];
        if(!empty($construction_options['notify_user'])) 
          $this->_notify_user = $construction_options['notify_user'];
        if(!empty($construction_options['notify_cmd'])) 
          $this->_notify_cmd = $construction_options['notify_cmd'];

        if(!array_key_exists('rs',$this->_construction_options)) {
          // creating a new record
          if (array_key_exists('hosting_order_id', $construction_options)) {
            $this->set_hosting_order_id($construction_options['hosting_order_id']);
          }
          if (array_key_exists('member_id', $construction_options)) {
            $this->set_member_id($construction_options['member_id']);
          }
          $this->set_service_id($construction_options['service_id']);
          $host = $this->_determine_appropriate_host();
          $this->set_item_host($host);
          $this->set_item_status('pending-insert');
        }
      }
      $this->_set_hosting_order_identifier();

      if(!$this->_set_hosting_order_unix_group())  {
        //unset($this);
        return;
      }
      if(!$this->_set_member_unix_group())  {
        //unset($this);
        return;
      }
    }

    function _get_select_sql_statement() {
      $item_id = $this->get_item_id();
      if(empty($item_id)) return false;
      return "SELECT * FROM red_item INNER JOIN " . $this->_child_table .
        " ON red_item.item_id = " . $this->_child_table . 
        ".item_id WHERE red_item.item_id = " . $item_id;
    }

    // Shared methods common to both ui and nodes
    function get_site_dir() {
      $find = array(
        '{unix_group_name}',
        '{identifier}',
        '{item_id}'
      );
      $hosting_order_id = intval($this->get_hosting_order_id());
      // Get the item id associated with this hosting order. We need it when 
      // creating and deleting.
      $sql = "SELECT item_id FROM red_item JOIN red_item_web_conf USING(item_id)
        WHERE hosting_order_id = $hosting_order_id AND 
        item_status != 'deleted'";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      if(empty($row))  {
        $this->set_error("Couldn't get the item id for this hosting order.",'system');
        return false;
      }
      $item_id = $row[0];

      $replace = array(
        $this->get_member_unix_group_name(),
        $this->get_hosting_order_identifier(),
        $item_id
      );
        
      $site_dir = str_replace($find,$replace,$this->get_site_dir_template());
      if(substr($site_dir,-1) == '/') $site_dir = substr($site_dir,0,-1);
      return $site_dir;
    }

    function _get_initialize_sql($item_id) {
      $service_table = red_get_service_table_for_item_id($item_id,$this->_sql_resource);
      $item_id = intval($item_id);
      $sql = "SELECT * FROM red_item INNER JOIN $service_table ".
        "ON red_item.item_id = $service_table.item_id ".
        "WHERE red_item.item_id = " . $item_id;
      return $sql;
    }

    function _initialize_from_recordset($rs) {
      parent::_initialize_from_recordset($rs);
      if($rs['item_status'] == 'pending-delete') {
        $this->set_delete_flag();
      }
      if($rs['item_status'] == 'pending-disable') {
        $this->set_disable_flag();
      }
    }

    function _set_common_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array ('item_id' => array (
                               'req' => false,
                               'pcre'   => RED_ID_MATCHER,
                               'pcre_explanation'   => RED_ID_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => 'Config ID',
                               'user_insert' => FALSE,
                               'user_update' => FALSE,
                               'user_visible' => FALSE,
                               'weight' => 0,
                               'tblname'   => 'red_item'),
                             'hosting_order_id' => array (
                               'req' => false,
                               'pcre'   => RED_ID_MATCHER,
                               'pcre_explanation'   => RED_ID_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => 'Hosting Order ID',
                               'user_update' => FALSE,
                               'user_insert' => FALSE,
                               'user_visible' => FALSE,
                               'weight' => 1,
                               'tblname'   => 'red_item'),
                             'member_id' => array (
                               'req' => false,
                               'pcre'   => RED_ID_MATCHER,
                               'pcre_explanation'   => RED_ID_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => 'Member ID',
                               'user_update' => FALSE,
                               'user_insert' => FALSE,
                               'user_visible' => FALSE,
                               'weight' => 2,
                               'tblname'   => 'red_item'),
                             'service_id' => array (
                               'req'   => TRUE,
                               'pcre'   => RED_ID_MATCHER,
                               'pcre_explanation'   => RED_ID_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => 'Service ID',
                               'user_update' => FALSE,
                               'user_insert' => FALSE,
                               'user_visible' => FALSE,
                               'weight' => 3,
                               'tblname' => 'red_item'),
                             'item_host' => array (
                               // item_host can be empty to signify a db only item
                               'req'   => FALSE,
                               'pcre'   => RED_HOST_MATCHER,
                               'pcre_explanation'   => RED_HOST_EXPLANATION,
                               'type'  => 'text',
                               'fname'  => 'Host',
                               'user_update' => FALSE,
                               'user_insert' => FALSE,
                               'user_visible' => FALSE,
                               'weight' => 4,
                               'tblname' => 'red_item'),
                             'item_status' => array (
                               'req'   => TRUE,
                               'pcre'   => RED_STATUS_MATCHER,
                               'pcre_explanation'   => RED_STATUS_EXPLANATION,
                               'type'  => 'text',
                               'fname'  => 'Item Status',
                               'user_insert' => FALSE,
                               'user_update' => FALSE,
                               'user_visible' => TRUE,
                               'weight' => 5,
                               'tblname' => 'red_item'),
                             'item_quota' => array (
                               'req' => false,
                               'pcre'   => RED_BYTES_MATCHER,
                               'pcre_explanation'   => RED_BYTES_EXPLANATION,
                               'description' => red_t("Restrict this item to a maximum disk quota (0 means no quota is in place). Valid values include: 10b, 100kb, 500mb, or 3gb"),
                               'type'  => 'int',
                               'fname'  => red_t('Quota'),
                               'user_insert' => FALSE,
                               'user_update' => FALSE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'weight' => 101,
                               'tblname'   => 'red_item'),
                             'item_disk_usage' => array (
                               'req' => false,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Current Disk Usage'),
                               'user_insert' => FALSE,
                               'user_update' => FALSE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'weight' => 100,
                               'tblname'   => 'red_item'),
                             'item_quota_notify_percent' => array (
                               'req' => false,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Notification percent'),
                               'description'  => red_t('Notify member contacts when disk usage reaches this percent of quota'),
                               'user_insert' => FALSE,
                               'user_update' => FALSE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'weight' => 102,
                               'tblname'   => 'red_item'),
                             'item_quota_notify_date' => array (
                               'req'   => FALSE,
                               'pcre'   => RED_DATETIME_MATCHER,
                               'pcre_explanation'   => RED_DATETIME_EXPLANATION,
                               'type'  => 'datetime',
                               'fname'  => red_t('Notification date'),
                               'description'  => red_t('Silence notifications until this date, e.g. 2021-06-30 20:30 (leave blank for immediate notification).'),
                               'user_insert' => FALSE,
                               'user_update' => FALSE,
                               'user_visible' => FALSE,
                               'weight' => 103,
                               'tblname' => 'red_item'),
                             'item_modified' => array (
                               'req'   => TRUE,
                               'pcre'   => RED_DATETIME_MATCHER,
                               'pcre_explanation'   => RED_DATETIME_EXPLANATION,
                               'type'  => 'datetime',
                               'fname'  => 'Last Modified',
                               'user_insert' => FALSE,
                               'user_update' => FALSE,
                               'user_visible' => TRUE,
                               'weight' => 6,
                               'tblname' => 'red_item'),
                                 ));


      // Now adjust the visibility of the disk usage and quota related fields
      // depending on whether this class is using them or not.
      if ($this->_track_disk_usage) {
        $this->_datafields['item_disk_usage']['user_visible'] = TRUE;
        $this->_datafields['item_quota']['user_visible'] = TRUE;
        $this->_datafields['item_quota']['user_update'] = TRUE;
        $this->_datafields['item_quota']['user_insert'] = TRUE;
        $this->_datafields['item_quota_notify_percent']['user_update'] = TRUE;
        $this->_datafields['item_quota_notify_percent']['user_insert'] = TRUE;
        $this->_datafields['item_quota_notify_date']['user_update'] = TRUE;
        $this->_datafields['item_quota_notify_date']['user_insert'] = TRUE;
      }
    }

    function set_item_id($value) {
      $this->_item_id = intval($value);
    }
    function get_item_id() {
      return $this->_item_id;
    }

    function set_service_id($value) {
      $this->_service_id = intval($value);
    }
    function get_service_id() {
      return $this->_service_id;
    }

    function set_hosting_order_id($value) {
      $this->_hosting_order_id = intval($value);
    }
    function get_hosting_order_id() {
      return $this->_hosting_order_id;
    }

    function set_member_id($value) {
      $this->_member_id = $value;
    }

    function set_item_status($value) {
      $this->_item_status = $value;
    }
    function get_item_status() {
      return $this->_item_status;
    }

    function set_item_host($value) {
      $this->_item_host= $value;
    }
    function get_item_host() {
      return $this->_item_host;
    }

    function set_item_quota($value) {
      if (empty($value)) {
        $value = 0;
      }
      $this->_item_quota = $value;
    }
    function get_item_quota() {
      return $this->_item_quota;
    }

    function set_item_disk_usage($value) {
      $this->_item_disk_usage = intval($value);
    }
    function get_item_disk_usage() {
      return $this->_item_disk_usage;
    }

    function set_item_quota_notify_percent($value) {
      if (empty($value)) {  
        $value = 0;
      }
      $this->_item_quota_notify_percent = intval($value);
    }
    function get_item_quota_notify_percent() {
      return $this->_item_quota_notify_percent;
    }
    
    function set_item_quota_notify_date($value) {
      $this->_item_quota_notify_date = $value;
    }
    function get_item_quota_notify_date() {
      return $this->_item_quota_notify_date;
    }

    function get_item_modified() {
      return $this->_item_modified;
    }
    function set_item_modified($value) {
      $this->_item_modified = $value;
    }

    function get_notify_user() {
      return $this->_notify_user;
    }

    function set_notify_background_process($value) {
      $this->_notify_background_process = $value;
    }
    function get_notify_background_process() {
      return $this->_notify_background_process;
    }

    function set_notify_user($value) {
      $this->_notify_user = $value;
    }

    function get_notify_cmd() {
      return $this->_notify_cmd;
    }

    function set_notify_cmd($value) {
      $this->_notify_cmd = $value;
    }

    function set_site_dir_template($value)  {
      $this->_site_dir_template = $value;
    }

    function get_site_dir_template() {
      return $this->_site_dir_template;
    }

    function write_error_to_database($index) {
      // write to database

      $error = addslashes($this->_errors[$index]['error']);
      $severity = addslashes($this->_errors[$index]['severity']);
      $type = addslashes($this->_errors[$index]['type']);
      $item_id = $this->get_item_id();

      // If item id is empty, set it to null
      // so we don't get a sql serror
      if(empty($item_id)) $item_id = 0;
      $sql = "INSERT INTO red_error_log SET ".
        "error_log_type = '$type',".
        "error_log_severity = '$severity',".
        "error_log_message = '$error',".
        "item_id = " . $item_id;
      // don't log sql errors - otherwise we'll
      // get a loop. Instead, screach everything to a halt
      if(FALSE === $this->_sql_query($sql,FALSE)) 
        trigger_error("Logging SQL statement failed. The DB error is: " . $this->_sql_error() . " The SQL statement was: $sql.");

    }

    function _set_hosting_order_unix_group() {
      $hosting_order_id = $this->get_hosting_order_id();
      if(empty($hosting_order_id))  {
        // We don't need to set the hosting order unix id if it is an item inherited directly
        // by the member.
        return true;
      }
      $sql = "SELECT red_hosting_order.unique_unix_group_id,".
        "unique_unix_group_name FROM ".
        "red_hosting_order INNER JOIN red_unique_unix_group ON ".
        "red_hosting_order.unique_unix_group_id = ".
        "red_unique_unix_group.unique_unix_group_id WHERE ".
        "hosting_order_id = $hosting_order_id";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      if(empty($row))  {
        $this->set_error("Hosting order: $hosting_order_id does not have a properly set unique_unix_group_id value.",'system');
        return false;
      }
      $this->_hosting_order_unix_group_id = $row[0];
      $this->_hosting_order_unix_group_name = $row[1];
      return true;
    }

    function get_member_id() {
      // An item might have either a member_id set (and no hosting_order_id),
      // or it might have a hosting_order_id and no member_id. If it has a
      // hosting_order_id and no member_id, then we derive the member_id.

      // If member_id is defined, return it.
      if(!empty($this->_member_id)) return $this->_member_id;

      // Otherwise, derive it.
      $hosting_order_id = $this->get_hosting_order_id();
      if(empty($hosting_order_id))  {
        $this->set_error("Failed to retreive the hosting_order_id when getting member id.",'system');
        return false;
      }
      $sql = "SELECT member_id FROM red_hosting_order WHERE ".
        "hosting_order_id = $hosting_order_id";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      $this->_member_id = $row[0];
      return $this->_member_id;
    }

    function get_member_parent_id() {
      if(!empty($this->_member_parent_id)) return $this->_member_parent_id;

      $hosting_order_id = $this->get_hosting_order_id();
      $member_id = $this->get_member_id();
      if (empty($hosting_order_id) && empty($member_id)) {
        $this->set_error("Failed to retreive the hosting_order_id when getting member parent id.",'system');
      }
      if(!empty($hosting_order_id))  {
        $sql = "SELECT member_parent_id FROM red_hosting_order JOIN red_member ".
          "USING(member_id)  WHERE hosting_order_id = $hosting_order_id";
      }
      else {
        $sql = "SELECT member_parent_id FROM red_member ".
          "WHERE member_id = $member_id";
      }
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      $this->_member_parent_id = $row[0];
      return $this->_member_parent_id;
    }

    function _set_member_unix_group() {
      $member_id = $this->get_member_id();
      $sql = "SELECT unique_unix_group_name FROM ".
        "red_member INNER JOIN ".
        "red_unique_unix_group ON ".
        "red_member.unique_unix_group_id = ".
        "red_unique_unix_group.unique_unix_group_id WHERE ".
        "member_id = $member_id";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      if(empty($row))  {
        $hosting_order_id = $this->get_hosting_order_id();
        $this->set_error("Hosting order: $hosting_order_id is not related to a red_member record with a valid unique_unix_group_id.",'system');
        return false;
      }
      $this->_member_unix_group_name = $row[0];
      return true;
    }

    function get_member_unix_group_name() {
      return $this->_member_unix_group_name;
    }

    function get_hosting_order_unix_group_name() {
      return $this->_hosting_order_unix_group_name;
    }

    function get_hosting_order_unix_group_id() {
      return intval($this->_hosting_order_unix_group_id);
    }

    function _set_hosting_order_identifier() {
      $hosting_order_id = $this->get_hosting_order_id();
      if(empty($hosting_order_id))  {
        return null;
      }
      $sql = "SELECT hosting_order_identifier FROM  ".
        "red_hosting_order WHERE ".
        "red_hosting_order.hosting_order_id = $hosting_order_id";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      $this->_hosting_order_identifier = $row[0];
    }

    function get_hosting_order_identifier() {
      return $this->_hosting_order_identifier;
    }

    // Server only methods
    function execute() {
      if(!$this->node_sanity_check()) {
        $message = 'Cannot do anything. Failed sanity check.';
        $this->set_error($message,'system');
        $this->set_item_status('hard-error');
        $this->_commit_to_db();
        return false;
      }
      $status = $this->get_item_status();

      // Validate to ensure nobody snuck something 
      // into the database and to run any node mode
      // only validation tests
      if(!$this->validate()) {
        $this->auto_set_item_status();
      }
      else {
        // If we validate, proceed
        switch($status)  {
          case 'pending-delete':
            if($this->delete()) {
              $this->set_item_status('deleted');
            } else {
              $this->auto_set_item_status();
            }
            break;
          case 'pending-update':
            if($this->update()) {
              $this->set_item_status('active');
            } else {
              $this->auto_set_item_status();
            }
            break;
          case 'pending-insert':
            if($this->insert()) {
              $this->set_item_status('active');
            } else {
              $this->auto_set_item_status();
            }
            break;
          case 'pending-restore':
            if($this->restore()) {
              $this->set_item_status('active');
            } else {
              $this->auto_set_item_status();
            }
            break;
          case 'pending-disable':
            if($this->disable()) {
              $this->set_item_status('disabled');
            } else {
              $this->auto_set_item_status();
            }
            break;
          }
      }  
      // Don't run the public commit_to_db() function
      // because it will refuse to commit if we fail
      // to validate. However when running in node
      // mode we want to commit the item_status change
      // to "error" regardless of validation problems.
      if(!$this->_commit_to_db()) return false;

      // If all went well (status = active) then run the
      // post execute function
      if($this->get_item_status() == 'active') {
        if(!$this->post_execute()) {
          $this->auto_set_item_status();
          if(!$this->_commit_to_db()) return false;
        }
      }
      return true;
    }

    function delete() {
      // should be overriden in child class
      return false;

    }

    function insert() {
      // should be overriden in child class
      return false;

    }

    function update() {
      // should be overriden in child class
      return false;

    }

    function restore() {
      // should be overriden in child class
      // FIXME this should be set to false
      // Only set to true while moving sites
      return true;

    }

    function post_execute() {
      return true;
    }

    function node_sanity_check() {
      // This should be overridden in the child class
      // if relevant (how could it not be relevant?).
      // True if the necessary files or database are
      // writable, false otherwise
      return true;
    }

    /*
     * This function can be called by node classes if 
     * the item modifies the data for another item and needs
     * the other item to be updated to get the new info
     */
    function node_update_related_item($item_id,$backend) {
      // Build generic construction options from existing object
      $construction_options = array(  
        'mode' => 'node', 
        'src_path' => $this->_src_path,
        'common_src_path' => $this->_common_src_path,
        'conf_path' => $this->_conf_path,
        'sql_resource' => $this->_sql_resource,
        'backends' => $backend,
      );

      $item_id = intval($item_id);
      // include errors in case we are reloading after a boo boo
      $sql = "SELECT * FROM red_item WHERE item_id = $item_id ".
        "AND (item_status = 'active' OR item_status = 'soft-error' ".
        "OR item_status = 'hard-error')";
      $result = $this->_sql_query($sql);

      $rs = $this->_sql_fetch_assoc($result);

      // artificially set to pending update to trigger update
      $rs['item_status'] = 'pending-update';
      $construction_options['rs'] =& $rs;

      $item =& red_item::get_red_object($construction_options);

      if(!isset($item) || empty($item)) return false;

      $item->set_site_dir_template($this->_site_dir_template);
      return $item->execute();
    }

    // This function should only be called if a function
    // returns false. Set the item_status based on
    // existence of errors
    function auto_set_item_status() {
      if(count($this->get_errors('all','soft')) > 0) {
        $this->set_item_status('soft-error');
        // overwrite if there are also hard errors
      }
      elseif(count($this->get_errors('all','hard')) > 0) {
        $this->set_item_status('hard-error');
      }
      else {
        // shouldn't be here
        $this->set_error('Something went wrong, but no hard or soft errors were set','system');
        $this->set_item_status('hard-error');
      }
    }

    // Child node classes can use this function to read
    // in a config file. The child class should define an 
    // array called _config_variables that includes each
    // config value in the conf file. In addition, the
    // child class should create an internal variable
    // (prefaced with an underscore) for each of these 
    // variables.
    function _set_config_values($conf_file) {
      if(!red_check_file($conf_file)) {
        echo("Failed to load $conf_file. When running as root the file must owned and only be writable by root.\n");
        return false;
      }
      // FIXME the config file gets loaded once for every instance
      // seems wasteful, especially since the script could create
      // many instances per every time it is run...
      require($conf_file);
      foreach($this->_config_variables as $k => $v) {
        $variable = "_$v";
        if(isset($$v))  { 
          $this->$variable = $$v;
        }
      }
      return true;
    }
    // Client only methods

    function _determine_appropriate_host() {
      // Special case for mysql databases.
      if ($this->get_service_id() == 20) {
        // Check this hosting order host and see if proxysql is enabled
        // on this host.
        $host = addslashes($this->get_hosting_order_host());
        $sql = "SELECT server_proxysql FROM red_server WHERE server = '$host'";
        $result = $this->_sql_query($sql);
        $row = $this->_sql_fetch_row($result);
        if ($row[0] == 1) {
          // We have proxysql enabled. We can fall through and we will take the
          // service_default configured for the service.
        }
        else {
          // We have to intervene and return the hosting_order_host instead so the
          // database is installed on the mosh itself.
          return $host;
        }
      }
      // Check the service
      $sql = "SELECT service_default_host FROM red_service WHERE ".
        "service_id = " . $this->get_service_id();
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      $default_host = $row[0];

      if($default_host != 'default') return $default_host;

      $sql = "SELECT hosting_order_host FROM red_hosting_order WHERE ".
        "hosting_order_id = " . $this->get_hosting_order_id();
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      return $row[0];
    }

    function notify_host() {
      if($this->_notify_host) {
        $bg = null;
        if($this->get_notify_background_process()) {
          $bg = '&';
        }
        $cmd = escapeshellcmd($this->get_notify_cmd()); 
        if(preg_match('#ssh$#', $cmd)) {
          $domain = $this->get_item_host();
          if($domain == '') return true;
          $user = $this->get_notify_user();
          $login = escapeshellarg("$user@$domain");
          $ret = exec("HOME=/var/www $cmd -T $login > /dev/null 2>&1 $bg",$output,$response);
        } 
        else {
          // We assume we are on the same server as the host (e.g. testing),
          // so no need for ssh.
          $ret = exec("$cmd $bg",$output,$response);
        }
        if($response != 0)  {  
          $error = "Failed ssh trigger: " . implode(' ',$output);
          $this->set_error($error,'system');
          return false;
        }
      }
      return true;
    }

      

    function is_in_modifiable_state() {
      if(preg_match('/^(pending-|hard-error|deleted|transfer-limbo)/',$this->get_item_status()) > 0) 
        return false;
      return true;
    }

    function is_in_deletable_state() {
      if(preg_match('/^(hard-error|deleted|pending-)/',$this->get_item_status()) > 0) 
        return false;
      return true;
    }
    function is_in_disableable_state() {
      if(preg_match('/^(hard-error|deleted|pending-)/',$this->get_item_status()) > 0) 
        return false;
      return true;
    }
    function commit_to_db() {
      // Update the last modified time
      $this->set_item_modified(date('Y-m-d H:i:s'));
      return parent::commit_to_db();
    }
          
    // This function actually commits to db
    function _commit_to_db() {
      // call parent
      if(!parent::_commit_to_db()) return false;
        
      // update child table
      // The node should never touch the child table,
      // it should only update red_item (this is a security
      // precaution - the db user should not have update
      // privs to the child tables).
      if($this->_mode == 'ui') {
        $sql = $this->_get_sql($this->_child_table);
        if(!$this->_sql_query($sql)) return FALSE;
      }

      return TRUE;
    }

    function _pre_commit_to_db() {
      $this->set_item_quota(red_machine_readable_bytes($this->get_item_quota()));
      $host = $this->get_item_host();
      if(empty($host)) {
        // If host is empty, then it means it is a database only
        // item, so there should be no node notification and
        // the item will be automatically active or deleted
        $this->_notify_host = false;
        if($this->_delete) {
          $this->set_item_status('deleted');
        }
        else {
          $this->set_item_status('active');
        }
      }
      elseif($this->exists_in_db()) {
        // It already exists - set appropriate item_status

        // Only set item_status if we're in ui mode
        // node mode functions will set it otherwise
        if($this->_mode == 'ui') {
            
          // If we're deleting an existing record
          // this flag is set by the ui (not set internally)
          // with the set_delete_flag().
          if($this->_delete) {
            if(!$this->is_in_deletable_state()) {
              $message = 'You cannot delete a record that is '.
                'already deleted or has a status set to '.
                'error or pending. Please wait until the '.
                'status changes to make your change. '.
                'If the status is set to soft-error, please '.
                'read the error message and re-edit your record '.
                'to fix it. Then, if you want, you can delete it. '.
                'If the status is set to hard-error please '.
                'notify support.';
              $this->set_error($message,'system');
              return false;
            }
            else {
              $this->set_item_status('pending-delete');
            }
          } 
          elseif($this->_disable) {
            if(!$this->is_in_disableable_state()) {
              $message = 'You cannot disable a record that is '.
                'already deleted or has a status set to '.
                'error or pending. Please wait until the '.
                'status changes to make your change. '.
                'If the status is set to soft-error, please '.
                'read the error message and re-edit your record '.
                'to fix it. Then, if you want, you can disable it. '.
                'If the status is set to hard-error please '.
                'notify support.';
              $this->set_error($message,'system');
              return false;
            }
            else {
              $this->set_item_status('pending-disable');
            }
          }
          elseif($this->get_item_status() == 'soft-error') {
            // convert all soft-errors to pending-restore because
            // we have no idea whether it was set to pending-insert
            // or pending-update before the node switched it to 
            // soft-error.
            $this->set_item_status('pending-restore');
          }
          // Otherwise, make sure it is modifiable 
          elseif(!$this->is_in_modifiable_state()) {
            $status = $this->get_item_status();
            if($status == 'hard-error') {
              $message = "The record has status set to hard-error. ".
                "Please open a support ticket!";
            } else {
              $message = "You cannot modify a record that with ".
                "the status set to $status.";
            }
            $this->set_error($message,'system');
            return false;

          }
          else {
            $this->set_item_status('pending-update');
          }
        }
      }
      return true;
    }

    function _post_commit_to_db() {
      if($this->_sql_action == 'insert') {
        // Always notify in inserts (only ui does inserts).
        return $this->notify_host();
      }
      // Only notify if we're the ui (node shouldn't do it)
      if($this->_mode == 'ui') return $this->notify_host();
    }


    function is_field_value_unique($field,$table,$value) {
      // don't count deleted items or items in limbo
      $sql = "SELECT red_item.item_id, $field FROM $table INNER JOIN red_item ".
        "ON $table.item_id = red_item.item_id WHERE $field = '$value' AND ".
        "red_item.item_status != 'deleted' AND item_status != ".
        "'transfer-limbo'";
      $result = $this->_sql_query($sql);

      // If no records exist...
      if($this->_sql_num_rows($result) == 0) return true;
      if($this->_sql_num_rows($result) == 1) {
        $row = $this->_sql_fetch_row($result);
        // If the record that exists is the record we're dealing with
        // return true
        if($row[0] == $this->get_item_id()) return true;
      }
      // otherwise return false
      return false;
    }

    function in_limbo() {
      // check to see if we're in transfer-limbo
      // we're in transfer limbo if there's a hosting order
      // record with hosting_order_status set to transfer-limbo
      // and identifier set to our identifier
      $identifier = $this->get_hosting_order_identifier();
      $sql = "SELECT hosting_order_id FROM red_hosting_order WHERE ".
        "hosting_order_identifier = '$identifier' AND " .
        "hosting_order_status = 'transfer-limbo'";
      $result = $this->_sql_query($sql);
      if($this->_sql_num_rows($result) == 0) return false;
      return true;
    }

    function get_hosting_order_host() {
      $hosting_order_id = $this->get_hosting_order_id();
      $sql = "SELECT hosting_order_host FROM red_hosting_order ".
        "WHERE hosting_order_id = $hosting_order_id";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      return $row[0];
    }

    function validate() {
      // don't validate on delete when status is transfer-limbo - 
      // they values won't validate because of duplicates, but that's ok.
      if($this->_delete && $this->get_item_status() == 'transfer-limbo') 
        return true;

      $this->track_disk_usage_validation();
      return parent::validate();
    }

    // Ensure disk usage relatd values are sane.
    function track_disk_usage_validation() {
      if (!$this->_track_disk_usage) return TRUE;
      if ($this->_delete) return TRUE;

      // quota can't be set lower then current disk usage.
      $disk_quota = red_machine_readable_bytes($this->get_item_quota());
      if ($this->get_item_quota() != 0 && $this->get_item_disk_usage() > $disk_quota) {
        $disk_usage_readable = red_human_readable_bytes($this->get_item_disk_usage());
        $this->set_error(red_t("You cannot set the quota lower then your current disk usage, which is @disk_usage", [ '@disk_usage' => $disk_usage_readable ]),'validation');
      } 

      // If the member quota is set, we can't exceed it.
      if ($this->get_member_quota() > 0) {
        $amount_over_quota = $this->get_amount_over_quota(); 
        if ($amount_over_quota > 0) {
          $quota_over = red_human_readable_bytes(($amount_over_quota));
          $this->set_error(red_t("Setting the quota for this item exceeds your membership quota by @quota_over", [ '@quota_over' => $quota_over ]),'validation');
        }
      }

      // Notification percent should be between 0 and 100.
      if ($this->get_item_quota_notify_percent() < 0 || $this->get_item_quota_notify_percent() > 100) {
        $this->set_error(red_t("Please set your notification percent to a number between 0 and 100"),'validation');
      }

      // Ensure the notification is in the future. 
      if (!empty($this->get_item_quota_notify_date())) {
        $date = strtotime($this->get_item_quota_notify_date());
        // The validation above should catch improperly formatted dates
        if ($date !== FALSE) {
          // The cron job runs once an hour, so let's give ourselves an hour of leeway here.
          if ($date + 3600 < time()) {
            $this->set_error(red_t("Your notification date is in the past."),'validation');
          }
        }
      }

      // Ensure the quota is parseable
      if (red_machine_readable_bytes($this->get_item_quota()) === FALSE) {
        $this->set_error(red_t("The quota does not seem to be in a readable format. Please enter a number followed by g, m, or k."),'validation');
      }
    }
    function get_edit_item_quota() {
      if (!$this->_datafields['item_quota']['user_insert'] && !$this->exists_in_db()) {
        return NULL;
      }
      elseif (!$this->_datafields['item_quota']['user_update'] && $this->exists_in_db()) {
        return NULL;
      }
      // Convert to human readable before displaying.
      $value = red_human_readable_bytes($this->get_item_quota());
      $attributes = [ 'class' => 'form-control', 'id' => 'item_quota' ];
      $type = 'text';
      return $this->_html_generator->get_input('sf_item_quota',$value, $type, $attributes);
    }

    function get_read_item_quota() {
      // Convert to human readable before displaying.
      $value = red_human_readable_bytes($this->get_item_quota());
      if ($value == 0) {
        return red_t("Not set");
      }
      return $value;
    }

    function get_read_item_disk_usage() {
      // Convert to human readable before displaying.
      return red_human_readable_bytes($this->get_item_disk_usage());
    }

    // Some modules allow a variable to be set in the
    // configuration file that will be different depending
    // on the parent_id, member_id, or hosting_order_id.
    //
    // These variables are set in the form of:
    // $var['default'] = 'some value';
    // $var['hosting_order_id'][123] = 'some other value';
    // $var['hosting_order_id'][434] = 'some other value';
    // $var['member_id'][242] = 'some other value';
    // $var['member_parent_id'][2222] = 'some other value';
    //
    // If we are using one of these variables, and it is
    // providing a file path, this function will determine
    // if all file paths exist
    function variable_conf_files_exists($var,&$message) {
      if(!is_array($var)) {
        if(!file_exists($var)) {
          $message = "I was not able to locate the following file: " . 
            $var . ".";
          return false;
        }
      } else {
        foreach($var as $k => $v) {
          if($k == 'default') {
            if(!file_exists($var[$k])) {
              $message = "I was not able to find the default conf file. Trying: " . 
                $var[$k] . ".";
              return false;
            }
          } else {
            if(is_array($v)) {
              foreach($v as $id => $path) {
                if(!file_exists($path)) {
                  $message = "I was not able to find the conf file with $k set to $id doesn't exist. Trying: " . 
                    $path . ".";
                  return false;
                }
              }
            }
          }
        }
      }
      return true;
    }

    // see above for explanation
    function choose_var_from_multi_dimensional_config($config) {
      // if it's not an array, return the variable. It should be the string which is the only
      // variable possible
      if(!is_array($config)) return $config;

      // start specific, then go general
      $keys = array('hosting_order_id','member_id','member_parent_id');
      foreach($keys as $key) {
        if(array_key_exists($key,$config)) {
          $func = 'get_' . $key;
          // e.g. get_hosting_order_id()
          $value = $this->$func();
          // if we have a match return it
          if(array_key_exists($value,$config[$key])) {
            return $config[$key][$value];
          }
        }
      }
      // no matches, try to return the default
      if(array_key_exists('default',$config)) {
        return $config['default'];
      }
      return false;
    }

    function get_service_id_for_table($table) {
      $table = addslashes($table);
      $sql = "SELECT service_id FROM red_service WHERE service_table = '$table'";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      return $row[0];
    }

    function get_mountpoint_for_host() {
      $host = addslashes($this->get_item_host());
      $sql = "SELECT mountpoint FROM red_server WHERE server = '$host'";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      return $row[0];
    }

    function get_member_quota() {
      return red_get_member_quota($this->_sql_resource, $this->get_member_id());
    }

    // If a quota is set for this membership, calculate their quotas for
    // user accounts and web sites and return the amount that
    // they are over or 0 if they are not over.
    function get_amount_over_quota() {
      $total = red_get_quota_for_all_items($this->_sql_resource, $this->get_member_id(), $this->get_item_id()); 
      $total += red_get_quota_for_all_vps($this->_sql_resource, $this->get_member_id());
      $member_quota = $this->get_member_quota();
      $diff = $total + intval($this->get_item_quota()) - $member_quota;

      if ($diff <= 0) {
        return 0;
      }
      return $diff;
    }

  }
}
?>
