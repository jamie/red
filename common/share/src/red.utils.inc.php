<?php
// Matchers
// logins may have an @ sign

// FIXME - this pattern should be 1,255 - lowering because of pcre3 bug
// that reports "Compilation failed: repeated subpattern is too long
define ('RED_LOGIN_REGEXP', '([a-z0-9\-_.@]){1,32}');
// the user portion of an email address may not have the @ sign
// FIXME - this pattern should be 1,251 - lowering because of pcre3 bug
// that reports "Compilation failed: repeated subpattern is too long
define ('RED_EMAIL_USER_REGEXP', '([a-zA-Z0-9\-_.]){1,100}');
// we should allow emails to be forwarded to addresses with a plus in them
// see: https://support.mayfirst.org/ticket/4794
define ('RED_EMAIL_FORWARD_USER_REGEXP', '([a-zA-Z0-9\-_.+]){1,100}');
define ('RED_IP4_REGEXP', '(([0-9]){1,3}\.){3}([0-9]){1,3}');
define ('RED_IP6_GROUP_REGEXP', '([0-9abcdefABCDEF]){1,4}');
define ('RED_IP6_REGEXP', '(::1|::FFFF:'.RED_IP4_REGEXP.'|('.RED_IP6_GROUP_REGEXP.':+){1,15}'.RED_IP6_GROUP_REGEXP.')');
define ('RED_MD5_REGEXP', '^\$1\$(.*){31}');
define ('RED_SHA512_REGEXP', '\$6\$rounds=[0-9]{4,9}(.*){93,}');
define ('RED_DOMAIN_LABEL_REGEXP', '([\-0-9a-zA-Z_]){1,63}');
define ('RED_DOMAIN_WILDCARD_LABEL_REGEXP', '([\-0-9a-zA-Z*]){1,63}');
# Loose also allows _ which is not stricly allowed in domain names
# but is allowed (and common) in txt records
define ('RED_DOMAIN_LOOSE_WILDCARD_LABEL_REGEXP', '([_\-0-9a-zA-Z*]){1,63}');
define ('RED_DOMAIN_LOOSE_LABEL_REGEXP', '([_\-0-9a-zA-Z]){1,63}');
define ('RED_DOMAIN_REGEXP', '(localhost|(' . RED_DOMAIN_LABEL_REGEXP . '\.){1,18}' . RED_DOMAIN_LABEL_REGEXP . ')');   // arbitrarily only allowing 18 groups of labels - e.g. sub.sub.sub.sub.domain.org (since this is used to match the zone file for ipv6, which can have 18 parts to it).
define ('RED_DOMAIN_WILDCARD_REGEXP', '(localhost|(' . RED_DOMAIN_WILDCARD_LABEL_REGEXP . '\.){1,6}' . RED_DOMAIN_LABEL_REGEXP . ')');   // arbitrarily only allowing 6 groups of labels - e.g. sub.sub.sub.sub.domain.org
define ('RED_DOMAIN_LOOSE_WILDCARD_REGEXP', '(localhost|(' . RED_DOMAIN_LOOSE_WILDCARD_LABEL_REGEXP . '\.){1,6}' . RED_DOMAIN_LOOSE_LABEL_REGEXP . ')');   // arbitrarily only allowing 6 groups of labels - e.g. sub.sub.sub.sub.domain.org
define ('RED_DOMAIN_LOOSE_REGEXP', '(localhost|(' . RED_DOMAIN_LOOSE_LABEL_REGEXP . '\.){1,6}' . RED_DOMAIN_LOOSE_LABEL_REGEXP . ')');   // arbitrarily only allowing 6 groups of labels - e.g. sub.sub.sub.sub.domain.org
define ('RED_EMAIL_REGEXP', RED_EMAIL_USER_REGEXP.'@'.RED_DOMAIN_REGEXP);
define ('RED_EMAIL_FORWARD_REGEXP', RED_EMAIL_FORWARD_USER_REGEXP.'@'.RED_DOMAIN_REGEXP);
define ('RED_PATH_REGEXP', '([a-zA-Z0-9\-_./]){1,255}');
define ('RED_OPENID_REGEXP','https?:\/\/' . RED_DOMAIN_REGEXP . RED_PATH_REGEXP);
define ('RED_OPENID_OR_LOGIN_REGEXP', RED_OPENID_REGEXP . '|' . RED_LOGIN_REGEXP);
define ('RED_MULTIPLE_OPENID_OR_LOGIN_MATCHER', '#^((' . RED_OPENID_OR_LOGIN_REGEXP . ')(,|, |)){1,}$#');
define ('RED_MULTIPLE_EMAIL_REGEXP','('.RED_EMAIL_REGEXP.'(,|, |)){1,}');
define ('RED_MULTIPLE_EMAIL_AND_LOGIN_REGEXP','(('.RED_EMAIL_FORWARD_REGEXP.'|'.RED_LOGIN_REGEXP.')(,|, |)){1,}');
define ('RED_MULTIPLE_DOMAIN_REGEXP','('.RED_DOMAIN_REGEXP.'( |)){1,}');
define ('RED_MULTIPLE_DOMAIN_WILDCARD_REGEXP','('.RED_DOMAIN_WILDCARD_REGEXP.'( |)){1,}');
define ('RED_MYSQL_DB_REGEXP', '([a-zA-Z]+([a-zA-Z_0-9]+)?){1,64}');
define ('RED_DOMAIN_MATCHER', '/^' . RED_DOMAIN_REGEXP . '$/');
# unix groups are used to create default .mayfirst.org domains, so must
# match the lowest common denominator
define ('RED_UNIX_GROUP_MATCHER', '/^' . RED_DOMAIN_LABEL_REGEXP . '$/');
define ('RED_SERVER_NAME_MATCHER', '/^('.RED_DOMAIN_REGEXP.'|'.RED_DOMAIN_LABEL_REGEXP.')$/');
define('RED_IP_MATCHER','/^[0-9.:a-fA-F]+$/');
define('RED_IP_WILDCARD_MATCHER','/^('.RED_IP4_REGEXP.'|'.RED_IP6_REGEXP.'|\*)$/');
define ('RED_DOMAIN_WILDCARD_MATCHER', '/^' . RED_DOMAIN_WILDCARD_REGEXP . '$/');
define ('RED_DOMAIN_LOOSE_WILDCARD_MATCHER', '/^' . RED_DOMAIN_LOOSE_WILDCARD_REGEXP . '$/');
define ('RED_DOMAIN_LOOSE_MATCHER', '/^' . RED_DOMAIN_LOOSE_WILDCARD_REGEXP . '$/');
define ('RED_MULTIPLE_DOMAIN_MATCHER', '/^' . RED_MULTIPLE_DOMAIN_REGEXP . '$/');
define ('RED_MULTIPLE_DOMAIN_WILDCARD_MATCHER', '/^' . RED_MULTIPLE_DOMAIN_WILDCARD_REGEXP . '$/');
define ('RED_MULTIPLE_EMAIL_MATCHER', '/^' . RED_MULTIPLE_EMAIL_REGEXP . '$/');
// either an email address or just @domain.org for a catchall
define ('RED_VIRTUAL_EMAIL_MATCHER', '/^(' . RED_EMAIL_USER_REGEXP . '|)\@'.RED_DOMAIN_REGEXP .'$/');
define ('RED_EMAIL_MATCHER', '/^' . RED_EMAIL_REGEXP . '$/');
define ('RED_LIST_NAME_MATCHER', '/^' . RED_EMAIL_USER_REGEXP . '$/');
define ('RED_MYSQL_DB_MATCHER', '/^'.RED_MYSQL_DB_REGEXP . '$/');
define ('RED_MYSQL_DBS_MATCHER', '/^(:?' . RED_MYSQL_DB_REGEXP . '?:?)*$/');
define ('RED_SQL_PRIV_MATCHER', '/^(full|read)$/');
define ('RED_MYSQL_USER_MATCHER', '/^([a-zA-Z]+([a-zA-Z_0-9]+)?){1,16}$/');
define ('RED_LOGIN_MATCHER', '/^' . RED_LOGIN_REGEXP . '$/');
define ('RED_TINYINT_MATCHER', '/^(25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?)$/');
define ('RED_SSH_PUBLIC_KEY_MATCHER', '/^ssh.*/');
define ('RED_EMAIL_RECIPIENT_MATCHER', '/^' . RED_MULTIPLE_EMAIL_AND_LOGIN_REGEXP . '$/');
define ('RED_ID_MATCHER', '/^[0-9]+$/');
define ('RED_INT_MATCHER', '/^[0-9]+$/');
define ('RED_BYTES_MATCHER', '/^[0-9.]+([bkmg]b?)?$/i');
define ('RED_DECIMAL_MATCHER', '/^[0-9]+\.[0-9]+$/');
define ('RED_MONEY_MATCHER', '/^[0-9]+(\.[0-9]{2})?$/');
define ('RED_STATUS_MATCHER',  '/^(active|pending-update|pending-delete|pending-insert|deleted|hard-error|soft-error|pending-restore|transfer-limbo|disabled|pending-disable)$/');
define ('RED_MEMBER_BENEFITS_LEVEL_MATCHER',  '/^(basic|standard|extra)$/');
define ('RED_MEMBER_STATUS_MATCHER',  '/^(active|inactive|suspended)$/');
define ('RED_INVOICE_STATUS_MATCHER',  '/^(unpaid|paid|void|eaten|review)$/');
define ('RED_INVOICE_TYPE_MATCHER',  '/^(membership|benefits)$/');
define ('RED_WEB_APP_UPDATE_MATCHER', '/(none|core)/' );
define ('RED_BOOL_MATCHER', '/^[yn]$/');
define ('RED_DATE_MATCHER', '/^(\d){4}-(\d){2}-(\d){2}$/');
define ('RED_CURRENCY_MATCHER', '/^USD|MXN$/');
define ('RED_DATETIME_MATCHER', '/^(\d){4}-(\d){2}-(\d){2}\s(\d){2}:(\d){2}(:(\d){2})?$/');
define ('RED_TEXT_MATCHER', '/^.*$/');
define ('RED_DNS_TEXT_MATCHER', '/^.{1,1024}$/');
define ('RED_ANYTHING_MATCHER', '/.*/');
define ('RED_PASSWORD_HASH_MATCHER', '/^' . RED_MD5_REGEXP . '|' . RED_SHA512_REGEXP . '$/');
define ('RED_HOST_MATCHER', '/^(' . RED_DOMAIN_REGEXP . '(|:([0-9]){1,5})(|,)){1,}$/'); // like domain, but includes optional :port and can have multiple defined, separated by commas 
define ('RED_WEB_CONF_KEY_MATCHER', '#^(ServerAdmin|Alias|ScriptAlias|ScriptAliasMatch|DirectoryIndex|RedirectCond|RedirectMatch|RedirectEngine|RewriteEngine|RewriteCond|RewriteRule|RewriteMap|Options|AllowOverride|Order|Allow|Deny|<Directory|</Directory>|<Files|</Files>|<FilesMatch|</FilesMatch>|RewriteBase|Redirect|AuthUserFile|AuthName|AuthType|require|Require|<Location|</Location>|<LocationMatch|</LocationMatch>|SetEnv|AddType|SuexecUserGroup|LogLevel|Action|AddHandler|Satisfy|AuthOpenIDEnabled|AuthOpenIDDBLocation|AuthOpenIDTrusted|AuthOpenIDDistrusted|AuthOpenIDUseCookie|AuthOpenIDTrustRoot|AuthOpenIDCookieName|AuthOpenIDLoginPage|ErrorDocument|PerlAccessHandler|SetHandler|PerlHandler|DAV|FcgidMaxRequestLen|FcgidMaxProcessesPerClass|ProxyErrorOverride|ProxyPass|SSLVerifyClient|SSLVerifyDepth|SSLCACertificateFile|Header|RequestHeader|<If|</If>)$#i');
define ('RED_WEB_CONF_STATUS_MATCHER', '/^(enabled|disabled)$/');
define ('RED_PATH_MATCHER', '#^' . RED_PATH_REGEXP . '$#');
define ('RED_DNS_TYPE_MATCHER', '/^(host|a|mx|text|cname|srv|aaaa|ptr|mailstore|sshfp)$/');
define ('RED_YES_NO_MATCHER', '/^(y|n)$/');
define ('RED_ACTIVE_DELETED_MATCHER', '/^(active|deleted)$/');
define ('RED_HOSTING_ORDER_STATUS_MATCHER', '/^(active|deleted|disabled)$/');
define ('RED_AUTO_RESPONSE_ACTION_MATCHER', '/^(respond_and_deliver|respond_only)$/');
define('RED_MEMBER_TERM_MATCHER','/^(month|year)$/');
define('RED_PAYMENT_METHOD_MATCHER','/^(credit|check|cash|transfer|other)$/');
define('RED_MEMBER_TYPE_MATCHER','/^(individual|organization)$/');
define('RED_WEB_APP_MATCHER','/^(drupal9|drupal5|drupal6|drupal7|wordpress|backdrop)$/');
define('RED_INTERFACE_LANG_MATCHER','/^(en_US|es_MX)$/');
define('RED_TAG_MATCHER', '/^[a-zA-Z\-0-9.]+$/');
define('RED_COUNTRY_CODE_MATCHER','/^(ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|ss|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$/');
define('RED_STREET_NUMBER_MATCHER','/^[a-zA-Z0-9\-]+$/');
define('RED_PROVINCE_MATCHER',"/^[a-zA-Z.'\- ]+$/");
define('RED_ZERO_ONE_MATCHER',"/^(0|1)$/");

// Explanations
define ('RED_MYSQL_DB_EXPLANATION', red_t('Database names must be between 1 and 64 characters, start with a letter, and only contain letters, numbers and underscores.'));
define ('RED_MYSQL_DBS_EXPLANATION', red_t('Multiple database names must be between 1 and 64 characters, start with a letter, and only contain letters, numbers and underscores. Different names must be separated by a colon.'));
define ('RED_MYSQL_USER_EXPLANATION', red_t('Database user names must be between 1 and 16 characters, start with a letter, and only contain letters, numbers and underscores.'));
define ('RED_SQL_PRIV_EXPLANATION', red_t('Privileges can only be full or read.'));
define ('RED_EMAIL_EXPLANATION', red_t('Valid emails must be in the form user@domain.org with only letters, numbers, underscores, periods and dashes.'));
define('RED_IP_EXPLANATION', red_t('Valid IP addresses only have numbers, periods, colons or the letters a-f.'));
define('RED_IP_WILDCARD_EXPLANATION', red_t('Valid IP addresses must have 4 groups 1 - 3 numbers separated by periods, for example: 1.23.456.789, or a wildcard (*).'));
define ('RED_LIST_NAME_EXPLANATION', red_t('List name can only contain letters, numbers, dashes and underscores.'));
define ('RED_VIRTUAL_EMAIL_EXPLANATION', red_t('Valid emails must be in the form user@domain.org with only letters, numbers, underscores, periods and dashes. Or, you may use @domain.org for a catchall email address that will receive mail for any message sent to a user that does not exist.'));
define ('RED_MULTIPLE_OPENID_OR_LOGIN_EXPLANATION', red_t('Please enter a comma separated list of either login names or OpenID addresses (such as http://id.mayfirst.org/user).'));
define ('RED_MULTIPLE_EMAIL_EXPLANATION', red_t('Valid emails must be in the form user@domain.org with only letters, numbers, underscores, periods and dashes. You can enter multiple emails separated by a comma.'));
define ('RED_MULTIPLE_EMAIL_AND_LOGIN_EXPLANATION', red_t('You may enter one or more comma separated lists of email addresses or user logins. Valid emails must be in the form user@domain.org. Emails and login can only have letters, numbers, underscores, periods and dashes.'));
define ('RED_ID_EXPLANATION', red_t('ID values must be all numeric.'));
define ('RED_INT_EXPLANATION', red_t('Integer values must be all numeric, such as 1 or 234, but not 34.5.'));
define ('RED_BYTES_EXPLANATION', red_t('Please enter a whole number, optionally followed by a b for bytes, k for kilobytes, m for megabytes, or g for gigabytes.'));
define ('RED_MONEY_EXPLANATION', red_t('Money values must be all numeric with an optional period followed by two additional numbers.'));
define ('RED_DECIMAL_EXPLANATION', red_t('Decimal values must be all numeric, with a period in the middle.'));
define ('RED_STATUS_EXPLANATION', red_t( 'The following status values are allowed: active, pending-update, pending-delete, pending-insert, pending-restore, deleted, soft-error, and hard-error.'));
define ('RED_HOSTING_ORDER_STATUS_EXPLANATION', red_t( 'The following status values are allowed: active, disabled and deleted.'));
define ('RED_INVOICE_TYPE_EXPLANATION', red_t( 'The following invoice types are allowed: membership, benefits.'));
define ('RED_WEB_APP_UPDATE_EXPLANATION', red_t('Web app updates can only be set to none or core.'));
define ('RED_INVOICE_STATUS_EXPLANATION', red_t( 'The following status values are allowed: unpaid, paid, eaten, void, review.'));
define ('RED_MEMBER_BENEFITS_LEVEL_EXPLANATION', red_t( 'The following levels are allowed: basic, standard, and extra.'));
define ('RED_MEMBER_STATUS_EXPLANATION', red_t( 'The following status values are allowed: active, inactive and suspended.'));
define ('RED_ACTIVE_DELETED_EXPLANATION', red_t( 'The following status values are allowed: active or deleted.'));
define ('RED_DATE_EXPLANATION', red_t('Datetime must be entered as yyyy-mm-dd.'));
define ('RED_CURRENCY_EXPLANATION', red_t('Currency must be either USD (US Dollars) or MXN (Mexican Pesos).'));
define ('RED_DATETIME_EXPLANATION', red_t('Datetime must be entered as yyyy-mm-dd hh:mm:ss.'));
define ('RED_BOOL_EXPLANATION', red_t('Only y and n are allowed.'));
define ('RED_TEXT_EXPLANATION', red_t('That value failed the text test. Were you trying to sneak in a line break or something?'));
define ('RED_DNS_TEXT_EXPLANATION', red_t('A DNS txt field is limited to 1024 characters.'));
define ('RED_ANYTHING_EXPLANATION', red_t('There some kind of strange validation error. You should be able to enter anything you want, but what ever it is you entered did not pass.'));
define ('RED_TINYINT_EXPLANATION', red_t('Please enter a value between 0 and 255.'));
define ('RED_WEB_CONF_STATUS_EXPLANATION', red_t('Status must either be enabled or disabled.'));
define ('RED_SSH_PUBLIC_KEY_EXPLANATION', red_t('A public key should start with ssh.'));
define ('RED_PASSWORD_HASH_EXPLANATION', red_t('There was an error encrypting your password. I was expecting either an md5 hash (begining with $1$) or a SHA512 hash (beginning with $6$).'));
define ('RED_DOMAIN_EXPLANATION', red_t('A valid domain has only numbers, letters, and dashes and at least one period.'));
define ('RED_UNIX_GROUP_EXPLANATION', red_t('A valid group has only letters, numbers and dashes (underscores are not allowed).'));
define ('RED_SERVER_NAME_EXPLANATION', red_t('A valid server name is either a domain or one segment of a domain.'));
define ('RED_DOMAIN_WILDCARD_EXPLANATION', red_t('A valid domain has at least one period and has only letters, numbers and dashes. You may use a single * to denote a wildcard.'));
define ('RED_DOMAIN_LOOSE_WILDCARD_EXPLANATION', red_t('A valid "loose" domain has at least one period and has only letters, numbers, dashes and underscores. You may use a single * to denote a wildcard.'));
define ('RED_DOMAIN_LOOSE_EXPLANATION', red_t('A valid domain has at least one period and has only letters, numbers, dashes and underscores. You may not use a an * in the zone name.'));
define ('RED_MULTIPLE_DOMAIN_EXPLANATION', red_t('A valid domain has at least one period. You can add multiple domains separated by spaces.'));
define ('RED_MULTIPLE_DOMAIN_WILDCARD_EXPLANATION', red_t('A valid domain has at least one period. You can add multiple domains separated by spaces. You can use * as a wild card.'));
define ('RED_LOGIN_EXPLANATION', red_t('Login names can only use letters, number, dashes, periods and underscores.  A login name must not exceed 32 characters.'));
define ('RED_EMAIL_RECIPIENT_EXPLANATION', red_t('Email recipients should either be a valid email address (user@domain.org) or a valid login name. In either case, only letters numbers, dashes, periods, and underscores are allowed. With email address, you may list up to five separated by commas.'));
define ('RED_HOST_EXPLANATION', red_t('A valid host is in the form: host.tld[:port].'));
define ('RED_WEB_CONF_KEY_EXPLANATION', red_t('Only the following values are allowed in web configuration settings: ServerAdmin, Alias, ScriptAlias, ScriptAliasMatch, RedirectCond, RedirectMatch, RedirectEngine, RewriteCond, RewriteRule, RewriteEngine, RewriteMap, AllowOverride, Order, Deny, Options, Allow, Directory (both opening and closing), Files and FilesMatch (both opening and closing), RewriteBase, DirectoryIndex, Redirect, AuthUserFile, AuthName, AuthType, require, Location (both opening and closing), SetEnv, AddType, SuexecUserGroup, ErrorLog, LogLevel, Action, Satisfy, AddHandler, SetHandler, PerlHandler, PerlAccessHandler, ErrorDocument, DAV, FcgidMaxRequestLen,FcgidMaxProcessesPerClass, SSLVerifyClient, SSLVerifyDepth, SSLCACertificateFile, all AuthOpenID related keys, RequestHeader, Header, and If.'));
define ('RED_PATH_EXPLANATION', red_t('A path can only contain numbers, letters, periods, dashes, underscores and forward slashes.'));
define('RED_DNS_TYPE_EXPLANATION', red_t('Only the following dns types are supported: host, a, mx, text, cname, srv, sshfp and aaaa.'));
define('RED_YES_NO_EXPLANATION', red_t('Please enter either \'y\' or \'n\'.'));
define('RED_AUTO_RESPONSE_ACTION_EXPLANATION', red_t('Auto response action should be either respond_and_deliver or respond_only.'));
define('RED_MEMBER_TERM_EXPLANATION', red_t('Term can only be month or year'));
define('RED_PAYMENT_METHOD_EXPLANATION', red_t('Payment method can only be credit, check, cash, other'));
define('RED_MEMBER_TYPE_EXPLANATION', red_t('Type can only be organization or individual.'));
define('RED_WEB_APP_EXPLANATION', red_t('Only Drupal and Mediawiki are supported web apps.'));
define('RED_INTERFACE_LANG_EXPLANATION',red_t("Only es_MX and en_US are supported languages."));
define('RED_COUNTRY_CODE_EXPLANATION',red_t("Please use two character top level domain abbreviations for country."));
define('RED_STREET_NUMBER_EXPLANATION',red_t("Please only use numbers and letters. Apartment numbers, etc. should go in the postal address extra field."));
define('RED_PROVINCE_EXPLANATION',red_t("Province can only have letters, periods, dashes, spaces and single quotes."));
define('RED_TAG_EXPLANATION', red_t("Please limit tags to letters, numbers, period or dash."));
define('RED_ZERO_ONE_EXPLANATION', red_t("Please enter 0 or 1."));

function red_generate_random_password($length = 15) {
  // Round $length to be evenly divisible by 4
  $round = ceil($length/4) * 4;
  // Get contents of /dev/urandom
  $urand = file_get_contents('/dev/urandom', 0, null, -1, ($round/4)*3);
  // Check for problems with /dev/urandom stream
  if (FALSE === $urand) {
    return FALSE;
  }
  // Make sure enough bytes were generated by /dev/urandom
  if (strlen($urand) < ($round/4)*3) {
    return FALSE;
  }
  // Generate a raw sha256 hash of our /dev/urandom output
  $hash = substr(hash('sha256',$urand,$raw_output = true), 0, ($round/4)*3);
  // Get a base64_encoded substring of the hash with specified password length
  $password = substr(base64_encode($hash),0,$length);
  return $password;
}

function red_create_directory_recursively($full_path,$group_writable = FALSE) {
  $dirs = explode('/', $full_path);
  $built_path = '';
  foreach($dirs as $dir) {
    if($dir <> '') {
      $dir = "/$dir";
      if(!file_exists($built_path . $dir)) {
        if(!mkdir($built_path . $dir)) return false;
        if($group_writable) {
          if(!chmod($built_path . $dir,0775)) return false;
        }
      }
      $built_path .= $dir;
    }
  }
  return true;
}

// If $exists is set, it will continue going up the
// file hieararchy until it finds a parent directory
// that exists
function red_get_parent_directory($directory, $exists = FALSE)
{
  // make sure first character is a slash
  if(substr($directory,0,1) != '/') return false;
  if(substr($directory,-1) == '/') $directory = substr($directory,0,-1);
  $last_slash = strrpos($directory,'/');
  $parent = substr($directory,0,$last_slash);
  if(is_dir($parent) || !$exists) return $parent;
  return red_get_parent_directory($parent,$exists);
}

function red_get_service_table_for_service_id($service_id,$resource)
{
  if(empty($service_id)) return false;
  $sql = "SELECT service_table FROM red_service WHERE ".
    "service_id = $service_id";
  $result = red_sql_query($sql,$resource);
  $row = red_sql_fetch_row($result);
  return $row[0];
}

function red_get_service_table_for_item_id($item_id,$resource)
{
  if(empty($item_id)) return false;
  $sql = "SELECT service_table FROM red_service INNER JOIN red_item ".
    "ON red_service.service_id = red_item.service_id WHERE ".
    "red_item.item_id = $item_id";
  $result = red_sql_query($sql,$resource);
  if(red_sql_num_rows($result) == 0) return false;
  $row = red_sql_fetch_row($result);
  return $row[0];
}

function red_preg_match_array($regexp,$array)
{
  reset($array);
  foreach($array as $line) {
    if(preg_match($regexp,$line)) return true;
  }
  return false;
}

function red_append_to_file($file,$content)
{
  if(!$handle = fopen($file,'a')) return false;
  if(false === fwrite($handle,$content)) return false;
  fclose($handle);
  return true;
}

// Insert a line at line number specified
function red_insert_into_file($file,$content,$insert_before_line,$direction = 'bottom',$comment = '#')
{
  $file_contents = red_file($file);

  // add a line for the one we're about to insert
  $total_lines_final = count($file_contents) + 1;

  if($direction == 'bottom') {
    $insert_before_line = $total_lines_final - $insert_before_line;
  }

  // Return error if you want to insert the line at a line number greater
  // than the total lines in the file. In other words, there are 10 lines
  // and I want to insert before line 15 (regardless if it's from the top
  // or the bottom). Note - if there are no lines in the file and you want
  // to insert before line 1 from the top (meaning make it the first line),
  // that's ok. If there are no lines and you want to insert 1 line from
  // the bottom, that's an error - because that is not possible. The
  // resulting file will have the given line as the last line rather than
  // being one line before the last line.
  if($total_lines_final < $insert_before_line || $insert_before_line < 1)
    return false;

  $current_line = 1;
  $new_file_contents = array();
  if(count($file_contents) == 0 && $insert_before_line == 1) {
    // File is empty, insert the line
    $new_file_contents[] = $content;
  }
  else {
    reset($file_contents);
    foreach($file_contents as $file_line) {
      if($current_line == $insert_before_line) {
        $new_file_contents[$current_line] = $content;
        $current_line++;
      }
      $new_file_contents[$current_line] = $file_line;
      $current_line++;
    }
  }
  return red_write_to_file($file,implode('',$new_file_contents),$comment);
}

function red_file($file_path,$comment = '#')
{
  $content = file($file_path);
  // remove the comments
  if(is_array($content)) {
    foreach($content as $k => $v) {
      // remove blank lines and comments
      $v = trim($v);
      if(substr($v,0,1) == $comment || empty($v)) unset($content[$k]);
    }
  }
  return $content;

}

function red_delete_from_file($file,$key,$delimiter,$comment = '#')
{
  $content = red_file($file,$comment);
  $regexp = "/^$key$delimiter/";
  reset($content);
  foreach($content as $key => $line) {
    // delete empty lines and matching lines
    if(preg_match($regexp,$line) || $line == '') unset($content[$key]);
  }
  return red_write_to_file($file,implode('',$content),$comment);
}

function red_update_file($file,$key,$delimiter,$updated_line,$comment = '#')
{
  $content = red_file($file,$comment);
  $regexp = "/^$key$delimiter/";
  reset($content);
  foreach($content as $key => $line) {
    // replace matching lines
    if(preg_match($regexp,$line)) $content[$key] = $updated_line;
  }
  return red_write_to_file($file,implode('',$content),$comment);
}

function red_return_line($file,$key,$delimiter)
{
  $content = red_file($file);
  $regexp = "/^$key$delimiter/";
  reset($content);
  foreach($content as $key => $line) {
    // replace matching lines
    if(preg_match($regexp,$line)) return $line;
  }
  return false;
}

function red_delete_directory_recursively($dir_name)
{
  // thanks  czambran at gmail dot com
  if(empty($dir_name))  {
    return true;
  }
  if(is_dir($dir_name))  {
    $dir = dir($dir_name);
    while($file = $dir->read())  {
      if($file != '.' && $file != '..')  {
        // don't follow symlinks!
        if(is_dir($dir_name.'/'.$file) && !is_link($dir_name.'/'.$file)) {
          red_delete_directory_recursively($dir_name.'/'.$file);
        }
        else  {
          if(!unlink($dir_name.'/'.$file)) return false;
        }
      }
    }
    $dir->close();
    if(!rmdir($dir_name)) return false;
  }
  return true;
}

function red_write_to_file($file,$content,$comment = '#')
{
  if(!$handle = fopen($file,'w')) return false;
  $pre_pend = '';
  if(!is_null($comment)) {
    $pre_pend = "$comment This file was created by Red. Best not to modify\n".
      "$comment by hand unless you know what you're doing.\n";
  }
  $content = $pre_pend . $content;
  if(!fwrite($handle,$content)) return false;
  fclose($handle);
  return true;
}

function red_key_exists_in_file($key,$delimiter,$file_path)
{
  $file = file($file_path);
  if(is_array($file)) {
    $regexp = '/^' . preg_quote($key) . $delimiter . '/';
    return red_preg_match_array($regexp,$file);
  }
  return false;
}

function red_get_all_keys_in_file($delimiter,$file)
{
  $content = file($file);
  $keys = array();
  $regexp = "/^(.*?)$delimiter/";
  foreach($content as $line) {
    if(preg_match($regexp,$line,$matches)) $keys[] = $matches[1];
  }
  return $keys;
}

function red_is_good_password($password)
{
  if(strlen($password) < 6) return false;
  $regexps = array('/[a-zA-Z]/','/[^a-zA-Z]/');
  foreach($regexps as $regexp) {
    if(!preg_match($regexp,$password)) return false;
  }
  return true;
}

// This function is repeated in the red.utils.inc.php file
// with the name red_load_file.
function red_check_file($file,$restriction = 'write-restrict')
{
  if(!file_exists($file)) return false;

  // When running on test docker images, files will not be owned by root
  // since they are mounted from the host. Skip checks if running in docker.
  if (file_exists('/.dockerenv')) {
    return true;
  }
  if(posix_getuid() == 0) {
    // If we're be run with root privs, make sure the config
    // file is only writable by root
    if(fileowner($file) != 0) {
      return false;
    }
    $perms = fileperms($file);
    $group_octal_perms = intval(substr(sprintf('%o',$perms),-2,1));
    $user_octal_perms = intval(substr(sprintf('%o',$perms),-1,1));

    if($group_octal_perms > 5 || $user_octal_perms > 5) return false;
    if($restriction == 'read-restrict') {
      if($group_octal_perms > 3 || $user_octal_perms > 3) return false;
    }

  }
  return true;
}

// Database functions
function red_sql_query($sql,$resource)
{
  if(false === $result = @mysqli_query($resource,$sql)) {
    return false;
  }
  return $result;
}
function red_sql_fetch_row($result)
{
  return @mysqli_fetch_row($result);
}

function red_sql_fetch_assoc($result)
{
  return @mysqli_fetch_assoc($result);
}

function red_sql_insert_id($resource)
{
  return mysqli_insert_id($resource);
}

function red_sql_num_rows($result)
{
  return @mysqli_num_rows($result);
}

function red_sql_error($resource)
{
  return @mysqli_error($resource);
}

function red_chown_symlink($dir_name,$user)
{
  if(posix_getuid() == 0) {
    // PHP can't chown symlinks! grr. It chowns the target
    // Is chown available?
    if($chown_cmd = red_search_path('chown')) {
      if (red_fork_exec_wait($chown_cmd,
          array('--no-dereference',
            '--', // avoids passing weird options if $dir_name or $user start with a -
            $user,
            $dir_name)))
        return false;
    }
    // return true if we can't find chown - it simply means
    // that a symlink will be left owned by root
  }
  return true;
}

function red_chgrp_symlink($dir_name,$group)
{
  if(posix_getuid() == 0) {
    // PHP can't chgrp symlinks! grr. It chowns the target
    // Is chown available?
    if($chgrp_cmd = red_search_path('chgrp')) {
      if (red_fork_exec_wait($chgrp_cmd,
          array('--no-dereference',
            '--', // avoids passing weird options if $dir_name or $group start with a -
            $group,
            $dir_name)))
        return false;
    }
    // return true if we can't find chgrp - it simply means
    // that a symlink will be left with a group of root
  }
  return true;
}

function red_search_path($arg)
{
  // looking for anything executable on the path
  $paths = explode(":", getenv('PATH'));
  foreach($paths as $p) {
    $f = $p."/".$arg;
    if (file_exists($f) && ($s = stat($f)) && ($s['mode'] & 0111))
      return $f;
  }
}

function red_make_world_writable_recursively($dir_name)
{
  if(empty($dir_name))  {
    return true;
  }
  if(file_exists($dir_name))  {
    $dir = dir($dir_name);
    while($file = $dir->read())  {
      if($file != '.' && $file != '..')  {
        // don't follow symlinks!
        if(is_dir($dir_name.'/'.$file) && !is_link($dir_name.'/'.$file)) {
          if(!chmod($dir_name.'/'.$file,0777)) return false;
          red_make_world_writable_recursively($dir_name.'/'.$file);
        }
        elseif(is_file($dir_name.'/'.$file)) {
          if(!chmod($dir_name.'/'.$file,0666)) return false;
        }
      }
    }
    $dir->close();
    if(is_dir($dir_name))  {
      if(!chmod($dir_name.'/'.$file,0777)) return false;
    }
    else {
      if(!chmod($dir_name.'/'.$file,0666)) return false;
    }
  }
  return true;
}

function red_mail($to,$subject,$body,$headers = null)
{
  return mail($to,$subject,$body, $headers);
}

/*
 * Return true if a single ping packet is successful
 *
 */
function red_single_ping_result($ip)
{
  $cmd = "/bin/ping";
  $args = '-q -c 1 -w 1 ' . escapeshellarg($ip);

  $return_line = exec("$cmd $args",$output,$return_value);
  if($return_value !== 0) {
    return false;
  }
  return true;
}

/*
 * Check an IP to see if we have connectivity
 */
function red_check_online_status($tries = 5,$ip = '1.1.1.1')
{
  $try = 0;
  while($try < $tries) {
    if(red_single_ping_result($ip)) return true;
    $try++;
  }
  return false;
}

/*
 *
 * Check domain to see if it has an mx record
 * that resolves to the given server.
 *
 * This function does not need to be called
 * from the $server being checked. It can be
 * called from anywhere.
 *
 * This function will fail in the event that $check_local
 * is false and an mx record resolves to a domain that
 * resolves to an IP address that is different from the IP
 * address of the $server, but still is hosted by
 * the $server. This corner case can only be determined
 * by running on the server in question (see function
 * red_ip_assigned_to_local_server() function below).
 *
 * Check local is false by default. When we are running
 * in client mode, we are not running on the server in
 * question.
 *
 * If $chck_local is true, then the additional local
 * server check is made.
 *
 */
function red_domain_mx_resolves_to_server($domain,$server,$check_local = false)
{
  $mx_hosts = array();
  if(getmxrr($domain,$mx_hosts)) {
    // return true if we are listed as at least one of the
    // mx records
    if(in_array($server,$mx_hosts))
      return true;

    // the mx record may resolve to a different domain name
    // that has an IP address that resolves to $server
    $server_ip = gethostbyname($server);
    foreach($mx_hosts as $mx_host) {
      // $mx_ip is an array
      $mx_ips = gethostbynamel($mx_host);
      foreach($mx_ips as $mx_ip) {
        if($mx_ip == $server_ip) return true;
        if($check_local) {
          if(red_ip_assigned_to_local_server($mx_ip))
            return true;
        }
      }
    }
  }
  return false;
}

/*
 * Check all local IP addresses and see if any of them
 * match the given $ip. This function should only
 * be called when you want to compare the $ip with
 * the IP addresses assigned to the local server.
 *
 * It requires the iproute package.
 *
 */
function red_ip_assigned_to_local_server($ip)
{
  $cmd = "ip -o addr show";
  exec($cmd,$output);
  foreach($output as $line) {
    // check for both ipv4 and ipv6
    if(preg_match('# inet6? ([0-9:.a-zA-Z]+)/#',$line,$matches)) {
      // We only need one match to return true
      if($matches[1] == $ip) return true;
    }
  }
  return false;
}

/* Safely chown a file by first
 * checking to make sure there
 * is no more than 1 link
 */
function red_chown($file,$user)
{
  // if we're not root (meaning we're in a testing environment)
  // simply return true
  if(posix_getuid() != 0) return true;
  if(!is_dir($file)) {
    $stat = stat($file);
    if($stat[3] > 1) return false;
  }
  return chown($file,$user);
}

function red_chgrp($file,$user)
{
  // if we're not root (meaning we're in a testing environment)
  // simply return true
  if(posix_getuid() != 0) return true;
  if(!is_dir($file)) {
    $stat = stat($file);
    if($stat[3] > 1) return false;
  }
  return chgrp($file,$user);
}

function red_convert_to_friendly_date($iso_date,$date_format = 'mmddyyyy',$date_delimiter = '/')
{
  if($date_format == 'mmddyyyy') {
    $date = substr($iso_date,5,2) . $date_delimiter;
    $date .= substr($iso_date,8,2) . $date_delimiter;
    $date .= substr($iso_date,0,4);
  }
  elseif($date_format == 'ddmmyyyy') {
    $date = substr($iso_date,8,2) . $date_delimiter;
    $date .= substr($iso_date,5,2) . $date_delimiter;
    $date .= substr($iso_date,0,4);
  }
  else {
    $date = $iso_date;
  }

  return $date;
}

function red_db_convert_to_db_acceptable_date($date,&$validated_date,$date_format = 'mmddyyyy',$date_delimiter = '/') {
  // If the date is blank return true
  if($date == '') {
    $validated_date = '';
    return TRUE;
  }

  // First check for and replace the given delimiter (as well
  // as common delimter) with a dash
  $delimiters = array('-','/','.',$date_delimiter);
  $delimiters = array_unique($delimiters);
  $date = str_replace($delimiters,'-',$date);

  // Now if there are no dashes then we're hosed
  if(!preg_match('/-/',$date)) {
    return FALSE;
  }

  // Allow all 0 dates (00-00-0000, 0-0-0000, etc.)
  // also allow time at the end (don't include $)
  if(preg_match('/^(0){1,4}-(0){1,4}-(0){1,4}/',$date)) {
    $validated_date = '0000-00-00';
    return TRUE;
  }

  // Create an array with the month, date, and year parts
  // First remove the time
  $parts = explode('-',$date);
  // See if it is already in ISO format
  if(
    count($parts) > 2 &&
    strlen($parts[0]) == 4 &&
    checkdate(intval($parts[1]),intval($parts[2]),intval($parts[0]))) {
    $validated_date = $date;

    return TRUE;
  }

  if($date_format == 'yyyymmdd') {
    $year = $parts[0];
    $month = $parts[1];
    $date = $parts[2];
  } elseif($date_format == 'mmddyyyy') {
    $month = $parts[0];
    $date = $parts[1];
    $year = $parts[2];
  } elseif($date_format == 'ddmmyyyy') {
    $date = $parts[0];
    $month = $parts[1];
    $year = $parts[2];
  }

  // Sanity checks
  if  (  (strlen($month) <> 1 && strlen($month) <> 2) ||
    (strlen($date) <> 1 && strlen($date) <> 2) ||
    (strlen($year) <> 2 && strlen($year) <> 4)
      ) {
    return FALSE;
  }

  if(strlen($month) == 1) {
    $month = '0' . $month;
  }

  if(strlen($date) == 1) {
    $date = '0' . $date;
  }

  if(strlen($year) == 2) {
    if($year < '25') {
      $year = '20' . $year;
    }
    else {
      $year = '19' . $year;
    }
  }

  if(!checkdate($month,$date,$year)) {
    return FALSE;
  }
  $validated_date = $year . '-' . $month . '-' . $date;
  return TRUE;
}

function red_convert_to_db_acceptable_date($date,&$validated_date,$date_format = 'mmddyyyy',$date_delimiter = '/') {
  // If the date is blank return true
  if($date == '') {
    $validated_date = '';
    return TRUE;
  }

  //
  // First check for and replace the given delimiter (as well
  // as common delimter) with a dash
  $delimiters = array('-','/','.',$date_delimiter);
  $delimiters = array_unique($delimiters);
  $date = str_replace($delimiters,'-',$date);

  // Now if there are no dashes then we're hosed
  if(!preg_match('/-/',$date)) {
    return FALSE;
  }

  // Allow all 0 dates (00-00-0000, 0-0-0000, etc.)
  // also allow time at the end (don't include $)
  if(preg_match('/^(0){1,4}-(0){1,4}-(0){1,4}/',$date)) {
    $validated_date = '0000-00-00';
    return TRUE;
  }

  // Create an array with the month, date, and year parts
  // First remove the time
  $parts = explode('-',$date);
  // See if it is already in ISO format
  if(
    count($parts) > 2 &&
    strlen($parts[0]) == 4 &&
    checkdate(intval($parts[1]),intval($parts[2]),intval($parts[0]))) {
    $validated_date = $date;

    return TRUE;
  }

  if($date_format == 'yyyymmdd') {
    $year = $parts[0];
    $month = $parts[1];
    $date = $parts[2];
  }
  elseif($date_format == 'mmddyyyy') {
    $month = $parts[0];
    $date = $parts[1];
    $year = $parts[2];
  }
  elseif($date_format == 'ddmmyyyy') {
    $date = $parts[0];
    $month = $parts[1];
    $year = $parts[2];
  }

  // Sanity checks
  if  (  (strlen($month) <> 1 && strlen($month) <> 2) ||
    (strlen($date) <> 1 && strlen($date) <> 2) ||
    (strlen($year) <> 2 && strlen($year) <> 4)
      ) {
    return FALSE;
  }

  if(strlen($month) == 1) {
    $month = '0' . $month;
  }

  if(strlen($date) == 1) {
    $date = '0' . $date;
  }

  if(strlen($year) == 2) {
    if($year < '25') {
      $year = '20' . $year;
    }
    else {
      $year = '19' . $year;
    }
  }

  if(!checkdate($month,$date,$year)) {
    return FALSE;
  }
  $validated_date = $year . '-' . $month . '-' . $date;
  return TRUE;
}

function red_convert_to_db_acceptable_date_time($date_time,&$converted_date_time,$date_format = 'mmddyyyy',$date_delimiter = '/') {
  if(preg_match('/^(\d){1,4}.(\d){1,2}.(\d){1,4}/',$date_time,$date_matches)) {
    $date = $date_matches[0];
    // Now the time
    if(preg_match('/(\d){1,2}:(\d){2}(|:)(|\d){2}(| am| pm)$/i',$date_time,$time_matches)) {
      $time = $time_matches[0];
      $hour = intval($time_matches[1]);
      $minute = intval($time_matches[2]);
      $second_colon = $time_matches[3];
      $seconds = intval($time_matches[4]);
      $dorn = trim($time_matches[5]);
      if(red_convert_to_db_acceptable_date($date,$converted_date)) {
        // sanity checking
        if($hour > 24 || $minute > 60 || ($seconds <> '' && $seconds > 60)) {
          return FALSE;
        }
        $dorn = strtolower($dorn);
        if($dorn == '') {
          // do nothing
        }
        else {
          // Chop the am/pm from the time
          $time = substr($time,0,-3);
          if($dorn == 'pm' && $hour < 12) {
            // replace the hour and chop of the space am or space pm
            $time = $hour = $hour + 12 . substr($time,2);
          }
          $converted_date_time = $converted_date . ' ' . $time;
          return TRUE;
        }
      }
    }
  }
  return FALSE;
}

function red_get_ip_for_host($host,$sql_resource)
{
  // useful for testing on my dev machine
  if($host == 'localhost') return '127.0.0.1';
  $host = addslashes($host);
  $sql = "SELECT dns_ip FROM red_item_dns INNER JOIN red_item USING ".
    "(item_id) WHERE item_status = 'active' AND dns_fqdn = '$host' ".
    "AND dns_type = 'a' LIMIT 1";
  $result = red_sql_query($sql,$sql_resource);
  $row = red_sql_fetch_row($result);
  if(red_sql_num_rows($result) == 0) return 'Unknown IP';
  return $row[0];
}

function red_truncate_from_middle($string,$max) {
  $len = strlen($string);
  if($len < $max) return $string;
  $half = ceil($max / 2);
  return substr($string,0,$half) . '...' . substr($string,-$half,$half);
}

// Return true if the passed in unix_id is in use
// Optionally pass a member_id or
// hosting_order_id to not include in the results.
// This function is useful for determining if a unix
// group is being used by a member or hosting order
// not includig the one being deleting
function red_unix_group_id_in_use($unix_group_id,$sql_resource,$member_id = null, $hosting_order_id = null) {
  $keys = array('member','hosting_order');
  foreach($keys as $key => $unused) {
    $table = "red_$key";
    $sql = "SELECT unique_unix_group_id FROM $table ".
      "JOIN unique_unix_group USING(unique_unix_group_id) WHERE  ".
      "red_unique_unix_group.unique_unix_group_id = $unix_group_id AND ".
      "unique_unix_group_status = 'active' AND $table.status = ".
      "'active'";
    $result = red_sql_query($sql,$sql_resource);
    while($row = red_sql_fetch_row($result,$sql_resource)) {
      $id = $row[0];
      if($key == 'hosting_order' && $id = $hosting_order_id) continue;
      return true;
    }
  }
  return false;
}

function red_htmlentities($value) {
  return htmlentities($value,ENT_COMPAT,'UTF-8');
}

function red_set_message($messages,$type = 'info', $key = NULL) {
  if(is_array($messages)) {
    // check for special format provided by red library
    if(array_key_exists('error',$messages)) {
      // we're only interested in displaying the error message
      // itself
      red_set_message($messages['error'],$type);
    } else {
      // in case we get an array of errors
      foreach($messages as $k => $message) {
        red_set_message($message,$type, $k);
      }
    }
  } else {
    $prefix = is_null($key) ? $key : $key . ': ';
    if (php_sapi_name() == "cli") {
      // If we are running via cli, echo the output.
      echo $prefix . $messages . "\n";
    }
    else {
      $_SESSION['red']['messages'][$type][] = $prefix . $messages;
    }
  }
}


// Like system(), but without invoking the shell (so it's safer in
// terms of accidentally passing through garbage characters)
// note that $cmd must be a full path. Optionally pass user
// you want to run as (provided that chpst is installed).
function red_fork_exec_wait($cmd, $args = array(), $env = array(), $as_user = null) {
  $chpst_bin = '/usr/bin/chpst';
  if(!file_exists($cmd)) {
    return FALSE;
  }
  $status = 0;
  $pid = pcntl_fork();
  if(!is_null($as_user)) {
    if(file_exists($chpst_bin)) {
      array_unshift($args, $cmd);
      $cmd = $chpst_bin;
      $args = array_merge(array('-u',$as_user), $args);
    } else {
      print("error: chpst not found");
      return FALSE;
    }
  }
  if (-1 == $pid) {
    // what are we supposed to do here?
    print("error: failed to fork\n");
    return -1;
  } elseif (0 == $pid) { //this is the child process
    pcntl_exec($cmd, $args, $env);
  } else {
    do {
      pcntl_waitpid($pid, $status);
    } while (!pcntl_wifexited($status));
  }
  return pcntl_wexitstatus($status);
}


function red_subscribe_email_to_list($list, $email, $login) {
  return red_list_action('subscribe', $list, $email, $login);
}

function red_unsubscribe_email_from_list($list, $email, $login) {
  return red_list_action('unsubscribe', $list, $email, $login);
}

function red_list_action($action, $list, $email, $login) {
  // pcntl_exec is not an option when calling via the web,
  // the funciton is not available
  $cmd = '/usr/bin/ssh';
  $login = escapeshellarg($login);
  $additional_args = escapeshellarg("$action $list $email");
  $ret = exec("HOME=/var/www $cmd $login $additional_args > /dev/null 2>&1",$output,$response);
  if($response != 0)   return false;
  return true;
}

function red_add_password_to_cache($item_id, $password, $sql_resource) {
  $item_id = intval($item_id);
  $password = base64_encode($password);
  if(empty($item_id) || empty($password)) return FALSE;
  $sql = "INSERT INTO red_password_cache SET password = '$password', item_id = $item_id";
  return red_sql_query($sql, $sql_resource);
}

function red_get_password_from_cache($item_id, $sql_resource) {
  $item_id = intval($item_id);
  $sql = "SELECT password FROM red_password_cache WHERE item_id = $item_id";
  $result = red_sql_query($sql, $sql_resource);
  if(red_sql_num_rows($result) == 0) return NULL;
  $row = red_sql_fetch_row($result);
  return base64_decode($row[0]);
}

function red_delete_password_from_cache($item_id, $sql_resource) {
  $item_id = intval($item_id);
  $sql = "DELETE FROM red_password_cache WHERE item_id = $item_id";
  return red_sql_query($sql, $sql_resource);
}

/**
 * Convert from bytes to a number with a label.
 *
 **/
function red_human_readable_bytes($size) {
  // Don't double convert. If we are not an integer, then just return
  // what we were given.
  if (!preg_match('/^[0-9]+$/', $size)) {
    return $size;
  }

  // If we are 0, return 0 because 0b looks funny.
  if ($size === 0) {
    return $size;
  }
  if ($size >= 1073741824) {
    return round($size / 1024 / 1024 / 1024, 1) . 'gb';
  } 
  elseif ($size >= 1048576) {
    return round($size / 1024 / 1024, 1) . 'mb';
  } 
  elseif($size >= 1024) {
    return round($size / 1024, 1) . 'kb';
  } 
  else {
    return $size . 'b';
  }
}

/**
 * Convert from human readable to bytes
 *
 * Return FALSE if we can't parse it.
 *
 **/
function red_machine_readable_bytes($size) {
  $size = trim($size);

  // If empty, return 0, the default.
  if (empty($size)) {
    return 0;
  }

  // If there is more then one period in the size we bail early.
  if (substr_count($size, '.') > 1) {
    return FALSE;
  }

  // If no label, then we assume bytes.
  if (preg_match('/^[0-9.]+$/', $size)) {
    return ceil($size);
  } 

  // If there is no number, we bail.
  if (!preg_match('/[0-9]/', $size)) {
    return FALSE;
  }

  // Convert to lower case.
  $size = strtolower($size);

  // Bust it out. We expect any of the following:
  //  * 12g
  //  * 12.34m
  //  * 12.34mb
  //  * etc.
  // You can add units: b, k, kb, m, mg, g, gb after
  // your number.
  if (preg_match('/^([0-9.]+)([bkmg]{1})b?$/', $size, $matches)) {
    $number = $matches[1];
    $unit = $matches[2];
    $multiplier = NULL;
    
    if ($unit == 'b') { 
      $multiplier = 1;
    }
    elseif ($unit == 'k') {
      $multiplier = 1024;
    }
    elseif ($unit == 'm') {
      $multiplier = 1024 * 1024;
    }
    elseif ($unit == 'g') {
      $multiplier = 1024 * 1024 * 1024;
    }
    // Ensure we return an integer and always round up to avoid quota
    // suprises. When in doubt, allow a little more rather then a little
    // less.
    return ceil($multiplier * $number);
  }
  else {
    // We got something we cannot parse.
    return FALSE;
  }

}

/**
 * Get the total hard disk use of all VPSs
 */
function red_get_quota_for_all_vps($sql_resource, $member_id, $exclude_vps_id = NULL) {
  $member_id = intval($member_id);
  $exclude_vps_id = intval($exclude_vps_id);
  $sql = "SELECT SUM(vps_hd) FROM red_vps WHERE member_id = $member_id";
  if ($exclude_vps_id) {
    $sql .= " AND vps_id = $exclude_vps_id";
  }
  $result = red_sql_query($sql, $sql_resource);
  $row = red_sql_fetch_row($result);
  if (!$row) {
    return 0;
  }
  return $row[0];
}

/**
 * Get total allocated quota for membership
 *
 * Get the total quota for all the non-deleted items
 * items for this membership. Optioanlly you can exlude
 * an given item_id if you are determining whether a change
 * to that item id will make you over quota.
 *
 **/

function red_get_quota_for_all_items($sql_resource, $member_id, $exclude_item_id = NULL) {
  $member_id = intval($member_id);

  // We'll convert NULL to 0 if they don't want to exclude an item_id and
  // our sql statement can remain the same (excluding item_id 0 doesn't in
  // fact exclude any items).
  $item_id = intval($exclude_item_id);

  // Calculate total quota for user accounts.
  $sql = "SELECT COALESCE(SUM(user_account_quota), 0) FROM red_item JOIN red_item_user_account
    USING(item_id) JOIN red_hosting_order USING(hosting_order_id) WHERE 
    hosting_order_status = 'active' AND red_hosting_order.member_id = $member_id 
    AND item_status != 'deleted' AND item_status != 'pending-delete'
    AND red_item.item_id != $item_id";
  $result = red_sql_query($sql, $sql_resource);
  $row = red_sql_fetch_row($result);
  $user_account_quota = $row[0];
    
  // Calculate total quota for web conf.
  $sql = "SELECT COALESCE(SUM(web_conf_quota), 0) FROM red_item JOIN red_item_web_conf
    USING(item_id) JOIN red_hosting_order USING(hosting_order_id) WHERE 
    hosting_order_status = 'active' AND red_hosting_order.member_id = $member_id 
    AND item_status != 'deleted' AND item_status != 'pending-delete'
    AND red_item.item_id != $item_id";

  red_sql_query($sql, $sql_resource);
  $row = red_sql_fetch_row($result);
  $web_conf_quota = $row[0];

  // Calculate total quota for databases.
  $sql = "SELECT COALESCE(SUM(mysql_db_quota), 0) FROM red_item JOIN red_item_mysql_db
    USING(item_id) JOIN red_hosting_order USING(hosting_order_id) WHERE 
    hosting_order_status = 'active' AND red_hosting_order.member_id = $member_id 
    AND item_status != 'deleted' AND item_status != 'pending-delete'
    AND red_item.item_id != $item_id";
  $result = red_sql_query($sql_resource, $sql);
  $row = red_sql_fetch_row($result);
  $mysql_db_quota = $row[0];

  return $web_conf_quota + $user_account_quota + $mysql_db_quota;
}

function red_get_member_quota($sql_resource, $member_id) {
  $member_id = intval($member_id);
  $sql = "SELECT member_quota FROM red_member WHERE member_id = $member_id";
  $result = red_sql_query($sql_resource, $sql);
  $row = red_sql_fetch_row($result);
  // When running on a node, we may not have permission to query this table, so just
  // return 0, which means no quota, to prevent any validation errors.
  if (!$row) {
    return 0;
  }
  return $row[0];
}
?>
