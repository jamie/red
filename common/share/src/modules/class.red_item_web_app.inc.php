<?php

if(!class_exists('red_item_web_app')) {
  class red_item_web_app extends red_item {
    var $_web_app_name;
    var $_web_app_update = 'core';
    var $_human_readable_description;
    var $_human_readable_name;
    var $_available_web_app_names = [ 
      'drupal9' => 'Drupal9',
      'wordpress' => 'WordPress',
      'backdrop' => 'Backdrop',
    ];
    var $_available_web_app_updates;

    function __construct($construction_options) {
      parent::__construct($construction_options);
      $this->_available_web_app_updates = [
        'core' => red_t('Please apply updates to the core code automatically'),
        'none' => red_t('No updates, I take full responsibility for the security of this application'),
      ];
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array ('web_app_name' => array (
                               'req' => true,
                               'pcre'   => RED_WEB_APP_MATCHER,
                               'pcre_explanation'   => RED_WEB_APP_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Application to install'),
                               'user_insert' => TRUE,
                               'user_update' => FALSE,
                               'user_visible' => TRUE,
                               'input_type' => 'select',
                               'text_length' => 20,
                               'text_max_length' => 64,
                               'tblname'   => 'red_item_web_app',
                               'options' => $this->_available_web_app_names,
                               'filter' => true),
                             'web_app_update' => array (
                               'req' => TRUE,
                               'pcre'   => RED_WEB_APP_UPDATE_MATCHER,
                               'pcre_explanation'   => RED_WEB_APP_UPDATE_EXPLANATION,
                               'type'  => 'varchar',
                               'input_type' => 'select',
                               'fname'  => red_t('Security Updates'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'options' => $this->_available_web_app_updates,
                               'tblname'   => 'red_item_web_app',
                               'filter' => true),
                            
      ));

    }

    function set_web_app_name($value) {
      $this->_web_app_name = $value;
    }
    function get_web_app_name() {
      return $this->_web_app_name;
    }
    function set_web_app_update($value) {
      $this->_web_app_update = $value;
    }
    function get_web_app_update() {
      return $this->_web_app_update;
    }
    
    function additional_validation() {
      if($this->_delete) return true;
      if(!$this->get_web_conf_login()) {
        $this->set_error(red_t("You must first create a web configuration before you can install a web application"),'validation');
      }
    }

    function get_web_conf_login() {
      $sql = "SELECT web_conf_login FROM red_item JOIN red_item_web_conf " .
        "USING (item_id) WHERE item_status = 'active' " .
        "AND hosting_order_id = " . intval($this->get_hosting_order_id());
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      if(empty($row)) return false;
      return $row[0];
    }
  }  
}

?>
