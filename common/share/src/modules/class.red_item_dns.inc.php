<?php

// email-account - this class is for mainipulating email accounts
if(!class_exists('red_item_dns')) {
  class red_item_dns extends red_item {
    var $_dns_zone;
    var $_dns_type;
    var $_dns_fqdn;
    var $_dns_ip;
    var $_dns_ttl = 3600;
    var $_dns_server_name;
    var $_sshfp_algorithm_options = array(
      0 => '--Choose One--',
      1 => 'RSA',
      2 => 'DSA',
      3 => 'ECDSA',
      4 => 'Ed25519',
    );
    var $_sshfp_type_options = array(
      0 => '--Choose One--',
      1 => 'SHA-1',
      2 => 'SHA-256',
    );
    var $_dns_text;
    var $_dns_dist = 0;
    var $_dns_weight = 0;
    var $_dns_port = 0;
    var $_dns_type_options = array(
      'a' => 'a',
      'mx' => 'mx',
      'cname' => 'cname',
      'text' => 'text',
      'srv' => 'srv',
      'ptr' => 'ptr',
      'aaaa' => 'ipv6 (aaaa)',
			'sshfp' => 'sshfp',
			'mailstore' => 'mailstore (internal only)',

    );
    // domains that anyone can make subdomain of.
    // NOTE: afscme1368.org, ufcw464a.org are listed temporarily to ease
    // transition to knot 
    var $_globally_allowed_domains = array('mayfirst.org','mayfirst.info', 'in-addr.arpa','rocus.org', 'indymedia.org', 'afscme1368.org', 'ufcw464a.org', 'mayfirst.cx');
    // We stopped serving private IP ranges when we switched to Knot. These domains were grandfathered in, plus mayfirst.cx which is explicitly for 
    // private IP range.
    var $_allowed_private_range_fqdns = array('iatsenbf3-dev.syntonic.openflows.com', 'lothinternal.tenthcircle.net', 'dev2.johnjay.openflows.com', 'jjmedia2.openflows.com', 'testhostingorder.org', 'www.testhostingorder.org', 'testgroup.mayfirst.org', 'mayfirst.cx');
    var $_human_readable_description;
    var $_human_readable_name;

    function __construct($co) {
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array (
                             'dns_zone' => array (
                               'req' => TRUE,  
                               'pcre'   => RED_DOMAIN_MATCHER,
                               'pcre_explanation'   => RED_DOMAIN_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Zone name'),
                               'user_insert' => FALSE,
                               'user_update' => FALSE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_dns',
                               'filter' => false),
                             'dns_type' => array (
                               'req' => true,
                               'pcre'   => RED_DNS_TYPE_MATCHER,
                               'pcre_explanation'   => RED_DNS_TYPE_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('DNS Type'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                             'dns_fqdn' => array (
                               'req' => TRUE,  
                               'pcre'   => RED_DOMAIN_LOOSE_WILDCARD_MATCHER,
                               'pcre_explanation'   => RED_DOMAIN_LOOSE_WILDCARD_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Full Domain Name (e.g. www.mygroup.org)'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 255,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                             'dns_ip' => array (
                               'req' => false,
                               'pcre'   => RED_IP_MATCHER,
                               'pcre_explanation'   => RED_IP_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('IP Address'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 15,
                               'text_max_length' => 79,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                             'dns_ttl' => array (
                               'req' => true,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Time to live'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 6,
                               'text_max_length' => 6,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                             'dns_server_name' => array (
                               'req' => false,
                               'pcre'   => RED_SERVER_NAME_MATCHER,
                               'pcre_explanation'   => RED_SERVER_NAME_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Server Name'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 255,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                             'dns_text' => array (
                               'req' => false,
                               'pcre'   => RED_DNS_TEXT_MATCHER,
                               'pcre_explanation'   => RED_DNS_TEXT_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Text'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 40,
                               'text_max_length' => 1024,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                             'dns_dist' => array (
                               'req' => false,
                               'pcre'   => RED_TINYINT_MATCHER,
                               'pcre_explanation'   => RED_TINYINT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Distance'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 2,
                               'text_max_length' => 3,
                               'tblname'   => 'red_item_dns'),
                            'dns_weight' => array (
                               'req' => false,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Weight'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 6,
                               'text_max_length' => 6,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                            'dns_port' => array (
                               'req' => false,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Port'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 6,
                               'text_max_length' => 6,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                            'dns_sshfp_algorithm' => array (
                               'req' => false,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('SSF Fingerprint Algorithm'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'select',
                               'tblname'   => 'red_item_dns',
                                'options' => $this->get_sshfp_algorithm_options(),
                               'filter' => false),
                             'dns_sshfp_type' => array (
                               'req' => false,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('SSH Fingerprint Type'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'select',
                               'tblname'   => 'red_item_dns',
                                'options' => $this->get_sshfp_type_options(),
                               'filter' => false),
                             'dns_sshfp_fpr' => array (
                               'req' => false,
                               'pcre'   => RED_ANYTHING_MATCHER,
                               'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Data'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 88,
                               'text_max_length' => '512',
                               'tblname'   => 'red_item_dns',
                               'filter' => false),


  ));
    }

    /**
     * Get SSF Type options.
     */
    function get_sshfp_type_options() {
      return $this->_sshfp_type_options;
    }

    /**
     * Get SSF Algorythm options.
     */
    function get_sshfp_algorithm_options() {
      return $this->_sshfp_algorithm_options;
    }

    /**
     * Check if passed in domain is local
     *
     * The mayfirst.cx domain is reserved in our cabinet for local use
     * only and only resolved to 10.9.67.0/24 IP address. It is inserted
     * into our local authoritative DNS servers.
     */
    function is_local($domain) {
      return preg_match('/mayfirst\.cx$/', $domain);
    }

    function set_dns_zone($value = NULL) {
      // Automatically set if emtpy
      if(empty($value)) {
        $value = $this->get_calculated_zone();
      }
      $this->_dns_zone = $value;
    }
    function get_dns_zone() {
      return $this->_dns_zone;
    }
    static function get_rfc4183_snippet($base, $last) {
      // Add ranges that are delegated by upstream using rfc4183
      // here. See http://faq.he.net/index.php/Reverse_DNS
      // for more information. 
      $rfc4183s = array(
        0 => array(
          'base' => '15.66.216',
          'min' => 2,
          'max' => 30,
          'return' => '0-27',
        ),
      );
      foreach($rfc4183s as $candidate) {
        if($candidate['base'] == $base) {
          if($last >= $candidate['min'] && $last <= $candidate['max']) {
            return $candidate['return'];
          }
        }
      }
      return NULL;
    }
    
    function get_calculated_zone() {
      $fqdn = $this->get_dns_fqdn();
      $ip = $this->get_dns_ip();
      $type = $this->get_dns_type();
      if(!empty($fqdn) && $type != 'ptr') {
        // Always break it down to last two pieces.
        // I.e. the following should return: mayfirst.org
        // mayfirst.org, joe.mayfirst.org, joe-bob.mayfirst.org, bob.mayfirst.org
        // www.joe.bob.mayfirst.org
        //
        // Unless the domain is one of the exceptions list in
        // /etc/red-dns-soa-exeptions.conf in which case use the
        // last three pieces.
        $allowed_pieces = 2;  
        // Check for exception by getting the last two pieces of the domain
        $exceptions_file = '/etc/red-dns-soa-exceptions.conf';
        if(preg_match('/\.?([^.]+\.[^.]+)$/', $fqdn, $matches)) {
          // Find this domain (terminated with white space) in the conf file.
          if(red_return_line($exceptions_file, $matches[1], '\s')) {
            $allowed_pieces = 3;
          }
        }
        $pieces = explode('.', $fqdn);
        while(count($pieces) > $allowed_pieces) {
          array_shift($pieces);
        }
        return implode('.', $pieces);
      }
      elseif(!empty($ip) && $type == 'ptr') {
        return red_item_dns::get_calculated_reverse_zone($ip);
      }
      return NULL;
    }  

    static function get_calculated_reverse_zone($ip = NULL) {
      if(!empty($ip)) {
        if(filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
          // ipv6 address. We only have one zone file.
          return "6.1.1.0.1.0.0.0.0.7.4.0.1.0.0.2.ip6.arpa";
        }
        else {
          // ipv4, Get that last three pieces of the given IP.
          $pieces = explode('.', $ip);
          if(count($pieces > 2)) {
            $first = array_pop($pieces);
            $second = array_pop($pieces);
            $third = array_pop($pieces);
            $fourth = array_pop($pieces);

            // Now check... a normal zone for 1.2.3.4 would be
            // 3.2.1.in-addr.arpa
            // However, some of our blocks use rfc4183, which might 
            // need a zone file in the form of:
            // 1-10.3.2.1.in-addr.arpa instad.
            $base = "${second}.${third}.{$fourth}"; 
            $snippet = red_item_dns::get_rfc4183_snippet($base, $first);
            if($snippet) {
              return "${snippet}.${base}.in-addr.arpa";
            }
            return "${base}.in-addr.arpa"; 
          }
        }
      }
      return NULL;
    }
    function set_dns_type($value) {
      $this->_dns_type = $value;
    }
    function get_dns_type() {
      return $this->_dns_type;
    }

    function set_dns_fqdn($value) {
      $this->_dns_fqdn = $value;
    }
    function get_dns_fqdn() {
      return $this->_dns_fqdn;
    }

    function set_dns_ip($value) {
      $this->_dns_ip = $value;
    }
    function get_dns_ip() {
      return $this->_dns_ip;
    }

    function set_dns_ttl($value) {
      $this->_dns_ttl = $value;
    }
    function get_dns_ttl() {
      return $this->_dns_ttl;
    }

    function set_dns_server_name($value) {
      $this->_dns_server_name = $value;
    }
    function get_dns_server_name() {
      return $this->_dns_server_name;
    }

    function set_dns_text($value) {
      $this->_dns_text = $value;
    }
    function get_dns_text() {
      return $this->_dns_text;
    }

    function set_dns_dist($value) {
      $this->_dns_dist = $value;
    }
    function get_dns_dist() {
      return $this->_dns_dist;
    }

    function set_dns_weight($value) {
      $this->_dns_weight = $value;
    }
    function get_dns_weight() {
      return $this->_dns_weight;
    }

    function set_dns_port($value) {
      $this->_dns_port = $value;
    }
    function get_dns_port() {
      return $this->_dns_port;
    }

    function set_dns_sshfp_type($value) {
      $this->_dns_sshfp_type = $value;
    }
    function get_dns_sshfp_type() {
      return $this->_dns_sshfp_type;
    }

    function set_dns_sshfp_algorithm($value) {
      $this->_dns_sshfp_algorithm = $value;
    }
    function get_dns_sshfp_algorithm() {
      return $this->_dns_sshfp_algorithm;
    }

    function set_dns_sshfp_fpr($value) {
      $this->_dns_sshfp_fpr = $value;
    }
    function get_dns_sshfp_fpr() {
      return $this->_dns_sshfp_fpr;
    }

    function get_dns_type_options() {
      return $this->_dns_type_options;
    }
    
    function validate() {
      // FIXME - we probably should not be modifying data in the validate function
      // But we need to auto-set the zone field before we validate or we will get 
      // a validation error.
      $this->set_dns_zone();
      return parent::validate();
    }

    function additional_validation() {
      // If we're deleting - don't need to do validation
      if($this->_delete) return;

      $type = $this->get_dns_type();
      $server_name = $this->get_dns_server_name();
      $ip = $this->get_dns_ip();
      $text = $this->get_dns_text();
      $ttl = intval($this->get_dns_ttl());
      $fqdn = $this->get_dns_fqdn();

      if ($this->is_local($fqdn)) {
        if (!empty($ip)) {
          if (!preg_match('/^10\.9\.67\./', $ip)) {
            $this->set_error(red_t("The domain mayfirst.cx is reserved for IPs in the 10.9.67.0/24 range"), 'validation');
          }
        }
      }
      else {
        if($ttl < 300) {
          $this->set_error(red_t("Please use a minimum time to live value of 300."), 'validation');
        }

        // No private ranges allowed
        if(!empty($ip) && !filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE) && !in_array($fqdn, $this->_allowed_private_range_fqdns)) {
          // Private IP addresses are only allowed if we are running tests.
          // So, test for any of the testing constants.
          if(!defined('RED_TEST')) {
            $this->set_error(red_t("Private IP ranges are not allowed. You entered: $ip"),'validation');
          }
        }
      }

      if(!empty($ip)) {
        // Must be valid IP address.
        if(!filter_var($ip, FILTER_VALIDATE_IP)) {
          $this->set_error(red_t('That IP address does not appear to a valid ipv4 or ipv6 address.'),'validation');
        }
      }

      if($type == 'mx' ||  $type == 'cname' || $type == 'srv') {
        if(empty($server_name)) {
          $this->set_error(red_t('MX, SRV and CNAME records require a server name'),'validation');
        }
      }

      if($type == 'text') {
        if(empty($text)) {
          $this->set_error(red_t('Text records requires a value in the text field.'),'validation');
        }
        if(preg_match('/"/', $text)) {
          $this->set_error(red_t('Text records cannot include a double quote.'),'validation');
        }  
      }

      if($type == 'a' ||  $type == 'ptr') {
        if(empty($ip)) {
          $this->set_error(red_t("You must enter an IP address for A and PTR records."), 'validation');
        }
      }

      if($type == 'aaaa') {
        // Ensure we have a valid ipv6 address. 
        if(!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
          $this->set_error(red_t("That did not seem to be a valid ipv6 address."), 'validation');
        }
      }
      if($type == 'sshfp') {
        // Ensure we have required fields. 
        if ($this->get_dns_sshfp_type() == 0) {
          $this->set_error(red_t("Please choose a valid sshfp fingerprint type."), 'validation');
        }
        if ($this->get_dns_sshfp_algorithm() == 0) {
          $this->set_error(red_t("Please choose a valid sshfp algorithm."), 'validation');
        }
        if ($this->get_dns_sshfp_fpr() == '') {
          $this->set_error(red_t("Please enter a fingerprint."), 'validation');
        }
      }
      if($type == 'ptr') {
        if(!filter_var($ip, FILTER_VALIDATE_IP)) {
          $this->set_error(red_t("Sorry, that IP did not look like a valid ipv4 or ipv6 address."), 'validation');
        }
        if(filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
          // If it is ipv6, ensure it is in our range. FIXME: this should be configured somewhere.
          if (!preg_match('/^2001:470:1:116:/', $ip)) {
            $this->set_error(red_t("We can only offer ipv6 pointer records for ipv6 addresses in the range specified to us (2001:470:1:116::/64)."), 'validation');
          }
        }
      }
      if(FALSE !== $existing_ttl = $this->mismatched_fqdn_ttls()) {
        $msg_args = array('@ttl' => $existing_ttl);
        $msg = red_t("All records with the same domain name and type must have the same time to live value. Try using @ttl as the time to live instead.", $msg_args);
        $this->set_error($msg, 'validation');
      }
      if($type == 'ptr') {
        if(FALSE !== $existing_ttl = $this->mismatched_ip_ttls()) {
          $msg_args = array('@ttl' => $existing_ttl);
          $msg = red_t("There is already a ptr record with the same IP address but a different time to live.Try using @ttl as the time to live instead.", $msg_args);
          $this->set_error($msg, 'validation');
        }
      }
      if($type == 'srv') {
        // Make sure the service/protocol fields look reasonable.
        $parts = explode('.', $this->get_dns_fqdn());
        // Should be in format: _http._tcp.domain.name.org
        // First, ensure the first two pieces start with an underscore.
        if(!preg_match('/^_/', $parts[0]) || !preg_match('/^_/', $parts[1])) {
          $this->set_error(red_t("The domain name must start with _service._protocol (e.g. _xmpp._tcp.mayfirst.org)."), 'validation');
        }
        // Only supported protocols are tcp and udp
        if($parts[1] != '_udp' && $parts[1] != '_tcp' && $parts[1] != '_tls') {
          $this->set_error(red_t("The protocol (second part after the service) must be either _tcp or _udp or _tls."), 'validation');
        }
        // Ensure port is set
        if(empty($this->get_dns_port())) {
          $this->set_error(red_t("Please set a valid port when creating a srv record."), 'validation');
        }
      }
      $matches = array();
      if(preg_match_all('/\*/', $fqdn, $matches)) {
        // Make sure we only have one asterisk.
        if(count($matches[0]) > 1) {
          $msg = red_t("Only one asterisk is allowed in a domain name.");
          $this->set_error($msg, 'validation');
        }
      }

      if($type != 'text' && $type != 'cname' && $type != 'srv') {
        # enforce stricter domain name checking
        if(!preg_match(RED_DOMAIN_WILDCARD_MATCHER, $this->get_dns_fqdn())) {
          $this->set_error(red_t('Domains cannot have an underscore in them.'),'validation');
        }
      }
      if(!$this->fqdn_is_allowed()) {
        $this->set_error(red_t('That domain name is already in use by another member or hosting order.'),'validation');
      }
      if(!empty($ip) && !empty($server_name)) {
        $this->set_error(red_t("You may not enter both a server name and an IP address. If you are entering an MX record, only enter a server name ('$ip' and '$server_name')."),'validation');
      }
      // CNAMES should not have any matching fqdn's
      if($type == 'cname') {
        // make sure this fqdn is not in use
        $exact_match = true;
        if($this->domain_in_use($fqdn,$exact_match)) {
          $this->set_error(red_t('That domain name is already entered in the system. When entering a CNAME, the only instance of that domain name should be the Cname record. Please remove the other instance of the domain name and try again. If you just want an alias, then create an A record instead.'),'validation');
        }
      }
    }

    /*
     * Check if another record exists with the same ip and type
     * but with a different ttl. This check avoids having a 
     * reverse lookup zone in which two records have mis-matched
     * ttls. Return FALSE if there are no mis-matched ttls
     * and the ttl of one existing row if it exists.
     */
    function mismatched_ip_ttls() {
      $ip = addslashes($this->get_dns_ip());
      $ttl = addslashes($this->get_dns_ttl());
      $item_id = intval($this->get_item_id());

      $sql = "SELECT red_item.item_id, dns_ttl FROM red_item JOIN red_item_dns USING(item_id) " .
        "WHERE item_status != 'deleted' AND item_status != 'transfer-limbo' AND ".
        "item_status != 'pending-delete' AND item_status != 'disabled' AND dns_type = 'ptr' " .
        "AND dns_ttl != '$ttl' AND dns_ip = '$ip' AND red_item.item_id != $item_id";
      $result = $this->_sql_query($sql);
      if($this->_sql_num_rows($result) == 0) return false;
      $row = $this->_sql_fetch_row($result);
      return $row[1];
    }

    /*
     * Check if another record exists with the same fqdn and type
     * but with a different ttl. 
     */
    function mismatched_fqdn_ttls() {
      $fqdn = addslashes($this->get_dns_fqdn());
      $type = addslashes($this->get_dns_type());
      $ttl = addslashes($this->get_dns_ttl());
      $item_id = intval($this->get_item_id());

      $sql = "SELECT red_item.item_id, dns_ttl FROM red_item JOIN red_item_dns USING(item_id) " .
        "WHERE item_status != 'deleted' AND item_status != 'transfer-limbo' AND ".
        "item_status != 'pending-delete' AND item_status != 'disabled' AND dns_fqdn = '$fqdn' AND dns_type = '$type' " .
        "AND dns_ttl != '$ttl' AND red_item.item_id != $item_id AND dns_type != 'ptr'";
      $result = $this->_sql_query($sql);
      if($this->_sql_num_rows($result) == 0) return false;
      $row = $this->_sql_fetch_row($result);
      return $row[1];
    }

    function fqdn_is_allowed() {
      $fqdn = $this->get_dns_fqdn();
      $parts = explode('.',$fqdn);
      $tld = array_pop($parts);
      $name = array_pop($parts);
      $short_domain = "$name.$tld";
      if(in_array($short_domain,$this->_globally_allowed_domains)) {
        // it's a globally available domain - make sure nobody is using
        // it or a shorter versions of it. For example, if the fqdn is 
        // bar.mayfirst.org make sure nobody is using that domain. Or if
        // the fqdn is foo.bar.mayfirst.org make sure nobody has 
        // bar.mayfirst.org
        //
        // Also make sure that if someone puts in an exact globally
        // allowed domain (like mayfirst.org) that it's not allowed if 
        // that is taken somewhere else
        $sub = '';
        if(count($parts) > 0) $sub = array_pop($parts) . '.'; 
        $check = "${sub}${short_domain}";
        $exact_match = false;
        $hosting_order_id = $this->domain_in_use($check,$exact_match);
      }
      else {
        // regular domain - allowed provided nobody else
        // has a variation of it
        $exact_match = false;
        $hosting_order_id = $this->domain_in_use($short_domain,$exact_match);
      }
      // not in use by anyone
      if(false === $hosting_order_id) return true;

      // in use by the current hosting order - so allowed
      if($hosting_order_id == $this->get_hosting_order_id()) return true;

      // in use by another hosting order tied to the same member - so allowed
      $member_id = $this->get_member_id();
      if($this->does_hosting_order_belong_to_member($hosting_order_id,$member_id)) return true;

      // otherwise - already in use
      return false;
    }

    function does_hosting_order_belong_to_member($hosting_order_id,$member_id) {
      $sql = "SELECT hosting_order_id FROM red_hosting_order " .
        "WHERE hosting_order_id = $hosting_order_id ".
        "AND member_id = $member_id";
      $result = $this->_sql_query($sql);
      if($this->_sql_num_rows($result) == 0) return false;
      return true;
    }

    // returns false if not in use or the hosting_order_id
    // of the hosting order it is in use by
    function domain_in_use($domain,$exact_match=false) {
      $condition = "(dns_fqdn = '$domain' OR dns_fqdn LIKE '.$domain')";
      if($exact_match) $condition = "dns_fqdn = '$domain'";
      $sql = "SELECT red_item.item_id,hosting_order_id FROM red_item_dns ".
        "INNER JOIN red_item ON red_item_dns.item_id = red_item.item_id ".
        "WHERE $condition AND item_status != 'deleted' AND item_status != 'disabled'";
      $result = $this->_sql_query($sql);

      // no records, not in use
      if($this->_sql_num_rows($result) == 0) return false;

      $row = $this->_sql_fetch_row($result);
      // one record
      if($this->_sql_num_rows($result) == 1)  {
        $item_id = $row[0];
        // we're updating the record we found - so ignore it
        if($item_id == $this->get_item_id()) return false;
      }
      // return the hosting order id of the first record
      return $row[1];
    }

    /**
     * Return list of authoritative name servers
     */
    function get_nameservers() {
      if ($this->is_local($this->get_dns_fqdn())) {
        return array('a.ns.mayfirst.cx', 'b.ns.mayfirst.cx');
      }
      return array('a.ns.mayfirst.org', 'b.ns.mayfirst.org');
    }  

    
  }
}

?>
