<?php

// email-account - this class is for mainipulating email accounts
if(!class_exists('red_item_user_account')) {
  class red_item_user_account extends red_item {
    var $_user_account_login;
    var $_user_account_firstname;
    var $_user_account_lastname;
    var $_user_account_password;
    var $_user_account_uid;
    var $_user_account_recovery_email;
    var $_user_account_auto_response;
    var $_user_account_auto_response_reply_from;
    var $_user_account_auto_response_action;
    var $_user_account_gpg_ids;
    var $_user_account_gpg_public_key;
    var $_user_account_auto_response_action_options;
    var $_user_account_mountpoint;
    var $_human_readable_description;
    var $_human_readable_name;
    var $_track_disk_usage = TRUE;

    function __construct($co) {
      parent::__construct($co);
      $this->_set_child_datafields();
      $this->_user_account_auto_response_action_options = array(
        '' => red_t('Do not send auto response'),
        'respond_and_deliver' => red_t('Respond and deliver messages'),
        'respond_only' => red_t('Respond but do not deliver')
      );

      // If it's a new item, we have to autoset the mountopint if one is
      // specified in red_server for this item's host.
      if (!$this->exists_in_db()) {
        $this->set_user_account_mountpoint($this->get_mountpoint_for_host());
      }
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array ('user_account_firstname' => array (
                               'req' => false,
                               'pcre'   => RED_TEXT_MATCHER,
                               'pcre_explanation'   => RED_TEXT_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('First name'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 128,
                               'tblname'   => 'red_item_user_account',
                               'filter' => true),
                             'user_account_lastname' => array (
                               'req' => false,
                               'pcre'   => RED_TEXT_MATCHER,
                               'pcre_explanation'   => RED_TEXT_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Last name'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 128,
                               'tblname'   => 'red_item_user_account',
                               'filter' => true),
                             'user_account_login' => array (
                               'req' => true,
                               'pcre'   => RED_LOGIN_MATCHER,
                               'pcre_explanation'   => RED_LOGIN_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Login name'),
                               'user_insert' => TRUE,
                               'user_update' => FALSE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 128,
                               'tblname'   => 'red_item_user_account',
                               'filter' => true),
                             'user_account_password' => array (
                               'req' => true,
                               'pcre'   => RED_PASSWORD_HASH_MATCHER,
                               'pcre_explanation'   => RED_PASSWORD_HASH_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Password'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'password',
                               'text_length' => 30,
                               'text_max_length' => 128,
                               'tblname'   => 'red_item_user_account'),
                             'user_account_uid' => array (
                               'req' => false,  // it is required, but not by user
                               'pcre'   => RED_ID_MATCHER,
                               'pcre_explanation'   => RED_ID_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('User ID'),
                               'user_insert' => FALSE,
                               'user_update' => FALSE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_user_account'),
                             'user_account_recovery_email' => array (
                               'req' => false,  
                               'pcre'   => RED_EMAIL_MATCHER,
                               'pcre_explanation'   => RED_EMAIL_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Recovery Email'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'tblname'   => 'red_item_user_account'),
                             'user_account_auto_response_action' => array (
                               'req' => false,  
                               'pcre'   => RED_AUTO_RESPONSE_ACTION_MATCHER,
                               'pcre_explanation'   => RED_AUTO_RESPONSE_ACTION_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Auto Responder Action'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_user_account'),
                             'user_account_auto_response' => array (
                               'req' => false,  
                               'pcre'   => RED_ANYTHING_MATCHER,
                               'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
                               'type'  => 'text',
                               'fname'  => red_t('Auto Responder Message'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'textarea',
                               'textarea_cols' => 50,
                               'textarea_rows' => 5,
                               'tblname'   => 'red_item_user_account'),
                             'user_account_auto_response_reply_from' => array (
                               'req' => false,  
                               'pcre'   => RED_EMAIL_MATCHER,
                               'pcre_explanation'   => RED_EMAIL_EXPLANATION,
                               'type'  => 'text',
                               'fname'  => red_t('Auto Responder Reply From Address'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'tblname'   => 'red_item_user_account'),
                             'user_account_gpg_public_key' => array (
                               'req' => false,  
                               'pcre'   => RED_ANYTHING_MATCHER,
                               'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
                               'type'  => 'text',
                               'fname'  => red_t('GPG Public Key'),
                               'user_insert' => FALSE,
                               'user_update' => FALSE,
                               'user_visible' => FALSE,
                               'input_type' => 'textarea',
                               'textarea_cols' => 50,
                               'textarea_rows' => 5,
                               'tblname'   => 'red_item_user_account'),
                             'user_account_gpg_ids' => array (
                               'req' => false,  
                               'pcre'   => RED_ANYTHING_MATCHER,
                               'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
                               'type'  => 'text',
                               'fname'  => red_t('GPG User Ids (one per line)'),
                               'user_insert' => FALSE,
                               'user_update' => FALSE,
                               'user_visible' => FALSE,
                               'input_type' => 'textarea',
                               'textarea_cols' => 50,
                               'textarea_rows' => 3,
                               'tblname'   => 'red_item_user_account'),
                            'user_account_mountpoint' => array (
                               'req' => false,  
                               'pcre'   => RED_ANYTHING_MATCHER,
                               'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Mount Point'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 8,
                               'text_max_length' => 8,
                               'tblname'   => 'red_item_user_account',
                               'filter' => false),
                                 ));

    }

    function set_user_account_login($value) {
      $this->_user_account_login = strtolower($value);
    }
    function get_user_account_login() {
      return $this->_user_account_login;
    }

    function set_user_account_firstname($value) {
      $this->_user_account_firstname = $value;
    }
    function get_user_account_firstname() {
      return $this->_user_account_firstname;
    }
    function set_user_account_lastname($value) {
      $this->_user_account_lastname = $value;
    }
    function get_user_account_lastname() {
      return $this->_user_account_lastname;
    }

    // Note - this function is overritten by the ui
    // child class so that passwords get encrypted
    // when entered into the system.
    function set_user_account_password($value) {
      $this->_user_account_password = $value;
    }
    function get_user_account_password() {
      return $this->_user_account_password;
    }

    function set_user_account_auto_response($value) {
      $this->_user_account_auto_response = $value;
    }
    function get_user_account_auto_response() {
      return $this->_user_account_auto_response;
    }
    function set_user_account_auto_response_action($value) {
      $this->_user_account_auto_response_action = $value;
    }
    function get_user_account_auto_response_action() {
      return $this->_user_account_auto_response_action;
    }
    function set_user_account_auto_response_reply_from($value) {
      $this->_user_account_auto_response_reply_from = $value;
    }
    function get_user_account_auto_response_reply_from() {
      return $this->_user_account_auto_response_reply_from;
    }
    function set_user_account_gpg_public_key($value) {
      $this->_user_account_gpg_public_key = $value;
    }
    function get_user_account_gpg_public_key() {
      return $this->_user_account_gpg_public_key;
    }
    function set_user_account_gpg_ids($value) {
      $this->_user_account_gpg_ids = $value;
    }
    function get_user_account_gpg_ids() {
      return $this->_user_account_gpg_ids;
    }

    function set_user_account_uid($value) {
      $this->_user_account_uid = intval($value);
    }
    function get_user_account_uid() {
      return intval($this->_user_account_uid);
    }
    function set_user_account_recovery_email($value) {
      $this->_user_account_recovery_email = $value;
    }
    function get_user_account_recovery_email() {
      return $this->_user_account_recovery_email;
    }
    function set_user_account_mountpoint($value) {
      $this->_user_account_mountpoint = $value;
    }
    function get_user_account_mountpoint() {
      return $this->_user_account_mountpoint;
    }

    function get_reserved_logins() {
      // override this function to add additional 
      // reserved logins
      return array('root','daemon','bin','sys','sync','games','man','lp',
        'mail','new','uucp','proxy','www-data','backup','list','irc','gnats',
        'nobody','Debian-exim','identd','sshd','postfix','gdm','messagebus',
        'mysql','guest','postgres','ogo','fetchmail','dnslog','dnscache',
        'tinydns','axfrdns','otrs','zope','clamav','nagios','amavis',
        'ftp','mayfirst','vmail','asterisk','awstats','shadow');
    }

    function is_login_reserved($login) {
      return in_array($login,$this->get_reserved_logins());
    }

    // Override validate to ensure they are not trying to get a 
    // reserved login.
    function additional_validation() {
      // make sure the login is not reserved
      $login = $this->get_user_account_login();

      $reserved = $this->is_login_reserved($login);
      $taken = !$this->is_field_value_unique('user_account_login','red_item_user_account',$login);

      if(($reserved || $taken) && !$this->_delete) {
        $this->set_error(red_t('That login is already taken. Please choose a different login.'),'validation');
      }

      // Make sure (this should not be possible, but just to be safe)
      // that the uid is over 500 for security reason
      // Note - if the UID is set to 0 - then it means it's a new
      // record (not that it will actually get UID 0 :-).
      $uid = $this->get_user_account_uid();
      if($uid <> 0 && $uid < 500) {
        $this->set_error(red_t('Only values over 500 are allowed, not @uid.', array('@uid' => $uid)),'validation','hard');
      }
        
      // If auto response is set, make sure auto_response_from_email
      // is also set
      $action = $this->get_user_account_auto_response_action();
      $from = $this->get_user_account_auto_response_reply_from();
      $message = $this->get_user_account_auto_response();
      if(!empty($action)) {
        if(empty($from)) {
          $this->set_error(red_t('You must specify the email address you want your auto response to be sent from.'),'validation');
        }
        if(empty($message)) {
          $this->set_error(red_t('You must specify the auto response message that you want sent.'),'validation');
        }
      }
      // If the user is deleting a record, make sure it's not being
      // used in: email_address, server_access, hosting order access
      // or web_status
      if($this->_delete) {
        $login = $this->get_user_account_login();
        $checks = array(  array(  'table' => 'red_item_email_address',
                    'field' => 'email_address_recipient' ),
                  array(   'table' => 'red_item_server_access',
                    'field' => 'server_access_login'),
                  array(   'table' => 'red_item_web_conf',
                    'field' => 'web_conf_login'),
                  array(   'table' => 'red_item_hosting_order_access',
                    'field' => 'hosting_order_access_login')

                       );
        // To construct messages, create a friendly name from the
        // the table name
        $search = array('red_item_','_');
        $replace = array('',' ');
          
        $hosting_order_id = intval($this->get_hosting_order_id());
        foreach($checks as $check) {
          $table = $check['table'];
          $field = $check['field'];
          $sql = "SELECT red_item.item_id FROM red_item INNER JOIN $table ".
            "USING(item_id) WHERE $field = ".
            "'$login' AND item_status != 'deleted' AND item_status ".
            "!= 'pending-delete' && item_status != 'transfer-limbo' ".
            "AND hosting_order_id = $hosting_order_id";
          $result = $this->_sql_query($sql);
          if($this->_sql_num_rows($result) > 0) {
            $friendly = str_replace($search,$replace,$table);
            $message = red_t("This user account is being used in another service. Please check all the service tabs and delete any corresponding records first.");
            $this->set_error($message,'validation');
          }
        }
      }
    }

    // This function can be called statically
    static function get_related_user_account_logins($hosting_order_id = null,$sql_resource = null) {
      if(is_null($hosting_order_id)) {
        $hosting_order_id = $this->get_hosting_order_id();
      }
      $hosting_order_id = intval($hosting_order_id);

      if(is_null($sql_resource)) {
        $sql_resource = $this->_sql_resource;
      }

      $sql = "SELECT user_account_login FROM red_item INNER JOIN ".
        "red_item_user_account ON red_item.item_id = ".
        "red_item_user_account.item_id WHERE hosting_order_id = ".
        $hosting_order_id . " AND (item_status = ".
        "'active' OR item_status = 'pending-insert' OR item_status = 'disabled') ".
        "ORDER BY user_account_login";
      //print_r($this->_sql_resource);
      $result = red_sql_query($sql,$sql_resource);
      $ret = array();
      while($row = red_sql_fetch_row($result)) {
        $user_account = $row[0];
        $ret[$user_account] = $user_account;
      }
      if(count($ret) == 0) $ret[''] = red_t('[No user accounts have been entered!]');
      return $ret;
    }
  }  
}

?>
