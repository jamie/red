<?php

if(!class_exists('red_item_mysql_user')) {
  class red_item_mysql_user extends red_item {
    var $_mysql_user_name;
    var $_mysql_user_password;
    var $_mysql_user_db;
    var $_mysql_user_priv;
    var $_mysql_user_max_connections = 25;
    var $_human_readable_description;
    var $_human_readable_name;
    var $_illegal_user_names = [ 'root' ];

    // If the web app is creating the database, we need to save a clear text
    // password for web app setup. Otherwise, we should not do this.
    var $_mysql_user_clear_password;

    function set_mysql_user_clear_password($value) {
      $this->_mysql_user_clear_password = $value;
    }
    function get_mysql_user_clear_password() {
      return $this->_mysql_user_clear_password;
    }

    function __construct($co) {
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array ('mysql_user_name' => array (
                               'req' => true,
                               'pcre'   => RED_MYSQL_USER_MATCHER,
                               'pcre_explanation'   => RED_MYSQL_USER_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Database User Name'),
                               'user_insert' => true,
                               'user_update' => false,
                               'user_visible' => true,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 64,
                               'tblname'   => 'red_item_mysql_user',
                               'filter' => true),
                             'mysql_user_password' => array (
                               'req' => true,
                               'pcre'   => RED_TEXT_MATCHER,
                               'pcre_explanation'   => RED_TEXT_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Database password'),
                               'user_insert' => true,
                               'user_update' => true,
                               'user_visible' => false,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 64,
                               'tblname'   => 'red_item_mysql_user'),
                             'mysql_user_db' => array (
                               'req' => true,
                               'pcre'   => RED_MYSQL_DBS_MATCHER,
                               'pcre_explanation'   => RED_MYSQL_DBS_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('DB to grant access'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 64,
                               'tblname'   => 'red_item_mysql_user',
                               'filter' => true),
                             'mysql_user_priv' => array (
                               'req' => true,
                               'pcre'   => RED_SQL_PRIV_MATCHER,
                               'pcre_explanation'   => RED_SQL_PRIV_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Type of access'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 64,
                               'tblname'   => 'red_item_mysql_user',
                               'filter' => true),
                             'mysql_user_max_connections' => array (
                               'req' => true,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Max Connections'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 4,
                               'text_max_length' => 4,
                               'tblname'   => 'red_item_mysql_user',
                               'filter' => true),

                                 ));


      $this->_datafields['item_host']['user_visible'] = TRUE;
    }

    function set_mysql_user_name($value) {
      $this->_mysql_user_name = $value;
    }
    function get_mysql_user_name() {
      return $this->_mysql_user_name;
    }
    function set_mysql_user_password($value) {
      $this->_mysql_user_password = $value;
    }
    function get_mysql_user_password() {
      return $this->_mysql_user_password;
    }

    function set_mysql_user_db($value) {
      $this->_mysql_user_db = $value;
    }
    function get_mysql_user_db() {
      return $this->_mysql_user_db;
    }

    function set_mysql_user_priv($value) {
      $this->_mysql_user_priv = $value;
    }
    function get_mysql_user_priv() {
      return $this->_mysql_user_priv;
    }
    function set_mysql_user_max_connections($value) {
      $this->_mysql_user_max_connections = $value;
    }
    function get_mysql_user_max_connections() {
      return $this->_mysql_user_max_connections;
    }

    function additional_validation() {
      if (in_array($this->get_mysql_user_name(), $this->_illegal_user_names)) {
        $this->set_error(red_t("Invalid database username."),'validation');
      }
      if($this->_delete) return true;
      if(!$this->database_selection_is_valid()) {
        $this->set_error(red_t("Invalid database selected. You can only choose databases that are on the same host."),'validation');
      }
      if(!$this->is_field_value_unique('mysql_user_name','red_item_mysql_user',$this->get_mysql_user_name())) {
        $this->set_error(red_t('The database user name you chose is already taken.'),'validation');
      }
    }

    function get_over_quota_databases() {
      $ret = [];
      // Check if we are providing access to a database that is over quota.
      $user_dbs = explode(':',$this->get_mysql_user_db());
      foreach ($user_dbs as $db) {
        $db = trim($db);
        // skip empties
        if(empty($db)) continue;
        $db_safe = addslashes($db);
        $sql = "SELECT item_disk_usage, item_quota FROM red_item JOIN 
          red_item_mysql_db USING(item_id) WHERE item_status != 'deleted' AND 
          mysql_db_name = '$db_safe'";
        $result = $this->_sql_query($sql);
        while($row = $this->_sql_fetch_row($result)) {
          $disk_usage = $row[0];
          $quota = $row[1];
          if (empty($quota)) {
            // 0 means no quota.
            continue;
          }
          if ($quota < $disk_usage) {
            $ret[] = $db_safe;
          }
        }
      }
      if (count($ret) > 0) {
        return $ret;
      }
      return FALSE;
    }
    
    function database_selection_is_valid() {
      $user_dbs = explode(':',$this->get_mysql_user_db());
      $allowed_dbs = $this->get_available_databases();
      $locked_in_db_host = NULL;
      foreach ($user_dbs as $db) {
        $db = trim($db);
        // skip empties (will be caught by previous validation)
        if(empty($db)) continue;

        // This database is not in the list of allowed databases. You can't pick a database
        // from one host for a user on a different host (and you definitely can't pick
        // a database not owned by your membership).
        if(!array_key_exists($db,$allowed_dbs)) return FALSE;

        // Also... you can create a new user that has access to two databases on 
        // different hosts.
        $host = $allowed_dbs[$db];
        if (is_null($locked_in_db_host)) {
          // This is the first db.
          $locked_in_db_host = $host;
        }
        if ($host != $locked_in_db_host) {
          // This database is different from a previous database.
          return FALSE;
        }
      }
      return TRUE;
    }

    function get_priv_options() {
      return array('full' => 'full', 'read' => 'read');
    }

    function get_available_databases() {
      $ret = array();
      $member_id = intval($this->get_member_id());
      // For a new user, show all databases owned by this member.
      if (!$this->exists_in_db()) {
        $sql = "SELECT mysql_db_name, item_host FROM red_item JOIN red_item_mysql_db ".
          "USING(item_id) JOIN red_hosting_order USING (hosting_order_id) WHERE " .
          "red_hosting_order.member_id = $member_id " .
          " AND (item_status = 'active' OR item_status = 'pending-insert' OR ".
          "item_status  = 'pending-update' OR item_status = 'disabled' )";
      }
      else {
        // For an existing user, only show databases on the same server as the user.
        $item_host = addslashes($this->get_item_host());
        $sql = "SELECT mysql_db_name, item_host FROM red_item JOIN red_item_mysql_db ".
          "USING(item_id) JOIN red_hosting_order USING(hosting_order_id) ".
          " WHERE red_hosting_order.member_id = $member_id " . 
          " AND item_host = '$item_host' AND  (item_status = 'active' OR ".
          "item_status = 'pending-insert' OR ".
          "item_status  = 'pending-update' OR item_status = 'disabled' )";
      }
      $result = $this->_sql_query($sql);
      while($row = $this->_sql_fetch_row($result)) {
        $db = $row[0];
        $host = $row[1];
        $ret[$db] = $host;
      }
      return $ret;
    }

    // Add the plain text password to the password cache file so it can be used by
    // web_apps that need to use it for the configuration file.
    function _post_commit_to_db() {
      if(!$this->set_clear_password()) return FALSE;
      return parent::_post_commit_to_db();
    }

    function set_clear_password() {
      if($this->_delete) return TRUE;
      // Only run if we are directed to save the clear password.
      $password = $this->get_mysql_user_clear_password();
      if(is_null($password)) {
        // No clear password to save.
        return TRUE;
      }
      $item_id = $this->get_item_id();
      if(red_add_password_to_cache($item_id, $password, $this->_sql_resource)) {
        return TRUE;
      }
      red_set_message(red_t("Error setting clear password for MySQL user."), 'error');
      return FALSE;
    }

    // Override item_host. For mysql users, we have to pick the same
    // host as the database we are configured to use.
    function _pre_commit_to_db() {
      $user_dbs = explode(':',$this->get_mysql_user_db());
      $dbs_with_hosts = $this->get_available_databases();
      foreach($user_dbs as $db) {
        if (empty($db)) continue;
        // Pick the first chosen database that is not empty and return
        // it's host.
        $this->set_item_host($dbs_with_hosts[$db]);
        break;
      }
      return parent::_pre_commit_to_db();
    }
  }  
}

?>
