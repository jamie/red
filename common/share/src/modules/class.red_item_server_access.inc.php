<?php

// email-account - this class is for mainipulating email accounts
if(!class_exists('red_item_server_access')) {
  class red_item_server_access extends red_item {
    var $_server_access_login;
    var $_server_access_public_key;

    var $_human_readable_description;
    var $_human_readable_name;

    function __construct($co) {
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array ('server_access_login' => array (
                               'req' => true,
                               'pcre'   => RED_LOGIN_MATCHER,
                               'pcre_explanation'   => RED_LOGIN_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Login name'),
                               'user_insert' => TRUE,
                               'user_update' => FALSE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 128,
                               'tblname'   => 'red_item_server_access',
                               'filter' => true),
                             'server_access_public_key' => array (
                               'req' => false,  
                               'pcre'   => RED_SSH_PUBLIC_KEY_MATCHER,
                               'pcre_explanation'   => RED_SSH_PUBLIC_KEY_EXPLANATION,
                               'type'  => 'text',
                               'fname'  => red_t('Public Key (optional)'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'textarea',
                               'textarea_cols' => 50,
                               'textarea_rows' => 5,
                               'tblname'   => 'red_item_server_access')
                                 ));

    }

    function set_server_access_login($value) {
      $this->_server_access_login = $value;
    }
    function get_server_access_login() {
      return $this->_server_access_login;
    }

    function set_server_access_public_key($value) {
      $this->_server_access_public_key = $value;
    }
    function get_server_access_public_key() {
      return $this->_server_access_public_key;
    }

    function additional_validation() {
      $related_user_accounts = $this->get_related_user_account_logins();
      if(!in_array($this->get_server_access_login(),$related_user_accounts) && !$this->_delete) {
        $this->set_error(red_t("You can only specify a user account that exists."),'validation');
      }
    }

    function get_related_user_account_logins() {
      require_once($this->_common_src_path . '/modules/class.red_item_user_account.inc.php');
      return red_item_user_account::get_related_user_account_logins($this->get_hosting_order_id(),$this->_sql_resource);
    }
  }  
}

?>
