<?php

 // cron job -for manipulating cron jobs 
if(!class_exists('red_item_cron')) {
  class red_item_cron extends red_item {
    var $_cron_login;
    var $_cron_schedule;
    var $_cron_cmd;
    var $_human_readable_description;
    var $_human_readable_name;

    function __construct($co = array()) {
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array (
                             'cron_login' => array (
                               'req' => true,
                               'pcre'   => RED_LOGIN_MATCHER,
                               'pcre_explanation'   => RED_LOGIN_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Login'),
                               'user_insert' => TRUE,
                               'user_update' => FALSE,
                               'user_visible' => TRUE,
                               'tblname'   => 'red_item_cron',
                               'filter' => true),
                             'cron_cmd' => array (
                               'req' => true,
                               'pcre'   => false,
                               'pcre_explanation'   => false,
                               'type'  => 'varchar',
                               'fname'  => red_t('Command'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 100,
                               'text_max_length' => 255,
                               'tblname'   => 'red_item_cron',
                               'filter' => true),
                             'cron_schedule' => array (
                               'req' => true,
                               'pcre'   => false,
                               'pcre_explanation'   => false,
                               'type'  => 'varchar',
                               'fname'  => red_t('Schedule'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 128,
                               'tblname'   => 'red_item_cron'),
                           ));

    }

    function set_cron_login($value) {
      $this->_cron_login = strtolower($value);
    }
    function get_cron_login() {
      return $this->_cron_login;
    }

    function set_cron_schedule($value) {
      $this->_cron_schedule = trim($value);
    }
    function get_cron_schedule() {
      return $this->_cron_schedule;
    }

    function set_cron_cmd($value) {
      $this->_cron_cmd = $value;
    }
    function get_cron_cmd() {
      return $this->_cron_cmd;
    }

    function additional_validation() {
      $login = $this->get_cron_login();
      // Check the login 
      // If login is left blank, it will be caught by the normal
      // validation, so skip it (we don't want to give two error
      // messages on the same field).
      if(!empty($login)) {
        // must be a valid login (unless we're deleting)
        if(!in_array($login,$this->get_related_user_account_logins()) && !$this->_delete && !$this->_disable ) {
          $this->set_error(red_t("You must specify a user account that exists and has server access."),'validation');
        }
      }
      $schedule = $this->get_cron_schedule();
      // Check the schedule 
      // If schedule is left blank, it will be caught by the normal
      // validation, so skip it (we don't want to give two error
      // messages on the same field).
      if(!empty($schedule)) {
        // loads of possibilities
        // check for special values
        $specials = array('@reboot','@yearly','@annually','@monthly','@weekly','@daily','@midnight','@hourly');
        if(!in_array($schedule,$specials)) { 
          // If it's not a special value, it must be 5 white space 
          // separated values
          $allowed_times = array(
            '\*', // *
            '([0-9]{1,2}(\-[0-9]{1,2})?,?)+', // 4 or 11 or 5-12 or 5-12,2-3
            '([0-9]{1,2}\-[0-9]{1,2}|\*)\/[0-9]{1,2}', // 0-23/2 or */2
            'sun|mon|tue|wed|thu|fri|sat',
            'jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec',    
          );

          // Replace tabs and multiple spaces with single spaces
          $replace = array("\t",'    ','   ','  ');
          $schedule = str_replace($replace,' ',$schedule);
          $array = explode(' ',$schedule);
          if(count($array) != 5) {
            $count = count($array);
            $this->set_error(red_t("Invalid scheduled time. Please enter 5 space separated values."),'validation');
          } else {
            foreach($array as $line) {
              reset($allowed_times);
              $pass = false;
              foreach($allowed_times as $preg) {
                if(preg_match("/^$preg$/",$line))
                  $pass = true;
              }
              if(!$pass)
                $this->set_error(red_t("Invalid scheduled time."),'validation');
            }
          }
        }
      }
    }

    function get_related_user_account_logins() {
      $hosting_order_id = intval($this->get_hosting_order_id());

      $sql = "SELECT server_access_login FROM red_item INNER JOIN ".
        "red_item_server_access USING(item_id) WHERE hosting_order_id = ".
        $hosting_order_id . " AND (item_status = ".
        "'active' OR item_status = 'pending-insert') ".
        "ORDER BY server_access_login";
      //print_r($this->_sql_resource);
      $result = red_sql_query($sql,$this->_sql_resource);
      $ret = array();
      while($row = red_sql_fetch_row($result)) {
        $user_account = $row[0];
        $ret[$user_account] = $user_account;
      }
      if(count($ret) == 0) $ret[''] = red_t("[No user accounts have been granted server access!]");
      return $ret;
    }
      
  }  
}



?>
