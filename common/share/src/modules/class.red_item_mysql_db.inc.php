<?php

if(!class_exists('red_item_mysql_db')) {
  class red_item_mysql_db extends red_item {
    var $_track_disk_usage = TRUE;
    var $_mysql_db_name;
    var $_mysql_db_host;
    var $_human_readable_description;
    var $_human_readable_name;
    var $_illegal_database_name_patterns = [ '^information_schema$', '^performance_schema$', '^mysql$', '^lost\+found$', '^aria_log', '^ibdata', '^ib_logfile', '^ibtmp', '^phpmyadmin$', '^tc\.log$' ];
    var $_item_quota = '1gb';

    function __construct($construction_options) {
      parent::__construct($construction_options);
      $this->_set_child_datafields();
      $selected_db_host = $this->get_mysql_db_host();
      if (empty($selected_db_host)) {
        $this->set_mysql_db_host($this->get_default_db_host());
      }
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
        array (
          'mysql_db_name' => array (
             'req' => true,
             'pcre'   => RED_MYSQL_DB_MATCHER,
             'pcre_explanation'   => RED_MYSQL_DB_EXPLANATION,
             'type'  => 'varchar',
             'fname'  => red_t('Database name'),
             'user_insert' => TRUE,
             'user_update' => FALSE,
             'user_visible' => TRUE,
             'input_type' => 'text',
             'text_length' => 20,
             'text_max_length' => 64,
             'tblname'   => 'red_item_mysql_db',
             'filter' => true
          ),
          'mysql_db_host' => array (
             'req' => false,
             'pcre'   => RED_DOMAIN_MATCHER,
             'pcre_explanation'   => RED_DOMAIN_EXPLANATION,
             'type'  => 'varchar',
             'fname'  => red_t('Database Host'),
             'user_insert' => FALSE,
             'user_update' => FALSE,
             'user_visible' => TRUE,
             'input_type' => 'text',
             'text_length' => 20,
             'text_max_length' => 64,
             'tblname'   => 'red_item_mysql_db',
             'filter' => true
           ),
        )
      );
      $this->_datafields['item_host']['user_visible'] = TRUE;
    }

    function set_mysql_db_name($value) {
      $this->_mysql_db_name = $value;
    }
    function get_mysql_db_name() {
      return $this->_mysql_db_name;
    }
    function set_mysql_db_host($value = NULL) {
      $this->_mysql_db_host = $value;
    }
    function get_mysql_db_host() {
      return $this->_mysql_db_host;
    }
    // Databases have an item_host (which is the virtual guest hosting the database)
    // as well as a mysql_db_host which how they access the database (should be localhost
    // for all database, but there is room for change).
    function get_default_db_host() {
      return 'localhost';  
    }

    function additional_validation() {
      if ($this->get_item_quota() == 0) {
        $this->set_error(red_t('The database quota cannot be set to 0.'),'validation');
      }
      foreach($this->_illegal_database_name_patterns as $pattern) {
        if (preg_match("/$pattern/", $this->get_mysql_db_name())) {
          $this->set_error(red_t('The database name you chose is not allowed.'),'validation');
        }
      }

      if(!$this->_delete && !$this->is_field_value_unique('mysql_db_name','red_item_mysql_db',$this->get_mysql_db_name())) {
        $this->set_error(red_t('The database name you chose is already taken.'),'validation');
      }
    }
  }
}

?>
