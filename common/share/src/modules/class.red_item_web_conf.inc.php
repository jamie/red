<?php

// email-account - this class is for mainipulating email accounts
if(!class_exists('red_item_web_conf')) {
  class red_item_web_conf extends red_item {
    var $_track_disk_usage = TRUE;
    var $_web_conf_login;
    var $_web_conf_execute_as_user;
    var $_web_conf_settings;
    var $_web_conf_cache_settings;
    var $_web_conf_domain_names;
    var $_web_conf_port;
    var $_web_conf_cgi = 0;
    var $_web_conf_max_processes = '12';
    var $_web_conf_max_memory = '256';
    // 1 means add https, 0 means http only.
    var $_web_conf_tls;
    // a value for key or cert means use that key or cert, leave blank
    // means use lets encrypt.
    var $_web_conf_tls_key;
    var $_web_conf_tls_cert;
    // 0 means don't redirect http to https automaticalluy, 1 means redirect
    // (but leave exception for .well-known/acme-challenge).
    var $_web_conf_tls_redirect = 1;
    var $_web_conf_document_root;
    // 0 means no logging at all, 1 means only error logging, 2 means full logging. 
    var $_web_conf_logging = 2;
    var $_web_conf_php_version = NULL;
    var $_web_conf_mountpoint;
    var $_web_conf_php_version_options = array(
      '7.4' => '7.4',
      '7.3' => '7.3',
      '7.2' => '7.2',
      '7.1' => '7.1',
      '7.0' => '7.0',
      '5.6' => '5.6 - DEPRECATED, will not be available after 2018-12-31',
    );

    var $_human_readable_description;
    var $_human_readable_name;
    var $_delete_confirmation_message;

    // Specify which keys should be validated against which tests
    var $_email_values = array('ServerAdmin');
    var $_path_values = array(
      'AuthUserFile',
      'AuthOpenIDDBLocation',
      'SSLCACertificateFile'
    );

    // Double values are two values separated by a space
    // For these, test both parts
    var $_double_values = array('Alias','ScriptAlias');

    function __construct($co) {
      parent::__construct($co);
      $this->_set_child_datafields();
      // If it's a new item, we have to autoset the mountopint if one is
      // specified in red_server for this item's host.
      if (!$this->exists_in_db()) {
        $this->set_web_conf_mountpoint($this->get_mountpoint_for_host());
      }
      return true;
    }

    function get_default_php_version_for_host($host) {
      $default_php_version = NULL;  
      $server = addslashes($host);
      $sql = "SELECT server_default_php_version FROM red_server WHERE server = '$server'";
      $result = $this->_sql_query($sql);
      $row = $this->_sql_fetch_row($result);
      if ($row) {
        $default_php_version = $row[0];
      }
      return $default_php_version;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                     array( 'web_conf_login' => array (
                               'req' => true,
                               'pcre'   => RED_LOGIN_MATCHER,
                               'pcre_explanation'   => RED_LOGIN_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Owner'),
                               'description' => red_t("i.e, the sftp user, files will be owned by this user"),
                               'user_insert' => TRUE,
                               'user_update' => FALSE,
                               'user_visible' => TRUE,
                               'tblname'   => 'red_item_web_conf',
                               'filter' => true),
                             'web_conf_execute_as_user' => array (
                               'req' => true,
                               'pcre'   => RED_LOGIN_MATCHER,
                               'pcre_explanation'   => RED_LOGIN_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Execute as'),
                               'description' => red_t("The user that site scripts will execuate as."),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'tblname'   => 'red_item_web_conf',
                               'options' => $this->get_related_user_account_logins(),
                               'input_type' => 'select',
                               'filter' => true),
                             'web_conf_domain_names' => array (
                               'req' => true,
                               'pcre'   => RED_MULTIPLE_DOMAIN_WILDCARD_MATCHER,
                               'pcre_explanation'   => RED_MULTIPLE_DOMAIN_WILDCARD_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Domain Names'),
                               'description' => red_t("List all domain names this site should respond to."),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'text_length' => 88,
                               'text_max_length' => '512',
                               'tblname'   => 'red_item_web_conf',
                               'filter' => true),
                             'web_conf_tls' => array (
                               'req' => true,
                               'pcre'   => RED_TINYINT_MATCHER,
                               'pcre_explanation'   => RED_TINYINT_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Encryption'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'select',
                               'options' => array(0 => 'http only', 1 => 'https enabled'),
                               'tblname'   => 'red_item_web_conf',
                               'filter' => true),
                            'web_conf_document_root' => array (
                               'req' => false,
                               'pcre'   => RED_PATH_MATCHER,
                               'pcre_explanation'   => RED_PATH_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Document Root'),
                               'description' => red_t("If your actual web root is in a subdirectory, list the subdirectory here."),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'text_length' => 88,
                               'text_max_length' => '512',
                               'tblname'   => 'red_item_web_conf',
                               'filter' => true),
                             'web_conf_settings' => array (
                               'req' => false,
                               'pcre'   => false,
                               'pcre_explanation'   => false,
                               'type'  => 'varchar',
                               'fname'  => red_t('Settings'),
                               'description' => red_t("Add any additional apache configurations you want."),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_web_conf',
                               'input_type' => 'textarea',
                               'textarea_cols' => 50,
                               'textarea_rows' => 5,
                               'filter' => true),
                             'web_conf_logging' => array (
                               'req' => true,
                               'pcre'   => RED_TINYINT_MATCHER,
                               'pcre_explanation'   => RED_TINYINT_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Logging'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_web_conf',
                               'input_type' => 'select',
                               'options' => array(0 => red_t("Disable Logging"), 1 => red_t("Error logging only"), 2 => red_t("Access and Error logging")),
                               'filter' => true),
                            'web_conf_max_processes' => array (
                               'req' => true,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Max PHP Processes'),
                               'description' => red_t("Maximum number of allowed PHP processes, 0 to disable PHP"),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_web_conf',
                               'filter' => true),
                            'web_conf_max_memory' => array (
                               'req' => true,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Max Allowed Memory'),
                               'description' => red_t("PHP memory limit in Mb, contact support if you need more than 512"),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_web_conf',
                               'filter' => true),
                            'web_conf_cgi' => array (
                               'req' => true,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Enable CGI'),
                               'description' => red_t("CGI is not needed for PHP"),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_web_conf',
                               'input_type' => 'select',
                               'options' => array(0 => red_t('disabled'), 1 => red_t('enabled')),
                               'filter' => true),
                            'web_conf_tls_redirect' => array (
                               'req' => true,
                               'pcre'   => RED_TINYINT_MATCHER,
                               'pcre_explanation'   => RED_TINYINT_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Redirect http'),
                               'description' => red_t("This setting is ignored on http only sites."),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_web_conf',
                               'input_type' => 'select',
                               'options' => array(0 => red_t("Do not redirect traffic"), 1 => red_t("Redirect http traffic to https")),
                               'filter' => true),
                            'web_conf_tls_key' => array (
                               'req' => false,
                               'pcre'   => RED_PATH_MATCHER,
                               'pcre_explanation'   => RED_PATH_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('TLS Key path'),
                               'description' => red_t("Absolute path to https key (leave blank to use Lets Encrypt)"),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'text_length' => 88,
                               'text_max_length' => '512',
                               'tblname'   => 'red_item_web_conf',
                               'filter' => true),
                            'web_conf_tls_cert' => array (
                               'req' => false,
                               'pcre'   => RED_PATH_MATCHER,
                               'pcre_explanation'   => RED_PATH_EXPLANATION,
                               'type'  => 'varchar',
                               'fname' => red_t("TLS Cert path"),
                               'description'  => red_t('Absolute path to https cert (leave blank to use Lets Encrypt)'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'text_length' => 88,
                               'text_max_length' => '512',
                               'tblname'   => 'red_item_web_conf',
                               'filter' => true),
                           'web_conf_cache_settings' => array (
                               'req' => false,
                               'pcre'   => false,
                               'pcre_explanation'   => false,
                               'type'  => 'varchar',
                               'fname'  => red_t('Cache Settings'),
                               'description' => red_t("Settings used by nginx caching server"),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_web_conf',
                               'input_type' => 'textarea',
                               'textarea_cols' => 50,
                               'textarea_rows' => 5,
                               'filter' => true),
                           'web_conf_php_version' => array (
                               'req' => true,
                               'pcre'   => RED_DECIMAL_MATCHER,
                               'pcre_explanation'   => RED_DECIMAL_EXPLANATION,
                               'type'  => 'decimal',
                               'fname'  => red_t('PHP Version'),
                               'description' => red_t("Choosing the non-default version may result in being auto-upgraded when that version stops being supported. Choose the default version to minimize transitions."),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_web_conf',
                               'input_type' => 'select',
                               'options' => $this->_web_conf_php_version_options,
                               'filter' => true),
                            'web_conf_mountpoint' => array (
                               'req' => false,  
                               'pcre'   => RED_ANYTHING_MATCHER,
                               'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Mount Point'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 8,
                               'text_max_length' => 8,
                               'tblname'   => 'red_item_web_conf',
                               'filter' => false),

        ));

    }

    function set_web_conf_login($value) {
      $this->_web_conf_login = $value;
    }
    function get_web_conf_login() {
      return $this->_web_conf_login;
    }

    function set_web_conf_execute_as_user($value) {
      $this->_web_conf_execute_as_user = $value;
    }
    function get_web_conf_execute_as_user() {
      return $this->_web_conf_execute_as_user;
    }

    function set_web_conf_domain_names($value) {
      $this->_web_conf_domain_names = $value;
    }
    function get_web_conf_domain_names() {
      return $this->_web_conf_domain_names;
    }

    function set_web_conf_settings($value) {
      $this->_web_conf_settings = $value;
    }
    function get_web_conf_settings() {
      return $this->_web_conf_settings;
    }

    function set_web_conf_max_processes($value) {
      $this->_web_conf_max_processes = trim($value);
    }
    function get_web_conf_max_processes() {
      return $this->_web_conf_max_processes;
    }

    function set_web_conf_cgi($value) {
      $this->_web_conf_cgi = trim($value);
    }
    function get_web_conf_cgi() {
      return $this->_web_conf_cgi;
    }
    function set_web_conf_logging($value) {
      $this->_web_conf_logging = trim($value);
    }
    function get_web_conf_logging() {
      return $this->_web_conf_logging;
    }

    function set_web_conf_document_root($value) {
      $this->_web_conf_document_root = trim($value);
    }
    function get_web_conf_document_root() {
      return $this->_web_conf_document_root;
    }
    function set_web_conf_tls($value) {
      $this->_web_conf_tls = trim($value);
    }
    function get_web_conf_tls() {
      return $this->_web_conf_tls;
    }
    function set_web_conf_tls_key($value) {
      $this->_web_conf_tls_key = trim($value);
    }
    function get_web_conf_tls_key() {
      return $this->_web_conf_tls_key;
    }
    function set_web_conf_tls_cert($value) {
      $this->_web_conf_tls_cert = trim($value);
    }
    function get_web_conf_tls_cert() {
      return $this->_web_conf_tls_cert;
    }
    function set_web_conf_tls_redirect($value) {
      $this->_web_conf_tls_redirect = trim($value);
    }
    function get_web_conf_tls_redirect() {
      return $this->_web_conf_tls_redirect;
    }

    function set_web_conf_max_memory($value) {
      $this->_web_conf_max_memory = trim($value);
    }
    function get_web_conf_max_memory() {
      return $this->_web_conf_max_memory;
    }
    function set_web_conf_cache_settings($value) {
      $this->_web_conf_cache_settings = trim($value);
    }
    function get_web_conf_cache_settings() {
      return $this->_web_conf_cache_settings;
    }
    function set_web_conf_php_version($value) {
      $this->_web_conf_php_version = trim($value);
    }
    function get_web_conf_php_version() {
      if (empty($this->_web_conf_php_version)) {
        // Set default if we can.
        $host = $this->get_item_host();
        if ($host) {
          $this->_web_conf_php_version = $this->get_default_php_version_for_host($host);
        } 
      }
      return $this->_web_conf_php_version;
    }

    function set_web_conf_mountpoint($value) {
      $this->_web_conf_mountpoint = $value;
    }
    function get_web_conf_mountpoint() {
      return $this->_web_conf_mountpoint;
    }

    function additional_validation() {
      // If we're deleting - don't need to do validation
      // (and we don't want to choke on something like, user does
      // not exist)
      if($this->_delete) return;
      $login = $this->get_web_conf_login();

      $web_conf_count = $this->web_conf_count();
      if($web_conf_count > 0) {
        $msg = red_t("You can only have one web configuration per hosting order.");
        $this->set_error($msg,'validation');
      }

      // make sure login as execute_as_user are valid users
      if($login != 'none' && !in_array($login,$this->get_related_user_account_logins())) {
        $this->set_error(red_t("You must specify a user account that exists."),'validation');
      }
      $execute_as_user = $this->get_web_conf_execute_as_user();
      if($execute_as_user != 'none' && !in_array($execute_as_user,$this->get_related_user_account_logins())) {
        $this->set_error(red_t("You must specify a user account that exists."),'validation');
      }

      $settings = $this->get_web_conf_settings();
      $settings_array = explode("\n",$settings);

      foreach($settings_array as $line) {
        $line = trim($line);
        // Allow blank lines and lines that start with a comment
        // per dkg's request
        if($line == '') continue;
        if(preg_match('/^#/',$line)) continue;
        // all lines must have at least one space to match the
        // regexp below (FIXME: need a better regexp). If the
        // line has no spaces, like: </Directory>, add one to the
        // end so it matches
        if(false === strpos($line,' ')) $line .= ' ';
        if(preg_match('#^(.*?) (.*)?#',$line,$matches) > 0) {
          $key = trim($matches[1]);
          $given_value = '';
          if(array_key_exists(2,$matches)) $given_value = trim($matches[2]);
          if(!preg_match(RED_WEB_CONF_KEY_MATCHER,$key)) {
            $line = $this->get_htmlentities($line);
            $key = $this->get_htmlentities($key);
            $this->set_error(RED_WEB_CONF_KEY_EXPLANATION, 'validation');
            $this->set_error(red_t("You entered: @key on line: @line.",array('@key' => $key, '@line' => $line)),'validation');
            continue 1;
          }
          // red_set_message("Sanity checkeroo, key: $key, given value: $given_value");
          if($key == 'LogLevel') {
            $allowed = array('debug','info','notice','warn','error','crit','alert','emerg');
            if(!in_array($given_value,$allowed))
              $this->set_error(red_t("LogLevel can only be set to one of: @loglevels.",array('@loglevels' => implode(' ',$allowed))),'validation');
          }
          if($key == 'Satisfy') {
            $allowed = array('All','Any');
            if(!in_array($given_value,$allowed)) {
              $this->set_error(red_t("Satisfy should be set to either All or Any."),'validation');
            }
          }
          if ($key == 'SuexecUserGroup' ) {
            $group_name = $this->get_hosting_order_unix_group_name();
            $login = $this->get_web_conf_login();
            $user_group = explode(' ',$given_value);
            if(($user_group[0] != $login && $user_group != '#user#') || $user_group[1] != $group_name) {
              $this->set_error("SuexecUserGroup must be set to $login $group_name", 'validation');
            }
          }
          $check = '';
          if(in_array($key,$this->_email_values)) $check = 'email';
          if(in_array($key,$this->_path_values)) $check = 'path';

          // If this value actually has two values in it
          // (such as Alias or ScriptAlias)
          if(in_array($key,$this->_double_values)) {
            $values_array = explode(' ',$given_value);
          } else {
            $values_array = array($given_value);
          }

          foreach($values_array as $value) {
            switch($check) {
              case 'email':
                if(!preg_match(RED_EMAIL_MATCHER,$value))
                  $this->set_error(RED_EMAIL_EXPLANATION,'validation');
                break;
              case 'path':
                if(!preg_match(RED_PATH_MATCHER,$value))
                  $this->set_error(RED_PATH_EXPLANATION,'validation');
                break;
            }
          }

        } else {
          // might have html code that will hide it
          $line = $this->get_htmlentities($line);
          $this->set_error(red_t("There was an error parsing your configuration. Please review your changes and try again."),'validation');
        }

      }
      
    }

    // NOTE: this allows logins from other hosting orders
    // assigned to the same unique unix group, not just
    // this particular hosting order!
    // also includes pending-insert accounts - so that
    // a script can add a user_account and web_conf at the
    // same time, provided it enters the user_account first.
    // and lastly, allow disabled logins so you can disable
    // a login while still having a valid web conf (in case you
    // want to disable logins, but still run the site)
    function get_related_user_account_logins() {
      $group_id = $this->get_hosting_order_unix_group_id();
      $sql = "SELECT user_account_login FROM red_item INNER JOIN ".
        "red_item_user_account ON red_item.item_id = ".
        "red_item_user_account.item_id INNER JOIN red_hosting_order ".
        "ON red_hosting_order.hosting_order_id = ".
        "red_item.hosting_order_id WHERE ".
        "red_hosting_order.unique_unix_group_id = $group_id ".
        " AND (item_status = 'active' OR item_status = 'pending-insert' ".
        " OR item_status = 'disabled') ORDER BY user_account_login";

      $result = $this->_sql_query($sql);
      $ret = array();
      while($row = $this->_sql_fetch_row($result)) {
        $user_account = $row[0];
        $ret[$user_account] = $user_account;
      }

      return $ret;
    }

    /**
     * Count existing web confs other than this one
     *
     * We need to be sure there are no
     * other web confs in place.
     **/
    function web_conf_count() {
      $hosting_order_id = intval($this->get_hosting_order_id());
      $item_id = intval($this->get_item_id());
      $sql = "SELECT red_item.item_id FROM red_item INNER JOIN ".
        "red_item_web_conf ON red_item.item_id = ".
        "red_item_web_conf.item_id WHERE ".
        "hosting_order_id = $hosting_order_id  AND red_item.item_id != $item_id ".
        "AND red_item.item_status != 'deleted'";
      $result = $this->_sql_query($sql);
      return intval($this->_sql_num_rows($result));
    }

  }

}



?>
