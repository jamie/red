<?php

// email-account - this class is for mainipulating email accounts
if(!class_exists('red_item_email_address')) {
  class red_item_email_address extends red_item {
    var $_email_address;
    var $_email_address_recipient;
    var $_human_readable_description;
    var $_human_readable_name;

    function __construct($co) {
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array ('email_address' => array (
                               'req' => true,
                               'pcre'   => RED_VIRTUAL_EMAIL_MATCHER,
                               'pcre_explanation'   => RED_VIRTUAL_EMAIL_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Email adddress'),
                               'user_update' => FALSE,
                               'user_insert' => TRUE,
                               'user_visible' => TRUE,
                               'css_class' => 'red_email',
                               'input_type' => 'text',
                               'text_length' => 40,
                               'text_max_length' => 255,
                               'tblname'   => 'red_item_email_address',
                               'filter' => true),
                             'email_address_recipient' => array (
                               'req' => true,
                               'pcre'   => RED_EMAIL_RECIPIENT_MATCHER,
                               'pcre_explanation'   => RED_EMAIL_RECIPIENT_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Recipient'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 255,
                               'tblname'   => 'red_item_email_address',
                               'filter' => true)
                                 ));

    }

    function set_email_address($value) {
      $this->_email_address = strtolower($value);
    }
    function get_email_address() {
      return $this->_email_address;
    }

    function set_email_address_recipient($value) {
      $this->_email_address_recipient = strtolower($value);
    }
    function get_email_address_recipient() {
      return $this->_email_address_recipient;
    }

    function get_domain_portion_of_address() {
      $domain = strstr($this->get_email_address(),'@');
      return substr($domain,1);
    }

    function get_username_portion_of_address() {
      $address = $this->get_email_address();
      $at_char_pos = strpos($this->get_email_address(),'@');
      return substr($address,0,$at_char_pos);
    }

    function additional_validation() {
      // if we're deleting don't do additional validation
      if($this->_delete) return true;

      // Make sure email address is unique
      if(!$this->is_field_value_unique('email_address','red_item_email_address',$this->get_email_address())) {
        $this->set_error(red_t('The email address you chose is already taken.'),'validation');
      }

      $recipient = $this->get_email_address_recipient();
      $address = $this->get_email_address();

      // Now check the recipient
      // If recipient is left blank, it will be caught by the normal
      // validation, so skip it (we don't want to give two error
      // messages on the same field).
      if($recipient != '') {
        $recipients_array = explode(',',$recipient);
        // iterate through the list
        foreach($recipients_array as $single_recipient) {
          $single_recipient = trim($single_recipient);

          // make sure recipient and address are not the same
          if($single_recipient == $address) {
            $this->set_error(red_t('You have set your address to be the same as the recipient - this will cause an endless loop. Please choose either an existig user account or a different email address.'),'validation');
          }
          // if it's not an email, make sure it's a valid login
          if(!preg_match('/\@/',$single_recipient)) {
            // must be a valid login (unless we're deleting)
            if(!in_array($single_recipient,$this->get_related_user_account_logins()) & !$this->_delete) {
              $this->set_error(red_t("You must either specify a valid email address or a user account that exists."),'validation');
            }
          }
        }
      }

      // make sure they are not sneaking in a domain they don't control
      $domain = $this->get_domain_portion_of_address();
      $domain_options = $this->get_email_domains();
      if(!in_array($domain,$domain_options)) {
        // if we're in limbo - loosen restrictions to allow email creation on
        // servers that are not yet configured to accept mail for us.
        if(!$this->in_limbo()) {
          $this->set_error(red_t('That domain is not under your control. It does not have a corresponding MX record in your DNS settings.'),'validation');
        }
      }
    }

    function get_related_user_account_logins() {
      $sql = "SELECT user_account_login FROM red_item INNER JOIN ".
        "red_item_user_account ON red_item.item_id = ".
        "red_item_user_account.item_id WHERE hosting_order_id = ".
        $this->get_hosting_order_id() . " AND (item_status = ".
        "'active' OR item_status = 'pending-insert' OR item_status = 'disabled') ".
        "ORDER BY user_account_login";
      //print_r($this->_sql_resource);
      $result = $this->_sql_query($sql);
      $ret = array();
      while($row = $this->_sql_fetch_row($result)) {
        $user_account = $row[0];
        $ret[$user_account] = $user_account;
      }
      if(count($ret) == 0) $ret[''] = red_t('[No user accounts have been entered!]');
      return $ret;
    }

    // return an array of available domain names that can be used
    // when creating email addresses
    function get_email_domains() {
      // find all mx records matching this host
      // in the default hosting order id 
      $host = $this->get_item_host();
      $hosting_order_id = $this->get_hosting_order_id();
      $sql = "SELECT dns_fqdn FROM red_item_dns INNER JOIN red_item ON ".
        "red_item.item_id = red_item_dns.item_id WHERE ".
        "hosting_order_id = $hosting_order_id ".
        "AND (dns_type = 'mx' OR dns_type = 'mailstore') AND dns_fqdn != '$host' AND ". 
        "item_status != 'deleted' AND item_status != 'pending-delete' ".
        "AND item_status != 'transfer-limbo'";
      $result = $this->_sql_query($sql);
      $options = array();
      while($row = $this->_sql_fetch_row($result)) {
        $fqdn = $row[0];
        $options[$fqdn] = $fqdn;
      }
      return $options;
        
    }

    // Check if the passed domain name is configured with an mx 
    // record pointing to the primary host of this record
    //
    function mx_set_to_primary_host($domain) {
      // simply return true if we are offline (and can't reliably check)
      $tries = 1;
      //if(!red_check_online_status($tries)) return true;

      // by default, do not check to see if the $domain mx record
      // resolves to the computer this code is running on
      $check_local = false;

      // If we are running in server mode, however, we *do* want 
      // that additional check
      if($this->_mode == 'server') $check_local = true;

      $primary_host = $this->get_item_host();
      if(red_domain_mx_resolves_to_server($domain,$primary_host,$check_local))
        return true;

      return false;
    }
  }  
}



?>
