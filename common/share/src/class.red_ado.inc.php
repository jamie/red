<?php
/*
 * This is the red abstract data object
 */

if(!class_exists('red_ado')) {
  class red_ado extends red_db {
    var $_construction_options;
    var $_datafields = array();   // an array of all fields
    var $_exists_in_db = null;    // whether or not this object is in the db
    var $_sql_action;            // whether we're inserting or updating
    var $_errors = array();
    var $_error_types = array('system','validation');
    var $_error_severities = array('hard','soft');
    var $_key_field; // field name of primary key
    var $_key_table; // table name for primary key
    var $_html_generator; // object used to create html widgets
    var $_default_css_field_class = 'red-default-field';
    var $_javascript_includes;
    var $_delete = FALSE;    // a flag indicating that when this item
    // is committed it should be tagged as deleted.
    var $_disable = FALSE; // a flag indicating that when this item
    // is committed it should be tagged as disabled

    // override in child class or set to null to skip confirm on
    // delete
    var $_delete_confirmation_message = 'Are you sure you want to delete this item?';
    var $_human_readable_description = null;
    var $_human_readable_name = null;
    var $_help_link = null;

      
    // constructor
    function __construct($construction_options) {
      parent::__construct($construction_options['sql_resource']);

      $this->_construction_options = $construction_options;
      if(array_key_exists('rs',$construction_options)) {
        $rs = $construction_options['rs'];
        // set item id in case we need it for error logging
        $key_field = $this->get_key_field();
        if(!empty($key_field) && array_key_exists($key_field,$rs)) {
          $this->set_field_value($key_field,$rs[$key_field]);
        }
        // build from recordset
        $this->_initialize_from_recordset($rs);
      } elseif (array_key_exists('id',$construction_options)) {
        // build from the passed id
        $id = $construction_options['id'];
        if(!empty($id)) $this->_initialize_from_id($id);
      } elseif (array_key_exists('values',$construction_options)) {
        $this->_initialize_from_values($construction_options['values']);
      }
    }

    function get_sorted_datafields() {
      $sorted = [];
      $reconstituted = [];
      $index = 0;
      foreach($this->_datafields as $k => $v) {
        $weight = $index;
        if (isset($v['weight'])) {
          $weight = $v['weight'];
        }
        $v['key'] = $k;
        $sorted[$weight] = $v;
        $index++;
      }
      ksort($sorted);
      // Reconstitue
      foreach($sorted as $k => $v) {
        $key = $v['key'];
        unset($v['key']);
        $reconstituted[$key] = $v; 
      }
      return $reconstituted;
    }

    function _get_initialize_sql($id) {
      //override

    }

    function _initialize_from_id($id) {
      $sql = $this->_get_initialize_sql($id);  
      $result = $this->_sql_query($sql);
      $rs = $this->_sql_fetch_assoc($result);
      $this->_construction_options['rs'] =& $rs;
      return $this->_initialize_from_recordset($rs);
    }

    function get_javascript_includes() {
      // this attribute can be optionally set in the ui
      if(isset($this->_javascript_includes)) 
        return $this->_javascript_includes;
      return array();
    }

    function set_html_generator(&$html_generator) {
      if(!is_object($this->_html_generator)) {
        $this->_html_generator =& $html_generator;
      }
      return;
    }

    function get_key_field() {
      return $this->_key_field;
    }

    function get_key_table() {
      return $this->_key_table;
    }

    function _is_error_unique($error,$type,$severity) {
      foreach($this->_errors as $k => $v) {
        if($error == $v['error'] &&
          $type == $v['type'] &&
          $severity == $v['severity'])
          return false;
      }
      return true;
    }

    function set_error($error,$type,$severity = null) {
      if(is_null($severity)) {
        // default
        $severity = 'hard';

        // if it's a validation, change to soft
        if($type == 'validation') $severity = 'soft';
      }

      if(!in_array($type,$this->_error_types))
        trigger_error("The error: '$error' was set without specifying a valid error type. The type passed was: '$type'");
      if(!in_array($severity,$this->_error_severities))
        trigger_error("The error: '$error' was set without specifying a valid severity. The severity passed was: '$severity'");

      if($this->_is_error_unique($error,$type,$severity)) {
        $index = count($this->_errors);
        $this->_errors[$index]['type'] = $type;
        $this->_errors[$index]['severity'] = $severity;
        $this->_errors[$index]['error'] = $error;

        $this->write_error_to_database($index);
      }
    }

    function write_error_to_database($index) {
      // optionally override
      
    }

    function _get_select_sql_statement() {
      // override in child  
    }

    function _initialize_from_recordset($rs) {
      $this->_initialize_from_values($rs);
    }

    function _initialize_from_values($values) {
      foreach($values as $k => $v) {
        $this->set_field_value($k,$v);
      }
    }

    function set_field_value($field,$value) {
      $function = "set_$field";
      $this->$function($value);
    }
  
    // override sql_query so we can add logging
    function _sql_query($sql,$log = TRUE) {
      if(false === $result = @parent::_sql_query($sql)) {
        if($log) $this->set_error("The function _sql_query returned false. The sql error is: " . @mysqli_error($this->_sql_resource) . '. The sql statement is: ' . $sql . '.','system');
        //print_r(debug_backtrace(false));
        return false;
      }
      return $result;
    }

    function _sql_fetch_row($result) {
      return red_sql_fetch_row($result);
    }

    function _sql_fetch_assoc($result) {
      return red_sql_fetch_assoc($result);
    }

    function _sql_insert_id() {
      return red_sql_insert_id($this->_sql_resource);
    }

    function _sql_num_rows($result) {
      return red_sql_num_rows($result);
    }

    function _sql_error() {
      return red_sql_error($this->_sql_resource);
    }

    function get_delete_confirmation_message() {
      return $this->_delete_confirmation_message;
    }

    function get_human_readable_description() {
      return $this->_human_readable_description;
    }

    function get_human_readable_name() {
      return $this->_human_readable_name;
    }

    function get_field_value($field) {
      $function = "get_$field";
      return trim($this->$function());
    }

    function set_user_input($post) {
      // override in child class if you want
      // to do anything special
      // But if you do - please include the accept_user_input check!
      // First - if this record exists in the database
      // then - fill in the values from the db first. 
      // That way - if the submitted post variable does
      // not include all the fields in the table, the fields
      // not included will be preserved at their original
      // value rather than being updated to null.

      $key_field = $this->get_key_field();
      $submit_key_var = 'sf_' . $key_field;
      $this->set_field_value($key_field,$post[$submit_key_var]);
      $this->reset_to_db_values();  

      foreach($post as $k => $v) {
        if(preg_match('/sf_(.*)$/',$k,$matches)) {
          $field = $matches[1];
          if($this->accept_user_input($field))
            $this->set_field_value($field,$v);
        }
      }
    }

    function accept_user_input($field) {
      // determine whether this field will accept user input
      // for this operation
      if (!array_key_exists($field, $this->_datafields)) return FALSE;
      if($this->exists_in_db() && $this->_datafields[$field]['user_update']) return true;
      if(!$this->exists_in_db() && $this->_datafields[$field]['user_insert']) return true;
      return false;
    }

    function reset_to_db_values() {
      if($this->exists_in_db()) {
        if($sql = $this->_get_select_sql_statement()) {
          if($result = $this->_sql_query($sql)) {
            $row = $this->_sql_fetch_assoc($result);
            if(count($row) > 0) $this->_initialize_from_recordset($row);
          }
        }
      }
    }

    function get_edit_field($field) {
      // See if there is an overriding function
      $function_override = "get_edit_$field";
      if(method_exists($this,$function_override)) {
        return $this->$function_override();
      }
      else {
        return $this->get_auto_constructed_edit_field($field);
      }
    }

    function get_auto_constructed_edit_field($field) {
      // Make a best guess
      $value = $this->get_field_value($field);
      $key_field = $this->get_key_field();
      // default input values
      $type = 'text';
      $size = 20;
      $maxlength = 255;
      $cols = 5;
      $rows = 40;
      if(isset($this->_datafields[$field]['input_type']))
        $type = $this->_datafields[$field]['input_type'];
      if(isset($this->_datafields[$field]['text_length']))
        $size = $this->_datafields[$field]['text_length'];
      if(isset($this->_datafields[$field]['text_max_length']))
        $maxlength = $this->_datafields[$field]['text_max_length'];
      if(isset($this->_datafields[$field]['textarea_cols']))
        $cols = $this->_datafields[$field]['textarea_cols'];
      if(isset($this->_datafields[$field]['textarea_rows']))
        $rows = $this->_datafields[$field]['textarea_rows'];

      $attributes = array('class' => 'form-control');
      if (isset($this->_datafields[$field]['css_class'])) {
        $attributes['class'] .= " " . $this->_datafields[$field]['css_class'];
      }
      if($type == 'text' || $type == 'password') {
        $attributes['size'] = $size;
        $attributes['maxlength'] = $maxlength;
      }
      if($type == 'textarea') {
        $attributes['cols'] = $cols;
        $attributes['rows'] = $rows;
      }
      if($type == 'checkbox' || $type == 'select') {
        if(isset($this->_datafields[$field]['options']))
          $attributes['options'] = $this->_datafields[$field]['options'];
      }
      $attributes['id'] = $field;

      if($this->_datafields[$field]['user_insert'] && $this->_datafields[$field]['user_update']) {
        return $this->_html_generator->get_form_element("sf_$field",$value,$type,$attributes);
      }
      elseif($this->_datafields[$field]['user_insert'] && !$this->exists_in_db()) {
        // it's insertable and it's a new record
        return $this->_html_generator->get_form_element("sf_$field",$value,$type,$attributes);
      }
      elseif($this->_datafields[$field]['user_insert'] && $this->exists_in_db()) {
        return $this->get_read_field($field);

      }
      elseif($this->_datafields[$field]['user_update'] && $this->exists_in_db()) {
        // it's updateable and it's not a new record
        return $this->_html_generator->get_form_element("sf_$field",$value,$type,$attributes);
      }
      elseif($field == $key_field) {
        // hide field
        return $this->_html_generator->get_form_element("sf_$field",$value,'hidden',$attributes);
      }
    }

    function get_read_field($field) {
      // See if there is an overriding function
      $function_override = "get_read_$field";
      if(method_exists($this,$function_override)) {
        return $this->$function_override();
      }
      else {
        return $this->get_auto_constructed_read_field($field);
      }
    }

    function get_auto_constructed_read_field($field) {
      $value = $this->get_field_value($field);
      $value = nl2br($this->get_htmlentities($value));
      $css_class = $this->_default_css_field_class;
      if(array_key_exists('css_class',$this->_datafields[$field])) $css_class = $this->_datafields[$field]['css_class'];
      return "<span class=\"$css_class\">$value</span>";
      return $value;
    }

    function get_htmlentities($value) {
      $find = array('{','}');
      $replace = array('&#123;','&#125;');
      $ret = red_htmlentities($value);
      $ret = str_replace($find,$replace,$ret);
      return $ret;
    }

    function get_enumerate_data_block() {
      $row = $this->get_enumerate_data_block_row();
      return $this->_html_generator->get_table_cells($row);
    }

    function get_enumerate_data_block_row() {
      $enumerate_block_row = array();
      $sorted_datafields = $this->get_sorted_datafields();
      foreach($sorted_datafields as $k => $v) {
        if($v['user_visible']) {
          $enumerate_block_row[$k]['value'] = $this->get_read_field($k);
        }
      }
      return $enumerate_block_row;
    }

      
    function get_enumerate_header_block() {
      $enumerate_block_row = array();
      $sorted_datafields = $this->get_sorted_datafields();
      foreach($sorted_datafields as $k => $v) {
        if($v['user_visible']) {
          $enumerate_block_row[$k]['value'] = $v['fname'];
          $enumerate_block_row[$k]['attributes'] = array('class'=>'red-enumerate-field-label');
        }
      }
      return $this->_html_generator->get_table_cells($enumerate_block_row, array(), 'th');
    }

    function get_edit_block() {
      $elements = $this->get_edit_block_table_elements();
      return $this->_html_generator->get_table($elements, array('class' => 'table table-bordered table-condensed'));
    }

    function get_field_description_span($field) {
      $ret = NULL;
      if (array_key_exists('description', $field)) {
        $ret = $this->_html_generator->get_tag('small', $this->_html_generator->get_tag('em', $field['description']));
        $ret = $this->_html_generator->get_tag('div', $ret);
      }
      return $ret;
    }

    function get_edit_block_table_elements() {
      $edit_block_table = array();

      // This single row will contain all the hidden fields and the 
      // submit field
      $last_row = array();
      $last_row[0]['value'] = '';
      $last_row[0]['attributes'] = array('colspan' => '2');

      $sorted_datafields = $this->get_sorted_datafields();
      foreach($sorted_datafields as $k => $v) {
        // edit_block_row will be an array of all the fields (including 
        // hidden ones). The hidden fields will be of input type hidden.
        $edit_block_row = array();
        if($v['user_update'] || $v['user_insert']) {
          $edit_block_row[0]['value'] = $v['fname'];
          $edit_block_row[0]['attributes'] = array('class' => 'red-edit-field-label');
          $edit_block_row[1]['value'] = $this->get_edit_field($k) . $this->get_field_description_span($v);

          // add it to the table, with the field name converted to be used as a css id.
          $id = 'red-' . str_replace('_', '-', $k) . '-table-row';
          $edit_block_table[$id] = $edit_block_row;
        }
        else {
          // Add invisible fields to the last row
          $last_row[0]['value'] .= $this->get_edit_field($k);
        }
      }
      // Append the submit field
      $last_row[0]['value'] .= $this->_html_generator->get_form_submit('submit','Submit', array('class' => 'form-control btn btn-md btn-success'));
      // Append it to the table
      $edit_block_table[] = $last_row;
      return $edit_block_table;
    }

    function validate() {
      reset($this->_datafields);
      foreach($this->_datafields as $k => $v)  {
        $value = $this->get_field_value($k);
        $fname = $v['fname'];
        if($value == '') {
          if($v['req']) {
            $this->set_error("The field '$fname' is required but you left it blank.",'validation');
          }
          continue;
        }
        if($v['pcre'] && !preg_match($v['pcre'],$value))  {
          if(isset($v['pcre_explanation'])) {
            $this->set_error($v['pcre_explanation'] . ' You entered: ' . $value,'validation');
          }
          else {
            $this->set_error($v['pcre'],'validation');
          }
        }
      }
      $this->additional_validation();
      if(count($this->get_errors('validation','all')) == 0) return true;
      return false;
    }

    function additional_validation() {
      // optionally override.

    }
    function get_errors($type = 'all',$severity = 'all') {
      $ret = array();
      reset($this->_errors);
      foreach($this->_errors as $v)  {
        $current_type = $v['type'];
        $current_severity = $v['severity'];

        if( ($type == 'all' || $type = $current_type) && 
          ($severity == 'all' || $severity == $current_severity) ) {
          $ret[] = $v;
        }
      }
      return $ret;
    }

    function get_errors_as_string($type = 'all',$severity = 'all') {
      $errors = $this->get_errors($type,$severity);
      $messages = array();
      foreach ($errors as $error) {
        $messages[] = $error['error'];
      }
      return implode(' ',$messages);
    }
    function addtitional_validation() {
      // should be overridden in child class
    }

    function exists_in_db() {
      if(!is_null($this->_exists_in_db)) return $this->_exists_in_db;
      $key_field = $this->get_key_field();
      $key_table = $this->get_key_table();
      $id = $this->get_field_value($key_field);

      if(empty($id)) return false;

      $this->_exists_in_db = false;
      $sql = "SELECT $key_field FROM $key_table WHERE $key_field = ".
        $this->get_field_value($key_field);
      $result = $this->_sql_query($sql);
      if($this->_sql_num_rows($result) != 0) $this->_exists_in_db = true;
      return $this->_exists_in_db;
    }

    function is_in_modifiable_state() {
      // override in child  
      true;
    }

    function is_in_deletable_state() {
      // override in child  
      true;
    }

    function set_delete_flag() {
      $this->_delete = TRUE;
    }
    function set_disable_flag() {
      $this->_disable = TRUE;
    }
    function unset_disable_flag() {
      $this->_disable = FALSE;
    }
    function commit_to_db() {
      // should be called already, call again to be safe...
      $this->validate();
      if(count($this->get_errors('validation')) == 0) {
        if(!$this->_pre_commit_to_db()) return false;
        if(!$this->_commit_to_db()) return false;
        if(!$this->_post_commit_to_db()) return false;
        return true;
      }
      return false;
    }
          
    // This function actually commits to db
    function _commit_to_db() {
      if(!$this->exists_in_db()) {
        // It's a new record 
        $this->_sql_action = 'insert';
        $sql = $this->_get_sql($this->get_key_table()); 
        if(!$this->_sql_query($sql)) return FALSE;


        // set the key field appropriately
        $key_field = $this->get_key_field();
        $this->set_field_value($key_field,$this->_sql_insert_id());
      }
      else {
        $this->_sql_action = 'update';
        $sql = $this->_get_sql($this->get_key_table()); 
        if(!$this->_sql_query($sql)) return FALSE;

      }
      return TRUE;
    }

    function _pre_commit_to_db() {
      // override in child  
      return true;
    }

    function _post_commit_to_db() {
      // override in child
      return true;  
    }

    function _get_sql($table) {
      $key_table = $this->get_key_table();
      $key_field = $this->get_key_field();
      $sql_action = $this->_sql_action;
      $set_array = array();
      reset($this->_datafields);
      foreach ($this->_datafields as $k => $v)  {
        // Make sure the table name matches and either there is
        // no limit or if limit is set to user_editable, the field
        // is user editable
        if($v['tblname'] == $table) {
          $field = $k;
          // If this is the red_item table and we're inserting 
          // then omit it from the set clause so it will get an
          // auto number
          if($table == $key_table &&
            $field == $key_field &&
            $sql_action == 'insert') continue; 

          $type = $v['type'];
          $value = $this->get_field_value($field);
          if(($type == 'date' || $type == 'int' || $type == 'datetime') && $value === '') {
            $set_array[] = "$field = NULL";
          } elseif($type != 'int') {
            $set_array[] = "$field = '".addslashes($value)."'";
          }
          else {
            $set_array[] = "$field = $value";
          }
        }
      }
      if($sql_action == 'insert') {
        if($table != $key_table) {
          // If it's not the key table, throw in the key field 
          // which all child tables must have
          $set_array[] = "$key_field = " . $this->get_field_value($key_field);
        }
        return "INSERT INTO $table SET " . implode(',',$set_array);
      }
      else {
        return "UPDATE $table SET " . implode(',',$set_array) .
          " WHERE $key_field = " . $this->get_field_value($key_field);
      }
    }

    // this function creates a related object. It should be used by objects
    // that need related items to be created when they are created
    function create_related_object($obj_name, $values, $validate_only = false, $co = null) {
      if(is_null($co)) {
        $co = $this->_construction_options;
        // if our id or recordset is set, unset it
        if(array_key_exists('id',$co)) unset($co['id']);
        if(array_key_exists('rs',$co)) unset($co['rs']);
      }
      if(!empty($values) && !array_key_exists('values', $co)) {
        $co['values'] = $values;
      }

      $obj = new $obj_name($co);
      //print_r($obj->_datafields);
      if(!$obj) {
        $this->set_error("Failed to create related $obj_name.",'system');
        return false;
      }
      
      if(!$obj->validate()) {
        $this->set_error("Failed validation when creating $obj_name " . $obj->get_errors_as_string('validate'), 'validation');
        return false;
      }

      if($validate_only) return true;

      if(!$obj->commit_to_db()) {
        $this->set_error("Failed commit when creating $obj_name " . $obj->get_errors_as_string('system'), 'system');
        return false;
      }
      return $obj;
    }

    // this function takes a sql statement that selects an object id
    // and deletes all matching objects. It should be used for objects
    // that have to delete related objects when they are being deleted.
    function delete_objects($sql,$obj_name,$co = null) {
      $result = $this->_sql_query($sql);
      if(is_null($co)) {
        $co = $this->_construction_options;
        // if our id or recordset is set, unset it
        if(array_key_exists('id',$co)) unset($co['id']);
        if(array_key_exists('rs',$co)) unset($co['rs']);
      }
      while($row = $this->_sql_fetch_assoc($result)) {
        $co['rs'] = $row;
        $obj = new $obj_name($co);
        if(!$obj) {
          $this->set_error("Failed to create $obj_name when deleting.",'system');
          return false;
        }
        $obj->set_delete_flag();
        if(method_exists($obj,'set_notify_background_process')) {
          $obj->set_notify_background_process(FALSE);
        } 
        if(!$obj->validate()) {
          $error = $obj->get_errors_as_string();
          $this->set_error("Failed to validate when deleting $obj_name: $error.",'validation');
          return false;
        }
        if(!$obj->commit_to_db()) {
          $error = $obj->get_errors_as_string();
          $this->set_error("Failed to delete $obj_name. $error.",'system');
          return false;
        }
      }
      return true;
    }

    function printme() {
      return "This object does not have a print method defined.";
    }

    // Helper function for retrieving templates for email messages
    function get_email_template($name, $lang = 'en') {
      if(!array_key_exists('src_path', $this->_construction_options)) {
        return;
      }
      $path = $this->_construction_options['src_path'] . "/../email";
      return cred_get_message_template($path, $name, $lang);
    }
  }
}

?>
